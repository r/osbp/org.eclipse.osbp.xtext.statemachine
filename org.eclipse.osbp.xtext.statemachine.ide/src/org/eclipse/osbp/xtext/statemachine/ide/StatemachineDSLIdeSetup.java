/**
 *                                                                            
 *  Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.statemachine.ide;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLRuntimeModule;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLStandaloneSetup;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
public class StatemachineDSLIdeSetup extends StatemachineDSLStandaloneSetup {

	@Override
	public Injector createInjector() {
		return Guice.createInjector(Modules2.mixin(new StatemachineDSLRuntimeModule(), new StatemachineDSLIdeModule()));
	}
	
}
