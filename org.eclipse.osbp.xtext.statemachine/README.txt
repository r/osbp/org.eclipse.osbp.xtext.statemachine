Your code for xtext.statemachine goes in here.

Add all your sources as you usually do for plugin development.

If you need more bundles to implement the feature, duplicate this folder and ad the new bundle code into the copy

Do not forget to add your new bundle to the 
 - feature pom.xml
 - feature feature.xml
 - and the aggregator pom.xml
as it is done for this bundle.

Have fun!


You can delete this file from the final project.