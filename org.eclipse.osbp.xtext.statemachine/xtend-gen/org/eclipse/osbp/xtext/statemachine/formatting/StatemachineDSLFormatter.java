/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.statemachine.formatting;

import com.google.inject.Inject;
import org.eclipse.osbp.xtext.oxtype.formatting.GenericFormatter;
import org.eclipse.osbp.xtext.oxtype.services.OXtypeGrammarAccess;
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter;
import org.eclipse.xtext.formatting.impl.FormattingConfig;

/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#formatting
 * on how and when to use it
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
@SuppressWarnings("all")
public class StatemachineDSLFormatter extends AbstractDeclarativeFormatter {
  @Inject
  private OXtypeGrammarAccess grammarAccess;
  
  @Override
  protected void configureFormatting(final FormattingConfig c) {
    final GenericFormatter genericFormatter = new GenericFormatter();
    genericFormatter.formatFirstLevelBlocks(c, this.grammar.getGrammar(), "Statemachine");
    genericFormatter.genericFormatting(c, this.grammar, this.grammarAccess);
  }
}
