/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.statemachine.jvmmodel;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import com.vaadin.data.Container;
import com.vaadin.ui.Audio;
import com.vaadin.ui.Video;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.abstractstatemachine.AbstractDataProvider;
import org.eclipse.osbp.abstractstatemachine.AbstractEventSource;
import org.eclipse.osbp.abstractstatemachine.AbstractPeripheralService;
import org.eclipse.osbp.abstractstatemachine.AbstractStateMachine;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.common.xtext.extensions.AnnotationExtension;
import org.eclipse.osbp.dsl.dto.xtext.extensions.DtoModelExtensions;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LDataType;
import org.eclipse.osbp.dsl.semantic.common.types.LDateType;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoFeature;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoReference;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableEvent;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSuggestTextFieldEvents;
import org.eclipse.osbp.ui.api.functionlibrary.IFunctionLibraryService;
import org.eclipse.osbp.ui.api.message.MessageEvent;
import org.eclipse.osbp.ui.api.statemachine.IBeeper;
import org.eclipse.osbp.ui.api.statemachine.IStateMachine;
import org.eclipse.osbp.xtext.statemachine.FSM;
import org.eclipse.osbp.xtext.statemachine.FSMAbstractFilter;
import org.eclipse.osbp.xtext.statemachine.FSMAction;
import org.eclipse.osbp.xtext.statemachine.FSMActionButtonCaption;
import org.eclipse.osbp.xtext.statemachine.FSMActionButtonImage;
import org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition;
import org.eclipse.osbp.xtext.statemachine.FSMActionDTOClear;
import org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldClear;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldConcatenation;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldFilterToggle;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldGet;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldKeystroke;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldRemove;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSet;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSource;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceBoolean;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvaluate;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvent;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceInteger;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceNumber;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceString;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceTranslate;
import org.eclipse.osbp.xtext.statemachine.FSMActionItemInvisible;
import org.eclipse.osbp.xtext.statemachine.FSMActionItemVisible;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBeeper;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBlinkRate;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralClear;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCursorType;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDestroyWindow;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDeviceBrightness;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralInterCharacterWait;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeFormat;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeRepeatWait;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeType;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeUnitWait;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralOpenDrawer;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAcknowledge;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTClose;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTResponse;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPlayer;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBitmap;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintCut;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintRegisterBitmap;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleDisplayText;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadTareWeight;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadWeight;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleTareWeight;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleWeightUnit;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleZero;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureCapture;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClear;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClose;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureIdle;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureOpen;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSound;
import org.eclipse.osbp.xtext.statemachine.FSMActionScheduler;
import org.eclipse.osbp.xtext.statemachine.FSMAndFilter;
import org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter;
import org.eclipse.osbp.xtext.statemachine.FSMCompareFilter;
import org.eclipse.osbp.xtext.statemachine.FSMControl;
import org.eclipse.osbp.xtext.statemachine.FSMControlButton;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEvent;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventEvent;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventIdentity;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard;
import org.eclipse.osbp.xtext.statemachine.FSMControlDTO;
import org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlField;
import org.eclipse.osbp.xtext.statemachine.FSMControlFieldAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlFieldLayout;
import org.eclipse.osbp.xtext.statemachine.FSMControlFilter;
import org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral;
import org.eclipse.osbp.xtext.statemachine.FSMControlScheduler;
import org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMDotExpression;
import org.eclipse.osbp.xtext.statemachine.FSMEvent;
import org.eclipse.osbp.xtext.statemachine.FSMFilter;
import org.eclipse.osbp.xtext.statemachine.FSMFilterProperty;
import org.eclipse.osbp.xtext.statemachine.FSMFunction;
import org.eclipse.osbp.xtext.statemachine.FSMGuard;
import org.eclipse.osbp.xtext.statemachine.FSMInternalType;
import org.eclipse.osbp.xtext.statemachine.FSMIsNullFilter;
import org.eclipse.osbp.xtext.statemachine.FSMKeyMapper;
import org.eclipse.osbp.xtext.statemachine.FSMLikeFilter;
import org.eclipse.osbp.xtext.statemachine.FSMModel;
import org.eclipse.osbp.xtext.statemachine.FSMNotFilter;
import org.eclipse.osbp.xtext.statemachine.FSMOperation;
import org.eclipse.osbp.xtext.statemachine.FSMOperationParameter;
import org.eclipse.osbp.xtext.statemachine.FSMOrFilter;
import org.eclipse.osbp.xtext.statemachine.FSMPackage;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevice;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceCashDrawer;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceLineDisplay;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePOSPrinter;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceScale;
import org.eclipse.osbp.xtext.statemachine.FSMRef;
import org.eclipse.osbp.xtext.statemachine.FSMState;
import org.eclipse.osbp.xtext.statemachine.FSMStorage;
import org.eclipse.osbp.xtext.statemachine.FSMStorageRetrieve;
import org.eclipse.osbp.xtext.statemachine.FSMStringFilter;
import org.eclipse.osbp.xtext.statemachine.FSMTrigger;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLFactory;
import org.eclipse.osbp.xtext.statemachine.StatemachineEnums;
import org.eclipse.osbp.xtext.statemachine.jvmmodel.StatemachineDSLModelGenerator;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.common.types.JvmAnnotationReference;
import org.eclipse.xtext.common.types.JvmConstructor;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.JvmEnumerationLiteral;
import org.eclipse.xtext.common.types.JvmEnumerationType;
import org.eclipse.xtext.common.types.JvmField;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmMember;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.JvmVisibility;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.compiler.output.ITreeAppendable;
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer;
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;

/**
 * <p>
 * FSM
 * This inferrer infers models of extension statemachine and generates code for a enum based statemachine
 * </p>
 * 
 * @author Joerg Riegel
 */
@SuppressWarnings("all")
public class StatemachineDSLJvmModelInferrer extends AbstractModelInferrer {
  @Inject
  @Extension
  private JvmTypesBuilder _jvmTypesBuilder;
  
  @Inject
  @Extension
  private StatemachineDSLModelGenerator _statemachineDSLModelGenerator;
  
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Inject
  @Extension
  private AnnotationExtension _annotationExtension;
  
  @Inject
  @Extension
  private DtoModelExtensions _dtoModelExtensions;
  
  private final String binderPostfix = "ServiceBinder";
  
  protected void _infer(final FSMPackage statemachinePkg, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    this._statemachineDSLModelGenerator.generatePckgName(statemachinePkg, acceptor);
    final JvmGenericType bindercls = this._jvmTypesBuilder.toClass(statemachinePkg, statemachinePkg.getName().toString().concat(this.binderPostfix));
    bindercls.setSimpleName(StringExtensions.toFirstUpper(bindercls.getSimpleName()));
    final Procedure1<JvmGenericType> _function = (JvmGenericType it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(SuppressWarnings.class, "serial");
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      EList<JvmAnnotationReference> _annotations_1 = it.getAnnotations();
      JvmAnnotationReference _annotationRef_1 = this._annotationTypesBuilder.annotationRef(Component.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations_1, _annotationRef_1);
      it.setPackageName(this._iQualifiedNameProvider.getFullyQualifiedName(statemachinePkg).toString());
      this._jvmTypesBuilder.setDocumentation(it, this._jvmTypesBuilder.getDocumentation(statemachinePkg));
      this.toBinderFields(it, statemachinePkg);
      this.toBinderOperations(it, statemachinePkg, bindercls.getSimpleName());
    };
    acceptor.<JvmGenericType>accept(bindercls, _function);
    EList<FSM> _statemachines = statemachinePkg.getStatemachines();
    for (final FSM statemachine : _statemachines) {
      {
        final String clsName = StringExtensions.toFirstUpper(this.className(statemachine));
        final JvmGenericType cls = this._jvmTypesBuilder.toClass(statemachinePkg, clsName);
        cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(AbstractStateMachine.class));
        cls.setSimpleName(StringExtensions.toFirstUpper(cls.getSimpleName()));
        EObject _eContainer = statemachinePkg.eContainer();
        this._jvmTypesBuilder.setFileHeader(cls, this._jvmTypesBuilder.getDocumentation(((FSMModel) _eContainer)));
        String _string = this._iQualifiedNameProvider.getFullyQualifiedName(statemachinePkg).toString();
        String _plus = (_string + ".");
        String _lowerCase = clsName.toLowerCase();
        final String pkgName = (_plus + _lowerCase);
        final Procedure1<JvmGenericType> _function_1 = (JvmGenericType it) -> {
          this.toConstructor(it, statemachine);
          this.toFields(it, statemachine);
          this.toOperations(it, statemachine);
          it.setPackageName(pkgName);
        };
        acceptor.<JvmGenericType>accept(cls, _function_1);
        final JvmGenericType eventCls = this._jvmTypesBuilder.toClass(statemachinePkg, "EventEmitter");
        eventCls.setSimpleName(StringExtensions.toFirstUpper(eventCls.getSimpleName()));
        eventCls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(AbstractEventSource.class));
        final Procedure1<JvmGenericType> _function_2 = (JvmGenericType it) -> {
          this.generateEventEmitters(it, statemachine);
          it.setPackageName(pkgName);
        };
        acceptor.<JvmGenericType>accept(eventCls, _function_2);
        final ArrayList<FSMControlDTOAttribute> associatedDtos = CollectionLiterals.<FSMControlDTOAttribute>newArrayList();
        final Predicate<FSMControl> _function_3 = (FSMControl it) -> {
          return (it instanceof FSMControlPeripheral);
        };
        final Consumer<FSMControl> _function_4 = (FSMControl p) -> {
          final Consumer<FSMPeripheralDeviceDisplay> _function_5 = (FSMPeripheralDeviceDisplay it) -> {
            associatedDtos.add(it.getOutput());
          };
          ((FSMControlPeripheral) p).getDisplays().forEach(_function_5);
        };
        statemachine.getControls().stream().filter(_function_3).forEach(_function_4);
        EList<FSMControl> _controls = statemachine.getControls();
        for (final FSMControl control : _controls) {
          {
            final String controlClsName = StringExtensions.toFirstUpper(control.getName().toString());
            final JvmGenericType controlCls = this._jvmTypesBuilder.toClass(statemachinePkg, controlClsName);
            controlCls.setSimpleName(StringExtensions.toFirstUpper(controlCls.getSimpleName()));
            boolean _matched = false;
            if (control instanceof FSMControlScheduler) {
              _matched=true;
              controlCls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(AbstractEventSource.class));
              final Procedure1<JvmGenericType> _function_5 = (JvmGenericType it) -> {
                this.generateSchedulers(it, ((FSMControlScheduler)control));
                it.setPackageName(pkgName);
              };
              acceptor.<JvmGenericType>accept(controlCls, _function_5);
            }
            if (!_matched) {
              if (control instanceof FSMControlButton) {
                _matched=true;
                controlCls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(AbstractEventSource.class));
                final Procedure1<JvmGenericType> _function_5 = (JvmGenericType it) -> {
                  this.generateButtons(it, ((FSMControlButton)control));
                  it.setPackageName(pkgName);
                };
                acceptor.<JvmGenericType>accept(controlCls, _function_5);
              }
            }
            if (!_matched) {
              if (control instanceof FSMControlField) {
                _matched=true;
                controlCls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(AbstractEventSource.class));
                final Procedure1<JvmGenericType> _function_5 = (JvmGenericType it) -> {
                  this.generateFields(it, ((FSMControlField)control));
                  it.setPackageName(pkgName);
                };
                acceptor.<JvmGenericType>accept(controlCls, _function_5);
              }
            }
            if (!_matched) {
              if (control instanceof FSMControlDTO) {
                _matched=true;
                controlCls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(AbstractDataProvider.class));
                final Procedure1<JvmGenericType> _function_5 = (JvmGenericType it) -> {
                  this.generateDTOs(it, ((FSMControlDTO)control), associatedDtos);
                  it.setPackageName(pkgName);
                };
                acceptor.<JvmGenericType>accept(controlCls, _function_5);
              }
            }
            if (!_matched) {
              if (control instanceof FSMControlPeripheral) {
                _matched=true;
                controlCls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(AbstractPeripheralService.class));
                final Procedure1<JvmGenericType> _function_5 = (JvmGenericType it) -> {
                  this.generatePeripheral(it, ((FSMControlPeripheral)control));
                  it.setPackageName(pkgName);
                };
                acceptor.<JvmGenericType>accept(controlCls, _function_5);
              }
            }
          }
        }
      }
    }
  }
  
  public void generateEventEmitters(final JvmGenericType eventCls, final FSM statemachine) {
    EList<FSMEvent> _events = statemachine.getEvents();
    for (final FSMEvent event : _events) {
      {
        JvmField field = this._jvmTypesBuilder.toField(event, event.getName(), this._typeReferenceBuilder.typeRef(YEmbeddableEvent.class));
        field.setVisibility(JvmVisibility.PRIVATE);
        EList<JvmMember> _members = eventCls.getMembers();
        this._jvmTypesBuilder.<JvmField>operator_add(_members, field);
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("get");
        String _firstUpper = StringExtensions.toFirstUpper(event.getName());
        _builder.append(_firstUpper);
        _builder.append("Event");
        final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
          final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("return ");
            String _name = event.getName();
            _builder_1.append(_name);
            _builder_1.append(";");
            it_1.append(_builder_1);
          };
          this._jvmTypesBuilder.setBody(it, _function_1);
        };
        JvmOperation operation = this._jvmTypesBuilder.toMethod(event, _builder.toString(), this._typeReferenceBuilder.typeRef(YEmbeddableEvent.class), _function);
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members_1 = eventCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, operation);
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("set");
        String _firstUpper_1 = StringExtensions.toFirstUpper(event.getName());
        _builder_1.append(_firstUpper_1);
        _builder_1.append("Event");
        final Procedure1<JvmOperation> _function_1 = (JvmOperation it) -> {
          it.setVisibility(JvmVisibility.PUBLIC);
          EList<JvmFormalParameter> _parameters = it.getParameters();
          JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(event, event.getName(), this._typeReferenceBuilder.typeRef(YEmbeddableEvent.class));
          this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
          final Procedure1<ITreeAppendable> _function_2 = (ITreeAppendable it_1) -> {
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append("statemachine.processEvent(statemachine, new MessageEvent(EventType.TRIGGER, \"");
            String _name = event.getName();
            _builder_2.append(_name);
            _builder_2.append("\"));");
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("this.");
            String _name_1 = event.getName();
            _builder_2.append(_name_1);
            _builder_2.append(" = ");
            String _name_2 = event.getName();
            _builder_2.append(_name_2);
            _builder_2.append(";");
            it_1.append(_builder_2);
          };
          this._jvmTypesBuilder.setBody(it, _function_2);
        };
        operation = this._jvmTypesBuilder.toMethod(event, _builder_1.toString(), 
          this._typeReferenceBuilder.typeRef(Void.TYPE), _function_1);
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members_2 = eventCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, operation);
      }
    }
  }
  
  public boolean generatePeripheral(final JvmGenericType controlCls, final FSMControlPeripheral peripheral) {
    boolean _xblockexpression = false;
    {
      final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        it.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(peripheral, "beeper", this._typeReferenceBuilder.typeRef(IBeeper.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(peripheral, "audio", this._typeReferenceBuilder.typeRef(Audio.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        EList<JvmFormalParameter> _parameters_2 = it.getParameters();
        JvmFormalParameter _parameter_2 = this._jvmTypesBuilder.toParameter(peripheral, "video", this._typeReferenceBuilder.typeRef(Video.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_2, _parameter_2);
        final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
          it_1.append(this.initDevices(peripheral));
        };
        this._jvmTypesBuilder.setBody(it, _function_1);
      };
      JvmOperation init = this._jvmTypesBuilder.toMethod(peripheral, "initDevices", this._typeReferenceBuilder.typeRef(boolean.class), _function);
      EList<JvmMember> _members = controlCls.getMembers();
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members, init);
      final Procedure1<JvmOperation> _function_1 = (JvmOperation it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        it.setVisibility(JvmVisibility.PUBLIC);
        final Procedure1<ITreeAppendable> _function_2 = (ITreeAppendable it_1) -> {
          it_1.append(this.releaseDevices(peripheral));
        };
        this._jvmTypesBuilder.setBody(it, _function_2);
      };
      JvmOperation release = this._jvmTypesBuilder.toMethod(peripheral, "releaseDevices", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_1);
      EList<JvmMember> _members_1 = controlCls.getMembers();
      _xblockexpression = this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, release);
    }
    return _xblockexpression;
  }
  
  public String initDevices(final FSMControlPeripheral peripheral) {
    final StringBuilder b = new StringBuilder();
    b.append("this.beeper = beeper;\nthis.audio=audio;\nthis.video=video;\n");
    final Consumer<FSMPeripheralDevicePOSPrinter> _function = (FSMPeripheralDevicePOSPrinter it) -> {
      b.append(this.initDevice(it));
    };
    peripheral.getPosPrinters().forEach(_function);
    final Consumer<FSMPeripheralDeviceCashDrawer> _function_1 = (FSMPeripheralDeviceCashDrawer it) -> {
      b.append(this.initDevice(it));
    };
    peripheral.getCashDrawers().forEach(_function_1);
    final Consumer<FSMPeripheralDeviceLineDisplay> _function_2 = (FSMPeripheralDeviceLineDisplay it) -> {
      b.append(this.initDevice(it));
    };
    peripheral.getLineDisplays().forEach(_function_2);
    final Consumer<FSMPeripheralDeviceScale> _function_3 = (FSMPeripheralDeviceScale it) -> {
      b.append(this.initDevice(it));
    };
    peripheral.getScales().forEach(_function_3);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("if(getPTIP() != null && getPTIP().length() > 0 && POSServiceBinder.getPosService() != null) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if(!POSServiceBinder.getPosService().openZVTChannel(getPTIP(), getPTPort())) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("LOGGER.error(\"could not open ZVT socket\");");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("initDone = true;");
    _builder.newLine();
    _builder.append("return true;");
    b.append(_builder);
    return b.toString();
  }
  
  public String releaseDevices(final FSMControlPeripheral peripheral) {
    final StringBuilder b = new StringBuilder();
    StringConcatenation _builder = new StringConcatenation();
    b.append(_builder);
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("super.releaseDevices();");
    _builder_1.newLine();
    _builder_1.append("if(getPTIP() != null && getPTIP().length() > 0 && POSServiceBinder.getPosService() != null) {");
    _builder_1.newLine();
    _builder_1.append("\t");
    _builder_1.append("POSServiceBinder.getPosService().closeZVTChannel();");
    _builder_1.newLine();
    _builder_1.append("}");
    _builder_1.newLine();
    _builder_1.append("windowsCreated = 0;");
    _builder_1.newLine();
    b.append(_builder_1);
    return b.toString();
  }
  
  public CharSequence initDevice(final FSMPeripheralDevice device) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("initDevice(\"");
    String _name = device.getName();
    _builder.append(_name);
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  public boolean generateSchedulers(final JvmGenericType controlCls, final FSMControlScheduler control) {
    boolean _xblockexpression = false;
    {
      final Consumer<FSMControlSchedulerAttribute> _function = (FSMControlSchedulerAttribute it) -> {
        String _name = it.getName();
        String _plus = (_name + "Scheduler");
        JvmField field = this._jvmTypesBuilder.toField(control, _plus, this._typeReferenceBuilder.typeRef(int.class));
        field.setVisibility(JvmVisibility.PRIVATE);
        EList<JvmMember> _members = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmField>operator_add(_members, field);
      };
      control.getSchedulers().forEach(_function);
      final Consumer<FSMControlSchedulerAttribute> _function_1 = (FSMControlSchedulerAttribute it) -> {
        String _name = it.getName();
        String _plus = (_name + "Scheduler");
        JvmOperation operation = this._jvmTypesBuilder.toGetter(control, _plus, this._typeReferenceBuilder.typeRef(int.class));
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members, operation);
        final FSMControlSchedulerAttribute scheduler = it;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("set");
        String _firstUpper = StringExtensions.toFirstUpper(it.getName());
        _builder.append(_firstUpper);
        _builder.append("Scheduler");
        final Procedure1<JvmOperation> _function_2 = (JvmOperation it_1) -> {
          it_1.setVisibility(JvmVisibility.PUBLIC);
          EList<JvmFormalParameter> _parameters = it_1.getParameters();
          JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, "delay", this._typeReferenceBuilder.typeRef(int.class));
          this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
          final Procedure1<ITreeAppendable> _function_3 = (ITreeAppendable it_2) -> {
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("Timer timer = new Timer(delay, action -> {");
            _builder_1.newLine();
            _builder_1.append("\t");
            _builder_1.append("statemachine.processEvent(statemachine, new MessageEvent(EventType.TRIGGER, \"");
            String _name_1 = scheduler.getEvent().getName();
            _builder_1.append(_name_1, "\t");
            _builder_1.append("\"));");
            _builder_1.newLineIfNotEmpty();
            _builder_1.append("});");
            _builder_1.newLine();
            _builder_1.append("timer.setRepeats(false);");
            _builder_1.newLine();
            _builder_1.append("timer.start();");
            it_2.append(_builder_1);
          };
          this._jvmTypesBuilder.setBody(it_1, _function_3);
        };
        operation = this._jvmTypesBuilder.toMethod(control, _builder.toString(), 
          this._typeReferenceBuilder.typeRef(Void.TYPE), _function_2);
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members_1 = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, operation);
      };
      control.getSchedulers().forEach(_function_1);
      final Procedure1<JvmOperation> _function_2 = (JvmOperation it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, "host", this._typeReferenceBuilder.typeRef(String.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(control, "port", this._typeReferenceBuilder.typeRef(int.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        it.setVisibility(JvmVisibility.PUBLIC);
        final Procedure1<ITreeAppendable> _function_3 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("// NOSONAR");
          _builder.newLine();
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_3);
      };
      JvmOperation init = this._jvmTypesBuilder.toMethod(control, "init", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_2);
      EList<JvmMember> _members = controlCls.getMembers();
      _xblockexpression = this._jvmTypesBuilder.<JvmOperation>operator_add(_members, init);
    }
    return _xblockexpression;
  }
  
  public boolean generateButtons(final JvmGenericType controlCls, final FSMControlButton control) {
    boolean _xblockexpression = false;
    {
      String _firstLower = StringExtensions.toFirstLower(control.getName());
      String _plus = (_firstLower + "Enabled");
      JvmField pad = this._jvmTypesBuilder.toField(control, _plus, 
        this._typeReferenceBuilder.typeRef(Boolean.class));
      pad.setVisibility(JvmVisibility.PRIVATE);
      EList<JvmMember> _members = controlCls.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members, pad);
      boolean _isHasRange = control.isHasRange();
      if (_isHasRange) {
        for (int i = control.getStart(); (i <= control.getEnd()); i++) {
          {
            String _rangedName = control.getRangedName();
            String _plus_1 = (_rangedName + Integer.valueOf(i));
            this.buildButtonField(control, controlCls, _plus_1, "", this._typeReferenceBuilder.typeRef(Number.class));
            String _rangedName_1 = control.getRangedName();
            String _plus_2 = (_rangedName_1 + Integer.valueOf(i));
            this.buildButtonField(control, controlCls, _plus_2, "Caption", this._typeReferenceBuilder.typeRef(String.class));
            String _rangedName_2 = control.getRangedName();
            String _plus_3 = (_rangedName_2 + Integer.valueOf(i));
            this.buildButtonField(control, controlCls, _plus_3, "Enabled", this._typeReferenceBuilder.typeRef(Boolean.class));
            String _rangedName_3 = control.getRangedName();
            String _plus_4 = (_rangedName_3 + Integer.valueOf(i));
            this.buildButtonField(control, controlCls, _plus_4, "Image", this._typeReferenceBuilder.typeRef(Object.class));
            String _rangedName_4 = control.getRangedName();
            String _plus_5 = (_rangedName_4 + Integer.valueOf(i));
            this.buildButtonField(control, controlCls, _plus_5, "Styles", this._typeReferenceBuilder.typeRef(String.class));
          }
        }
        for (int i = control.getStart(); (i <= control.getEnd()); i++) {
          {
            FSMControlButtonAttributeEventIdentity eventId = StatemachineDSLFactory.eINSTANCE.createFSMControlButtonAttributeEventIdentity();
            eventId.setIdentity(i);
            String _rangedName = control.getRangedName();
            String _plus_1 = (_rangedName + Integer.valueOf(i));
            this.buildButtonMethods(control, controlCls, _plus_1, "", this._typeReferenceBuilder.typeRef(Number.class), eventId);
            String _rangedName_1 = control.getRangedName();
            String _plus_2 = (_rangedName_1 + Integer.valueOf(i));
            this.buildButtonMethods(control, controlCls, _plus_2, "Caption", this._typeReferenceBuilder.typeRef(String.class), eventId);
            String _rangedName_2 = control.getRangedName();
            String _plus_3 = (_rangedName_2 + Integer.valueOf(i));
            this.buildButtonMethods(control, controlCls, _plus_3, "Enabled", this._typeReferenceBuilder.typeRef(Boolean.class), eventId);
            String _rangedName_3 = control.getRangedName();
            String _plus_4 = (_rangedName_3 + Integer.valueOf(i));
            this.buildButtonMethods(control, controlCls, _plus_4, "Image", this._typeReferenceBuilder.typeRef(Object.class), eventId);
            String _rangedName_4 = control.getRangedName();
            String _plus_5 = (_rangedName_4 + Integer.valueOf(i));
            this.buildButtonMethods(control, controlCls, _plus_5, "Styles", this._typeReferenceBuilder.typeRef(String.class), eventId);
          }
        }
      } else {
        final Consumer<FSMControlButtonAttribute> _function = (FSMControlButtonAttribute it) -> {
          this.buildButtonField(control, controlCls, it.getName(), "", this._typeReferenceBuilder.typeRef(Number.class));
          this.buildButtonField(control, controlCls, it.getName(), "Caption", this._typeReferenceBuilder.typeRef(String.class));
          this.buildButtonField(control, controlCls, it.getName(), "Enabled", this._typeReferenceBuilder.typeRef(Boolean.class));
          this.buildButtonField(control, controlCls, it.getName(), "Image", this._typeReferenceBuilder.typeRef(Object.class));
          this.buildButtonField(control, controlCls, it.getName(), "Styles", this._typeReferenceBuilder.typeRef(String.class));
        };
        control.getButtons().forEach(_function);
        final Consumer<FSMControlButtonAttribute> _function_1 = (FSMControlButtonAttribute it) -> {
          this.buildButtonMethods(control, controlCls, it.getName(), "", this._typeReferenceBuilder.typeRef(Number.class), it.getEvent());
          this.buildButtonMethods(control, controlCls, it.getName(), "Caption", this._typeReferenceBuilder.typeRef(String.class), it.getEvent());
          this.buildButtonMethods(control, controlCls, it.getName(), "Enabled", this._typeReferenceBuilder.typeRef(Boolean.class), it.getEvent());
          this.buildButtonMethods(control, controlCls, it.getName(), "Image", this._typeReferenceBuilder.typeRef(Object.class), it.getEvent());
          this.buildButtonMethods(control, controlCls, it.getName(), "Styles", this._typeReferenceBuilder.typeRef(String.class), it.getEvent());
        };
        control.getButtons().forEach(_function_1);
      }
      String _firstLower_1 = StringExtensions.toFirstLower(control.getName());
      String _plus_1 = (_firstLower_1 + "Enabled");
      JvmOperation getEnabled = this._jvmTypesBuilder.toGetter(control, _plus_1, 
        this._typeReferenceBuilder.typeRef(Boolean.class));
      getEnabled.setVisibility(JvmVisibility.PUBLIC);
      EList<JvmMember> _members_1 = controlCls.getMembers();
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, getEnabled);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("set");
      String _firstUpper = StringExtensions.toFirstUpper(control.getName());
      _builder.append(_firstUpper);
      _builder.append("Enabled");
      final Procedure1<JvmOperation> _function_2 = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        String _firstLower_2 = StringExtensions.toFirstLower(control.getName());
        String _plus_2 = (_firstLower_2 + "Enabled");
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, _plus_2, 
          this._typeReferenceBuilder.typeRef(Boolean.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        it.setVisibility(JvmVisibility.PUBLIC);
        final Procedure1<ITreeAppendable> _function_3 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder_1 = new StringConcatenation();
          {
            EList<FSMControlButtonAttribute> _buttons = control.getButtons();
            for(final FSMControlButtonAttribute button : _buttons) {
              _builder_1.append("set");
              String _firstUpper_1 = StringExtensions.toFirstUpper(button.getName());
              _builder_1.append(_firstUpper_1);
              _builder_1.append("Enabled(");
              String _firstLower_3 = StringExtensions.toFirstLower(control.getName());
              _builder_1.append(_firstLower_3);
              _builder_1.append("Enabled);");
              _builder_1.newLineIfNotEmpty();
            }
          }
          _builder_1.append("pcs.firePropertyChange(\"");
          String _firstLower_4 = StringExtensions.toFirstLower(control.getName());
          _builder_1.append(_firstLower_4);
          _builder_1.append("Enabled\", this.");
          String _firstLower_5 = StringExtensions.toFirstLower(control.getName());
          _builder_1.append(_firstLower_5);
          _builder_1.append("Enabled, this.");
          String _firstLower_6 = StringExtensions.toFirstLower(control.getName());
          _builder_1.append(_firstLower_6);
          _builder_1.append("Enabled = ");
          String _firstLower_7 = StringExtensions.toFirstLower(control.getName());
          _builder_1.append(_firstLower_7);
          _builder_1.append("Enabled);");
          _builder_1.newLineIfNotEmpty();
          it_1.append(_builder_1);
        };
        this._jvmTypesBuilder.setBody(it, _function_3);
      };
      JvmOperation setEnabled = this._jvmTypesBuilder.toMethod(control, _builder.toString(), this._typeReferenceBuilder.typeRef(Void.TYPE), _function_2);
      setEnabled.setVisibility(JvmVisibility.PUBLIC);
      EList<JvmMember> _members_2 = controlCls.getMembers();
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, setEnabled);
      boolean _hasImages = false;
      EList<FSMControlButtonAttribute> _buttons = control.getButtons();
      for (final FSMControlButtonAttribute button : _buttons) {
        boolean _isHasImage = button.isHasImage();
        if (_isHasImage) {
          _hasImages = true;
        }
      }
      final boolean hasImages = _hasImages;
      final Procedure1<JvmOperation> _function_3 = (JvmOperation it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, "host", this._typeReferenceBuilder.typeRef(String.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(control, "port", this._typeReferenceBuilder.typeRef(int.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        it.setVisibility(JvmVisibility.PUBLIC);
        final Procedure1<ITreeAppendable> _function_4 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("super.init(host, port);");
          _builder_1.newLine();
          _builder_1.append("set");
          String _firstUpper_1 = StringExtensions.toFirstUpper(control.getName());
          _builder_1.append(_firstUpper_1);
          _builder_1.append("Enabled(false);");
          _builder_1.newLineIfNotEmpty();
          {
            if (hasImages) {
              {
                EList<FSMControlButtonAttribute> _buttons_1 = control.getButtons();
                for(final FSMControlButtonAttribute button_1 : _buttons_1) {
                  {
                    boolean _isHasImage_1 = button_1.isHasImage();
                    if (_isHasImage_1) {
                      _builder_1.append("set");
                      String _firstUpper_2 = StringExtensions.toFirstUpper(button_1.getName());
                      _builder_1.append(_firstUpper_2);
                      _builder_1.append("Caption(null);");
                      _builder_1.newLineIfNotEmpty();
                    }
                  }
                }
              }
              _builder_1.append("if(statemachine != null) {");
              _builder_1.newLine();
              {
                EList<FSMControlButtonAttribute> _buttons_2 = control.getButtons();
                for(final FSMControlButtonAttribute button_2 : _buttons_2) {
                  {
                    if ((button_2.isHasImage() && (button_2.getImage().length() > 0))) {
                      _builder_1.append("\tset");
                      String _firstUpper_3 = StringExtensions.toFirstUpper(button_2.getName());
                      _builder_1.append(_firstUpper_3);
                      _builder_1.append("Image(statemachine.getThemeResourceService().getThemeResource(\"");
                      String _image = button_2.getImage();
                      _builder_1.append(_image);
                      _builder_1.append("\", ThemeResourceType.ICON));");
                      _builder_1.newLineIfNotEmpty();
                    }
                  }
                }
              }
              _builder_1.append("}");
              _builder_1.newLine();
            }
          }
          it_1.append(_builder_1);
        };
        this._jvmTypesBuilder.setBody(it, _function_4);
      };
      JvmOperation init = this._jvmTypesBuilder.toMethod(control, "init", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_3);
      EList<JvmMember> _members_3 = controlCls.getMembers();
      _xblockexpression = this._jvmTypesBuilder.<JvmOperation>operator_add(_members_3, init);
    }
    return _xblockexpression;
  }
  
  public boolean generateFields(final JvmGenericType controlCls, final FSMControlField control) {
    boolean _xblockexpression = false;
    {
      final Consumer<FSMControlFieldAttribute> _function = (FSMControlFieldAttribute it) -> {
        JvmField field = this._jvmTypesBuilder.toField(control, it.getName(), this._typeReferenceBuilder.typeRef(this.decodeType(it)));
        field.setVisibility(JvmVisibility.PRIVATE);
        EList<JvmMember> _members = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmField>operator_add(_members, field);
        FSMInternalType _attributeType = it.getAttributeType().getAttributeType();
        boolean _equals = Objects.equal(_attributeType, FSMInternalType.SUGGESTTEXT);
        if (_equals) {
          String _name = it.getName();
          String _plus = (_name + "Event");
          field = this._jvmTypesBuilder.toField(control, _plus, this._typeReferenceBuilder.typeRef(YSuggestTextFieldEvents.class));
          field.setVisibility(JvmVisibility.PRIVATE);
          EList<JvmMember> _members_1 = controlCls.getMembers();
          this._jvmTypesBuilder.<JvmField>operator_add(_members_1, field);
        }
        String _name_1 = it.getName();
        String _plus_1 = (_name_1 + "Enabled");
        field = this._jvmTypesBuilder.toField(control, _plus_1, this._typeReferenceBuilder.typeRef(Boolean.class));
        field.setVisibility(JvmVisibility.PRIVATE);
        EList<JvmMember> _members_2 = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmField>operator_add(_members_2, field);
      };
      control.getFields().forEach(_function);
      final Consumer<FSMControlFieldLayout> _function_1 = (FSMControlFieldLayout it) -> {
        String _name = it.getName();
        String _plus = (_name + "Enabled");
        JvmField layout = this._jvmTypesBuilder.toField(control, _plus, this._typeReferenceBuilder.typeRef(Boolean.class));
        layout.setVisibility(JvmVisibility.PRIVATE);
        EList<JvmMember> _members = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmField>operator_add(_members, layout);
      };
      control.getLayouts().forEach(_function_1);
      final Consumer<FSMControlFieldAttribute> _function_2 = (FSMControlFieldAttribute it) -> {
        JvmOperation operation = this._jvmTypesBuilder.toGetter(control, it.getName(), this._typeReferenceBuilder.typeRef(this.decodeType(it)));
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members, operation);
        final FSMControlFieldAttribute field = it;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("set");
        String _firstUpper = StringExtensions.toFirstUpper(it.getName());
        _builder.append(_firstUpper);
        final Procedure1<JvmOperation> _function_3 = (JvmOperation it_1) -> {
          EList<JvmFormalParameter> _parameters = it_1.getParameters();
          JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, field.getName(), this._typeReferenceBuilder.typeRef(this.decodeType(field)));
          this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
          it_1.setVisibility(JvmVisibility.PUBLIC);
          final Procedure1<ITreeAppendable> _function_4 = (ITreeAppendable it_2) -> {
            StringConcatenation _builder_1 = new StringConcatenation();
            String _decodeSetter = this.decodeSetter(field);
            _builder_1.append(_decodeSetter);
            it_2.append(_builder_1);
          };
          this._jvmTypesBuilder.setBody(it_1, _function_4);
        };
        operation = this._jvmTypesBuilder.toMethod(control, _builder.toString(), this._typeReferenceBuilder.typeRef(Void.TYPE), _function_3);
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members_1 = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, operation);
        String _name = it.getName();
        String _plus = (_name + "Enabled");
        operation = this._jvmTypesBuilder.toGetter(control, _plus, this._typeReferenceBuilder.typeRef(Boolean.class));
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members_2 = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, operation);
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("set");
        String _firstUpper_1 = StringExtensions.toFirstUpper(it.getName());
        _builder_1.append(_firstUpper_1);
        _builder_1.append("Enabled");
        final Procedure1<JvmOperation> _function_4 = (JvmOperation it_1) -> {
          EList<JvmFormalParameter> _parameters = it_1.getParameters();
          String _name_1 = field.getName();
          String _plus_1 = (_name_1 + "Enabled");
          JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, _plus_1, this._typeReferenceBuilder.typeRef(Boolean.class));
          this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
          it_1.setVisibility(JvmVisibility.PUBLIC);
          final Procedure1<ITreeAppendable> _function_5 = (ITreeAppendable it_2) -> {
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append("pcs.firePropertyChange(\"");
            String _name_2 = field.getName();
            _builder_2.append(_name_2);
            _builder_2.append("Enabled\", this.");
            String _name_3 = field.getName();
            _builder_2.append(_name_3);
            _builder_2.append("Enabled, this.");
            String _name_4 = field.getName();
            _builder_2.append(_name_4);
            _builder_2.append("Enabled = ");
            String _name_5 = field.getName();
            _builder_2.append(_name_5);
            _builder_2.append("Enabled);");
            it_2.append(_builder_2);
          };
          this._jvmTypesBuilder.setBody(it_1, _function_5);
        };
        operation = this._jvmTypesBuilder.toMethod(control, _builder_1.toString(), 
          this._typeReferenceBuilder.typeRef(Void.TYPE), _function_4);
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members_3 = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_3, operation);
        FSMInternalType _attributeType = it.getAttributeType().getAttributeType();
        boolean _equals = Objects.equal(_attributeType, FSMInternalType.SUGGESTTEXT);
        if (_equals) {
          String _name_1 = it.getName();
          String _plus_1 = (_name_1 + "Event");
          operation = this._jvmTypesBuilder.toGetter(control, _plus_1, this._typeReferenceBuilder.typeRef(YSuggestTextFieldEvents.class));
          operation.setVisibility(JvmVisibility.PUBLIC);
          EList<JvmMember> _members_4 = controlCls.getMembers();
          this._jvmTypesBuilder.<JvmOperation>operator_add(_members_4, operation);
          final FSMControlFieldAttribute eventfield = it;
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("set");
          String _firstUpper_2 = StringExtensions.toFirstUpper(it.getName());
          _builder_2.append(_firstUpper_2);
          _builder_2.append("Event");
          final Procedure1<JvmOperation> _function_5 = (JvmOperation it_1) -> {
            EList<JvmFormalParameter> _parameters = it_1.getParameters();
            String _name_2 = eventfield.getName();
            String _plus_2 = (_name_2 + "Event");
            JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, _plus_2, 
              this._typeReferenceBuilder.typeRef(YSuggestTextFieldEvents.class));
            this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
            it_1.setVisibility(JvmVisibility.PUBLIC);
            final Procedure1<ITreeAppendable> _function_6 = (ITreeAppendable it_2) -> {
              StringConcatenation _builder_3 = new StringConcatenation();
              _builder_3.append("pcs.firePropertyChange(\"");
              String _name_3 = field.getName();
              _builder_3.append(_name_3);
              _builder_3.append("Event\", this.");
              String _name_4 = field.getName();
              _builder_3.append(_name_4);
              _builder_3.append("Event, this.");
              String _name_5 = field.getName();
              _builder_3.append(_name_5);
              _builder_3.append("Event = ");
              String _name_6 = field.getName();
              _builder_3.append(_name_6);
              _builder_3.append("Event);");
              _builder_3.newLineIfNotEmpty();
              _builder_3.append("if(");
              String _name_7 = eventfield.getName();
              _builder_3.append(_name_7);
              _builder_3.append("Event == YSuggestTextFieldEvents.SELECTED && statemachine != null) {");
              _builder_3.newLineIfNotEmpty();
              _builder_3.append("\t");
              _builder_3.append("statemachine.processEvent(statemachine, new MessageEvent(MessageEvent.EventType.TRIGGER, \"on");
              String _firstUpper_3 = StringExtensions.toFirstUpper(field.getName());
              _builder_3.append(_firstUpper_3, "\t");
              _builder_3.append("Selection\"));");
              _builder_3.newLineIfNotEmpty();
              _builder_3.append("}");
              it_2.append(_builder_3);
            };
            this._jvmTypesBuilder.setBody(it_1, _function_6);
          };
          operation = this._jvmTypesBuilder.toMethod(control, _builder_2.toString(), 
            this._typeReferenceBuilder.typeRef(Void.TYPE), _function_5);
          operation.setVisibility(JvmVisibility.PUBLIC);
          EList<JvmMember> _members_5 = controlCls.getMembers();
          this._jvmTypesBuilder.<JvmOperation>operator_add(_members_5, operation);
        }
      };
      control.getFields().forEach(_function_2);
      final Consumer<FSMControlFieldLayout> _function_3 = (FSMControlFieldLayout it) -> {
        String _name = it.getName();
        String _plus = (_name + "Enabled");
        JvmOperation operation = this._jvmTypesBuilder.toGetter(control, _plus, this._typeReferenceBuilder.typeRef(Boolean.class));
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members, operation);
        final FSMControlFieldLayout layout = it;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("set");
        String _firstUpper = StringExtensions.toFirstUpper(it.getName());
        _builder.append(_firstUpper);
        _builder.append("Enabled");
        final Procedure1<JvmOperation> _function_4 = (JvmOperation it_1) -> {
          EList<JvmFormalParameter> _parameters = it_1.getParameters();
          String _name_1 = layout.getName();
          String _plus_1 = (_name_1 + "Enabled");
          JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, _plus_1, this._typeReferenceBuilder.typeRef(Boolean.class));
          this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
          it_1.setVisibility(JvmVisibility.PUBLIC);
          final Procedure1<ITreeAppendable> _function_5 = (ITreeAppendable it_2) -> {
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("pcs.firePropertyChange(\"");
            String _name_2 = layout.getName();
            _builder_1.append(_name_2);
            _builder_1.append("Enabled\", this.");
            String _name_3 = layout.getName();
            _builder_1.append(_name_3);
            _builder_1.append("Enabled, this.");
            String _name_4 = layout.getName();
            _builder_1.append(_name_4);
            _builder_1.append("Enabled = ");
            String _name_5 = layout.getName();
            _builder_1.append(_name_5);
            _builder_1.append("Enabled);");
            it_2.append(_builder_1);
          };
          this._jvmTypesBuilder.setBody(it_1, _function_5);
        };
        operation = this._jvmTypesBuilder.toMethod(control, _builder.toString(), 
          this._typeReferenceBuilder.typeRef(Void.TYPE), _function_4);
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members_1 = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, operation);
      };
      control.getLayouts().forEach(_function_3);
      final Procedure1<JvmOperation> _function_4 = (JvmOperation it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, "host", this._typeReferenceBuilder.typeRef(String.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(control, "port", this._typeReferenceBuilder.typeRef(int.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        it.setVisibility(JvmVisibility.PUBLIC);
        final Procedure1<ITreeAppendable> _function_5 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("super.init(host,port);");
          _builder.newLine();
          {
            EList<FSMControlFieldAttribute> _fields = control.getFields();
            for(final FSMControlFieldAttribute field : _fields) {
              _builder.append("pcs.firePropertyChange(\"");
              String _name = field.getName();
              _builder.append(_name);
              _builder.append("Enabled\", this.");
              String _name_1 = field.getName();
              _builder.append(_name_1);
              _builder.append("Enabled, this.");
              String _name_2 = field.getName();
              _builder.append(_name_2);
              _builder.append("Enabled = false);");
              _builder.newLineIfNotEmpty();
            }
          }
          {
            EList<FSMControlFieldLayout> _layouts = control.getLayouts();
            for(final FSMControlFieldLayout layout : _layouts) {
              _builder.append("pcs.firePropertyChange(\"");
              String _name_3 = layout.getName();
              _builder.append(_name_3);
              _builder.append("Enabled\", this.");
              String _name_4 = layout.getName();
              _builder.append(_name_4);
              _builder.append("Enabled, this.");
              String _name_5 = layout.getName();
              _builder.append(_name_5);
              _builder.append("Enabled = false);");
              _builder.newLineIfNotEmpty();
            }
          }
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_5);
      };
      JvmOperation init = this._jvmTypesBuilder.toMethod(control, "init", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_4);
      EList<JvmMember> _members = controlCls.getMembers();
      _xblockexpression = this._jvmTypesBuilder.<JvmOperation>operator_add(_members, init);
    }
    return _xblockexpression;
  }
  
  public boolean generateDTOs(final JvmGenericType controlCls, final FSMControlDTO control, final ArrayList<FSMControlDTOAttribute> associatedDtos) {
    boolean _xblockexpression = false;
    {
      final Consumer<FSMControlDTOAttribute> _function = (FSMControlDTOAttribute it) -> {
        this.buildDTOField(control, controlCls, it.getName(), this._typeReferenceBuilder.typeRef(this._iQualifiedNameProvider.getFullyQualifiedName(it.getAttributeType().getAttributeType()).toString()));
      };
      control.getDtos().forEach(_function);
      final Consumer<FSMControlFilter> _function_1 = (FSMControlFilter it) -> {
        final String name = StringExtensions.toFirstLower(it.getName());
        final String filterExpression = this.buildFilter(it.getFilter().getSource());
        JvmField field = this._jvmTypesBuilder.toField(control, it.getName(), 
          this._typeReferenceBuilder.typeRef(Container.Filter.class));
        field.setVisibility(JvmVisibility.PRIVATE);
        EList<JvmMember> _members = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmField>operator_add(_members, field);
        String _name = it.getName();
        String _plus = (_name + "Enabled");
        field = this._jvmTypesBuilder.toField(control, _plus, this._typeReferenceBuilder.typeRef(Boolean.class));
        field.setVisibility(JvmVisibility.PRIVATE);
        EList<JvmMember> _members_1 = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmField>operator_add(_members_1, field);
        EList<JvmMember> _members_2 = controlCls.getMembers();
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("get");
        String _firstUpper = StringExtensions.toFirstUpper(it.getName());
        _builder.append(_firstUpper);
        final Procedure1<JvmOperation> _function_2 = (JvmOperation it_1) -> {
          it_1.setVisibility(JvmVisibility.PUBLIC);
          final Procedure1<ITreeAppendable> _function_3 = (ITreeAppendable it_2) -> {
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("return this.");
            _builder_1.append(name);
            _builder_1.append(";");
            it_2.append(_builder_1);
          };
          this._jvmTypesBuilder.setBody(it_1, _function_3);
        };
        JvmOperation _method = this._jvmTypesBuilder.toMethod(control, _builder.toString(), this._typeReferenceBuilder.typeRef(Container.Filter.class), _function_2);
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, _method);
        EList<JvmMember> _members_3 = controlCls.getMembers();
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("set");
        String _firstUpper_1 = StringExtensions.toFirstUpper(it.getName());
        _builder_1.append(_firstUpper_1);
        final Procedure1<JvmOperation> _function_3 = (JvmOperation it_1) -> {
          it_1.setVisibility(JvmVisibility.PUBLIC);
          EList<JvmFormalParameter> _parameters = it_1.getParameters();
          JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, "create", this._typeReferenceBuilder.typeRef(Boolean.class));
          this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
          it_1.setVisibility(JvmVisibility.PUBLIC);
          final Procedure1<ITreeAppendable> _function_4 = (ITreeAppendable it_2) -> {
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append("if(create) {");
            _builder_2.newLine();
            _builder_2.append("\t");
            _builder_2.append("pcs.firePropertyChange(\"");
            _builder_2.append(name, "\t");
            _builder_2.append("\", this.");
            _builder_2.append(name, "\t");
            _builder_2.append(", this.");
            _builder_2.append(name, "\t");
            _builder_2.append(" = ");
            _builder_2.append(filterExpression, "\t");
            _builder_2.append(");");
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("} else {");
            _builder_2.newLine();
            _builder_2.append("\t");
            _builder_2.append("pcs.firePropertyChange(\"");
            _builder_2.append(name, "\t");
            _builder_2.append("\", this.");
            _builder_2.append(name, "\t");
            _builder_2.append(", this.");
            _builder_2.append(name, "\t");
            _builder_2.append(" = null);");
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("}");
            it_2.append(_builder_2);
          };
          this._jvmTypesBuilder.setBody(it_1, _function_4);
        };
        JvmOperation _method_1 = this._jvmTypesBuilder.toMethod(control, _builder_1.toString(), this._typeReferenceBuilder.typeRef(Void.TYPE), _function_3);
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_3, _method_1);
        String _name_1 = it.getName();
        String _plus_1 = (_name_1 + "Enabled");
        JvmOperation operation = this._jvmTypesBuilder.toGetter(control, _plus_1, this._typeReferenceBuilder.typeRef(Boolean.class));
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members_4 = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_4, operation);
        final FSMControlFilter filter = it;
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("set");
        String _firstUpper_2 = StringExtensions.toFirstUpper(it.getName());
        _builder_2.append(_firstUpper_2);
        _builder_2.append("Enabled");
        final Procedure1<JvmOperation> _function_4 = (JvmOperation it_1) -> {
          EList<JvmFormalParameter> _parameters = it_1.getParameters();
          String _name_2 = filter.getName();
          String _plus_2 = (_name_2 + "Enabled");
          JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, _plus_2, this._typeReferenceBuilder.typeRef(Boolean.class));
          this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
          it_1.setVisibility(JvmVisibility.PUBLIC);
          final Procedure1<ITreeAppendable> _function_5 = (ITreeAppendable it_2) -> {
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append("pcs.firePropertyChange(\"");
            String _name_3 = filter.getName();
            _builder_3.append(_name_3);
            _builder_3.append("Enabled\", this.");
            String _name_4 = filter.getName();
            _builder_3.append(_name_4);
            _builder_3.append("Enabled, this.");
            String _name_5 = filter.getName();
            _builder_3.append(_name_5);
            _builder_3.append("Enabled = ");
            String _name_6 = filter.getName();
            _builder_3.append(_name_6);
            _builder_3.append("Enabled);");
            _builder_3.newLineIfNotEmpty();
            _builder_3.append("set");
            String _firstUpper_3 = StringExtensions.toFirstUpper(filter.getName());
            _builder_3.append(_firstUpper_3);
            _builder_3.append("(");
            String _name_7 = filter.getName();
            _builder_3.append(_name_7);
            _builder_3.append("Enabled);");
            it_2.append(_builder_3);
          };
          this._jvmTypesBuilder.setBody(it_1, _function_5);
        };
        operation = this._jvmTypesBuilder.toMethod(control, _builder_2.toString(), 
          this._typeReferenceBuilder.typeRef(Void.TYPE), _function_4);
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members_5 = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_5, operation);
      };
      control.getFilters().forEach(_function_1);
      final Consumer<FSMControlDTOAttribute> _function_2 = (FSMControlDTOAttribute it) -> {
        this.buildDTOMethods(control, controlCls, it, this._typeReferenceBuilder.typeRef(IDto.class), control.getDtos(), associatedDtos);
      };
      control.getDtos().forEach(_function_2);
      final Procedure1<JvmOperation> _function_3 = (JvmOperation it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, "host", this._typeReferenceBuilder.typeRef(String.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(control, "port", this._typeReferenceBuilder.typeRef(int.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        it.setVisibility(JvmVisibility.PUBLIC);
        final Procedure1<ITreeAppendable> _function_4 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          {
            for(final FSMControlDTOAttribute dto : associatedDtos) {
              String _name = dto.getName();
              _builder.append(_name);
              _builder.append("=new ");
              String _name_1 = dto.getAttributeType().getAttributeType().getName();
              _builder.append(_name_1);
              _builder.append("();");
              _builder.newLineIfNotEmpty();
            }
          }
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_4);
      };
      JvmOperation init = this._jvmTypesBuilder.toMethod(control, "init", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_3);
      EList<JvmMember> _members = controlCls.getMembers();
      _xblockexpression = this._jvmTypesBuilder.<JvmOperation>operator_add(_members, init);
    }
    return _xblockexpression;
  }
  
  public boolean buildDTOField(final EObject control, final JvmDeclaredType controlCls, final String dtoName, final JvmTypeReference type) {
    boolean _xblockexpression = false;
    {
      JvmField field = this._jvmTypesBuilder.toField(control, dtoName, type);
      field.setVisibility(JvmVisibility.PRIVATE);
      EList<JvmMember> _members = controlCls.getMembers();
      _xblockexpression = this._jvmTypesBuilder.<JvmField>operator_add(_members, field);
    }
    return _xblockexpression;
  }
  
  public boolean buildDTOMethods(final EObject control, final JvmDeclaredType controlCls, final FSMControlDTOAttribute dto, final JvmTypeReference type, final EList<FSMControlDTOAttribute> dtos, final ArrayList<FSMControlDTOAttribute> associatedDtos) {
    boolean _xblockexpression = false;
    {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("get");
      String _firstUpper = StringExtensions.toFirstUpper(dto.getName());
      _builder.append(_firstUpper);
      final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
        it.setVisibility(JvmVisibility.PUBLIC);
        final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("return ");
          String _name = dto.getName();
          _builder_1.append(_name);
          _builder_1.append(";");
          it_1.append(_builder_1);
        };
        this._jvmTypesBuilder.setBody(it, _function_1);
      };
      JvmOperation operation = this._jvmTypesBuilder.toMethod(control, _builder.toString(), type, _function);
      operation.setVisibility(JvmVisibility.PUBLIC);
      EList<JvmMember> _members = controlCls.getMembers();
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members, operation);
      LDtoAbstractAttribute _primaryKeyAttribute = dto.getAttributeType().getAttributeType().getPrimaryKeyAttribute();
      boolean _tripleNotEquals = (_primaryKeyAttribute != null);
      if (_tripleNotEquals) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("get");
        String _firstUpper_1 = StringExtensions.toFirstUpper(dto.getName());
        _builder_1.append(_firstUpper_1);
        String _firstUpper_2 = StringExtensions.toFirstUpper(dto.getAttributeType().getAttributeType().getIdAttributeName());
        _builder_1.append(_firstUpper_2);
        final Procedure1<JvmOperation> _function_1 = (JvmOperation it) -> {
          it.setVisibility(JvmVisibility.PUBLIC);
          final Procedure1<ITreeAppendable> _function_2 = (ITreeAppendable it_1) -> {
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append("return ");
            String _name = dto.getName();
            _builder_2.append(_name);
            _builder_2.append(".get");
            String _firstUpper_3 = StringExtensions.toFirstUpper(dto.getAttributeType().getAttributeType().getIdAttributeName());
            _builder_2.append(_firstUpper_3);
            _builder_2.append("();");
            it_1.append(_builder_2);
          };
          this._jvmTypesBuilder.setBody(it, _function_2);
        };
        operation = this._jvmTypesBuilder.toMethod(control, _builder_1.toString(), 
          this._typeReferenceBuilder.typeRef(dto.getAttributeType().getAttributeType().getIdAttributeType().getName()), _function_1);
        operation.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmMember> _members_1 = controlCls.getMembers();
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, operation);
      }
      EList<LDtoFeature> _features = dto.getAttributeType().getAttributeType().getFeatures();
      for (final LDtoFeature f : _features) {
        {
          if ((f instanceof LDtoAttribute)) {
            this.buildDTOAttribute(control, controlCls, dto, ((LAttribute)f), null, associatedDtos);
          }
          if ((f instanceof LDtoInheritedAttribute)) {
            this.buildDTOAttribute(control, controlCls, dto, ((LDtoInheritedAttribute)f).getInheritedFeature(), ((LDtoInheritedAttribute)f), associatedDtos);
          }
          if ((f instanceof LDtoReference)) {
            final LDto targetDto = ((LDtoReference)f).getType();
            boolean _isToMany = this._dtoModelExtensions.isToMany(f);
            if (_isToMany) {
              if (((dtos != null) && IterableExtensions.<FSMControlDTOAttribute>exists(dtos, ((Function1<FSMControlDTOAttribute, Boolean>) (FSMControlDTOAttribute it) -> {
                String _name = it.getAttributeType().getAttributeType().getName();
                String _name_1 = targetDto.getName();
                return Boolean.valueOf(Objects.equal(_name, _name_1));
              })))) {
                StringConcatenation _builder_2 = new StringConcatenation();
                _builder_2.append("addTo");
                String _firstUpper_3 = StringExtensions.toFirstUpper(dto.getName());
                _builder_2.append(_firstUpper_3);
                String _firstUpper_4 = StringExtensions.toFirstUpper(((LDtoReference)f).getName());
                _builder_2.append(_firstUpper_4);
                final Procedure1<JvmOperation> _function_2 = (JvmOperation it) -> {
                  it.setVisibility(JvmVisibility.PUBLIC);
                  EList<JvmFormalParameter> _parameters = it.getParameters();
                  JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, ((LDtoReference)f).getName(), type);
                  this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
                  final Procedure1<ITreeAppendable> _function_3 = (ITreeAppendable it_1) -> {
                    StringConcatenation _builder_3 = new StringConcatenation();
                    String _name = dto.getName();
                    _builder_3.append(_name);
                    _builder_3.append(".addTo");
                    String _firstUpper_5 = StringExtensions.toFirstUpper(((LDtoReference)f).getName());
                    _builder_3.append(_firstUpper_5);
                    _builder_3.append("((");
                    String _string = ((LDtoReference)f).getType().getName().toString();
                    _builder_3.append(_string);
                    _builder_3.append(")");
                    String _name_1 = ((LDtoReference)f).getName();
                    _builder_3.append(_name_1);
                    _builder_3.append(");");
                    it_1.append(_builder_3);
                  };
                  this._jvmTypesBuilder.setBody(it, _function_3);
                };
                operation = this._jvmTypesBuilder.toMethod(control, _builder_2.toString(), 
                  this._typeReferenceBuilder.typeRef(Void.TYPE), _function_2);
                EList<JvmMember> _members_2 = controlCls.getMembers();
                this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, operation);
                StringConcatenation _builder_3 = new StringConcatenation();
                _builder_3.append("removeFrom");
                String _firstUpper_5 = StringExtensions.toFirstUpper(dto.getName());
                _builder_3.append(_firstUpper_5);
                String _firstUpper_6 = StringExtensions.toFirstUpper(((LDtoReference)f).getName());
                _builder_3.append(_firstUpper_6);
                final Procedure1<JvmOperation> _function_3 = (JvmOperation it) -> {
                  it.setVisibility(JvmVisibility.PUBLIC);
                  EList<JvmFormalParameter> _parameters = it.getParameters();
                  JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, ((LDtoReference)f).getName(), type);
                  this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
                  final Procedure1<ITreeAppendable> _function_4 = (ITreeAppendable it_1) -> {
                    StringConcatenation _builder_4 = new StringConcatenation();
                    String _name = dto.getName();
                    _builder_4.append(_name);
                    _builder_4.append(".removeFrom");
                    String _firstUpper_7 = StringExtensions.toFirstUpper(((LDtoReference)f).getName());
                    _builder_4.append(_firstUpper_7);
                    _builder_4.append("((");
                    String _string = ((LDtoReference)f).getType().getName().toString();
                    _builder_4.append(_string);
                    _builder_4.append(")");
                    String _name_1 = ((LDtoReference)f).getName();
                    _builder_4.append(_name_1);
                    _builder_4.append(");");
                    it_1.append(_builder_4);
                  };
                  this._jvmTypesBuilder.setBody(it, _function_4);
                };
                operation = this._jvmTypesBuilder.toMethod(control, _builder_3.toString(), 
                  this._typeReferenceBuilder.typeRef(Void.TYPE), _function_3);
                EList<JvmMember> _members_3 = controlCls.getMembers();
                this._jvmTypesBuilder.<JvmOperation>operator_add(_members_3, operation);
              }
            } else {
              StringConcatenation _builder_4 = new StringConcatenation();
              _builder_4.append("set");
              String _firstUpper_7 = StringExtensions.toFirstUpper(dto.getName());
              _builder_4.append(_firstUpper_7);
              String _firstUpper_8 = StringExtensions.toFirstUpper(((LDtoReference)f).getName());
              _builder_4.append(_firstUpper_8);
              final Procedure1<JvmOperation> _function_4 = (JvmOperation it) -> {
                it.setVisibility(JvmVisibility.PUBLIC);
                EList<JvmFormalParameter> _parameters = it.getParameters();
                JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, ((LDtoReference)f).getName(), type);
                this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
                final Procedure1<ITreeAppendable> _function_5 = (ITreeAppendable it_1) -> {
                  StringConcatenation _builder_5 = new StringConcatenation();
                  _builder_5.append("if(");
                  String _name = dto.getName();
                  _builder_5.append(_name);
                  _builder_5.append("!=null) {");
                  _builder_5.newLineIfNotEmpty();
                  _builder_5.append("\t");
                  String _name_1 = dto.getName();
                  _builder_5.append(_name_1, "\t");
                  _builder_5.append(".set");
                  String _firstUpper_9 = StringExtensions.toFirstUpper(((LDtoReference)f).getName());
                  _builder_5.append(_firstUpper_9, "\t");
                  _builder_5.append("((");
                  String _string = ((LDtoReference)f).getType().getName().toString();
                  _builder_5.append(_string, "\t");
                  _builder_5.append(")");
                  String _name_2 = ((LDtoReference)f).getName();
                  _builder_5.append(_name_2, "\t");
                  _builder_5.append(");");
                  CharSequence _notifySlaveReference = this.notifySlaveReference(dto, ((LDtoReference)f), associatedDtos);
                  _builder_5.append(_notifySlaveReference, "\t");
                  _builder_5.newLineIfNotEmpty();
                  _builder_5.append("}");
                  it_1.append(_builder_5);
                };
                this._jvmTypesBuilder.setBody(it, _function_5);
              };
              operation = this._jvmTypesBuilder.toMethod(control, _builder_4.toString(), 
                this._typeReferenceBuilder.typeRef(Void.TYPE), _function_4);
              EList<JvmMember> _members_4 = controlCls.getMembers();
              this._jvmTypesBuilder.<JvmOperation>operator_add(_members_4, operation);
              StringConcatenation _builder_5 = new StringConcatenation();
              _builder_5.append("get");
              String _firstUpper_9 = StringExtensions.toFirstUpper(dto.getName());
              _builder_5.append(_firstUpper_9);
              String _firstUpper_10 = StringExtensions.toFirstUpper(((LDtoReference)f).getName());
              _builder_5.append(_firstUpper_10);
              final Procedure1<JvmOperation> _function_5 = (JvmOperation it) -> {
                it.setVisibility(JvmVisibility.PUBLIC);
                EList<JvmFormalParameter> _parameters = it.getParameters();
                JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, ((LDtoReference)f).getName(), type);
                this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
                final Procedure1<ITreeAppendable> _function_6 = (ITreeAppendable it_1) -> {
                  StringConcatenation _builder_6 = new StringConcatenation();
                  _builder_6.append("return ");
                  String _name = dto.getName();
                  _builder_6.append(_name);
                  _builder_6.append(".get");
                  String _firstUpper_11 = StringExtensions.toFirstUpper(((LDtoReference)f).getName());
                  _builder_6.append(_firstUpper_11);
                  _builder_6.append("();");
                  it_1.append(_builder_6);
                };
                this._jvmTypesBuilder.setBody(it, _function_6);
              };
              operation = this._jvmTypesBuilder.toMethod(control, _builder_5.toString(), type, _function_5);
              EList<JvmMember> _members_5 = controlCls.getMembers();
              this._jvmTypesBuilder.<JvmOperation>operator_add(_members_5, operation);
            }
          }
          if ((f instanceof LDtoInheritedReference)) {
            final LDto targetDto_1 = ((LDtoInheritedReference)f).getType();
            boolean _isToMany_1 = this._dtoModelExtensions.isToMany(((LDtoInheritedReference)f).getInheritedFeature());
            if (_isToMany_1) {
              if (((dtos != null) && IterableExtensions.<FSMControlDTOAttribute>exists(dtos, ((Function1<FSMControlDTOAttribute, Boolean>) (FSMControlDTOAttribute it) -> {
                String _name = it.getAttributeType().getAttributeType().getName();
                String _name_1 = targetDto_1.getName();
                return Boolean.valueOf(Objects.equal(_name, _name_1));
              })))) {
                StringConcatenation _builder_6 = new StringConcatenation();
                _builder_6.append("addTo");
                String _firstUpper_11 = StringExtensions.toFirstUpper(dto.getName());
                _builder_6.append(_firstUpper_11);
                String _firstUpper_12 = StringExtensions.toFirstUpper(((LDtoInheritedReference)f).getInheritedFeature().getName());
                _builder_6.append(_firstUpper_12);
                final Procedure1<JvmOperation> _function_6 = (JvmOperation it) -> {
                  it.setVisibility(JvmVisibility.PUBLIC);
                  EList<JvmFormalParameter> _parameters = it.getParameters();
                  JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, ((LDtoInheritedReference)f).getInheritedFeature().getName(), type);
                  this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
                  final Procedure1<ITreeAppendable> _function_7 = (ITreeAppendable it_1) -> {
                    StringConcatenation _builder_7 = new StringConcatenation();
                    String _name = dto.getName();
                    _builder_7.append(_name);
                    _builder_7.append(".addTo");
                    String _firstUpper_13 = StringExtensions.toFirstUpper(((LDtoInheritedReference)f).getInheritedFeature().getName());
                    _builder_7.append(_firstUpper_13);
                    _builder_7.append("((");
                    String _string = ((LDtoInheritedReference)f).getType().getName().toString();
                    _builder_7.append(_string);
                    _builder_7.append(")");
                    String _name_1 = ((LDtoInheritedReference)f).getInheritedFeature().getName();
                    _builder_7.append(_name_1);
                    _builder_7.append(");");
                    it_1.append(_builder_7);
                  };
                  this._jvmTypesBuilder.setBody(it, _function_7);
                };
                operation = this._jvmTypesBuilder.toMethod(control, _builder_6.toString(), 
                  this._typeReferenceBuilder.typeRef(Void.TYPE), _function_6);
                EList<JvmMember> _members_6 = controlCls.getMembers();
                this._jvmTypesBuilder.<JvmOperation>operator_add(_members_6, operation);
                StringConcatenation _builder_7 = new StringConcatenation();
                _builder_7.append("removeFrom");
                String _firstUpper_13 = StringExtensions.toFirstUpper(dto.getName());
                _builder_7.append(_firstUpper_13);
                String _firstUpper_14 = StringExtensions.toFirstUpper(((LDtoInheritedReference)f).getInheritedFeature().getName());
                _builder_7.append(_firstUpper_14);
                final Procedure1<JvmOperation> _function_7 = (JvmOperation it) -> {
                  it.setVisibility(JvmVisibility.PUBLIC);
                  EList<JvmFormalParameter> _parameters = it.getParameters();
                  JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, ((LDtoInheritedReference)f).getInheritedFeature().getName(), type);
                  this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
                  final Procedure1<ITreeAppendable> _function_8 = (ITreeAppendable it_1) -> {
                    StringConcatenation _builder_8 = new StringConcatenation();
                    String _name = dto.getName();
                    _builder_8.append(_name);
                    _builder_8.append(".removeFrom");
                    String _firstUpper_15 = StringExtensions.toFirstUpper(((LDtoInheritedReference)f).getInheritedFeature().getName());
                    _builder_8.append(_firstUpper_15);
                    _builder_8.append("((");
                    String _string = ((LDtoInheritedReference)f).getType().getName().toString();
                    _builder_8.append(_string);
                    _builder_8.append(")");
                    String _name_1 = ((LDtoInheritedReference)f).getInheritedFeature().getName();
                    _builder_8.append(_name_1);
                    _builder_8.append(");");
                    it_1.append(_builder_8);
                  };
                  this._jvmTypesBuilder.setBody(it, _function_8);
                };
                operation = this._jvmTypesBuilder.toMethod(control, _builder_7.toString(), 
                  this._typeReferenceBuilder.typeRef(Void.TYPE), _function_7);
                EList<JvmMember> _members_7 = controlCls.getMembers();
                this._jvmTypesBuilder.<JvmOperation>operator_add(_members_7, operation);
              }
            } else {
              if (((dtos != null) && IterableExtensions.<FSMControlDTOAttribute>exists(dtos, ((Function1<FSMControlDTOAttribute, Boolean>) (FSMControlDTOAttribute it) -> {
                String _name = it.getAttributeType().getAttributeType().getName();
                String _name_1 = targetDto_1.getName();
                return Boolean.valueOf(Objects.equal(_name, _name_1));
              })))) {
                EList<JvmMember> _members_8 = controlCls.getMembers();
                this._jvmTypesBuilder.<JvmOperation>operator_add(_members_8, operation);
                StringConcatenation _builder_8 = new StringConcatenation();
                _builder_8.append("set");
                String _firstUpper_15 = StringExtensions.toFirstUpper(dto.getName());
                _builder_8.append(_firstUpper_15);
                String _firstUpper_16 = StringExtensions.toFirstUpper(((LDtoInheritedReference)f).getInheritedFeature().getName());
                _builder_8.append(_firstUpper_16);
                final Procedure1<JvmOperation> _function_8 = (JvmOperation it) -> {
                  it.setVisibility(JvmVisibility.PUBLIC);
                  EList<JvmFormalParameter> _parameters = it.getParameters();
                  JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, ((LDtoInheritedReference)f).getInheritedFeature().getName(), type);
                  this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
                  final Procedure1<ITreeAppendable> _function_9 = (ITreeAppendable it_1) -> {
                    StringConcatenation _builder_9 = new StringConcatenation();
                    String _name = dto.getName();
                    _builder_9.append(_name);
                    _builder_9.append(".set");
                    String _firstUpper_17 = StringExtensions.toFirstUpper(((LDtoInheritedReference)f).getInheritedFeature().getName());
                    _builder_9.append(_firstUpper_17);
                    _builder_9.append("((");
                    String _string = ((LDtoInheritedReference)f).getType().getName().toString();
                    _builder_9.append(_string);
                    _builder_9.append(")");
                    String _name_1 = ((LDtoInheritedReference)f).getInheritedFeature().getName();
                    _builder_9.append(_name_1);
                    _builder_9.append(");");
                    it_1.append(_builder_9);
                  };
                  this._jvmTypesBuilder.setBody(it, _function_9);
                };
                operation = this._jvmTypesBuilder.toMethod(control, _builder_8.toString(), 
                  this._typeReferenceBuilder.typeRef(Void.TYPE), _function_8);
                EList<JvmMember> _members_9 = controlCls.getMembers();
                this._jvmTypesBuilder.<JvmOperation>operator_add(_members_9, operation);
              }
            }
          }
        }
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("set");
      String _firstUpper_3 = StringExtensions.toFirstUpper(dto.getName());
      _builder_2.append(_firstUpper_3);
      final Procedure1<JvmOperation> _function_2 = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, dto.getName(), type);
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        it.setVisibility(JvmVisibility.PUBLIC);
        final Procedure1<ITreeAppendable> _function_3 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder_3 = new StringConcatenation();
          {
            boolean _isHasEvent = dto.isHasEvent();
            if (_isHasEvent) {
              _builder_3.append("if(statemachine!=null && !(this.");
              String _name = dto.getName();
              _builder_3.append(_name);
              _builder_3.append("==null && ");
              String _name_1 = dto.getName();
              _builder_3.append(_name_1);
              _builder_3.append("==null) && !(this.");
              String _name_2 = dto.getName();
              _builder_3.append(_name_2);
              _builder_3.append("!=null && this.");
              String _name_3 = dto.getName();
              _builder_3.append(_name_3);
              _builder_3.append(".equals((");
              String _name_4 = dto.getAttributeType().getAttributeType().getName();
              _builder_3.append(_name_4);
              _builder_3.append(")");
              String _name_5 = dto.getName();
              _builder_3.append(_name_5);
              _builder_3.append("))) {");
              _builder_3.newLineIfNotEmpty();
              _builder_3.append("\t");
              _builder_3.append("statemachine.processEvent(statemachine, new MessageEvent(MessageEvent.EventType.TRIGGER, \"");
              String _name_6 = dto.getEvent().getName();
              _builder_3.append(_name_6, "\t");
              _builder_3.append("\"));");
              _builder_3.newLineIfNotEmpty();
              _builder_3.append("}");
            }
          }
          _builder_3.newLineIfNotEmpty();
          {
            if ((dto.isIsAttached() && (dto.getDisplay() != null))) {
              {
                final Predicate<LFeature> _function_4 = (LFeature it_2) -> {
                  return ((it_2 instanceof LDtoReference) && ((LDtoReference) it_2).getType().equals(dto.getAttributeType().getAttributeType()));
                };
                List<LFeature> _collect = dto.getDisplay().getOutput().getAttributeType().getAttributeType().getAllFeatures().stream().filter(_function_4).collect(Collectors.<LFeature>toList());
                for(final LFeature match : _collect) {
                  _builder_3.append("set");
                  String _firstUpper_4 = StringExtensions.toFirstUpper(dto.getDisplay().getOutput().getName());
                  _builder_3.append(_firstUpper_4);
                  String _firstUpper_5 = StringExtensions.toFirstUpper(match.getName());
                  _builder_3.append(_firstUpper_5);
                  _builder_3.append("(");
                  String _name_7 = dto.getName();
                  _builder_3.append(_name_7);
                  _builder_3.append(");");
                }
              }
              _builder_3.newLineIfNotEmpty();
              {
                final Predicate<LFeature> _function_5 = (LFeature it_2) -> {
                  return ((it_2 instanceof LDtoInheritedReference) && ((LDtoInheritedReference) it_2).getType().equals(dto.getAttributeType().getAttributeType()));
                };
                List<LFeature> _collect_1 = dto.getDisplay().getOutput().getAttributeType().getAttributeType().getAllFeatures().stream().filter(_function_5).collect(Collectors.<LFeature>toList());
                for(final LFeature match_1 : _collect_1) {
                  _builder_3.append("set");
                  String _firstUpper_6 = StringExtensions.toFirstUpper(dto.getDisplay().getOutput().getName());
                  _builder_3.append(_firstUpper_6);
                  String _firstUpper_7 = StringExtensions.toFirstUpper(match_1.getName());
                  _builder_3.append(_firstUpper_7);
                  _builder_3.append("(");
                  String _name_8 = dto.getName();
                  _builder_3.append(_name_8);
                  _builder_3.append(");");
                }
              }
            }
          }
          _builder_3.newLineIfNotEmpty();
          _builder_3.append("pcs.firePropertyChange(\"");
          String _firstLower = StringExtensions.toFirstLower(dto.getName());
          _builder_3.append(_firstLower);
          _builder_3.append("\", this.");
          String _name_9 = dto.getName();
          _builder_3.append(_name_9);
          _builder_3.append(", this.");
          String _name_10 = dto.getName();
          _builder_3.append(_name_10);
          _builder_3.append(" = (");
          String _name_11 = dto.getAttributeType().getAttributeType().getName();
          _builder_3.append(_name_11);
          _builder_3.append(")");
          String _name_12 = dto.getName();
          _builder_3.append(_name_12);
          _builder_3.append(");");
          _builder_3.newLineIfNotEmpty();
          it_1.append(_builder_3);
        };
        this._jvmTypesBuilder.setBody(it, _function_3);
      };
      operation = this._jvmTypesBuilder.toMethod(control, _builder_2.toString(), 
        this._typeReferenceBuilder.typeRef(Void.TYPE), _function_2);
      boolean _xifexpression = false;
      boolean _contains = associatedDtos.contains(dto);
      boolean _not = (!_contains);
      if (_not) {
        boolean _xblockexpression_1 = false;
        {
          operation.setVisibility(JvmVisibility.PUBLIC);
          EList<JvmMember> _members_2 = controlCls.getMembers();
          this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, operation);
          StringConcatenation _builder_3 = new StringConcatenation();
          _builder_3.append("update");
          String _firstUpper_4 = StringExtensions.toFirstUpper(dto.getName());
          _builder_3.append(_firstUpper_4);
          final Procedure1<JvmOperation> _function_3 = (JvmOperation it) -> {
            it.setVisibility(JvmVisibility.PUBLIC);
            EList<JvmFormalParameter> _parameters = it.getParameters();
            JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, dto.getName(), type);
            this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
            final Procedure1<ITreeAppendable> _function_4 = (ITreeAppendable it_1) -> {
              StringConcatenation _builder_4 = new StringConcatenation();
              _builder_4.append("((IDTOService<");
              String _name = dto.getAttributeType().getAttributeType().getName();
              _builder_4.append(_name);
              _builder_4.append(">)dtoServices.get(\"");
              String _name_1 = dto.getName();
              _builder_4.append(_name_1);
              _builder_4.append("\")).update((");
              String _name_2 = dto.getAttributeType().getAttributeType().getName();
              _builder_4.append(_name_2);
              _builder_4.append(")");
              String _name_3 = dto.getName();
              _builder_4.append(_name_3);
              _builder_4.append(");");
              _builder_4.newLineIfNotEmpty();
              String _name_4 = dto.getAttributeType().getAttributeType().getName();
              _builder_4.append(_name_4);
              _builder_4.append(" dto = ((IDTOService<");
              String _name_5 = dto.getAttributeType().getAttributeType().getName();
              _builder_4.append(_name_5);
              _builder_4.append(">)dtoServices.get(\"");
              String _name_6 = dto.getName();
              _builder_4.append(_name_6);
              _builder_4.append("\")).reload((");
              String _name_7 = dto.getAttributeType().getAttributeType().getName();
              _builder_4.append(_name_7);
              _builder_4.append(")");
              String _name_8 = dto.getName();
              _builder_4.append(_name_8);
              _builder_4.append(");");
              _builder_4.newLineIfNotEmpty();
              _builder_4.append("set");
              String _firstUpper_5 = StringExtensions.toFirstUpper(dto.getName());
              _builder_4.append(_firstUpper_5);
              _builder_4.append("(dto);");
              it_1.append(_builder_4);
            };
            this._jvmTypesBuilder.setBody(it, _function_4);
          };
          operation = this._jvmTypesBuilder.toMethod(control, _builder_3.toString(), 
            this._typeReferenceBuilder.typeRef(Void.TYPE), _function_3);
          EList<JvmMember> _members_3 = controlCls.getMembers();
          this._jvmTypesBuilder.<JvmOperation>operator_add(_members_3, operation);
          StringConcatenation _builder_4 = new StringConcatenation();
          _builder_4.append("reload");
          String _firstUpper_5 = StringExtensions.toFirstUpper(dto.getName());
          _builder_4.append(_firstUpper_5);
          final Procedure1<JvmOperation> _function_4 = (JvmOperation it) -> {
            it.setVisibility(JvmVisibility.PUBLIC);
            EList<JvmFormalParameter> _parameters = it.getParameters();
            JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, dto.getName(), type);
            this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
            final Procedure1<ITreeAppendable> _function_5 = (ITreeAppendable it_1) -> {
              StringConcatenation _builder_5 = new StringConcatenation();
              String _name = dto.getAttributeType().getAttributeType().getName();
              _builder_5.append(_name);
              _builder_5.append(" dto = ((IDTOService<");
              String _name_1 = dto.getAttributeType().getAttributeType().getName();
              _builder_5.append(_name_1);
              _builder_5.append(">)dtoServices.get(\"");
              String _name_2 = dto.getName();
              _builder_5.append(_name_2);
              _builder_5.append("\")).reload((");
              String _name_3 = dto.getAttributeType().getAttributeType().getName();
              _builder_5.append(_name_3);
              _builder_5.append(")");
              String _name_4 = dto.getName();
              _builder_5.append(_name_4);
              _builder_5.append(");");
              _builder_5.newLineIfNotEmpty();
              _builder_5.append("set");
              String _firstUpper_6 = StringExtensions.toFirstUpper(dto.getName());
              _builder_5.append(_firstUpper_6);
              _builder_5.append("(dto);");
              it_1.append(_builder_5);
            };
            this._jvmTypesBuilder.setBody(it, _function_5);
          };
          operation = this._jvmTypesBuilder.toMethod(control, _builder_4.toString(), 
            this._typeReferenceBuilder.typeRef(Void.TYPE), _function_4);
          EList<JvmMember> _members_4 = controlCls.getMembers();
          this._jvmTypesBuilder.<JvmOperation>operator_add(_members_4, operation);
          StringConcatenation _builder_5 = new StringConcatenation();
          _builder_5.append("delete");
          String _firstUpper_6 = StringExtensions.toFirstUpper(dto.getName());
          _builder_5.append(_firstUpper_6);
          final Procedure1<JvmOperation> _function_5 = (JvmOperation it) -> {
            it.setVisibility(JvmVisibility.PUBLIC);
            EList<JvmFormalParameter> _parameters = it.getParameters();
            JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, dto.getName(), type);
            this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
            final Procedure1<ITreeAppendable> _function_6 = (ITreeAppendable it_1) -> {
              StringConcatenation _builder_6 = new StringConcatenation();
              _builder_6.append("((IDTOService<");
              String _name = dto.getAttributeType().getAttributeType().getName();
              _builder_6.append(_name);
              _builder_6.append(">)dtoServices.get(\"");
              String _name_1 = dto.getName();
              _builder_6.append(_name_1);
              _builder_6.append("\")).delete((");
              String _name_2 = dto.getAttributeType().getAttributeType().getName();
              _builder_6.append(_name_2);
              _builder_6.append(")");
              String _name_3 = dto.getName();
              _builder_6.append(_name_3);
              _builder_6.append(");");
              it_1.append(_builder_6);
            };
            this._jvmTypesBuilder.setBody(it, _function_6);
          };
          operation = this._jvmTypesBuilder.toMethod(control, _builder_5.toString(), 
            this._typeReferenceBuilder.typeRef(Void.TYPE), _function_5);
          EList<JvmMember> _members_5 = controlCls.getMembers();
          _xblockexpression_1 = this._jvmTypesBuilder.<JvmOperation>operator_add(_members_5, operation);
        }
        _xifexpression = _xblockexpression_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public CharSequence notifySlaveReference(final FSMControlDTOAttribute dto, final LDtoReference f, final ArrayList<FSMControlDTOAttribute> associatedDtos) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _contains = associatedDtos.contains(dto);
      if (_contains) {
        _builder.newLine();
        _builder.append("if(statemachine != null) {");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("Map<String, Object> data = new HashMap<>();");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("data.put(\"");
        QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(dto.getAttributeType().getAttributeType());
        _builder.append(_fullyQualifiedName, "\t");
        _builder.append("/");
        String _name = f.getName();
        _builder.append(_name, "\t");
        _builder.append("\", ");
        String _name_1 = f.getName();
        _builder.append(_name_1, "\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("statemachine.sendSlaveData(data);");
        _builder.newLine();
        _builder.append("}");
      }
    }
    _builder.append("\t");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  public CharSequence notifySlaveAttribute(final FSMControlDTOAttribute dto, final LAttribute f, final ArrayList<FSMControlDTOAttribute> associatedDtos) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _contains = associatedDtos.contains(dto);
      if (_contains) {
        _builder.newLine();
        _builder.append("if(statemachine != null) {");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("Map<String, Object> data = new HashMap<>();");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("data.put(\"");
        QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(dto.getAttributeType().getAttributeType());
        _builder.append(_fullyQualifiedName, "\t");
        _builder.append("/");
        String _name = f.getName();
        _builder.append(_name, "\t");
        _builder.append("\", ");
        String _name_1 = f.getName();
        _builder.append(_name_1, "\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("statemachine.sendSlaveData(data);");
        _builder.newLine();
        _builder.append("}");
      }
    }
    _builder.append("\t");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  public boolean buildDTOAttribute(final EObject control, final JvmDeclaredType controlCls, final FSMControlDTOAttribute dto, final LAttribute f, final LDtoInheritedAttribute prop, final ArrayList<FSMControlDTOAttribute> associatedDtos) {
    boolean _xblockexpression = false;
    {
      boolean isTimestamp = false;
      JvmTypeReference typeRef = this._dtoModelExtensions.toTypeReference(f.getType());
      LScalarType st = null;
      LScalarType _type = f.getType();
      boolean _matched = false;
      if (_type instanceof LDataType) {
        _matched=true;
        LScalarType _type_1 = f.getType();
        final LDataType _dt = ((LDataType) _type_1);
        st = _dt;
        if ((_dt.isDate() && Objects.equal(_dt.getDateType(), LDateType.TIMESTAMP))) {
          isTimestamp = true;
        }
      }
      if (!_matched) {
        if (_type instanceof LEnum) {
          _matched=true;
          LScalarType _type_1 = f.getType();
          st = ((LEnum) _type_1);
          if ((prop != null)) {
            typeRef = this._dtoModelExtensions.toDtoTypeReference(prop);
          }
        }
      }
      if (!_matched) {
        if (_type instanceof LBean) {
          _matched=true;
          LScalarType _type_1 = f.getType();
          st = ((LBean) _type_1);
          if ((prop != null)) {
            typeRef = this._dtoModelExtensions.toDtoTypeReference(prop);
          }
        }
      }
      if (isTimestamp) {
        typeRef = this._typeReferenceBuilder.typeRef("org.joda.time.DateTime");
      }
      final boolean _isTimestamp = isTimestamp;
      final LScalarType _st = st;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("get");
      String _firstUpper = StringExtensions.toFirstUpper(dto.getName());
      _builder.append(_firstUpper);
      String _firstUpper_1 = StringExtensions.toFirstUpper(f.getName());
      _builder.append(_firstUpper_1);
      final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
        it.setVisibility(JvmVisibility.PUBLIC);
        final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("if(");
          String _name = dto.getName();
          _builder_1.append(_name);
          _builder_1.append("!=null) {");
          _builder_1.newLineIfNotEmpty();
          _builder_1.append("\t");
          _builder_1.append("return ");
          {
            if (_isTimestamp) {
              _builder_1.append("new DateTime(");
            }
          }
          String _name_1 = dto.getName();
          _builder_1.append(_name_1, "\t");
          _builder_1.append(".get");
          String _firstUpper_2 = StringExtensions.toFirstUpper(f.getName());
          _builder_1.append(_firstUpper_2, "\t");
          _builder_1.append("()");
          {
            if (_isTimestamp) {
              _builder_1.append(")");
            }
          }
          _builder_1.append(";");
          _builder_1.newLineIfNotEmpty();
          _builder_1.append("} else {");
          _builder_1.newLine();
          _builder_1.append("\t");
          _builder_1.append("return ");
          String _nullReturnValue = this._dtoModelExtensions.toNullReturnValue(_st);
          _builder_1.append(_nullReturnValue, "\t");
          _builder_1.append(";");
          _builder_1.newLineIfNotEmpty();
          _builder_1.append("}");
          it_1.append(_builder_1);
        };
        this._jvmTypesBuilder.setBody(it, _function_1);
      };
      JvmOperation operation = this._jvmTypesBuilder.toMethod(control, _builder.toString(), typeRef, _function);
      final JvmTypeReference _typeRef = typeRef;
      EList<JvmMember> _members = controlCls.getMembers();
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members, operation);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("set");
      String _firstUpper_2 = StringExtensions.toFirstUpper(dto.getName());
      _builder_1.append(_firstUpper_2);
      String _firstUpper_3 = StringExtensions.toFirstUpper(f.getName());
      _builder_1.append(_firstUpper_3);
      final Procedure1<JvmOperation> _function_1 = (JvmOperation it) -> {
        it.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, f.getName(), _typeRef);
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        final Procedure1<ITreeAppendable> _function_2 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("if(");
          String _name = dto.getName();
          _builder_2.append(_name);
          _builder_2.append("!=null) {");
          _builder_2.newLineIfNotEmpty();
          _builder_2.append("\t");
          String _name_1 = dto.getName();
          _builder_2.append(_name_1, "\t");
          _builder_2.append(".set");
          String _firstUpper_4 = StringExtensions.toFirstUpper(f.getName());
          _builder_2.append(_firstUpper_4, "\t");
          _builder_2.append("(");
          String _name_2 = f.getName();
          _builder_2.append(_name_2, "\t");
          {
            if (_isTimestamp) {
              _builder_2.append(".toDate()");
            }
          }
          _builder_2.append(");");
          CharSequence _notifySlaveAttribute = this.notifySlaveAttribute(dto, f, associatedDtos);
          _builder_2.append(_notifySlaveAttribute, "\t");
          _builder_2.newLineIfNotEmpty();
          _builder_2.append("}");
          it_1.append(_builder_2);
        };
        this._jvmTypesBuilder.setBody(it, _function_2);
      };
      operation = this._jvmTypesBuilder.toMethod(control, _builder_1.toString(), 
        this._typeReferenceBuilder.typeRef(Void.TYPE), _function_1);
      EList<JvmMember> _members_1 = controlCls.getMembers();
      _xblockexpression = this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, operation);
    }
    return _xblockexpression;
  }
  
  public JvmTypeReference decodeFilterProperty(final FSMFilterProperty property, final ArrayList<String> parts) {
    parts.clear();
    FSMRef _path = property.getPath();
    return this.decodePath(((FSMDotExpression) _path), parts);
  }
  
  protected JvmTypeReference decodePath(final FSMDotExpression path, final ArrayList<String> parts) {
    JvmTypeReference type = null;
    final FSMRef head = path.getRef();
    boolean _matched = false;
    if (head instanceof FSMDotExpression) {
      _matched=true;
      JvmTypeReference tmpType = this.decodePath(((FSMDotExpression)head), parts);
      if ((type == null)) {
        type = tmpType;
      }
    }
    JvmTypeReference tmpType = this.decodeTail(path.getTail(), parts);
    if ((type == null)) {
      type = tmpType;
    }
    return type;
  }
  
  protected JvmTypeReference decodeTail(final LFeature tail, final ArrayList<String> parts) {
    JvmTypeReference type = null;
    EObject ref = null;
    boolean _matched = false;
    if (tail instanceof LDtoReference) {
      _matched=true;
      parts.add(((LDtoReference)tail).getName());
    }
    if (!_matched) {
      if (tail instanceof LDtoAttribute) {
        _matched=true;
        parts.add(((LDtoAttribute)tail).getName());
        type = this._dtoModelExtensions.getDatatype(tail).getJvmTypeReference();
      }
    }
    if (!_matched) {
      if (tail instanceof LDtoInheritedReference) {
        _matched=true;
        ref = ((LDtoInheritedReference) tail).getInheritedFeature();
        if ((ref instanceof LEntityReference)) {
          parts.add(((LEntityReference)ref).getName());
        }
      }
    }
    if (!_matched) {
      if (tail instanceof LDtoInheritedAttribute) {
        _matched=true;
        ref = ((LDtoInheritedAttribute) tail).getInheritedFeature();
        boolean _matched_1 = false;
        if (ref instanceof LBeanAttribute) {
          _matched_1=true;
          parts.add(((LBeanAttribute)ref).getName());
          type = this._dtoModelExtensions.getDatatype(((LFeature)ref)).getJvmTypeReference();
        }
        if (!_matched_1) {
          if (ref instanceof LEntityAttribute) {
            _matched_1=true;
            parts.add(((LEntityAttribute)ref).getName());
            type = this._dtoModelExtensions.getDatatype(((LFeature)ref)).getJvmTypeReference();
          }
        }
      }
    }
    if (!_matched) {
      parts.add("unknown");
    }
    return type;
  }
  
  public String buildFilter(final FSMAbstractFilter filter) {
    JvmTypeReference type = null;
    ArrayList<String> parts = CollectionLiterals.<String>newArrayList();
    boolean _matched = false;
    if (filter instanceof FSMAndFilter) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("new And(");
      String _buildFilter = this.buildFilter(((FSMAndFilter)filter).getFirst().getSource());
      _builder.append(_buildFilter);
      {
        EList<FSMFilter> _more = ((FSMAndFilter)filter).getMore();
        for(final FSMFilter f : _more) {
          _builder.append(",");
          String _buildFilter_1 = this.buildFilter(f.getSource());
          _builder.append(_buildFilter_1);
        }
      }
      _builder.append(")");
      return _builder.toString();
    }
    if (!_matched) {
      if (filter instanceof FSMOrFilter) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("new Or(");
        String _buildFilter = this.buildFilter(((FSMOrFilter)filter).getFirst().getSource());
        _builder.append(_buildFilter);
        {
          EList<FSMFilter> _more = ((FSMOrFilter)filter).getMore();
          for(final FSMFilter f : _more) {
            _builder.append(",");
            String _buildFilter_1 = this.buildFilter(f.getSource());
            _builder.append(_buildFilter_1);
          }
        }
        _builder.append(")");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (filter instanceof FSMBetweenFilter) {
        _matched=true;
        type = this.decodeFilterProperty(((FSMBetweenFilter)filter).getPropertyId(), parts);
        final String property = parts.stream().collect(Collectors.joining(".", "\"", "\""));
        final String start = this.decodeActionFieldSource(((FSMBetweenFilter)filter).getStart(), type);
        final String end = this.decodeActionFieldSource(((FSMBetweenFilter)filter).getEnd(), type);
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("new Between(");
        _builder.append(property);
        _builder.append(",");
        _builder.append(start);
        _builder.append(",");
        _builder.append(end);
        _builder.append(")");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (filter instanceof FSMCompareFilter) {
        _matched=true;
        type = this.decodeFilterProperty(((FSMCompareFilter)filter).getPropertyId(), parts);
        final String property = parts.stream().collect(Collectors.joining(".", "\"", "\""));
        final String operand = this.decodeActionFieldSource(((FSMCompareFilter)filter).getOperand(), type);
        final String oper = StringExtensions.toFirstUpper(((FSMCompareFilter)filter).getOperation().getLiteral());
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("new ");
        _builder.append(oper);
        _builder.append("(");
        _builder.append(property);
        _builder.append(",");
        _builder.append(operand);
        _builder.append(")");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (filter instanceof FSMIsNullFilter) {
        _matched=true;
        type = this.decodeFilterProperty(((FSMIsNullFilter)filter).getPropertyId(), parts);
        final String property = parts.stream().collect(Collectors.joining(".", "\"", "\""));
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("new IsNull(");
        _builder.append(property);
        _builder.append(")");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (filter instanceof FSMLikeFilter) {
        _matched=true;
        type = this.decodeFilterProperty(((FSMLikeFilter)filter).getPropertyId(), parts);
        final String property = parts.stream().collect(Collectors.joining(".", "\"", "\""));
        final String value = this.decodeActionFieldSource(((FSMLikeFilter)filter).getValue(), type);
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("new Like(");
        _builder.append(property);
        _builder.append(",");
        _builder.append(value);
        _builder.append(",");
        String _string = Boolean.valueOf(Boolean.valueOf(((FSMLikeFilter)filter).isIgnoreCase()).booleanValue()).toString();
        _builder.append(_string);
        _builder.append(")");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (filter instanceof FSMNotFilter) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("new Not(");
        String _buildFilter = this.buildFilter(((FSMNotFilter)filter).getFilter().getSource());
        _builder.append(_buildFilter);
        _builder.append(")");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (filter instanceof FSMStringFilter) {
        _matched=true;
        type = this.decodeFilterProperty(((FSMStringFilter)filter).getPropertyId(), parts);
        final String property = parts.stream().collect(Collectors.joining(".", "\"", "\""));
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("new SimpleStringFilter(");
        _builder.append(property);
        _builder.append(",\"");
        String _filterString = ((FSMStringFilter)filter).getFilterString();
        _builder.append(_filterString);
        _builder.append("\",");
        String _string = Boolean.valueOf(Boolean.valueOf(((FSMStringFilter)filter).isIgnoreCase()).booleanValue()).toString();
        _builder.append(_string);
        _builder.append(",");
        String _string_1 = Boolean.valueOf(Boolean.valueOf(((FSMStringFilter)filter).isOnlyMatchPrefix()).booleanValue()).toString();
        _builder.append(_string_1);
        _builder.append(")");
        return _builder.toString();
      }
    }
    return "null";
  }
  
  public String decodeSetter(final FSMControlFieldAttribute field) {
    FSMInternalType _attributeType = field.getAttributeType().getAttributeType();
    if (_attributeType != null) {
      switch (_attributeType) {
        case BOOLEAN:
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("pcs.firePropertyChange(\"");
          String _name = field.getName();
          _builder.append(_name);
          _builder.append("\", this.");
          String _name_1 = field.getName();
          _builder.append(_name_1);
          _builder.append(", this.");
          String _name_2 = field.getName();
          _builder.append(_name_2);
          _builder.append(" = ");
          String _name_3 = field.getName();
          _builder.append(_name_3);
          _builder.append(");");
          _builder.newLineIfNotEmpty();
          return _builder.toString();
        case INTEGER:
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("pcs.firePropertyChange(\"");
          String _name_4 = field.getName();
          _builder_1.append(_name_4);
          _builder_1.append("\", this.");
          String _name_5 = field.getName();
          _builder_1.append(_name_5);
          _builder_1.append(", this.");
          String _name_6 = field.getName();
          _builder_1.append(_name_6);
          _builder_1.append(" = ");
          String _name_7 = field.getName();
          _builder_1.append(_name_7);
          _builder_1.append(");");
          _builder_1.newLineIfNotEmpty();
          return _builder_1.toString();
        case LONG:
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("pcs.firePropertyChange(\"");
          String _name_8 = field.getName();
          _builder_2.append(_name_8);
          _builder_2.append("\", this.");
          String _name_9 = field.getName();
          _builder_2.append(_name_9);
          _builder_2.append(", this.");
          String _name_10 = field.getName();
          _builder_2.append(_name_10);
          _builder_2.append(" = ");
          String _name_11 = field.getName();
          _builder_2.append(_name_11);
          _builder_2.append(");");
          _builder_2.newLineIfNotEmpty();
          return _builder_2.toString();
        case DOUBLE:
          StringConcatenation _builder_3 = new StringConcatenation();
          _builder_3.append("pcs.firePropertyChange(\"");
          String _name_12 = field.getName();
          _builder_3.append(_name_12);
          _builder_3.append("\", this.");
          String _name_13 = field.getName();
          _builder_3.append(_name_13);
          _builder_3.append(", this.");
          String _name_14 = field.getName();
          _builder_3.append(_name_14);
          _builder_3.append(" = ");
          String _name_15 = field.getName();
          _builder_3.append(_name_15);
          _builder_3.append(");");
          _builder_3.newLineIfNotEmpty();
          return _builder_3.toString();
        case STRING:
          StringConcatenation _builder_4 = new StringConcatenation();
          _builder_4.append("pcs.firePropertyChange(\"");
          String _name_16 = field.getName();
          _builder_4.append(_name_16);
          _builder_4.append("\", this.");
          String _name_17 = field.getName();
          _builder_4.append(_name_17);
          _builder_4.append(", this.");
          String _name_18 = field.getName();
          _builder_4.append(_name_18);
          _builder_4.append(" = ");
          String _name_19 = field.getName();
          _builder_4.append(_name_19);
          _builder_4.append(");");
          _builder_4.newLineIfNotEmpty();
          return _builder_4.toString();
        case DATE:
          StringConcatenation _builder_5 = new StringConcatenation();
          _builder_5.append("pcs.firePropertyChange(\"");
          String _name_20 = field.getName();
          _builder_5.append(_name_20);
          _builder_5.append("\", this.");
          String _name_21 = field.getName();
          _builder_5.append(_name_21);
          _builder_5.append(", this.");
          String _name_22 = field.getName();
          _builder_5.append(_name_22);
          _builder_5.append(" = ");
          String _name_23 = field.getName();
          _builder_5.append(_name_23);
          _builder_5.append(");");
          _builder_5.newLineIfNotEmpty();
          return _builder_5.toString();
        case SUGGESTTEXT:
          StringConcatenation _builder_6 = new StringConcatenation();
          _builder_6.append("pcs.firePropertyChange(\"");
          String _name_24 = field.getName();
          _builder_6.append(_name_24);
          _builder_6.append("\", this.");
          String _name_25 = field.getName();
          _builder_6.append(_name_25);
          _builder_6.append(", this.");
          String _name_26 = field.getName();
          _builder_6.append(_name_26);
          _builder_6.append(" = ");
          String _name_27 = field.getName();
          _builder_6.append(_name_27);
          _builder_6.append(");");
          _builder_6.newLineIfNotEmpty();
          _builder_6.append("if(");
          String _name_28 = field.getName();
          _builder_6.append(_name_28);
          _builder_6.append(" == null || ");
          String _name_29 = field.getName();
          _builder_6.append(_name_29);
          _builder_6.append(".isEmpty()) {");
          _builder_6.newLineIfNotEmpty();
          _builder_6.append("\t");
          _builder_6.append("set");
          String _firstUpper = StringExtensions.toFirstUpper(field.getName());
          _builder_6.append(_firstUpper, "\t");
          _builder_6.append("Event(YSuggestTextFieldEvents.CLEAR);");
          _builder_6.newLineIfNotEmpty();
          _builder_6.append("} else {");
          _builder_6.newLine();
          _builder_6.append("\t");
          _builder_6.append("set");
          String _firstUpper_1 = StringExtensions.toFirstUpper(field.getName());
          _builder_6.append(_firstUpper_1, "\t");
          _builder_6.append("Event(YSuggestTextFieldEvents.OPEN_POPUP);");
          _builder_6.newLineIfNotEmpty();
          _builder_6.append("}");
          return _builder_6.toString();
        case EMBEDDABLEEVENT:
          StringConcatenation _builder_7 = new StringConcatenation();
          _builder_7.append("pcs.firePropertyChange(\"");
          String _name_30 = field.getName();
          _builder_7.append(_name_30);
          _builder_7.append("\", this.");
          String _name_31 = field.getName();
          _builder_7.append(_name_31);
          _builder_7.append(", this.");
          String _name_32 = field.getName();
          _builder_7.append(_name_32);
          _builder_7.append(" = ");
          String _name_33 = field.getName();
          _builder_7.append(_name_33);
          _builder_7.append(");");
          _builder_7.newLineIfNotEmpty();
          return _builder_7.toString();
        default:
          break;
      }
    }
    return null;
  }
  
  public Class<?> decodeType(final FSMControlFieldAttribute field) {
    FSMInternalType _attributeType = field.getAttributeType().getAttributeType();
    if (_attributeType != null) {
      switch (_attributeType) {
        case BOOLEAN:
          return Boolean.class;
        case INTEGER:
          return Integer.class;
        case LONG:
          return Long.class;
        case DOUBLE:
          return Double.class;
        case STRING:
          return String.class;
        case DATE:
          return Date.class;
        case SUGGESTTEXT:
          return String.class;
        case EMBEDDABLEEVENT:
          return YEmbeddableEvent.class;
        default:
          break;
      }
    }
    return null;
  }
  
  public void buildButtonField(final EObject control, final JvmDeclaredType controlCls, final String buttonName, final String postFix, final JvmTypeReference type) {
    JvmField field = this._jvmTypesBuilder.toField(control, (buttonName + postFix), type);
    field.setVisibility(JvmVisibility.PRIVATE);
    EList<JvmMember> _members = controlCls.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members, field);
  }
  
  public void buildButtonMethods(final EObject control, final JvmDeclaredType controlCls, final String buttonName, final String postFix, final JvmTypeReference type, final FSMControlButtonAttributeEvent event) {
    JvmOperation operation = this._jvmTypesBuilder.toGetter(control, (buttonName + postFix), type);
    operation.setVisibility(JvmVisibility.PUBLIC);
    EList<JvmMember> _members = controlCls.getMembers();
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members, operation);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("set");
    String _firstUpper = StringExtensions.toFirstUpper(buttonName);
    _builder.append(_firstUpper);
    _builder.append(postFix);
    final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(control, (buttonName + postFix), type);
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      it.setVisibility(JvmVisibility.PUBLIC);
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        boolean _isEmpty = postFix.isEmpty();
        if (_isEmpty) {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("if(");
          _builder_1.append(buttonName);
          _builder_1.append(".longValue() != 0) {");
          _builder_1.newLineIfNotEmpty();
          {
            if ((event instanceof FSMControlButtonAttributeEventKeyboard)) {
              _builder_1.newLine();
              _builder_1.append("\t");
              _builder_1.append("statemachine.processEvent(statemachine, new MessageEvent(EventType.KEYBOARD, \"");
              String _keystroke = ((FSMControlButtonAttributeEventKeyboard) event).getKeystroke();
              _builder_1.append(_keystroke, "\t");
              _builder_1.append("\"));");
              _builder_1.newLineIfNotEmpty();
            } else {
              if ((event instanceof FSMControlButtonAttributeEventIdentity)) {
                _builder_1.newLine();
                _builder_1.append("\t");
                _builder_1.append("statemachine.processEvent(statemachine, new MessageEvent(EventType.ID, \"");
                int _identity = ((FSMControlButtonAttributeEventIdentity) event).getIdentity();
                _builder_1.append(_identity, "\t");
                _builder_1.append("\"));");
                _builder_1.newLineIfNotEmpty();
              } else {
                if ((event instanceof FSMControlButtonAttributeEventEvent)) {
                  _builder_1.newLine();
                  _builder_1.append("\t");
                  _builder_1.append("statemachine.processEvent(statemachine, new MessageEvent(EventType.TRIGGER, \"");
                  String _name = ((FSMControlButtonAttributeEventEvent) event).getEvent().getName();
                  _builder_1.append(_name, "\t");
                  _builder_1.append("\"));");
                  _builder_1.newLineIfNotEmpty();
                }
              }
            }
          }
          _builder_1.append("}");
          _builder_1.newLine();
          _builder_1.append("this.");
          _builder_1.append(buttonName);
          _builder_1.append(" = ");
          _builder_1.append(buttonName);
          _builder_1.append(";");
          it_1.append(_builder_1);
        } else {
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("pcs.firePropertyChange(\"");
          _builder_2.append(buttonName);
          _builder_2.append(postFix);
          _builder_2.append("\", this.");
          _builder_2.append(buttonName);
          _builder_2.append(postFix);
          _builder_2.append(", this.");
          _builder_2.append(buttonName);
          _builder_2.append(postFix);
          _builder_2.append(" = ");
          _builder_2.append(buttonName);
          _builder_2.append(postFix);
          _builder_2.append(");");
          it_1.append(_builder_2);
        }
      };
      this._jvmTypesBuilder.setBody(it, _function_1);
    };
    operation = this._jvmTypesBuilder.toMethod(control, _builder.toString(), this._typeReferenceBuilder.typeRef(Void.TYPE), _function);
    operation.setVisibility(JvmVisibility.PUBLIC);
    EList<JvmMember> _members_1 = controlCls.getMembers();
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, operation);
  }
  
  public void toConstructor(final JvmDeclaredType type, final FSM statemachine) {
    EList<JvmMember> _members = type.getMembers();
    final Procedure1<JvmConstructor> _function = (JvmConstructor it) -> {
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("super();");
        _builder.newLine();
        _builder.append("state = States.");
        String _name = statemachine.getInitialState().getName();
        _builder.append(_name);
        _builder.append(";");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_1);
    };
    JvmConstructor _constructor = this._jvmTypesBuilder.toConstructor(statemachine, _function);
    this._jvmTypesBuilder.<JvmConstructor>operator_add(_members, _constructor);
  }
  
  public void toFields(final JvmGenericType type, final FSM statemachine) {
    JvmField field = null;
    final Procedure1<JvmField> _function = (JvmField it) -> {
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("org.slf4j.LoggerFactory.getLogger(\"statemachine\")");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setInitializer(it, _function_1);
    };
    field = this._jvmTypesBuilder.toField(statemachine, "log", this._typeReferenceBuilder.typeRef(Logger.class), _function);
    field.setStatic(true);
    field.setVisibility(JvmVisibility.PRIVATE);
    EList<JvmMember> _members = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members, field);
    field = this._jvmTypesBuilder.toField(statemachine, "state", this._typeReferenceBuilder.typeRef("States"));
    field.setVisibility(JvmVisibility.PRIVATE);
    EList<JvmMember> _members_1 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_1, field);
  }
  
  public void toOperations(final JvmGenericType type, final FSM statemachine) {
    EList<JvmMember> _members = type.getMembers();
    final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("super.start();");
        _builder.newLine();
        _builder.append("schedule(this, 100, new MessageEvent(EventType.TRIGGER, \"");
        String _name = statemachine.getInitialEvent().getName();
        _builder.append(_name);
        _builder.append("\"));");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_1);
    };
    JvmOperation _method = this._jvmTypesBuilder.toMethod(statemachine, "start", this._typeReferenceBuilder.typeRef(Void.TYPE), _function);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members, _method);
    EList<JvmMember> _members_1 = type.getMembers();
    final Procedure1<JvmGenericType> _function_1 = (JvmGenericType it) -> {
      EList<JvmMember> _members_2 = it.getMembers();
      final Procedure1<JvmOperation> _function_2 = (JvmOperation it_1) -> {
        it_1.setAbstract(true);
        EList<JvmFormalParameter> _parameters = it_1.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(statemachine, "statemachine", this._typeReferenceBuilder.typeRef(IStateMachine.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it_1.getParameters();
        JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(statemachine, "event", this._typeReferenceBuilder.typeRef(MessageEvent.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
      };
      JvmOperation _method_1 = this._jvmTypesBuilder.toMethod(statemachine, "process", this._typeReferenceBuilder.typeRef("States"), _function_2);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, _method_1);
    };
    JvmGenericType _interface = this._jvmTypesBuilder.toInterface(statemachine, "State", _function_1);
    this._jvmTypesBuilder.<JvmGenericType>operator_add(_members_1, _interface);
    EList<JvmMember> _members_2 = type.getMembers();
    final Procedure1<JvmOperation> _function_2 = (JvmOperation it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(statemachine, "statemachine", this._typeReferenceBuilder.typeRef(IStateMachine.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      EList<JvmFormalParameter> _parameters_1 = it.getParameters();
      JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(statemachine, "event", this._typeReferenceBuilder.typeRef(MessageEvent.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
      final Procedure1<ITreeAppendable> _function_3 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("state = state.process(statemachine, event);");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_3);
    };
    JvmOperation _method_1 = this._jvmTypesBuilder.toMethod(statemachine, "doProcessEvent", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_2);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, _method_1);
    EList<JvmMember> _members_3 = type.getMembers();
    final Procedure1<JvmEnumerationType> _function_3 = (JvmEnumerationType it) -> {
      EList<JvmTypeReference> _superTypes = it.getSuperTypes();
      JvmTypeReference _typeRef = this._typeReferenceBuilder.typeRef("State");
      this._jvmTypesBuilder.<JvmTypeReference>operator_add(_superTypes, _typeRef);
      final Consumer<FSMState> _function_4 = (FSMState state) -> {
        this._jvmTypesBuilder.setDocumentation(it, this._jvmTypesBuilder.getDocumentation(state));
        EList<JvmMember> _members_4 = it.getMembers();
        final Procedure1<JvmEnumerationLiteral> _function_5 = (JvmEnumerationLiteral it_1) -> {
          final Procedure1<ITreeAppendable> _function_6 = (ITreeAppendable it_2) -> {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append("@Override");
            _builder.newLine();
            _builder.append("public States process(final IStateMachine statemachine, final MessageEvent event) {");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("statemachine.dispatchMessages(event);");
            _builder.newLine();
            {
              EList<FSMKeyMapper> _keyMapper = state.getKeyMapper();
              for(final FSMKeyMapper map : _keyMapper) {
                _builder.append("\t");
                _builder.append("if(event.getType()==EventType.FUNCTIONALKEYBOARD && \"");
                String _name = map.getKeyCode().getName();
                _builder.append(_name, "\t");
                _builder.append("\".equals(event.getId())) {");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("\t");
                _builder.append("event.setType(EventType.TRIGGER);");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("\t");
                _builder.append("event.setId(\"");
                String _name_1 = map.getKeyEvent().getName();
                _builder.append(_name_1, "\t\t");
                _builder.append("\");");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("}");
                _builder.newLine();
              }
            }
            _builder.append("\t");
            _builder.append("switch (event.getType()) {");
            _builder.newLine();
            {
              FSMControlFieldAttribute _keystroke = state.getKeystroke();
              boolean _tripleNotEquals = (_keystroke != null);
              if (_tripleNotEquals) {
                _builder.append("\t\tcase KEYBOARD:");
                _builder.newLineIfNotEmpty();
                _builder.append("\t\t\t");
                _builder.append("statemachine.append(\"");
                String _name_2 = state.getKeystroke().getName();
                _builder.append(_name_2, "\t\t\t");
                _builder.append("\", event.getId());");
                {
                  boolean _isHasKeyOperation = state.isHasKeyOperation();
                  if (_isHasKeyOperation) {
                    _builder.newLineIfNotEmpty();
                    String _decodeOperation = this.decodeOperation(state.getKeyOperation(), 0);
                    _builder.append(_decodeOperation);
                    _builder.newLineIfNotEmpty();
                  }
                }
                _builder.append("\t");
                _builder.append("break;");
                _builder.newLine();
              }
            }
            {
              FSMOperation _identity = state.getIdentity();
              boolean _tripleNotEquals_1 = (_identity != null);
              if (_tripleNotEquals_1) {
                _builder.append("\t\tcase ID:");
                _builder.newLineIfNotEmpty();
                _builder.append("\t\t\t");
                String _decodeOperation_1 = this.decodeOperation(state.getIdentity(), true, 0);
                _builder.append(_decodeOperation_1, "\t\t\t");
                _builder.newLineIfNotEmpty();
                _builder.append("\t\t\t");
                _builder.append("break;");
                _builder.newLine();
              }
            }
            {
              int _size = state.getTriggers().size();
              boolean _greaterThan = (_size > 0);
              if (_greaterThan) {
                _builder.append("\t\tcase TRIGGER:");
                _builder.newLineIfNotEmpty();
                _builder.append("\t\t\t");
                CharSequence _createTriggerEvents = this.createTriggerEvents(state);
                _builder.append(_createTriggerEvents, "\t\t\t");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("\t\t");
            _builder.append("default:");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("break;");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("}");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("return this;");
            _builder.newLine();
            _builder.append("}");
            it_2.append(_builder);
          };
          this._jvmTypesBuilder.setInitializer(it_1, _function_6);
        };
        JvmEnumerationLiteral _enumerationLiteral = this._jvmTypesBuilder.toEnumerationLiteral(state, state.getName(), _function_5);
        this._jvmTypesBuilder.<JvmEnumerationLiteral>operator_add(_members_4, _enumerationLiteral);
      };
      statemachine.getStates().forEach(_function_4);
    };
    JvmEnumerationType _enumerationType = this._jvmTypesBuilder.toEnumerationType(statemachine, "States", _function_3);
    this._jvmTypesBuilder.<JvmEnumerationType>operator_add(_members_3, _enumerationType);
  }
  
  public String className(final FSM statemachine) {
    String clsName = StringExtensions.toFirstUpper(statemachine.getName().toString());
    String[] parts = clsName.split(Pattern.quote("."));
    int len = parts.length;
    if ((len > 1)) {
      return StringExtensions.toFirstUpper(parts[(len - 1)]);
    }
    return clsName;
  }
  
  public CharSequence createTriggerEvents(final FSMState state) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("switch (event.getId()) {");
    _builder.newLine();
    {
      EList<FSMTrigger> _triggers = state.getTriggers();
      for(final FSMTrigger trig : _triggers) {
        {
          EList<FSMEvent> _triggers_1 = trig.getTriggers();
          for(final FSMEvent t : _triggers_1) {
            _builder.append("\tcase \"");
            String _name = t.getName();
            _builder.append(_name);
            _builder.append("\":");
            _builder.newLineIfNotEmpty();
          }
        }
        String _doGuards = this.doGuards(trig, 2);
        _builder.append(_doGuards);
        _builder.newLineIfNotEmpty();
      }
    }
    String _identation = this.identation(1);
    _builder.append(_identation);
    _builder.append("}");
    _builder.newLineIfNotEmpty();
    String _identation_1 = this.identation(1);
    _builder.append(_identation_1);
    _builder.append("break;");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  public String doGuards(final FSMTrigger trigger, final int level) {
    StringConcatenation _builder = new StringConcatenation();
    String _recurseGuards = this.recurseGuards(trigger, trigger.getGuards().iterator(), level);
    _builder.append(_recurseGuards);
    {
      if ((((trigger.getGuards().size() == 0) && (!trigger.isHasTransition())) || (trigger.getGuards().size() > 0))) {
        _builder.newLineIfNotEmpty();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("break;");
      }
    }
    return _builder.toString();
  }
  
  public String recurseGuards(final FSMTrigger trigger, final Iterator<FSMGuard> guards, final int level) {
    String body = "";
    boolean _hasNext = guards.hasNext();
    if (_hasNext) {
      FSMGuard guard = guards.next();
      StringConcatenation _builder = new StringConcatenation();
      CharSequence _decodeGuard = this.decodeGuard(guard, level);
      _builder.append(_decodeGuard);
      _builder.newLineIfNotEmpty();
      String _recurseGuards = this.recurseGuards(trigger, guards, (level + 1));
      _builder.append(_recurseGuards);
      _builder.newLineIfNotEmpty();
      String _identation = this.identation(level);
      _builder.append(_identation);
      _builder.append("}");
      _builder.newLineIfNotEmpty();
      CharSequence _decodeGuardOnFail = this.decodeGuardOnFail(guard, level);
      _builder.append(_decodeGuardOnFail);
      _builder.newLineIfNotEmpty();
      body = _builder.toString();
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _identation_1 = this.identation(level);
      _builder_1.append(_identation_1);
      _builder_1.append("statemachine.blockQueueTaking(true);");
      _builder_1.newLineIfNotEmpty();
      CharSequence _generateActions = this.generateActions(trigger, level);
      _builder_1.append(_generateActions);
      _builder_1.newLineIfNotEmpty();
      body = _builder_1.toString();
      boolean _isHasTransition = trigger.isHasTransition();
      if (_isHasTransition) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append(body);
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("/* --- entry actions begin --- */");
        _builder_2.newLine();
        CharSequence _generateEntryActions = this.generateEntryActions(trigger, level);
        _builder_2.append(_generateEntryActions);
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("/* --- entry actions end --- */");
        _builder_2.newLine();
        String _identation_2 = this.identation(level);
        _builder_2.append(_identation_2);
        _builder_2.append("statemachine.blockQueueTaking(false);");
        _builder_2.newLineIfNotEmpty();
        {
          boolean _isHasTransition_1 = trigger.isHasTransition();
          if (_isHasTransition_1) {
            String _identation_3 = this.identation(level);
            _builder_2.append(_identation_3);
            _builder_2.append("return ");
            String _name = trigger.getTransition().getName();
            _builder_2.append(_name);
            _builder_2.append(";");
            _builder_2.newLineIfNotEmpty();
          }
        }
        body = _builder_2.toString();
      } else {
        StringConcatenation _builder_3 = new StringConcatenation();
        _builder_3.append(body);
        _builder_3.newLineIfNotEmpty();
        String _identation_4 = this.identation(level);
        _builder_3.append(_identation_4);
        _builder_3.append("statemachine.blockQueueTaking(false);");
        body = _builder_3.toString();
      }
    }
    return body;
  }
  
  public String identation(final int level) {
    StringConcatenation _builder = new StringConcatenation();
    String body = _builder.toString();
    for (int i = 0; (i < level); i++) {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(body);
      _builder_1.append("\t");
      body = _builder_1.toString();
    }
    return body;
  }
  
  public CharSequence decodeGuard(final FSMGuard guard, final int level) {
    StringConcatenation _builder = new StringConcatenation();
    String _identation = this.identation(level);
    _builder.append(_identation);
    _builder.append("if((boolean)");
    String _call = this.call(((EObject) guard), this._iQualifiedNameProvider.getFullyQualifiedName(guard.getGroup()).toString(), guard.getGuard().getName(), StatemachineEnums.Functions.GUARD, null, null, false);
    _builder.append(_call);
    _builder.append(") {");
    return _builder;
  }
  
  public CharSequence decodeGuardOnFail(final FSMGuard guard, final int level) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _isHasOnFail = guard.isHasOnFail();
      if (_isHasOnFail) {
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("else {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        String _identation_1 = this.identation(level);
        _builder.append(_identation_1, "\t");
        _builder.append("Notification.show(statemachine.getTranslation(\"");
        String _onFailCaption = guard.getOnFailCaption();
        _builder.append(_onFailCaption, "\t");
        _builder.append("\"), statemachine.getTranslation(\"");
        String _onFailDescription = guard.getOnFailDescription();
        _builder.append(_onFailDescription, "\t");
        _builder.append("\"), Type.");
        String _name = guard.getOnFailType().getName();
        _builder.append(_name, "\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        String _identation_2 = this.identation(level);
        _builder.append(_identation_2, "\t\t");
        _builder.append("}");
      }
    }
    return _builder;
  }
  
  public CharSequence generateActions(final FSMTrigger trigger, final int level) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<FSMAction> _actions = trigger.getActions();
      for(final FSMAction action : _actions) {
        String _switchAction = this.switchAction(action, level);
        _builder.append(_switchAction);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  public CharSequence generateEntryActions(final FSMTrigger trigger, final int level) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<FSMAction> _conditions = trigger.getTransition().getConditions();
      for(final FSMAction action : _conditions) {
        String _switchAction = this.switchAction(action, level);
        _builder.append(_switchAction);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  public String switchAction(final FSMAction action, final int level) {
    boolean _matched = false;
    if (action instanceof FSMActionPeripheralBlinkRate) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      String _identation = this.identation(level);
      _builder.append(_identation);
      _builder.append("statemachine.set(\"blinkRate\", \"");
      String _name = ((FSMActionPeripheralBlinkRate)action).getDevice().getName();
      _builder.append(_name);
      _builder.append("\", ");
      int _blinkRate = ((FSMActionPeripheralBlinkRate)action).getBlinkRate();
      _builder.append(_blinkRate);
      _builder.append(");");
      _builder.newLineIfNotEmpty();
      return _builder.toString();
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralClear) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"clearDisplay\", \"");
        String _name = ((FSMActionPeripheralClear)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", \"\");");
        _builder.newLineIfNotEmpty();
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralCreateWindow) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"createWindow\", \"");
        String _name = ((FSMActionPeripheralCreateWindow)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", \"");
        int _viewportRow = ((FSMActionPeripheralCreateWindow)action).getViewportRow();
        _builder.append(_viewportRow);
        _builder.append("|");
        int _viewportColumn = ((FSMActionPeripheralCreateWindow)action).getViewportColumn();
        _builder.append(_viewportColumn);
        _builder.append("|");
        int _viewportHeight = ((FSMActionPeripheralCreateWindow)action).getViewportHeight();
        _builder.append(_viewportHeight);
        _builder.append("|");
        int _viewportWidth = ((FSMActionPeripheralCreateWindow)action).getViewportWidth();
        _builder.append(_viewportWidth);
        _builder.append("|");
        int _windowHeight = ((FSMActionPeripheralCreateWindow)action).getWindowHeight();
        _builder.append(_windowHeight);
        _builder.append("|");
        int _windowWidth = ((FSMActionPeripheralCreateWindow)action).getWindowWidth();
        _builder.append(_windowWidth);
        _builder.append("\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralCursorType) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"cursorType\", \"");
        String _name = ((FSMActionPeripheralCursorType)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", LineDisplayConst.");
        String _name_1 = ((FSMActionPeripheralCursorType)action).getCursorType().getName();
        _builder.append(_name_1);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralDestroyWindow) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"destroyWindow\", \"");
        String _name = ((FSMActionPeripheralDestroyWindow)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", \"\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralDeviceBrightness) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"deviceBrightness\", \"");
        String _name = ((FSMActionPeripheralDeviceBrightness)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", ");
        int _brightness = ((FSMActionPeripheralDeviceBrightness)action).getBrightness();
        _builder.append(_brightness);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralLineDisplayText) {
        _matched=true;
        boolean _isHasType = ((FSMActionPeripheralLineDisplayText)action).isHasType();
        if (_isHasType) {
          StringConcatenation _builder = new StringConcatenation();
          String _identation = this.identation(level);
          _builder.append(_identation);
          _builder.append("statemachine.set(\"displayLine\", \"");
          String _name = ((FSMActionPeripheralLineDisplayText)action).getDevice().getName();
          _builder.append(_name);
          _builder.append("\", ");
          CharSequence _concatenation = this.concatenation(((FSMActionPeripheralLineDisplayText)action).getText(), this._typeReferenceBuilder.typeRef(String.class));
          _builder.append(_concatenation);
          _builder.append("+\"|\"+LineDisplayConst.");
          String _name_1 = ((FSMActionPeripheralLineDisplayText)action).getTextType().getName();
          _builder.append(_name_1);
          _builder.append(");");
          return _builder.toString();
        } else {
          StringConcatenation _builder_1 = new StringConcatenation();
          String _identation_1 = this.identation(level);
          _builder_1.append(_identation_1);
          _builder_1.append("statemachine.set(\"displayLine\", \"");
          String _name_2 = ((FSMActionPeripheralLineDisplayText)action).getDevice().getName();
          _builder_1.append(_name_2);
          _builder_1.append("\", ");
          CharSequence _concatenation_1 = this.concatenation(((FSMActionPeripheralLineDisplayText)action).getText(), this._typeReferenceBuilder.typeRef(String.class));
          _builder_1.append(_concatenation_1);
          _builder_1.append(");");
          return _builder_1.toString();
        }
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralLineDisplayTextAt) {
        _matched=true;
        boolean _isHasType = ((FSMActionPeripheralLineDisplayTextAt)action).isHasType();
        if (_isHasType) {
          StringConcatenation _builder = new StringConcatenation();
          String _identation = this.identation(level);
          _builder.append(_identation);
          _builder.append("statemachine.set(\"displayTextAt\", \"");
          String _name = ((FSMActionPeripheralLineDisplayTextAt)action).getDevice().getName();
          _builder.append(_name);
          _builder.append("\", \"");
          int _row = ((FSMActionPeripheralLineDisplayTextAt)action).getRow();
          _builder.append(_row);
          _builder.append("|");
          int _column = ((FSMActionPeripheralLineDisplayTextAt)action).getColumn();
          _builder.append(_column);
          _builder.append("|\"+");
          CharSequence _concatenation = this.concatenation(((FSMActionPeripheralLineDisplayTextAt)action).getText(), this._typeReferenceBuilder.typeRef(String.class));
          _builder.append(_concatenation);
          _builder.append("+\"|\"+LineDisplayConst.");
          String _name_1 = ((FSMActionPeripheralLineDisplayTextAt)action).getTextType().getName();
          _builder.append(_name_1);
          _builder.append(");");
          return _builder.toString();
        } else {
          StringConcatenation _builder_1 = new StringConcatenation();
          String _identation_1 = this.identation(level);
          _builder_1.append(_identation_1);
          _builder_1.append("statemachine.set(\"displayTextAt\", \"");
          String _name_2 = ((FSMActionPeripheralLineDisplayTextAt)action).getDevice().getName();
          _builder_1.append(_name_2);
          _builder_1.append("\", \"");
          int _row_1 = ((FSMActionPeripheralLineDisplayTextAt)action).getRow();
          _builder_1.append(_row_1);
          _builder_1.append("|");
          int _column_1 = ((FSMActionPeripheralLineDisplayTextAt)action).getColumn();
          _builder_1.append(_column_1);
          _builder_1.append("|\"+");
          CharSequence _concatenation_1 = this.concatenation(((FSMActionPeripheralLineDisplayTextAt)action).getText(), this._typeReferenceBuilder.typeRef(String.class));
          _builder_1.append(_concatenation_1);
          _builder_1.append(");");
          return _builder_1.toString();
        }
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralInterCharacterWait) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"interCharacterWait\", \"");
        String _name = ((FSMActionPeripheralInterCharacterWait)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", ");
        int _wait = ((FSMActionPeripheralInterCharacterWait)action).getWait();
        _builder.append(_wait);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralMarqueeFormat) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"marqueeFormat\", \"");
        String _name = ((FSMActionPeripheralMarqueeFormat)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", LineDisplayConst.");
        String _name_1 = ((FSMActionPeripheralMarqueeFormat)action).getFormat().getName();
        _builder.append(_name_1);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralMarqueeRepeatWait) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"marqueeRepeatWait\", \"");
        String _name = ((FSMActionPeripheralMarqueeRepeatWait)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", ");
        int _wait = ((FSMActionPeripheralMarqueeRepeatWait)action).getWait();
        _builder.append(_wait);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralMarqueeType) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"marqueeType\", \"");
        String _name = ((FSMActionPeripheralMarqueeType)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", LineDisplayConst.");
        String _name_1 = ((FSMActionPeripheralMarqueeType)action).getMarqueeType().getName();
        _builder.append(_name_1);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralMarqueeUnitWait) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"marqueeUnitWait\", \"");
        String _name = ((FSMActionPeripheralMarqueeUnitWait)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", ");
        int _wait = ((FSMActionPeripheralMarqueeUnitWait)action).getWait();
        _builder.append(_wait);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralScroll) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"scroll\", \"");
        String _name = ((FSMActionPeripheralScroll)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", \"\"+LineDisplayConst.");
        String _name_1 = ((FSMActionPeripheralScroll)action).getDirection().getName();
        _builder.append(_name_1);
        _builder.append("+\"|");
        int _units = ((FSMActionPeripheralScroll)action).getUnits();
        _builder.append(_units);
        _builder.append("\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralOpenDrawer) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"openDrawer\", \"");
        String _name = ((FSMActionPeripheralOpenDrawer)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", 0);");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPrintBarcode) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"printBarcode\", \"");
        String _name = ((FSMActionPeripheralPrintBarcode)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", \"");
        String _data = ((FSMActionPeripheralPrintBarcode)action).getData();
        _builder.append(_data);
        _builder.append("|\"+POSPrinterConst.");
        String _name_1 = ((FSMActionPeripheralPrintBarcode)action).getBarcodeType().getName();
        _builder.append(_name_1);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPrintBitmap) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"printBitmap\", \"");
        String _name = ((FSMActionPeripheralPrintBitmap)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", ");
        int _bitmapId = ((FSMActionPeripheralPrintBitmap)action).getBitmapId();
        _builder.append(_bitmapId);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralDisplayText) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"");
        String _name = ((FSMActionPeripheralDisplayText)action).getDevice().getOutput().getName();
        _builder.append(_name);
        String _firstUpper = StringExtensions.toFirstUpper(((FSMActionPeripheralDisplayText)action).getAttribute().getName());
        _builder.append(_firstUpper);
        _builder.append("\", ");
        CharSequence _concatenation = this.concatenation(((FSMActionPeripheralDisplayText)action).getText(), this._typeReferenceBuilder.typeRef(String.class));
        _builder.append(_concatenation);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPrintCut) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"printCut\", \"");
        String _name = ((FSMActionPeripheralPrintCut)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\",");
        CharSequence _concatenation = this.concatenation(((FSMActionPeripheralPrintCut)action).getText(), this._typeReferenceBuilder.typeRef(String.class));
        _builder.append(_concatenation);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPrintNormal) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"printNormal\", \"");
        String _name = ((FSMActionPeripheralPrintNormal)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", ");
        CharSequence _concatenation = this.concatenation(((FSMActionPeripheralPrintNormal)action).getText(), this._typeReferenceBuilder.typeRef(String.class));
        _builder.append(_concatenation);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPrintRegisterBitmap) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"printerBitmap\", \"");
        int _bitmapId = ((FSMActionPeripheralPrintRegisterBitmap)action).getBitmapId();
        _builder.append(_bitmapId);
        _builder.append("\", \"");
        String _name = ((FSMActionPeripheralPrintRegisterBitmap)action).getName();
        _builder.append(_name);
        _builder.append("\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralScaleZero) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"zeroScale\", \"");
        String _name = ((FSMActionPeripheralScaleZero)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", \"\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralScaleTareWeight) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"tareWeight\", \"");
        String _name = ((FSMActionPeripheralScaleTareWeight)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", ");
        CharSequence _concatenation = this.concatenation(((FSMActionPeripheralScaleTareWeight)action).getValue(), this._typeReferenceBuilder.typeRef(String.class));
        _builder.append(_concatenation);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralScaleDisplayText) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"scaleDisplayText\", \"");
        String _name = ((FSMActionPeripheralScaleDisplayText)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\", ");
        CharSequence _concatenation = this.concatenation(((FSMActionPeripheralScaleDisplayText)action).getText(), this._typeReferenceBuilder.typeRef(String.class));
        _builder.append(_concatenation);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPTOpen) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"paymentTerminalOpen\", ");
        CharSequence _concatenation = this.concatenation(((FSMActionPeripheralPTOpen)action).getHost(), this._typeReferenceBuilder.typeRef(String.class));
        _builder.append(_concatenation);
        _builder.append("+\"|\"+");
        CharSequence _concatenation_1 = this.concatenation(((FSMActionPeripheralPTOpen)action).getPort(), this._typeReferenceBuilder.typeRef(int.class));
        _builder.append(_concatenation_1);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPTClose) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"paymentTerminalClose\", \"\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPTAcknowledge) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"paymentTerminalAcknowledge\", \"\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPTReversal) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"paymentTerminalReversal\", ");
        CharSequence _concatenation = this.concatenation(((FSMActionPeripheralPTReversal)action).getPassword(), this._typeReferenceBuilder.typeRef(String.class));
        _builder.append(_concatenation);
        _builder.append("+\"|\"+");
        CharSequence _concatenation_1 = this.concatenation(((FSMActionPeripheralPTReversal)action).getReceipt(), this._typeReferenceBuilder.typeRef(String.class));
        _builder.append(_concatenation_1);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPTRegistration) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"paymentTerminalRegistration\", ");
        CharSequence _concatenation = this.concatenation(((FSMActionPeripheralPTRegistration)action).getPassword(), this._typeReferenceBuilder.typeRef(String.class));
        _builder.append(_concatenation);
        _builder.append("+\"|");
        String _configuration = ((FSMActionPeripheralPTRegistration)action).getConfiguration();
        _builder.append(_configuration);
        _builder.append("\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPTAuthorization) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"paymentTerminalAuthorization\", ");
        CharSequence _concatenation = this.concatenation(((FSMActionPeripheralPTAuthorization)action).getAmount(), this._typeReferenceBuilder.typeRef(Double.class));
        _builder.append(_concatenation);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralSignatureOpen) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"signatureOpen\", \"\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralSignatureClose) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"signatureClose\", \"\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralSignatureClear) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"signatureClear\", \"\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralSignatureCapture) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"signatureCapture\", \"\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralSignatureIdle) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"signatureIdle\", \"\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralSignatureLabel) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"signatureLabel\", statemachine.getTranslation(\"");
        String _okLabel = ((FSMActionPeripheralSignatureLabel)action).getOkLabel();
        _builder.append(_okLabel);
        _builder.append("\")+\"|\"+statemachine.getTranslation(\"");
        String _clearLabel = ((FSMActionPeripheralSignatureLabel)action).getClearLabel();
        _builder.append(_clearLabel);
        _builder.append("\")+\"|\"+statemachine.getTranslation(\"");
        String _cancelLabel = ((FSMActionPeripheralSignatureLabel)action).getCancelLabel();
        _builder.append(_cancelLabel);
        _builder.append("\"));");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPrintReport) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"printReport\", \"");
        QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(((FSMActionPeripheralPrintReport)action).getReport());
        _builder.append(_fullyQualifiedName);
        {
          boolean _isHasFilter = ((FSMActionPeripheralPrintReport)action).isHasFilter();
          if (_isHasFilter) {
            _builder.append("|");
            String _key = ((FSMActionPeripheralPrintReport)action).getKey().getKey();
            _builder.append(_key);
          }
        }
        _builder.append("\", ");
        CharSequence _concatenation = this.concatenation(((FSMActionPeripheralPrintReport)action).getPrintService(), this._typeReferenceBuilder.typeRef(String.class));
        _builder.append(_concatenation);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralBeeper) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"beeper\", \"");
        String _string = Integer.valueOf(((FSMActionPeripheralBeeper)action).getDuration()).toString();
        _builder.append(_string);
        _builder.append("|");
        int _frequency = ((FSMActionPeripheralBeeper)action).getFrequency();
        _builder.append(_frequency);
        _builder.append("\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPlayer) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"player\", \"");
        String _tune = ((FSMActionPeripheralPlayer)action).getTune();
        _builder.append(_tune);
        _builder.append("\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralSound) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"sound\", \"");
        String _name = ((FSMActionPeripheralSound)action).getName();
        _builder.append(_name);
        _builder.append("\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldSet) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"");
        String _name = ((FSMActionFieldSet)action).getAttribute().getName();
        _builder.append(_name);
        _builder.append("\", ");
        CharSequence _concatenation = this.concatenation(((FSMActionFieldSet)action).getSource(), this._typeReferenceBuilder.typeRef(((FSMActionFieldSet)action).getAttribute().getAttributeType().getAttributeType().getLiteral()));
        _builder.append(_concatenation);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldRemove) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.remove(\"");
        String _name = ((FSMActionFieldRemove)action).getAttribute().getName();
        _builder.append(_name);
        _builder.append("\", -1);");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldClear) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.clear(\"");
        String _name = ((FSMActionFieldClear)action).getAttribute().getName();
        _builder.append(_name);
        _builder.append("\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldKeystroke) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.append(\"");
        String _name = ((FSMActionFieldKeystroke)action).getAttribute().getName();
        _builder.append(_name);
        _builder.append("\", event.getId());");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionItemVisible) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.enable(\"");
        String _name = ((FSMActionItemVisible)action).getAttribute().getName();
        _builder.append(_name);
        _builder.append("\", true);");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionItemInvisible) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.enable(\"");
        String _name = ((FSMActionItemInvisible)action).getAttribute().getName();
        _builder.append(_name);
        _builder.append("\", false);");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionButtonCaption) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.caption(\"");
        String _name = ((FSMActionButtonCaption)action).getAttribute().getName();
        _builder.append(_name);
        _builder.append("\", \"");
        FSMActionFieldConcatenation _caption = ((FSMActionButtonCaption)action).getCaption();
        _builder.append(_caption);
        _builder.append("\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionButtonImage) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.image(\"");
        String _name = ((FSMActionButtonImage)action).getAttribute().getName();
        _builder.append(_name);
        _builder.append("\", \"");
        String _image = ((FSMActionButtonImage)action).getImage();
        _builder.append(_image);
        _builder.append("\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionDTOFind) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.find(\"");
        String _name = ((FSMActionDTOFind)action).getDto().getAttributeType().getAttributeType().getName();
        _builder.append(_name);
        _builder.append("\", \"");
        String _name_1 = ((FSMActionDTOFind)action).getAttribute().getInheritedFeature().getName();
        _builder.append(_name_1);
        _builder.append("\", ");
        CharSequence _concatenation = this.concatenation(((FSMActionDTOFind)action).getSearch());
        _builder.append(_concatenation);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionDTOClear) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"");
        String _name = ((FSMActionDTOClear)action).getDto().getName();
        _builder.append(_name);
        _builder.append("\", null);");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldFilterToggle) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.toggle(\"");
        String _name = ((FSMActionFieldFilterToggle)action).getFilter().getName();
        _builder.append(_name);
        _builder.append("\");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMStorage) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.putStorage(\"");
        String _key = ((FSMStorage)action).getKey();
        _builder.append(_key);
        _builder.append("\",\"");
        String _attribute = ((FSMStorage)action).getAttribute();
        _builder.append(_attribute);
        _builder.append("\", ");
        CharSequence _concatenation = this.concatenation(((FSMStorage)action).getContent());
        _builder.append(_concatenation);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionScheduler) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _identation = this.identation(level);
        _builder.append(_identation);
        _builder.append("statemachine.set(\"");
        String _name = ((FSMActionScheduler)action).getScheduler().getName();
        _builder.append(_name);
        _builder.append("Scheduler\", ");
        int _delay = ((FSMActionScheduler)action).getScheduler().getDelay();
        _builder.append(_delay);
        _builder.append(");");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMOperation) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _decodeOperation = this.decodeOperation(((FSMOperation)action), level);
        _builder.append(_decodeOperation);
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionConditionalTransition) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        CharSequence _decodeGuard = this.decodeGuard(((FSMActionConditionalTransition)action).getGuard(), level);
        _builder.append(_decodeGuard);
        _builder.newLineIfNotEmpty();
        {
          EList<FSMAction> _actions = ((FSMActionConditionalTransition)action).getActions();
          for(final FSMAction act : _actions) {
            String _switchAction = this.switchAction(act, (level + 1));
            _builder.append(_switchAction);
            _builder.newLineIfNotEmpty();
          }
        }
        {
          int _length = ((Object[])Conversions.unwrapArray(((FSMActionConditionalTransition)action).getTransition().getConditions(), Object.class)).length;
          boolean _greaterThan = (_length > 0);
          if (_greaterThan) {
            _builder.append("/* --- entry actions begin --- */");
            _builder.newLine();
            {
              EList<FSMAction> _conditions = ((FSMActionConditionalTransition)action).getTransition().getConditions();
              for(final FSMAction condition : _conditions) {
                String _switchAction_1 = this.switchAction(condition, level);
                _builder.append(_switchAction_1);
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("/* --- entry actions end --- */");
            _builder.newLine();
          }
        }
        String _identation = this.identation((level + 1));
        _builder.append(_identation);
        _builder.append("statemachine.blockQueueTaking(false);");
        _builder.newLineIfNotEmpty();
        String _identation_1 = this.identation((level + 1));
        _builder.append(_identation_1);
        _builder.append("return ");
        String _name = ((FSMActionConditionalTransition)action).getTransition().getName();
        _builder.append(_name);
        _builder.append(";");
        _builder.newLineIfNotEmpty();
        String _identation_2 = this.identation(level);
        _builder.append(_identation_2);
        _builder.append("}");
        return _builder.toString();
      }
    }
    return null;
  }
  
  public CharSequence concatenation(final FSMActionFieldConcatenation concatenation) {
    return this.concatenation(concatenation, null);
  }
  
  public CharSequence concatenation(final FSMActionFieldConcatenation concatenation, final JvmTypeReference cast) {
    StringConcatenation _builder = new StringConcatenation();
    String _decodeActionFieldSource = this.decodeActionFieldSource(concatenation.getFirst(), cast);
    _builder.append(_decodeActionFieldSource);
    {
      EList<FSMActionFieldSource> _more = concatenation.getMore();
      for(final FSMActionFieldSource element : _more) {
        _builder.append("+");
        String _decodeActionFieldSource_1 = this.decodeActionFieldSource(element, cast);
        _builder.append(_decodeActionFieldSource_1);
      }
    }
    return _builder;
  }
  
  public String decodeActionFieldSource(final FSMActionFieldSource action, final JvmTypeReference cast) {
    boolean _matched = false;
    if (action instanceof FSMActionFieldSourceString) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("\"");
      String _text = ((FSMActionFieldSourceString)action).getText();
      _builder.append(_text);
      _builder.append("\"");
      return _builder.toString();
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldSourceNumber) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        Double _value = ((FSMActionFieldSourceNumber)action).getValue();
        _builder.append(_value);
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldSourceInteger) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        int _value = ((FSMActionFieldSourceInteger)action).getValue();
        _builder.append(_value);
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldSourceBoolean) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        boolean _isValue = ((FSMActionFieldSourceBoolean)action).isValue();
        _builder.append(_isValue);
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldSourceEvaluate) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("statemachine.");
        String _literal = ((FSMActionFieldSourceEvaluate)action).getEvaluationtype().getLiteral();
        _builder.append(_literal);
        _builder.append("()");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldSourceEvent) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("event.getId()");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldSourceTranslate) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("statemachine.getTranslation(\"");
        String _text = ((FSMActionFieldSourceTranslate)action).getText();
        _builder.append(_text);
        _builder.append("\")");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldGet) {
        _matched=true;
        String attrType = ((FSMActionFieldGet)action).getAttribute().getAttributeType().getAttributeType().getLiteral();
        boolean _equals = attrType.equals("SuggestText");
        if (_equals) {
          attrType = "String";
        }
        String castPre = "";
        String castPost = "";
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("statemachine.get(\"");
        String _name = ((FSMActionFieldGet)action).getAttribute().getName();
        _builder.append(_name);
        _builder.append("\")");
        String getter = _builder.toString();
        if (((cast != null) && (!cast.getSimpleName().equals(attrType)))) {
          if ((attrType.equals("String") || attrType.equals("SuggestText"))) {
            StringConcatenation _builder_1 = new StringConcatenation();
            String _simpleName = cast.getSimpleName();
            _builder_1.append(_simpleName);
            _builder_1.append(".parse");
            String _simpleName_1 = cast.getSimpleName();
            _builder_1.append(_simpleName_1);
            _builder_1.append("((String)");
            castPre = _builder_1.toString();
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append(".toString())");
            castPost = _builder_2.toString();
          } else {
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append("(");
            _builder_3.append(attrType);
            _builder_3.append(")");
            castPre = _builder_3.toString();
          }
        }
        return ((castPre + getter) + castPost);
      }
    }
    if (!_matched) {
      if (action instanceof FSMOperation) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        {
          if ((cast != null)) {
            _builder.append("(");
            String _simpleName = cast.getSimpleName();
            _builder.append(_simpleName);
            _builder.append(")");
          }
        }
        String _decodeOperation = this.decodeOperation(((FSMOperation)action), 0);
        _builder.append(_decodeOperation);
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMFunction) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        {
          if ((cast != null)) {
            _builder.append("(");
            String _simpleName = cast.getSimpleName();
            _builder.append(_simpleName);
            _builder.append(")");
          }
        }
        String _decodeFunction = this.decodeFunction(((FSMFunction)action), 0);
        _builder.append(_decodeFunction);
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMStorageRetrieve) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        {
          if ((cast != null)) {
            _builder.append("(");
            String _simpleName = cast.getSimpleName();
            _builder.append(_simpleName);
            _builder.append(")");
          }
        }
        _builder.append("statemachine.getStorage(\"");
        String _key = ((FSMStorageRetrieve)action).getKey();
        _builder.append(_key);
        _builder.append("\", \"");
        String _attribute = ((FSMStorageRetrieve)action).getAttribute();
        _builder.append(_attribute);
        _builder.append("\")");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionFieldSourceDtoAttribute) {
        _matched=true;
        String typeName = ((FSMActionFieldSourceDtoAttribute)action).getAttribute().getInheritedFeature().getType().getName();
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("statemachine.get(\"");
        String _name = ((FSMActionFieldSourceDtoAttribute)action).getDto().getName();
        _builder.append(_name);
        String _firstUpper = StringExtensions.toFirstUpper(((FSMActionFieldSourceDtoAttribute)action).getAttribute().getInheritedFeature().getName());
        _builder.append(_firstUpper);
        _builder.append("\")");
        {
          if ((((cast != null) && (!typeName.equals("String"))) && cast.getSimpleName().equals("String"))) {
            _builder.append(".toString()");
          }
        }
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralPTResponse) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("statemachine.get(\"PaymentTerminalResponse\")");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralScaleReadWeight) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("statemachine.get(\"ReadWeight\", \"");
        String _name = ((FSMActionPeripheralScaleReadWeight)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\")");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralScaleReadTareWeight) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("statemachine.get(\"ReadTareWeight\", \"");
        String _name = ((FSMActionPeripheralScaleReadTareWeight)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\")");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (action instanceof FSMActionPeripheralScaleWeightUnit) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("statemachine.get(\"WeightUnit\", \"");
        String _name = ((FSMActionPeripheralScaleWeightUnit)action).getDevice().getName();
        _builder.append(_name);
        _builder.append("\")");
        return _builder.toString();
      }
    }
    return null;
  }
  
  public String decodeOperation(final FSMOperation operation, final int level) {
    return this.decodeOperation(operation, false, level);
  }
  
  public String decodeOperation(final FSMOperation operation, final boolean isIdentity, final int level) {
    StringConcatenation _builder = new StringConcatenation();
    String _identation = this.identation(level);
    _builder.append(_identation);
    _builder.append("if(!((boolean)");
    String _call = this.call(((EObject) operation), this._iQualifiedNameProvider.getFullyQualifiedName(operation.getGroup()).toString(), operation.getOperation().getName(), StatemachineEnums.Functions.OPERATION, operation.getFirst(), ((FSMOperationParameter[])Conversions.unwrapArray(operation.getMore(), FSMOperationParameter.class)), isIdentity);
    _builder.append(_call);
    _builder.append(")) {");
    _builder.newLineIfNotEmpty();
    String _identation_1 = this.identation(level);
    _builder.append(_identation_1);
    _builder.append("\tNotification.show(\"Operation failed\", \"");
    String _string = this._iQualifiedNameProvider.getFullyQualifiedName(operation.getGroup()).toString();
    _builder.append(_string);
    _builder.append(".");
    String _name = operation.getOperation().getName();
    _builder.append(_name);
    _builder.append("\", Type.ERROR_MESSAGE);");
    _builder.newLineIfNotEmpty();
    String _identation_2 = this.identation(level);
    _builder.append(_identation_2);
    _builder.append("\tstatemachine.blockQueueTaking(false);");
    _builder.newLineIfNotEmpty();
    String _identation_3 = this.identation(level);
    _builder.append(_identation_3);
    _builder.append("\treturn this;\t");
    _builder.newLineIfNotEmpty();
    String _identation_4 = this.identation(level);
    _builder.append(_identation_4);
    _builder.append("}");
    return _builder.toString();
  }
  
  public String decodeFunction(final FSMFunction function, final int level) {
    StringConcatenation _builder = new StringConcatenation();
    String _identation = this.identation(level);
    _builder.append(_identation);
    String _call = this.call(((EObject) function), this._iQualifiedNameProvider.getFullyQualifiedName(function.getGroup()).toString(), function.getFunction().getName(), StatemachineEnums.Functions.FUNCTION, function.getFirst(), ((FSMOperationParameter[])Conversions.unwrapArray(function.getMore(), FSMOperationParameter.class)), false);
    _builder.append(_call);
    return _builder.toString();
  }
  
  public String call(final EObject parent, final String className, final String name, final StatemachineEnums.Functions function, final FSMOperationParameter first, final FSMOperationParameter[] more, final boolean isIdentity) {
    FSM statemachine = null;
    EObject eObj = parent;
    while ((!(eObj instanceof FSMPackage))) {
      {
        if ((eObj instanceof FSM)) {
          statemachine = ((FSM) eObj);
        }
        eObj = eObj.eContainer();
      }
    }
    if ((eObj instanceof FSMPackage)) {
      if (function != null) {
        switch (function) {
          case GUARD:
            StringConcatenation _builder = new StringConcatenation();
            String _name = ((FSMPackage)eObj).getName();
            String _plus = (_name + ".");
            String _firstUpper = StringExtensions.toFirstUpper(this._iQualifiedNameProvider.getFullyQualifiedName(eObj).getLastSegment());
            String _plus_1 = (_plus + _firstUpper);
            _builder.append(_plus_1);
            _builder.append(this.binderPostfix);
            _builder.append(".getFunctionLibraryService().guard(statemachine, \"");
            _builder.append(className);
            _builder.append("\", \"");
            _builder.append(name);
            _builder.append("\")");
            return _builder.toString();
          case OPERATION:
            StringConcatenation _builder_1 = new StringConcatenation();
            String _name_1 = ((FSMPackage)eObj).getName();
            String _plus_2 = (_name_1 + ".");
            String _firstUpper_1 = StringExtensions.toFirstUpper(this._iQualifiedNameProvider.getFullyQualifiedName(eObj).getLastSegment());
            String _plus_3 = (_plus_2 + _firstUpper_1);
            _builder_1.append(_plus_3);
            _builder_1.append(this.binderPostfix);
            _builder_1.append(".getFunctionLibraryService().operation(statemachine, \"");
            _builder_1.append(className);
            _builder_1.append("\", ");
            String _processParameters = this.processParameters(name, first, more, isIdentity);
            _builder_1.append(_processParameters);
            _builder_1.append(")");
            return _builder_1.toString();
          case FUNCTION:
            StringConcatenation _builder_2 = new StringConcatenation();
            String _name_2 = ((FSMPackage)eObj).getName();
            String _plus_4 = (_name_2 + ".");
            String _firstUpper_2 = StringExtensions.toFirstUpper(this._iQualifiedNameProvider.getFullyQualifiedName(eObj).getLastSegment());
            String _plus_5 = (_plus_4 + _firstUpper_2);
            _builder_2.append(_plus_5);
            _builder_2.append(this.binderPostfix);
            _builder_2.append(".getFunctionLibraryService().function(statemachine, \"");
            _builder_2.append(className);
            _builder_2.append("\", ");
            String _processParameters_1 = this.processParameters(name, first, more, isIdentity);
            _builder_2.append(_processParameters_1);
            _builder_2.append(")");
            return _builder_2.toString();
          default:
            break;
        }
      }
    } else {
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append("/* error: parents not found */");
      return _builder_3.toString();
    }
    return null;
  }
  
  public String processParameters(final String name, final FSMOperationParameter first, final FSMOperationParameter[] more, final boolean isIdentity) {
    if ((first == null)) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("\"");
      _builder.append(name);
      _builder.append("\"");
      {
        if (isIdentity) {
          _builder.append(", event.getId()");
        }
      }
      return _builder.toString();
    }
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("\"");
    _builder_1.append(name);
    _builder_1.append("\", ");
    {
      if (isIdentity) {
        _builder_1.append("event.getId(), ");
      }
    }
    CharSequence _concatenation = this.concatenation(first.getSource());
    _builder_1.append(_concatenation);
    String body = _builder_1.toString();
    if ((more != null)) {
      for (final FSMOperationParameter para : more) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append(body);
        _builder_2.append(", ");
        CharSequence _concatenation_1 = this.concatenation(para.getSource());
        _builder_2.append(_concatenation_1);
        body = _builder_2.toString();
      }
    }
    return body;
  }
  
  public void toBinderFields(final JvmDeclaredType type, final FSMPackage datamart) {
    JvmField field = null;
    final Procedure1<JvmField> _function = (JvmField it) -> {
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("org.slf4j.LoggerFactory.getLogger(\"servicebinder\")");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setInitializer(it, _function_1);
    };
    field = this._jvmTypesBuilder.toField(datamart, "log", this._typeReferenceBuilder.typeRef(Logger.class), _function);
    field.setStatic(true);
    field.setVisibility(JvmVisibility.PRIVATE);
    EList<JvmMember> _members = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members, field);
    field = this._jvmTypesBuilder.toField(datamart, "functionLibraryService", this._typeReferenceBuilder.typeRef(IFunctionLibraryService.class));
    field.setStatic(true);
    field.setVisibility(JvmVisibility.PRIVATE);
    EList<JvmMember> _members_1 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_1, field);
  }
  
  public void toBinderOperations(final JvmDeclaredType type, final FSMPackage datamart, final String binderClassName) {
    EList<JvmMember> _members = type.getMembers();
    final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
      it.setVisibility(JvmVisibility.PUBLIC);
      it.setStatic(true);
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("return functionLibraryService;");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_1);
    };
    JvmOperation _method = this._jvmTypesBuilder.toMethod(datamart, "getFunctionLibraryService", this._typeReferenceBuilder.typeRef(IFunctionLibraryService.class), _function);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members, _method);
    EList<JvmMember> _members_1 = type.getMembers();
    final Procedure1<JvmOperation> _function_1 = (JvmOperation it) -> {
      JvmAnnotationReference annotationRef = this._annotationTypesBuilder.annotationRef(Reference.class);
      this._annotationExtension.addAnnAttr(annotationRef, datamart, "cardinality", ReferenceCardinality.MANDATORY);
      this._annotationExtension.addAnnAttr(annotationRef, datamart, "policy", ReferencePolicy.STATIC);
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, annotationRef);
      it.setVisibility(JvmVisibility.PUBLIC);
      it.setSynchronized(true);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(datamart, "functionLibraryService", 
        this._typeReferenceBuilder.typeRef(IFunctionLibraryService.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      final Procedure1<ITreeAppendable> _function_2 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append(binderClassName);
        _builder.append(".functionLibraryService = functionLibraryService;");
        _builder.newLineIfNotEmpty();
        _builder.append("log.debug(\"FSM FunctionLibraryServiceService bound\");");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_2);
    };
    JvmOperation _method_1 = this._jvmTypesBuilder.toMethod(datamart, "bindFunctionLibraryService", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_1);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, _method_1);
    EList<JvmMember> _members_2 = type.getMembers();
    final Procedure1<JvmOperation> _function_2 = (JvmOperation it) -> {
      it.setVisibility(JvmVisibility.PUBLIC);
      it.setSynchronized(true);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(datamart, "functionLibraryService", 
        this._typeReferenceBuilder.typeRef(IFunctionLibraryService.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      final Procedure1<ITreeAppendable> _function_3 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append(binderClassName);
        _builder.append(".functionLibraryService = null;");
        _builder.newLineIfNotEmpty();
        _builder.append("log.debug(\"FSM FunctionLibraryServiceService unbound\");");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_3);
    };
    JvmOperation _method_2 = this._jvmTypesBuilder.toMethod(datamart, "unbindFunctionLibraryService", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_2);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, _method_2);
  }
  
  public void infer(final EObject statemachinePkg, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    if (statemachinePkg instanceof FSMPackage) {
      _infer((FSMPackage)statemachinePkg, acceptor, isPreIndexingPhase);
      return;
    } else if (statemachinePkg != null) {
      _infer(statemachinePkg, acceptor, isPreIndexingPhase);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(statemachinePkg, acceptor, isPreIndexingPhase).toString());
    }
  }
}
