/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.statemachine.jvmmodel;

import com.google.common.base.Objects;
import com.vaadin.data.util.filter.And;
import com.vaadin.data.util.filter.Between;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.IsNull;
import com.vaadin.data.util.filter.Like;
import com.vaadin.data.util.filter.Not;
import com.vaadin.data.util.filter.Or;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.inject.Inject;
import javax.swing.Timer;
import jpos.BaseControl;
import jpos.CashDrawer;
import jpos.JposException;
import jpos.LineDisplay;
import jpos.LineDisplayConst;
import jpos.POSPrinter;
import jpos.POSPrinterConst;
import jpos.config.JposEntry;
import jpos.config.simple.SimpleEntry;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.osbp.abstractstatemachine.AbstractStateMachine;
import org.eclipse.osbp.abstractstatemachine.POSServiceBinder;
import org.eclipse.osbp.abstractstatemachine.TaskEventSource;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSuggestTextFieldEvents;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.eclipse.osbp.runtime.common.i18n.ITranslator;
import org.eclipse.osbp.ui.api.message.MessageEvent;
import org.eclipse.osbp.ui.api.statemachine.IStateMachine;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.xtext.basic.generator.BasicDslGeneratorUtils;
import org.eclipse.osbp.xtext.i18n.I18NModelGenerator;
import org.eclipse.osbp.xtext.statemachine.FSMPackage;
import org.eclipse.xtext.common.types.JvmEnumerationLiteral;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.XExpression;
import org.eclipse.xtext.xbase.compiler.GeneratorConfig;
import org.eclipse.xtext.xbase.compiler.ImportManager;
import org.eclipse.xtext.xbase.compiler.XbaseCompiler;
import org.eclipse.xtext.xbase.compiler.output.ITreeAppendable;
import org.eclipse.xtext.xbase.compiler.output.TreeAppendable;
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor;
import org.eclipse.xtext.xbase.jvmmodel.JvmModelAssociator;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.joda.time.DateTime;
import org.osgi.service.useradmin.User;

@SuppressWarnings("all")
public class StatemachineDSLModelGenerator extends I18NModelGenerator {
  @Inject
  @Extension
  private BasicDslGeneratorUtils _basicDslGeneratorUtils;
  
  @Inject
  @Extension
  private JvmModelAssociator _jvmModelAssociator;
  
  @Inject
  private XbaseCompiler compiler;
  
  public static String pckgName = null;
  
  public void generatePckgName(final FSMPackage pckg, final IJvmDeclaredTypeAcceptor acceptor) {
    StatemachineDSLModelGenerator.pckgName = pckg.getName();
  }
  
  @Override
  public void doGenerate(final Resource input, final IFileSystemAccess fsa) {
    super.doGenerate(input, fsa);
  }
  
  @Override
  public TreeAppendable createAppendable(final EObject context, final ImportManager importManager, final GeneratorConfig config) {
    TreeAppendable _xblockexpression = null;
    {
      this.setBuilder(context.eResource());
      this._basicDslGeneratorUtils.addImportFor(this, importManager, this._typeReferenceBuilder, User.class, ResourceBundle.class, Locale.class, List.class, ITranslator.class, AbstractStateMachine.class, IStateMachine.class, MessageEvent.class, MessageEvent.EventType.class, TaskEventSource.class, LineDisplayConst.class, POSPrinterConst.class, IThemeResourceService.ThemeResourceType.class, YSuggestTextFieldEvents.class, DateTime.class, Notification.class, Notification.Type.class, IDTOService.class, LineDisplay.class, POSPrinter.class, CashDrawer.class, SimpleEntry.class, JposException.class, JposEntry.Prop.class, Constructor.class, BaseControl.class, POSServiceBinder.class, InvocationTargetException.class, Timer.class, Compare.Greater.class, Compare.GreaterOrEqual.class, Compare.Less.class, Compare.LessOrEqual.class, Compare.Equal.class, And.class, Or.class, Between.class, IsNull.class, Like.class, Not.class, SimpleStringFilter.class, HashMap.class, Map.class, IEclipseContext.class, Panel.class, MPart.class, HttpPut.class, HttpGet.class, HttpPost.class, HttpClientBuilder.class, CloseableHttpClient.class, CloseableHttpResponse.class, EntityUtils.class, ParseException.class, IOException.class);
      _xblockexpression = super.createAppendable(context, importManager, config);
    }
    return _xblockexpression;
  }
  
  public void generateInitialization(final JvmEnumerationLiteral it, final ITreeAppendable appendable, final GeneratorConfig config) {
    Procedure1<? super ITreeAppendable> _compilationStrategy = this._jvmTypeExtensions.getCompilationStrategy(it);
    boolean _notEquals = (!Objects.equal(_compilationStrategy, null));
    if (_notEquals) {
      appendable.append(" { ");
      appendable.increaseIndentation();
      appendable.newLine();
      this._jvmTypeExtensions.getCompilationStrategy(it).apply(appendable);
      appendable.decreaseIndentation();
      appendable.newLine();
      appendable.append("}");
    } else {
      final XExpression expression = this._jvmModelAssociator.getAssociatedExpression(it);
      if (((!Objects.equal(expression, null)) && config.isGenerateExpressions())) {
        boolean _hasErrors = this._errorSafeExtensions.hasErrors(expression);
        if (_hasErrors) {
          appendable.append(" /* Skipped initializer because of errors */");
        } else {
          appendable.append(" = ");
          this.compiler.compileAsJavaExpression(expression, appendable, it.getType());
        }
      }
    }
  }
  
  @Override
  public void generateEnumLiteral(final JvmEnumerationLiteral it, final ITreeAppendable appendable, final GeneratorConfig config) {
    appendable.newLine();
    this.generateJavaDoc(it, appendable, config);
    this.generateAnnotations(it.getAnnotations(), appendable, true, config);
    appendable.append(it.getSimpleName());
    this.generateInitialization(it, appendable, config);
  }
}
