/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.statemachine.scoping;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference;
import org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryFunction;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryGuard;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryOperation;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryParameter;
import org.eclipse.osbp.xtext.statemachine.FSM;
import org.eclipse.osbp.xtext.statemachine.FSMAction;
import org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText;
import org.eclipse.osbp.xtext.statemachine.FSMControl;
import org.eclipse.osbp.xtext.statemachine.FSMControlButton;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlDTO;
import org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlField;
import org.eclipse.osbp.xtext.statemachine.FSMControlFieldAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlFieldLayout;
import org.eclipse.osbp.xtext.statemachine.FSMControlFilter;
import org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral;
import org.eclipse.osbp.xtext.statemachine.FSMControlScheduler;
import org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMDTOType;
import org.eclipse.osbp.xtext.statemachine.FSMDotExpression;
import org.eclipse.osbp.xtext.statemachine.FSMDtoRef;
import org.eclipse.osbp.xtext.statemachine.FSMFunction;
import org.eclipse.osbp.xtext.statemachine.FSMGuard;
import org.eclipse.osbp.xtext.statemachine.FSMInternalType;
import org.eclipse.osbp.xtext.statemachine.FSMOperation;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceCashDrawer;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceLineDisplay;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePOSPrinter;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePT;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceScale;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceSignature;
import org.eclipse.osbp.xtext.statemachine.FSMRef;
import org.eclipse.osbp.xtext.statemachine.FSMState;
import org.eclipse.osbp.xtext.statemachine.FSMStorage;
import org.eclipse.osbp.xtext.statemachine.FSMTrigger;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;
import org.eclipse.osbp.xtext.statemachine.StatemachineEnums;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.MapBasedScope;
import org.eclipse.xtext.xbase.annotations.typesystem.XbaseWithAnnotationsBatchScopeProvider;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class StatemachineDSLScopeProvider extends XbaseWithAnnotationsBatchScopeProvider {
  @Override
  public IScope getScope(final EObject context, final EReference reference) {
    IScope _switchResult = null;
    boolean _matched = false;
    if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_CONDITIONAL_TRANSITION__GUARD)) {
      _matched=true;
      return this.getFunctionService(context, StatemachineEnums.Functions.GUARD);
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_OPERATION__OPERATION)) {
        _matched=true;
        return this.getFunctionService(context, StatemachineEnums.Functions.OPERATION);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_GUARD__GUARD)) {
        _matched=true;
        return this.getFunctionService(context, StatemachineEnums.Functions.GUARD);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_FUNCTION__FUNCTION)) {
        _matched=true;
        return this.getFunctionService(context, StatemachineEnums.Functions.FUNCTION);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_STATE__IDENTITY)) {
        _matched=true;
        return this.getFunctionService(context, StatemachineEnums.Functions.OPERATION);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_STATE__KEYSTROKE)) {
        _matched=true;
        return this.getStringFields(context);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_STATE__KEY_OPERATION)) {
        _matched=true;
        return this.getFunctionService(context, StatemachineEnums.Functions.OPERATION);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_BLINK_RATE__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_CLEAR__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_CURSOR_TYPE__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_DESTROY_WINDOW__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCROLL__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.LINEDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_OPEN_DRAWER__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.CASHDRAWER);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PRINT_BARCODE__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.POSPRINTER);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PRINT_BITMAP__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.POSPRINTER);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PRINT_CUT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.POSPRINTER);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PRINT_NORMAL__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.POSPRINTER);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_OPEN__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_CLOSE__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_REGISTRATION__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_REVERSAL__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_RESPONSE__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SIGNATURE);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SIGNATURE);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SIGNATURE);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SIGNATURE);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SIGNATURE);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SIGNATURE);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.CUSTOMERDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_CONTROL_DTO_ATTRIBUTE__DISPLAY)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.CUSTOMERDISPLAY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_BUTTON_CAPTION__ATTRIBUTE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.BUTTON);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_BUTTON_IMAGE__ATTRIBUTE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.BUTTON);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_GET__ATTRIBUTE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.FIELD);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_CLEAR__ATTRIBUTE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.FIELD);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_KEYSTROKE__ATTRIBUTE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.FIELD);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_SET__ATTRIBUTE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.FIELD);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_REMOVE__ATTRIBUTE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.FIELD);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_ITEM_VISIBLE__ATTRIBUTE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.ITEM);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_ITEM_INVISIBLE__ATTRIBUTE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.ITEM);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_DTO_FIND__DTO)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.DTO);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_PERIPHERAL_DEVICE_DISPLAY__OUTPUT)) {
        _matched=true;
        return this.getDTO(context, false);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_DTO_FIND__ATTRIBUTE)) {
        _matched=true;
        return this.getFindDTOAttributes(context);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_DTO_CLEAR__DTO)) {
        _matched=true;
        return this.getDTO(context, true);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE__DTO)) {
        _matched=true;
        return this.getDTO(context, false);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE__ATTRIBUTE)) {
        _matched=true;
        return this.getDTOAttributes(((FSMActionFieldSourceDtoAttribute) context));
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_DOT_EXPRESSION__TAIL)) {
        _matched=true;
        return this.getFilterPath(((FSMDotExpression) context));
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_SCHEDULER__SCHEDULER)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SCHEDULER);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PRINT_REPORT__KEY)) {
        _matched=true;
        return this.getStorageKey(context, StatemachineEnums.StorageAccess.KEY);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_FILTER_TOGGLE__FILTER)) {
        _matched=true;
        return this.getFilter(context);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__ATTRIBUTE)) {
        _matched=true;
        return this.getDTOAttributes(context);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SCALE);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SCALE);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SCALE);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SCALE);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_ZERO__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SCALE);
      }
    }
    if (!_matched) {
      if (Objects.equal(reference, StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT__DEVICE)) {
        _matched=true;
        return this.getActions(context, StatemachineEnums.Controls.SCALE);
      }
    }
    if (!_matched) {
      _switchResult = super.getScope(context, reference);
    }
    return _switchResult;
  }
  
  public IScope getDTOAttributes(final EObject context) {
    final ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    FSMActionPeripheralDisplayText displayText = ((FSMActionPeripheralDisplayText) context);
    FSMPeripheralDeviceDisplay _device = null;
    if (displayText!=null) {
      _device=displayText.getDevice();
    }
    FSMControlDTOAttribute _output = null;
    if (_device!=null) {
      _output=_device.getOutput();
    }
    FSMDTOType _attributeType = null;
    if (_output!=null) {
      _attributeType=_output.getAttributeType();
    }
    LDto _attributeType_1 = null;
    if (_attributeType!=null) {
      _attributeType_1=_attributeType.getAttributeType();
    }
    LDto dto = _attributeType_1;
    if ((dto != null)) {
      final Function1<Object, Boolean> _function = (Object it) -> {
        return Boolean.valueOf((it instanceof LDtoInheritedAttribute));
      };
      final Function1<Object, LDtoInheritedAttribute> _function_1 = (Object it) -> {
        return ((LDtoInheritedAttribute) it);
      };
      final Consumer<LDtoInheritedAttribute> _function_2 = (LDtoInheritedAttribute it) -> {
        result.add(EObjectDescription.create(it.getName(), it));
      };
      IterableExtensions.map(IterableExtensions.filter(dto.getAllFeatures(), _function), _function_1).forEach(_function_2);
      final Function1<Object, Boolean> _function_3 = (Object it) -> {
        return Boolean.valueOf((it instanceof LDtoAttribute));
      };
      final Function1<Object, LDtoAttribute> _function_4 = (Object it) -> {
        return ((LDtoAttribute) it);
      };
      final Consumer<LDtoAttribute> _function_5 = (LDtoAttribute it) -> {
        result.add(EObjectDescription.create(it.getName(), it));
      };
      IterableExtensions.map(IterableExtensions.filter(dto.getAllFeatures(), _function_3), _function_4).forEach(_function_5);
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getStorageKey(final EObject context, final StatemachineEnums.StorageAccess access) {
    final ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject statemachine = context;
    while (((statemachine != null) && (!(statemachine instanceof FSM)))) {
      statemachine = statemachine.eContainer();
    }
    if (((statemachine != null) && (statemachine instanceof FSM))) {
      final Consumer<FSMState> _function = (FSMState it) -> {
        final Consumer<FSMTrigger> _function_1 = (FSMTrigger it_1) -> {
          final Consumer<FSMAction> _function_2 = (FSMAction it_2) -> {
            if ((it_2 instanceof FSMStorage)) {
              if (access != null) {
                switch (access) {
                  case KEY:
                    result.add(EObjectDescription.create(((FSMStorage)it_2).getKey(), it_2));
                    break;
                  case ATTRIBUTE:
                    result.add(EObjectDescription.create(((FSMStorage)it_2).getAttribute(), it_2));
                    break;
                  default:
                    break;
                }
              }
            }
          };
          it_1.getActions().forEach(_function_2);
        };
        it.getTriggers().forEach(_function_1);
      };
      ((FSM) statemachine).getStates().forEach(_function);
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getStringFields(final EObject context) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject statemachine = ((EObject) context);
    while (((statemachine != null) && (!(statemachine instanceof FSM)))) {
      statemachine = statemachine.eContainer();
    }
    if ((((statemachine != null) && (statemachine instanceof FSM)) && (((FSM) statemachine).getControls() != null))) {
      EList<FSMControl> _controls = ((FSM) statemachine).getControls();
      for (final FSMControl control : _controls) {
        if ((control instanceof FSMControlField)) {
          EList<FSMControlFieldAttribute> _fields = ((FSMControlField) control).getFields();
          for (final FSMControlFieldAttribute field : _fields) {
            if ((Objects.equal(field.getAttributeType().getAttributeType(), FSMInternalType.STRING) || 
              Objects.equal(field.getAttributeType().getAttributeType(), FSMInternalType.SUGGESTTEXT))) {
              result.add(EObjectDescription.create(field.getName(), field));
            }
          }
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getFilter(final EObject context) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject statemachine = ((EObject) context);
    while (((statemachine != null) && (!(statemachine instanceof FSM)))) {
      statemachine = statemachine.eContainer();
    }
    if ((((statemachine != null) && (statemachine instanceof FSM)) && (((FSM) statemachine).getControls() != null))) {
      EList<FSMControl> _controls = ((FSM) statemachine).getControls();
      for (final FSMControl control : _controls) {
        if ((control instanceof FSMControlDTO)) {
          EList<FSMControlFilter> _filters = ((FSMControlDTO) control).getFilters();
          for (final FSMControlFilter filter : _filters) {
            result.add(EObjectDescription.create(filter.getName(), filter));
          }
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getDTOAttributes(final FSMActionFieldSourceDtoAttribute dtoFind) {
    LDto _attributeType = dtoFind.getDto().getAttributeType().getAttributeType();
    LDto dto = ((LDto) _attributeType);
    return this.scopeDTOAttributes(dto, dtoFind);
  }
  
  public IScope getDTO(final EObject context, final boolean withEvent) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject statemachine = ((EObject) context);
    while (((statemachine != null) && (!(statemachine instanceof FSM)))) {
      statemachine = statemachine.eContainer();
    }
    if ((((statemachine != null) && (statemachine instanceof FSM)) && (((FSM) statemachine).getControls() != null))) {
      EList<FSMControl> _controls = ((FSM) statemachine).getControls();
      for (final FSMControl control : _controls) {
        if ((control instanceof FSMControlDTO)) {
          EList<FSMControlDTOAttribute> _dtos = ((FSMControlDTO)control).getDtos();
          for (final FSMControlDTOAttribute dto : _dtos) {
            if (((!withEvent) || dto.isHasEvent())) {
              result.add(EObjectDescription.create(dto.getName(), dto));
            }
          }
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getFindDTOAttributes(final EObject context) {
    FSMActionDTOFind dtoFind = ((FSMActionDTOFind) context);
    LDto _attributeType = dtoFind.getDto().getAttributeType().getAttributeType();
    LDto dto = ((LDto) _attributeType);
    return this.scopeDTOAttributes(dto, context);
  }
  
  public IScope scopeDTOAttributes(final LDto dto, final EObject context) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    if ((dto != null)) {
      this.addAttributes(dto, context, result);
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getFilterPath(final FSMDotExpression path) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    this.scopeDotPath(path, result);
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  protected void scopeDotPath(final FSMDotExpression expr, final ArrayList<IEObjectDescription> result) {
    final FSMRef head = expr.getRef();
    boolean _matched = false;
    if (head instanceof FSMDtoRef) {
      _matched=true;
      this.addFeatures(((FSMDtoRef)head).getDto().getAttributeType().getAttributeType(), expr, result);
    }
    if (!_matched) {
      if (head instanceof FSMDotExpression) {
        _matched=true;
        LFeature _tail = ((FSMDotExpression)head).getTail();
        boolean _matched_1 = false;
        if (_tail instanceof LDtoInheritedReference) {
          _matched_1=true;
          LFeature _tail_1 = ((FSMDotExpression)head).getTail();
          this.addFeatures(((LDtoInheritedReference) _tail_1).getType(), expr, result);
        }
        if (!_matched_1) {
          if (_tail instanceof LDtoInheritedAttribute) {
            _matched_1=true;
            LFeature _tail_1 = ((FSMDotExpression)head).getTail();
            this.addFeatures(((LDtoInheritedAttribute) _tail_1).getType(), expr, result);
          }
        }
      }
    }
  }
  
  protected void _addFeatures(final LDto dto, final FSMDotExpression context, final ArrayList<IEObjectDescription> result) {
    final Function1<Object, Boolean> _function = (Object it) -> {
      return Boolean.valueOf((it instanceof LDtoInheritedAttribute));
    };
    final Function1<Object, LDtoInheritedAttribute> _function_1 = (Object it) -> {
      return ((LDtoInheritedAttribute) it);
    };
    final Consumer<LDtoInheritedAttribute> _function_2 = (LDtoInheritedAttribute it) -> {
      EObject ref = null;
      boolean _eIsProxy = it.getInheritedFeature().eIsProxy();
      if (_eIsProxy) {
        LAttribute _inheritedFeature = it.getInheritedFeature();
        final URI uri = ((InternalEObject) _inheritedFeature).eProxyURI();
        ref = context.eResource().getResourceSet().getEObject(uri, true);
      } else {
        ref = it.getInheritedFeature();
      }
      boolean _matched = false;
      if (ref instanceof LBeanAttribute) {
        _matched=true;
        result.add(EObjectDescription.create(((LBeanAttribute)ref).getName(), ((LFeature) it)));
      }
      if (!_matched) {
        if (ref instanceof LEntityAttribute) {
          _matched=true;
          result.add(EObjectDescription.create(((LEntityAttribute)ref).getName(), ((LFeature) it)));
        }
      }
    };
    IterableExtensions.map(IterableExtensions.filter(dto.getAllFeatures(), _function), _function_1).forEach(_function_2);
    final Function1<Object, Boolean> _function_3 = (Object it) -> {
      return Boolean.valueOf((it instanceof LDtoInheritedReference));
    };
    final Function1<Object, LDtoInheritedReference> _function_4 = (Object it) -> {
      return ((LDtoInheritedReference) it);
    };
    final Consumer<LDtoInheritedReference> _function_5 = (LDtoInheritedReference it) -> {
      EObject ref = null;
      boolean _eIsProxy = it.getInheritedFeature().eIsProxy();
      if (_eIsProxy) {
        LReference _inheritedFeature = it.getInheritedFeature();
        final URI uri = ((InternalEObject) _inheritedFeature).eProxyURI();
        ref = context.eResource().getResourceSet().getEObject(uri, true);
      } else {
        ref = it.getInheritedFeature();
      }
      if ((ref instanceof LEntityReference)) {
        result.add(EObjectDescription.create(((LEntityReference)ref).getName(), ((LFeature) it)));
      }
    };
    IterableExtensions.map(IterableExtensions.filter(dto.getAllFeatures(), _function_3), _function_4).forEach(_function_5);
  }
  
  protected void _addFeatures(final LEnum enumx, final FSMDotExpression context, final ArrayList<IEObjectDescription> result) {
    final Consumer<LEnumLiteral> _function = (LEnumLiteral it) -> {
      result.add(EObjectDescription.create(it.getName(), it));
    };
    enumx.getLiterals().forEach(_function);
  }
  
  public void addAttributes(final LDto dto, final EObject context, final ArrayList<IEObjectDescription> result) {
    final Consumer<LDtoInheritedAttribute> _function = (LDtoInheritedAttribute it) -> {
      LEntityAttribute attr = null;
      boolean _eIsProxy = it.getInheritedFeature().eIsProxy();
      if (_eIsProxy) {
        LAttribute _inheritedFeature = it.getInheritedFeature();
        final URI uri = ((InternalEObject) _inheritedFeature).eProxyURI();
        EObject _eObject = context.eResource().getResourceSet().getEObject(uri, true);
        attr = ((LEntityAttribute) _eObject);
      } else {
        LAttribute _inheritedFeature_1 = it.getInheritedFeature();
        attr = ((LEntityAttribute) _inheritedFeature_1);
      }
      result.add(EObjectDescription.create(attr.getName(), ((LDtoInheritedAttribute) it)));
    };
    Iterables.<LDtoInheritedAttribute>filter(dto.getAllFeatures(), LDtoInheritedAttribute.class).forEach(_function);
  }
  
  public IScope getActions(final EObject context, final StatemachineEnums.Controls controlType) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    EObject statemachine = ((EObject) context);
    while (((statemachine != null) && (!(statemachine instanceof FSM)))) {
      statemachine = statemachine.eContainer();
    }
    if ((((statemachine != null) && (statemachine instanceof FSM)) && (((FSM) statemachine).getControls() != null))) {
      EList<FSMControl> _controls = ((FSM) statemachine).getControls();
      for (final FSMControl control : _controls) {
        {
          if ((Objects.equal(controlType, StatemachineEnums.Controls.BUTTON) && (control instanceof FSMControlButton))) {
            EList<FSMControlButtonAttribute> _buttons = ((FSMControlButton) control).getButtons();
            for (final FSMControlButtonAttribute button : _buttons) {
              result.add(EObjectDescription.create(button.getName(), button));
            }
          }
          if ((Objects.equal(controlType, StatemachineEnums.Controls.SCHEDULER) && (control instanceof FSMControlScheduler))) {
            EList<FSMControlSchedulerAttribute> _schedulers = ((FSMControlScheduler) control).getSchedulers();
            for (final FSMControlSchedulerAttribute scheduler : _schedulers) {
              result.add(EObjectDescription.create(scheduler.getName(), scheduler));
            }
          }
          if ((Objects.equal(controlType, StatemachineEnums.Controls.FIELD) && (control instanceof FSMControlField))) {
            EList<FSMControlFieldAttribute> _fields = ((FSMControlField) control).getFields();
            for (final FSMControlFieldAttribute field : _fields) {
              result.add(EObjectDescription.create(field.getName(), field));
            }
          }
          if ((Objects.equal(controlType, StatemachineEnums.Controls.ITEM) && ((control instanceof FSMControlButton) || (control instanceof FSMControlField)))) {
            if ((control instanceof FSMControlButton)) {
              EList<FSMControlButtonAttribute> _buttons_1 = ((FSMControlButton) control).getButtons();
              for (final FSMControlButtonAttribute button_1 : _buttons_1) {
                result.add(EObjectDescription.create(button_1.getName(), button_1));
              }
              result.add(EObjectDescription.create(((FSMControlButton)control).getName(), control));
            }
            if ((control instanceof FSMControlField)) {
              EList<FSMControlFieldAttribute> _fields_1 = ((FSMControlField) control).getFields();
              for (final FSMControlFieldAttribute field_1 : _fields_1) {
                result.add(EObjectDescription.create(field_1.getName(), field_1));
              }
              EList<FSMControlFieldLayout> _layouts = ((FSMControlField) control).getLayouts();
              for (final FSMControlFieldLayout layout : _layouts) {
                result.add(EObjectDescription.create(layout.getName(), layout));
              }
            }
          }
          if ((Objects.equal(controlType, StatemachineEnums.Controls.DTO) && (control instanceof FSMControlDTO))) {
            EList<FSMControlDTOAttribute> _dtos = ((FSMControlDTO) control).getDtos();
            for (final FSMControlDTOAttribute dto : _dtos) {
              result.add(EObjectDescription.create(dto.getName(), dto));
            }
          }
          if ((Objects.equal(controlType, StatemachineEnums.Controls.LINEDISPLAY) && (control instanceof FSMControlPeripheral))) {
            EList<FSMPeripheralDeviceLineDisplay> _lineDisplays = ((FSMControlPeripheral) control).getLineDisplays();
            for (final FSMPeripheralDeviceLineDisplay element : _lineDisplays) {
              result.add(EObjectDescription.create(element.getName(), element));
            }
          }
          if ((Objects.equal(controlType, StatemachineEnums.Controls.CUSTOMERDISPLAY) && (control instanceof FSMControlPeripheral))) {
            EList<FSMPeripheralDeviceDisplay> _displays = ((FSMControlPeripheral) control).getDisplays();
            for (final FSMPeripheralDeviceDisplay element_1 : _displays) {
              result.add(EObjectDescription.create(element_1.getName(), element_1));
            }
          }
          if ((Objects.equal(controlType, StatemachineEnums.Controls.SIGNATURE) && (control instanceof FSMControlPeripheral))) {
            EList<FSMPeripheralDeviceSignature> _signaturePads = ((FSMControlPeripheral) control).getSignaturePads();
            for (final FSMPeripheralDeviceSignature element_2 : _signaturePads) {
              result.add(EObjectDescription.create(element_2.getName(), element_2));
            }
          }
          if ((Objects.equal(controlType, StatemachineEnums.Controls.POSPRINTER) && (control instanceof FSMControlPeripheral))) {
            EList<FSMPeripheralDevicePOSPrinter> _posPrinters = ((FSMControlPeripheral) control).getPosPrinters();
            for (final FSMPeripheralDevicePOSPrinter element_3 : _posPrinters) {
              result.add(EObjectDescription.create(element_3.getName(), element_3));
            }
          }
          if ((Objects.equal(controlType, StatemachineEnums.Controls.CASHDRAWER) && (control instanceof FSMControlPeripheral))) {
            EList<FSMPeripheralDeviceCashDrawer> _cashDrawers = ((FSMControlPeripheral) control).getCashDrawers();
            for (final FSMPeripheralDeviceCashDrawer element_4 : _cashDrawers) {
              result.add(EObjectDescription.create(element_4.getName(), element_4));
            }
          }
          if ((Objects.equal(controlType, StatemachineEnums.Controls.SCALE) && (control instanceof FSMControlPeripheral))) {
            EList<FSMPeripheralDeviceScale> _scales = ((FSMControlPeripheral) control).getScales();
            for (final FSMPeripheralDeviceScale element_5 : _scales) {
              result.add(EObjectDescription.create(element_5.getName(), element_5));
            }
          }
          if ((Objects.equal(controlType, StatemachineEnums.Controls.PAYMENTTERMINAL) && (control instanceof FSMControlPeripheral))) {
            EList<FSMPeripheralDevicePT> _paymentTerminals = ((FSMControlPeripheral) control).getPaymentTerminals();
            for (final FSMPeripheralDevicePT element_6 : _paymentTerminals) {
              result.add(EObjectDescription.create(element_6.getName(), element_6));
            }
          }
        }
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public IScope getFunctionService(final EObject context, final StatemachineEnums.Functions function) {
    ArrayList<IEObjectDescription> result = CollectionLiterals.<IEObjectDescription>newArrayList();
    if (function != null) {
      switch (function) {
        case GUARD:
          EList<FunctionLibraryGuard> _guards = ((FSMGuard) context).getGroup().getGuards();
          for (final FunctionLibraryGuard element : _guards) {
            result.add(EObjectDescription.create(element.getName(), ((FunctionLibraryGuard) element)));
          }
          break;
        case OPERATION:
          EList<FunctionLibraryOperation> _operations = ((FSMOperation) context).getGroup().getOperations();
          for (final FunctionLibraryOperation element_1 : _operations) {
            result.add(EObjectDescription.create(element_1.getName(), ((FunctionLibraryOperation) element_1)));
          }
          break;
        case FUNCTION:
          EList<FunctionLibraryFunction> _functions = ((FSMFunction) context).getGroup().getFunctions();
          for (final FunctionLibraryFunction element_2 : _functions) {
            int _size = element_2.getParams().size();
            boolean _greaterThan = (_size > 0);
            if (_greaterThan) {
              FunctionLibraryParameter _get = element_2.getParams().get(0);
              FunctionLibraryParameter first = ((FunctionLibraryParameter) _get);
              boolean _equals = "IStateMachine".equals(first.getParameterType().getSimpleName());
              if (_equals) {
                result.add(EObjectDescription.create(element_2.getName(), ((FunctionLibraryFunction) element_2)));
              }
            }
          }
          break;
        default:
          break;
      }
    }
    return MapBasedScope.createScope(IScope.NULLSCOPE, result);
  }
  
  public void addFeatures(final LScalarType enumx, final FSMDotExpression context, final ArrayList<IEObjectDescription> result) {
    if (enumx instanceof LEnum) {
      _addFeatures((LEnum)enumx, context, result);
      return;
    } else if (enumx instanceof LDto) {
      _addFeatures((LDto)enumx, context, result);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(enumx, context, result).toString());
    }
  }
}
