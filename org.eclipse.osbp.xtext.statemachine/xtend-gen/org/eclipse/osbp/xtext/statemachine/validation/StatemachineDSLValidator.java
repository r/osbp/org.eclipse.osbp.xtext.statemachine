/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.statemachine.validation;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.xtext.statemachine.FSMControlButton;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;
import org.eclipse.osbp.xtext.statemachine.validation.AbstractStatemachineDSLValidator;
import org.eclipse.xtext.validation.Check;

/**
 * Custom validation rules.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
@SuppressWarnings("all")
public class StatemachineDSLValidator extends AbstractStatemachineDSLValidator {
  @Check
  public void checkEventTypeAndRange(final FSMControlButtonAttributeEventKeyboard event) {
    EObject eObj = event.eContainer().eContainer();
    if ((eObj instanceof FSMControlButton)) {
      FSMControlButton controlButton = ((FSMControlButton) eObj);
      FSMControlButtonEventType eventType = controlButton.getEventType();
      FSMControlButtonEventType id = FSMControlButtonEventType.IDENTITY;
      boolean hasRange = controlButton.isHasRange();
      int _size = controlButton.getButtons().size();
      boolean hasButtons = (_size != 0);
      if ((hasRange && (!eventType.equals(id)))) {
        this.error("A button range can only be defined for the identity event type.", controlButton, StatemachineDSLPackage.Literals.FSM_CONTROL_BUTTON__HAS_RANGE);
      }
      if ((hasRange && hasButtons)) {
        this.warning("The buttons defined will be ignored because a button range was defined", controlButton, StatemachineDSLPackage.Literals.FSM_CONTROL_BUTTON__BUTTONS);
      }
    }
  }
}
