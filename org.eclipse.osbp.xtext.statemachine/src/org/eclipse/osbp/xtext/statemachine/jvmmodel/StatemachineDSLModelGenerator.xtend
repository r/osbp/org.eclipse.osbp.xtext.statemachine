/**
 *                                                                            
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 * 
 */
package org.eclipse.osbp.xtext.statemachine.jvmmodel

import com.vaadin.data.util.filter.And
import com.vaadin.data.util.filter.Between
import com.vaadin.data.util.filter.Compare
import com.vaadin.data.util.filter.IsNull
import com.vaadin.data.util.filter.Like
import com.vaadin.data.util.filter.Not
import com.vaadin.data.util.filter.Or
import com.vaadin.data.util.filter.SimpleStringFilter
import com.vaadin.ui.Notification
import com.vaadin.ui.Panel
import java.io.IOException
import java.lang.reflect.Constructor
import java.lang.reflect.InvocationTargetException
import java.text.ParseException
import java.util.HashMap
import java.util.List
import java.util.Locale
import java.util.Map
import java.util.ResourceBundle
import javax.inject.Inject
import javax.swing.Timer
import jpos.BaseControl
import jpos.CashDrawer
import jpos.JposException
import jpos.LineDisplay
import jpos.LineDisplayConst
import jpos.POSPrinter
import jpos.POSPrinterConst
import jpos.config.JposEntry.Prop
import jpos.config.simple.SimpleEntry
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpPut
import org.apache.http.impl.client.HttpClientBuilder
import org.eclipse.e4.core.contexts.IEclipseContext
import org.eclipse.e4.ui.model.application.ui.basic.MPart
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.osbp.abstractstatemachine.AbstractStateMachine
import org.eclipse.osbp.abstractstatemachine.POSServiceBinder
import org.eclipse.osbp.abstractstatemachine.TaskEventSource
import org.eclipse.osbp.ecview.core.^extension.model.^extension.YSuggestTextFieldEvents
import org.eclipse.osbp.runtime.common.filter.IDTOService
import org.eclipse.osbp.runtime.common.i18n.ITranslator
import org.eclipse.osbp.ui.api.message.MessageEvent
import org.eclipse.osbp.ui.api.statemachine.IStateMachine
import org.eclipse.osbp.ui.api.themes.IThemeResourceService.ThemeResourceType
import org.eclipse.osbp.xtext.basic.generator.BasicDslGeneratorUtils
import org.eclipse.osbp.xtext.i18n.I18NModelGenerator
import org.eclipse.osbp.xtext.statemachine.FSMPackage
import org.eclipse.xtext.common.types.JvmEnumerationLiteral
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.xbase.compiler.GeneratorConfig
import org.eclipse.xtext.xbase.compiler.ImportManager
import org.eclipse.xtext.xbase.compiler.XbaseCompiler
import org.eclipse.xtext.xbase.compiler.output.ITreeAppendable
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor
import org.eclipse.xtext.xbase.jvmmodel.JvmModelAssociator
import org.joda.time.DateTime
import org.osgi.service.useradmin.User
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.util.EntityUtils

class StatemachineDSLModelGenerator extends I18NModelGenerator {
	@Inject extension BasicDslGeneratorUtils
	@Inject extension JvmModelAssociator
	@Inject XbaseCompiler compiler
	
	var public static String pckgName = null
	
	def void generatePckgName(FSMPackage pckg, IJvmDeclaredTypeAcceptor acceptor) {
		pckgName = pckg.getName
	}

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		super.doGenerate(input, fsa)
	}
	
	override createAppendable(EObject context, ImportManager importManager, GeneratorConfig config) { 
		// required to initialize the needed builder to avoid deprecated methods
		builder = context.eResource
		// ---------
		addImportFor(importManager, _typeReferenceBuilder
			,User
			,ResourceBundle
			,Locale
			,List
			,ITranslator
			,AbstractStateMachine
			,IStateMachine
			,MessageEvent
			,MessageEvent.EventType
			,TaskEventSource
			,LineDisplayConst
			,POSPrinterConst
			,ThemeResourceType
			,YSuggestTextFieldEvents
			,DateTime
			,Notification
			,Notification.Type
			,IDTOService
			,LineDisplay
			,POSPrinter
			,CashDrawer
			,SimpleEntry
			,JposException
			,Prop
			,Constructor
			,BaseControl
			,POSServiceBinder
			,InvocationTargetException
			,Timer
			,Compare.Greater
			,Compare.GreaterOrEqual
			,Compare.Less
			,Compare.LessOrEqual
			,Compare.Equal
			,And
			,Or
			,Between
			,IsNull
			,Like
			,Not
			,SimpleStringFilter
			,HashMap
			,Map
			,IEclipseContext
			,Panel
			,MPart
			,HttpPut
			,HttpGet
			,HttpPost
			,HttpClientBuilder
			,CloseableHttpClient
			,CloseableHttpResponse
			,EntityUtils
			,ParseException
			,IOException
		)
		super.createAppendable(context, importManager, config)
	}
		
	def void generateInitialization(JvmEnumerationLiteral it, ITreeAppendable appendable, GeneratorConfig config) {
		if (compilationStrategy != null) {
			appendable.append(" { ")
			appendable.increaseIndentation
			appendable.newLine
			compilationStrategy.apply(appendable)
			appendable.decreaseIndentation
			appendable.newLine
			appendable.append("}")
		} else {
			val expression = associatedExpression
			if (expression != null && config.generateExpressions) {
				if(expression.hasErrors()) {
					appendable.append(" /* Skipped initializer because of errors */")
				} else {
					appendable.append(" = ")
					compiler.compileAsJavaExpression(expression, appendable, type)
				}
			}
		}
	}

	override generateEnumLiteral(JvmEnumerationLiteral it, ITreeAppendable appendable, GeneratorConfig config) {
		appendable.newLine
		generateJavaDoc(appendable, config)
		annotations.generateAnnotations(appendable, true, config)
		appendable.append(simpleName)
		generateInitialization(appendable, config)
	}
}