/**
 *                                                                            
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 * 
 */
package org.eclipse.osbp.xtext.statemachine.validation
import org.eclipse.xtext.validation.Check
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard
import org.eclipse.osbp.xtext.statemachine.FSMControlButton
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage

/**
 * Custom validation rules. 
 *
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
class StatemachineDSLValidator extends AbstractStatemachineDSLValidator {

	@Check
	def checkEventTypeAndRange(FSMControlButtonAttributeEventKeyboard event) {
		var eObj = event.eContainer.eContainer
		if(eObj instanceof FSMControlButton) {
			var controlButton = eObj as FSMControlButton
//			println(controlButton.name)
			var eventType = controlButton.eventType
			var id = FSMControlButtonEventType.IDENTITY
			var hasRange = controlButton.hasRange
			var hasButtons = controlButton.buttons.size != 0
			
			if(hasRange && !eventType.equals(id)) {
				error('A button range can only be defined for the identity event type.', controlButton, StatemachineDSLPackage.Literals.FSM_CONTROL_BUTTON__HAS_RANGE)
			}
 			if(hasRange && hasButtons) {
				warning('The buttons defined will be ignored because a button range was defined', controlButton, StatemachineDSLPackage.Literals.FSM_CONTROL_BUTTON__BUTTONS)
			}
		}
		
	}
}
