/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.osbp.dsl.xtext.types.bundles.BundleSpaceTypeProviderFactory;
import org.eclipse.osbp.dsl.xtext.types.bundles.BundleSpaceTypeScopeProvider;
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoaderFactory;
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoaderProvider;
import org.eclipse.osbp.xtext.builder.types.loader.runtime.TypeLoaderFactory;
import org.eclipse.osbp.xtext.builder.types.loader.runtime.TypeLoaderProvider;

@SuppressWarnings("restriction")
public class StatemachineDSLBundleSpaceRuntimeModule extends
		StatemachineDSLRuntimeModule {

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.xtext.datamartdsl.AbstractDatamartDSLRuntimeModule#bindIJvmTypeProvider$Factory()
	 */
	// contributed by org.eclipse.xtext.generator.types.TypesGeneratorFragment
	public Class<? extends org.eclipse.xtext.common.types.access.IJvmTypeProvider.Factory> bindIJvmTypeProvider$Factory() {
		return BundleSpaceTypeProviderFactory.class;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.xtext.datamartdsl.AbstractDatamartDSLRuntimeModule#bindAbstractTypeScopeProvider()
	 */
	// contributed by org.eclipse.xtext.generator.types.TypesGeneratorFragment
	public Class<? extends org.eclipse.xtext.common.types.xtext.AbstractTypeScopeProvider> bindAbstractTypeScopeProvider() {
		return BundleSpaceTypeScopeProvider.class;
	}

	/**
	 * Bind i type loader factory.
	 *
	 * @return the class<? extends i type loader factory>
	 */
	public Class<? extends ITypeLoaderFactory> bindITypeLoaderFactory() {
		return TypeLoaderFactory.class;
	}

	/**
	 * I type loader provider.
	 *
	 * @return the class<? extends i type loader provider>
	 */
	public Class<? extends ITypeLoaderProvider> ITypeLoaderProvider() {
		return TypeLoaderProvider.class;
	}


}
