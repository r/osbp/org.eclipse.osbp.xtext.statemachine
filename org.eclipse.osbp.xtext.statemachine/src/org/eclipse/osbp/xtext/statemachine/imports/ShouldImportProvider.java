/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.statemachine.imports;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryDSLPackage;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryStatemachineGroup;
import org.eclipse.osbp.xtext.oxtype.imports.DefaultShouldImportProvider;
import org.eclipse.osbp.xtext.reportdsl.Report;
import org.eclipse.osbp.xtext.reportdsl.ReportDSLPackage;

public class ShouldImportProvider extends DefaultShouldImportProvider {

	@Override
	protected boolean doShouldImport(EObject toImport, EReference eRef, EObject context) {
		return toImport instanceof FunctionLibraryStatemachineGroup || toImport instanceof LDto
				|| toImport instanceof Report;
	}

	@Override
	protected boolean doShouldProposeAllElements(EObject object, EReference reference) {
		EClass type = reference.getEReferenceType();
		return FunctionLibraryDSLPackage.Literals.FUNCTION_LIBRARY_STATEMACHINE_GROUP.isSuperTypeOf(type)
				|| ReportDSLPackage.Literals.REPORT.isSuperTypeOf(type)
				|| OSBPDtoPackage.Literals.LDTO.isSuperTypeOf(type);
	}
}
