/**
 *                                                                            
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 * 
 */
 
package org.eclipse.osbp.xtext.statemachine.scoping

import java.util.ArrayList
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.InternalEObject
import org.eclipse.osbp.dsl.semantic.common.types.LEnum
import org.eclipse.osbp.dsl.semantic.common.types.LFeature
import org.eclipse.osbp.dsl.semantic.dto.LDto
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference
import org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryFunction
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryGuard
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryOperation
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryParameter
import org.eclipse.osbp.xtext.statemachine.FSM
import org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute
import org.eclipse.osbp.xtext.statemachine.FSMControlButton
import org.eclipse.osbp.xtext.statemachine.FSMControlDTO
import org.eclipse.osbp.xtext.statemachine.FSMControlField
import org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral
import org.eclipse.osbp.xtext.statemachine.FSMControlScheduler
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage
import org.eclipse.osbp.xtext.statemachine.FSMDotExpression
import org.eclipse.osbp.xtext.statemachine.FSMDtoRef
import org.eclipse.osbp.xtext.statemachine.StatemachineEnums
import org.eclipse.osbp.xtext.statemachine.FSMInternalType
import org.eclipse.osbp.xtext.statemachine.FSMStorage
import org.eclipse.xtext.resource.EObjectDescription
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.impl.MapBasedScope
import org.eclipse.xtext.xbase.annotations.typesystem.XbaseWithAnnotationsBatchScopeProvider
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText
import org.eclipse.osbp.dsl.semantic.dto.LDtoAttribute
import org.eclipse.osbp.xtext.statemachine.FSMGuard
import org.eclipse.osbp.xtext.statemachine.FSMOperation
import org.eclipse.osbp.xtext.statemachine.FSMFunction

class StatemachineDSLScopeProvider extends XbaseWithAnnotationsBatchScopeProvider {
	
	override getScope(EObject context, EReference reference) {
		switch reference {
			case StatemachineDSLPackage.Literals.FSM_ACTION_CONDITIONAL_TRANSITION__GUARD: 
				return getFunctionService(context, StatemachineEnums.Functions.GUARD)
			case StatemachineDSLPackage.Literals.FSM_OPERATION__OPERATION:
				return getFunctionService(context, StatemachineEnums.Functions.OPERATION)
			case StatemachineDSLPackage.Literals.FSM_GUARD__GUARD:
				return getFunctionService(context, StatemachineEnums.Functions.GUARD)
			case StatemachineDSLPackage.Literals.FSM_FUNCTION__FUNCTION:
				return getFunctionService(context, StatemachineEnums.Functions.FUNCTION)
			case StatemachineDSLPackage.Literals.FSM_STATE__IDENTITY:
				return getFunctionService(context, StatemachineEnums.Functions.OPERATION)
			case StatemachineDSLPackage.Literals.FSM_STATE__KEYSTROKE:
				return getStringFields(context)
			case StatemachineDSLPackage.Literals.FSM_STATE__KEY_OPERATION:
				return getFunctionService(context, StatemachineEnums.Functions.OPERATION)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_BLINK_RATE__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_CLEAR__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_CURSOR_TYPE__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_DESTROY_WINDOW__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCROLL__DEVICE:
				return getActions(context, StatemachineEnums.Controls.LINEDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_OPEN_DRAWER__DEVICE:
				return getActions(context, StatemachineEnums.Controls.CASHDRAWER)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PRINT_BARCODE__DEVICE:
				return getActions(context, StatemachineEnums.Controls.POSPRINTER)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PRINT_BITMAP__DEVICE:
				return getActions(context, StatemachineEnums.Controls.POSPRINTER)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PRINT_CUT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.POSPRINTER)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PRINT_NORMAL__DEVICE:
				return getActions(context, StatemachineEnums.Controls.POSPRINTER)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_OPEN__DEVICE:
				return getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_CLOSE__DEVICE:
				return getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE__DEVICE:
				return getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_REGISTRATION__DEVICE:
				return getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION__DEVICE:
				return getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_REVERSAL__DEVICE:
				return getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_RESPONSE__DEVICE:
				return getActions(context, StatemachineEnums.Controls.PAYMENTTERMINAL)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SIGNATURE)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SIGNATURE)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SIGNATURE)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SIGNATURE)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SIGNATURE)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SIGNATURE)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.CUSTOMERDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_CONTROL_DTO_ATTRIBUTE__DISPLAY:
				return getActions(context, StatemachineEnums.Controls.CUSTOMERDISPLAY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_BUTTON_CAPTION__ATTRIBUTE:
				return getActions(context, StatemachineEnums.Controls.BUTTON)
			case StatemachineDSLPackage.Literals.FSM_ACTION_BUTTON_IMAGE__ATTRIBUTE:
				return getActions(context, StatemachineEnums.Controls.BUTTON)
			case StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_GET__ATTRIBUTE:
				return getActions(context, StatemachineEnums.Controls.FIELD)
			case StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_CLEAR__ATTRIBUTE:
				return getActions(context, StatemachineEnums.Controls.FIELD)
			case StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_KEYSTROKE__ATTRIBUTE:
				return getActions(context, StatemachineEnums.Controls.FIELD)
			case StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_SET__ATTRIBUTE:
				return getActions(context, StatemachineEnums.Controls.FIELD)
			case StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_REMOVE__ATTRIBUTE:
				return getActions(context, StatemachineEnums.Controls.FIELD)
			case StatemachineDSLPackage.Literals.FSM_ACTION_ITEM_VISIBLE__ATTRIBUTE:
				return getActions(context, StatemachineEnums.Controls.ITEM)
			case StatemachineDSLPackage.Literals.FSM_ACTION_ITEM_INVISIBLE__ATTRIBUTE:
				return getActions(context, StatemachineEnums.Controls.ITEM)
			case StatemachineDSLPackage.Literals.FSM_ACTION_DTO_FIND__DTO:
				return getActions(context, StatemachineEnums.Controls.DTO)
			case StatemachineDSLPackage.Literals.FSM_PERIPHERAL_DEVICE_DISPLAY__OUTPUT:
				return getDTO(context, false)
			case StatemachineDSLPackage.Literals.FSM_ACTION_DTO_FIND__ATTRIBUTE:
				return getFindDTOAttributes(context)
			case StatemachineDSLPackage.Literals.FSM_ACTION_DTO_CLEAR__DTO:
				return getDTO(context, true)
			case StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE__DTO:
				return getDTO(context, false)
			case StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE__ATTRIBUTE:
				return getDTOAttributes(context as FSMActionFieldSourceDtoAttribute) 
			case StatemachineDSLPackage.Literals.FSM_DOT_EXPRESSION__TAIL:
				return getFilterPath(context as FSMDotExpression) 
			case StatemachineDSLPackage.Literals.FSM_ACTION_SCHEDULER__SCHEDULER:
				return getActions(context, StatemachineEnums.Controls.SCHEDULER)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PRINT_REPORT__KEY:
				return getStorageKey(context, StatemachineEnums.StorageAccess.KEY)
			case StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_FILTER_TOGGLE__FILTER:
				return getFilter(context)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__ATTRIBUTE:
				return getDTOAttributes(context)
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SCALE) 
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SCALE) 
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SCALE) 
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SCALE) 
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_ZERO__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SCALE) 
			case StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT__DEVICE:
				return getActions(context, StatemachineEnums.Controls.SCALE) 
			default:
				super.getScope(context, reference)
		}
	}
	
	def IScope getDTOAttributes(EObject context) {
		val result = <IEObjectDescription>newArrayList
		var displayText = context as FSMActionPeripheralDisplayText
		var dto = displayText?.device?.output?.attributeType?.attributeType
		if(dto !== null) {
			dto.allFeatures.filter[it instanceof LDtoInheritedAttribute].map[it as LDtoInheritedAttribute].forEach[
				result.add(EObjectDescription.create(it.name, it))
			]
			dto.allFeatures.filter[it instanceof LDtoAttribute].map[it as LDtoAttribute].forEach[
				result.add(EObjectDescription.create(it.name, it))
			]
		}
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getStorageKey(EObject context, StatemachineEnums.StorageAccess access) {
		val result = <IEObjectDescription>newArrayList
		var statemachine = context
		while (statemachine !== null && !(statemachine instanceof FSM)) {
			statemachine = statemachine.eContainer
		}
		if (statemachine !== null && statemachine instanceof FSM) {
			(statemachine as FSM).states.forEach[
				it.triggers.forEach[
					it.actions.forEach [
						if(it instanceof FSMStorage) {
							switch (access) {
								case KEY:
									result.add(EObjectDescription.create(it.key, it))
								case ATTRIBUTE:
									result.add(EObjectDescription.create(it.attribute, it))
							}
						}
					]
				]
			]
		}
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getStringFields(EObject context) {
		var result = <IEObjectDescription>newArrayList
		var statemachine = context as EObject
		while (statemachine !== null && !(statemachine instanceof FSM)) {
			statemachine = statemachine.eContainer
		}
		if (statemachine !== null && (statemachine instanceof FSM) && ((statemachine as FSM).controls !== null)) {
			for (control : (statemachine as FSM).controls) {
				if (control instanceof FSMControlField) {
					for (field : (control as FSMControlField).fields) {
						if(field.attributeType.attributeType == FSMInternalType.STRING ||
							field.attributeType.attributeType == FSMInternalType.SUGGESTTEXT) {
							result.add(EObjectDescription.create(field.name, field))
						}
					}
				}
			}
		}
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}

	def IScope getFilter(EObject context) {
		var result = <IEObjectDescription>newArrayList
		var statemachine = context as EObject
		while (statemachine !== null && !(statemachine instanceof FSM)) {
			statemachine = statemachine.eContainer
		}
		if (statemachine !== null && (statemachine instanceof FSM) && ((statemachine as FSM).controls !== null)) {
			for (control : (statemachine as FSM).controls) {
				if (control instanceof FSMControlDTO) {
					for (filter : (control as FSMControlDTO).filters) {
						result.add(EObjectDescription.create(filter.name, filter))
					}
				}
			}
		}
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getDTOAttributes(FSMActionFieldSourceDtoAttribute dtoFind) {
		var dto = dtoFind.dto.attributeType.attributeType as LDto
		return dto.scopeDTOAttributes(dtoFind)
	}

	def IScope getDTO(EObject context, boolean withEvent) {
		var result = <IEObjectDescription>newArrayList
		var statemachine = context as EObject
		while (statemachine !== null && !(statemachine instanceof FSM)) {
			statemachine = statemachine.eContainer
		}
		if (statemachine !== null && (statemachine instanceof FSM) && ((statemachine as FSM).controls !== null)) {
			for (control : (statemachine as FSM).controls) {
				if(control instanceof FSMControlDTO) {
					for(dto:control.dtos) {
						if(!withEvent || dto.hasEvent) {
							result.add(EObjectDescription.create(dto.name, dto))
						}
					}
				}
			}
		}
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getFindDTOAttributes(EObject context) {
		var dtoFind = context as FSMActionDTOFind
		var dto = dtoFind.dto.attributeType.attributeType as LDto
		return dto.scopeDTOAttributes(context)
	}
	
	def IScope scopeDTOAttributes(LDto dto, EObject context) {
		var result = <IEObjectDescription>newArrayList
		if (dto !== null) {
			dto.addAttributes(context, result)
		}
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}

	def IScope getFilterPath(FSMDotExpression path) {
		var result = <IEObjectDescription>newArrayList
		path.scopeDotPath(result)
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}

	protected def void scopeDotPath(FSMDotExpression expr, ArrayList<IEObjectDescription> result) {
		val head = expr.ref
		switch(head) {
			FSMDtoRef: 
				head.dto.attributeType.attributeType.addFeatures(expr, result)
			FSMDotExpression:
				switch (head.tail) {
					LDtoInheritedReference:
						(head.tail as LDtoInheritedReference).type.addFeatures(expr, result)
					LDtoInheritedAttribute:
						(head.tail as LDtoInheritedAttribute).type.addFeatures(expr, result)
				}
		}
	}
	
	def dispatch void addFeatures(LDto dto, FSMDotExpression context, ArrayList<IEObjectDescription> result) {
		dto.allFeatures.filter[it instanceof LDtoInheritedAttribute].map[it as LDtoInheritedAttribute].forEach[
			var EObject ref = null
			if (it.inheritedFeature.eIsProxy()) {
				// This provokes no problems of loops or cycling linking.
				val uri = (it.inheritedFeature as InternalEObject).eProxyURI
				ref = context.eResource.resourceSet.getEObject(uri, true)
			} else {
				ref = it.inheritedFeature
			}
			switch(ref) {
				LBeanAttribute:
					result.add(EObjectDescription.create(ref.name, it as LFeature))
				LEntityAttribute:
					result.add(EObjectDescription.create(ref.name, it as LFeature))
			}
		]
		dto.allFeatures.filter[it instanceof LDtoInheritedReference].map[it as LDtoInheritedReference].forEach[
			var EObject ref = null
			if (it.inheritedFeature.eIsProxy()) {
				// This provokes no problems of loops or cycling linking.
				val uri = (it.inheritedFeature as InternalEObject).eProxyURI
				ref = context.eResource.resourceSet.getEObject(uri, true)
			} else {
				ref = it.inheritedFeature
			}
			if(ref instanceof LEntityReference) {
				result.add(EObjectDescription.create(ref.name, it as LFeature))
			}
		]
	}
	
	def dispatch void addFeatures(LEnum enumx, FSMDotExpression context, ArrayList<IEObjectDescription> result) {
		enumx.literals.forEach[
			result.add(EObjectDescription.create(it.name, it))
		]
	}
	
	
	def void addAttributes(LDto dto, EObject context, ArrayList<IEObjectDescription> result) {
		dto.allFeatures.filter(LDtoInheritedAttribute).forEach[it|
			var LEntityAttribute attr = null
			if (it.inheritedFeature.eIsProxy()) {
				// This provokes no problems of loops or cycling linking.
				val uri = (it.inheritedFeature as InternalEObject).eProxyURI
				attr = context.eResource.resourceSet.getEObject(uri, true) as LEntityAttribute
			} else {
				attr = it.inheritedFeature as LEntityAttribute
			}
			result.add(EObjectDescription.create(attr.name, it as LDtoInheritedAttribute))
		]
	}
	
	def IScope getActions(EObject context, StatemachineEnums.Controls controlType) {
		var result = <IEObjectDescription>newArrayList
		var statemachine = context as EObject
		while (statemachine !== null && !(statemachine instanceof FSM)) {
			statemachine = statemachine.eContainer
		}
		if (statemachine !== null && (statemachine instanceof FSM) && ((statemachine as FSM).controls !== null)) {
			for (control : (statemachine as FSM).controls) {
				if (controlType == StatemachineEnums.Controls.BUTTON && control instanceof FSMControlButton) {
					for (button : (control as FSMControlButton).buttons) {
						result.add(EObjectDescription.create(button.name, button))
					}
				}
				if (controlType == StatemachineEnums.Controls.SCHEDULER && control instanceof FSMControlScheduler) {
					for (scheduler : (control as FSMControlScheduler).schedulers) {
						result.add(EObjectDescription.create(scheduler.name, scheduler))
					}
				}
				if (controlType == StatemachineEnums.Controls.FIELD && control instanceof FSMControlField) {
					for (field : (control as FSMControlField).fields) {
						result.add(EObjectDescription.create(field.name, field))
					}
				}
				if (controlType == StatemachineEnums.Controls.ITEM && (control instanceof FSMControlButton || control instanceof FSMControlField)) {
					if(control instanceof FSMControlButton) {
						for (button : (control as FSMControlButton).buttons) {
							result.add(EObjectDescription.create(button.name, button))
						}
						result.add(EObjectDescription.create(control.name, control))
					}
					if(control instanceof FSMControlField) {
						for (field : (control as FSMControlField).fields) {
							result.add(EObjectDescription.create(field.name, field))
						}
						for (layout : (control as FSMControlField).layouts) {
							result.add(EObjectDescription.create(layout.name, layout))
						}
					}
				}
				if (controlType == StatemachineEnums.Controls.DTO && control instanceof FSMControlDTO) {
					for (dto : (control as FSMControlDTO).dtos) {
						result.add(EObjectDescription.create(dto.name, dto))
					}
				}
				if (controlType == StatemachineEnums.Controls.LINEDISPLAY && control instanceof FSMControlPeripheral) {
					for (element : (control as FSMControlPeripheral).lineDisplays) {
						result.add(EObjectDescription.create(element.name, element))
					}
				}
				if (controlType == StatemachineEnums.Controls.CUSTOMERDISPLAY && control instanceof FSMControlPeripheral) {
					for (element : (control as FSMControlPeripheral).displays) {
						result.add(EObjectDescription.create(element.name, element))
					}
				}
				if (controlType == StatemachineEnums.Controls.SIGNATURE && control instanceof FSMControlPeripheral) {
					for (element : (control as FSMControlPeripheral).signaturePads) {
						result.add(EObjectDescription.create(element.name, element))
					}
				}
				if (controlType == StatemachineEnums.Controls.POSPRINTER && control instanceof FSMControlPeripheral) {
					for (element : (control as FSMControlPeripheral).posPrinters) {
						result.add(EObjectDescription.create(element.name, element))
					}
				}
				if (controlType == StatemachineEnums.Controls.CASHDRAWER && control instanceof FSMControlPeripheral) {
					for (element : (control as FSMControlPeripheral).cashDrawers) {
						result.add(EObjectDescription.create(element.name, element))
					}
				}
				if (controlType == StatemachineEnums.Controls.SCALE && control instanceof FSMControlPeripheral) {
					for (element : (control as FSMControlPeripheral).scales) {
						result.add(EObjectDescription.create(element.name, element))
					}
				}
				if (controlType == StatemachineEnums.Controls.PAYMENTTERMINAL && control instanceof FSMControlPeripheral) {
					for (element : (control as FSMControlPeripheral).paymentTerminals) {
						result.add(EObjectDescription.create(element.name, element))
					}
				}
			}
		}
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
	
	def IScope getFunctionService(EObject context, StatemachineEnums.Functions function) {
		var result = <IEObjectDescription>newArrayList
		switch function {
			case GUARD: 
				for (element : (context as FSMGuard).group.guards) {
					result.add(EObjectDescription.create(element.name, (element as FunctionLibraryGuard)))
				}
			case OPERATION:
				for (element : (context as FSMOperation).group.operations) {
					result.add(EObjectDescription.create(element.name, (element as FunctionLibraryOperation)))
				}
			case FUNCTION:
				for (element : (context as FSMFunction).group.functions) {
					if(element.params.size > 0) {
						var first = element.params.get(0) as FunctionLibraryParameter
						if("IStateMachine".equals(first.parameterType.simpleName)) {
							result.add(EObjectDescription.create(element.name, (element as FunctionLibraryFunction)))
						}
					}
				}
		}
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}

}
