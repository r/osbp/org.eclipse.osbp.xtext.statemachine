/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.osbp.xtext.builder.xbase.setups.XbaseBundleSpaceStandaloneSetup;

import com.google.inject.Guice;
import com.google.inject.Injector;

@SuppressWarnings("restriction")
public class StatemachineDSLBundleSpaceStandaloneSetup extends
		StatemachineDSLStandaloneSetup {
	/**
	 * Do setup.
	 */
	public static void doSetup() {
		new StatemachineDSLBundleSpaceStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.osbp.xtext.datamartdsl.DatamartDSLStandaloneSetupGenerated#createInjectorAndDoEMFRegistration()
	 */
	@Override
	public Injector createInjectorAndDoEMFRegistration() {
		XbaseBundleSpaceStandaloneSetup.doSetup();

		Injector injector = createInjector();
		register(injector);
		return injector;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.xtext.datamartdsl.DatamartDSLStandaloneSetupGenerated#createInjector()
	 */
	@Override
	public Injector createInjector() {
		return Guice.createInjector(new StatemachineDSLBundleSpaceRuntimeModule());
	}


}
