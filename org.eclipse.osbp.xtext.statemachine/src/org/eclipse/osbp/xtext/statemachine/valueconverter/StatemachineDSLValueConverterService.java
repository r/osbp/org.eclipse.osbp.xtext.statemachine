/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * 		Christophe Loetz - Initial implementation
 */
package org.eclipse.osbp.xtext.statemachine.valueconverter;

import org.eclipse.osbp.xtext.basic.valueconverter.TRANSLATABLESTRINGValueConverter;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.xbase.conversion.XbaseValueConverterService;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Adds a value conversion for the QualifiedNameWithWildCard rule.
 * 
 */
@SuppressWarnings("restriction")
@Singleton
public class StatemachineDSLValueConverterService extends XbaseValueConverterService {
	@Inject
	private TRANSLATABLESTRINGValueConverter converter;

	@ValueConverter(rule = "QualifiedNameWithWildCard")
	public IValueConverter<String> getQualifiedNameWithWildCard() {
		return getQualifiedNameValueConverter();
	}

	@ValueConverter(rule = "TRANSLATABLESTRING")
	public IValueConverter<String> getTranslatableStringValueConverter() {
		return converter;
	}

}
