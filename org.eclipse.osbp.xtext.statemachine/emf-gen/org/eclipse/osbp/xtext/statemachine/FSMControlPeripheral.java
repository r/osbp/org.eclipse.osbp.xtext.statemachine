/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Control Peripheral</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getLineDisplays <em>Line Displays</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getDisplays <em>Displays</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getPosPrinters <em>Pos Printers</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getCashDrawers <em>Cash Drawers</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getPaymentTerminals <em>Payment Terminals</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getSignaturePads <em>Signature Pads</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getScales <em>Scales</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlPeripheral()
 * @model
 * @generated
 */
public interface FSMControlPeripheral extends FSMControl {
	/**
	 * Returns the value of the '<em><b>Line Displays</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceLineDisplay}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Line Displays</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Line Displays</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlPeripheral_LineDisplays()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMPeripheralDeviceLineDisplay> getLineDisplays();

	/**
	 * Returns the value of the '<em><b>Displays</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Displays</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Displays</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlPeripheral_Displays()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMPeripheralDeviceDisplay> getDisplays();

	/**
	 * Returns the value of the '<em><b>Pos Printers</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePOSPrinter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pos Printers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pos Printers</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlPeripheral_PosPrinters()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMPeripheralDevicePOSPrinter> getPosPrinters();

	/**
	 * Returns the value of the '<em><b>Cash Drawers</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceCashDrawer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cash Drawers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cash Drawers</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlPeripheral_CashDrawers()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMPeripheralDeviceCashDrawer> getCashDrawers();

	/**
	 * Returns the value of the '<em><b>Payment Terminals</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePT}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Payment Terminals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Payment Terminals</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlPeripheral_PaymentTerminals()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMPeripheralDevicePT> getPaymentTerminals();

	/**
	 * Returns the value of the '<em><b>Signature Pads</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceSignature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature Pads</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature Pads</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlPeripheral_SignaturePads()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMPeripheralDeviceSignature> getSignaturePads();

	/**
	 * Returns the value of the '<em><b>Scales</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceScale}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scales</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scales</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlPeripheral_Scales()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMPeripheralDeviceScale> getScales();

} // FSMControlPeripheral
