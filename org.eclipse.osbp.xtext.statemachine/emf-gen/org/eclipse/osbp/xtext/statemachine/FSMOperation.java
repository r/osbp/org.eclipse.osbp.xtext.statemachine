/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryOperation;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryStatemachineGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMOperation#getGroup <em>Group</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMOperation#getOperation <em>Operation</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMOperation#getFirst <em>First</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMOperation#getMore <em>More</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMOperation()
 * @model
 * @generated
 */
public interface FSMOperation extends FSMActionFieldSource {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' reference.
	 * @see #setGroup(FunctionLibraryStatemachineGroup)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMOperation_Group()
	 * @model
	 * @generated
	 */
	FunctionLibraryStatemachineGroup getGroup();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMOperation#getGroup <em>Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group</em>' reference.
	 * @see #getGroup()
	 * @generated
	 */
	void setGroup(FunctionLibraryStatemachineGroup value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' reference.
	 * @see #setOperation(FunctionLibraryOperation)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMOperation_Operation()
	 * @model
	 * @generated
	 */
	FunctionLibraryOperation getOperation();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMOperation#getOperation <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(FunctionLibraryOperation value);

	/**
	 * Returns the value of the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First</em>' containment reference.
	 * @see #setFirst(FSMOperationParameter)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMOperation_First()
	 * @model containment="true"
	 * @generated
	 */
	FSMOperationParameter getFirst();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMOperation#getFirst <em>First</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First</em>' containment reference.
	 * @see #getFirst()
	 * @generated
	 */
	void setFirst(FSMOperationParameter value);

	/**
	 * Returns the value of the '<em><b>More</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMOperationParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>More</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>More</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMOperation_More()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMOperationParameter> getMore();

} // FSMOperation
