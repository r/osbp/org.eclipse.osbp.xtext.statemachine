/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryGuard;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryStatemachineGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Guard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getGroup <em>Group</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getGuard <em>Guard</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#isHasOnFail <em>Has On Fail</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailDescription <em>On Fail Description</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailCaption <em>On Fail Caption</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailType <em>On Fail Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMGuard()
 * @model
 * @generated
 */
public interface FSMGuard extends FSMLazyResolver {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' reference.
	 * @see #setGroup(FunctionLibraryStatemachineGroup)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMGuard_Group()
	 * @model
	 * @generated
	 */
	FunctionLibraryStatemachineGroup getGroup();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getGroup <em>Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group</em>' reference.
	 * @see #getGroup()
	 * @generated
	 */
	void setGroup(FunctionLibraryStatemachineGroup value);

	/**
	 * Returns the value of the '<em><b>Guard</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Guard</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guard</em>' reference.
	 * @see #setGuard(FunctionLibraryGuard)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMGuard_Guard()
	 * @model
	 * @generated
	 */
	FunctionLibraryGuard getGuard();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getGuard <em>Guard</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guard</em>' reference.
	 * @see #getGuard()
	 * @generated
	 */
	void setGuard(FunctionLibraryGuard value);

	/**
	 * Returns the value of the '<em><b>Has On Fail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has On Fail</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has On Fail</em>' attribute.
	 * @see #setHasOnFail(boolean)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMGuard_HasOnFail()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasOnFail();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#isHasOnFail <em>Has On Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has On Fail</em>' attribute.
	 * @see #isHasOnFail()
	 * @generated
	 */
	void setHasOnFail(boolean value);

	/**
	 * Returns the value of the '<em><b>On Fail Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Fail Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Fail Description</em>' attribute.
	 * @see #setOnFailDescription(String)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMGuard_OnFailDescription()
	 * @model unique="false"
	 * @generated
	 */
	String getOnFailDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailDescription <em>On Fail Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Fail Description</em>' attribute.
	 * @see #getOnFailDescription()
	 * @generated
	 */
	void setOnFailDescription(String value);

	/**
	 * Returns the value of the '<em><b>On Fail Caption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Fail Caption</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Fail Caption</em>' attribute.
	 * @see #setOnFailCaption(String)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMGuard_OnFailCaption()
	 * @model unique="false"
	 * @generated
	 */
	String getOnFailCaption();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailCaption <em>On Fail Caption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Fail Caption</em>' attribute.
	 * @see #getOnFailCaption()
	 * @generated
	 */
	void setOnFailCaption(String value);

	/**
	 * Returns the value of the '<em><b>On Fail Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.statemachine.FSMUserMessageType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Fail Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Fail Type</em>' attribute.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMUserMessageType
	 * @see #setOnFailType(FSMUserMessageType)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMGuard_OnFailType()
	 * @model unique="false"
	 * @generated
	 */
	FSMUserMessageType getOnFailType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailType <em>On Fail Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Fail Type</em>' attribute.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMUserMessageType
	 * @see #getOnFailType()
	 * @generated
	 */
	void setOnFailType(FSMUserMessageType value);

} // FSMGuard
