/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Peripheral Device POS Printer</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMPeripheralDevicePOSPrinter()
 * @model
 * @generated
 */
public interface FSMPeripheralDevicePOSPrinter extends FSMPeripheralDevice {
} // FSMPeripheralDevicePOSPrinter
