/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel fileExtensions='statemachine' modelName='StatemachineDSL' prefix='StatemachineDSL' updateClasspath='false' loadInitialization='false' literalsInterface='true' copyrightText='Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)\n All rights reserved. This program and the accompanying materials \n are made available under the terms of the Eclipse Public License 2.0  \n which accompanies this distribution, and is available at \n https://www.eclipse.org/legal/epl-2.0/ \n \n SPDX-License-Identifier: EPL-2.0 \n\n Based on ideas from Xtext, Xtend, Xcore\n  \n Contributors:  \n \t\tJoerg Riegel - Initial implementation \n ' basePackage='org.eclipse.osbp.xtext'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore rootPackage='statemachinedsl'"
 * @generated
 */
public interface StatemachineDSLPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "statemachine";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.eclipse.org/xtext/statemachine/StatemachineDSL";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "statemachinedsl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StatemachineDSLPackage eINSTANCE = org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMModelImpl <em>FSM Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMModelImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMModel()
	 * @generated
	 */
	int FSM_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Import Section</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_MODEL__IMPORT_SECTION = 0;

	/**
	 * The feature id for the '<em><b>Packages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_MODEL__PACKAGES = 1;

	/**
	 * The number of structural features of the '<em>FSM Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>FSM Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMLazyResolverImpl <em>FSM Lazy Resolver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMLazyResolverImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLazyResolver()
	 * @generated
	 */
	int FSM_LAZY_RESOLVER = 1;

	/**
	 * The number of structural features of the '<em>FSM Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_LAZY_RESOLVER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = 0;

	/**
	 * The number of operations of the '<em>FSM Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_LAZY_RESOLVER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMBaseImpl <em>FSM Base</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMBaseImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMBase()
	 * @generated
	 */
	int FSM_BASE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_BASE__NAME = FSM_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_BASE_FEATURE_COUNT = FSM_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_BASE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_BASE_OPERATION_COUNT = FSM_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPackageImpl <em>FSM Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPackageImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPackage()
	 * @generated
	 */
	int FSM_PACKAGE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PACKAGE__NAME = OSBPTypesPackage.LPACKAGE__NAME;

	/**
	 * The feature id for the '<em><b>Statemachines</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PACKAGE__STATEMACHINES = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PACKAGE_FEATURE_COUNT = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PACKAGE___ERESOLVE_PROXY__OBJECT = OSBPTypesPackage.LPACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PACKAGE_OPERATION_COUNT = OSBPTypesPackage.LPACKAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMImpl <em>FSM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSM()
	 * @generated
	 */
	int FSM = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__NAME = FSM_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__DESCRIPTION = FSM_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__DESCRIPTION_VALUE = FSM_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Initial Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__INITIAL_EVENT = FSM_BASE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Initial State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__INITIAL_STATE = FSM_BASE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__EVENTS = FSM_BASE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Controls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__CONTROLS = FSM_BASE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__STATES = FSM_BASE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>FSM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FEATURE_COUNT = FSM_BASE_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION_COUNT = FSM_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMControl <em>FSM Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControl()
	 * @generated
	 */
	int FSM_CONTROL = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL__NAME = FSM_BASE__NAME;

	/**
	 * The number of structural features of the '<em>FSM Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FEATURE_COUNT = FSM_BASE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_OPERATION_COUNT = FSM_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonImpl <em>FSM Control Button</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButton()
	 * @generated
	 */
	int FSM_CONTROL_BUTTON = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON__NAME = FSM_CONTROL__NAME;

	/**
	 * The feature id for the '<em><b>Event Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON__EVENT_TYPE = FSM_CONTROL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Buttons</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON__BUTTONS = FSM_CONTROL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Has Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON__HAS_RANGE = FSM_CONTROL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON__START = FSM_CONTROL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON__END = FSM_CONTROL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Ranged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON__RANGED_NAME = FSM_CONTROL_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>FSM Control Button</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_FEATURE_COUNT = FSM_CONTROL_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_CONTROL___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Button</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_OPERATION_COUNT = FSM_CONTROL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldImpl <em>FSM Control Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlField()
	 * @generated
	 */
	int FSM_CONTROL_FIELD = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD__NAME = FSM_CONTROL__NAME;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD__FIELDS = FSM_CONTROL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Layouts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD__LAYOUTS = FSM_CONTROL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Control Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD_FEATURE_COUNT = FSM_CONTROL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_CONTROL___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD_OPERATION_COUNT = FSM_CONTROL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOImpl <em>FSM Control DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlDTO()
	 * @generated
	 */
	int FSM_CONTROL_DTO = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO__NAME = FSM_CONTROL__NAME;

	/**
	 * The feature id for the '<em><b>Dtos</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO__DTOS = FSM_CONTROL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO__FILTERS = FSM_CONTROL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Control DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO_FEATURE_COUNT = FSM_CONTROL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_CONTROL___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO_OPERATION_COUNT = FSM_CONTROL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlSchedulerImpl <em>FSM Control Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlSchedulerImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlScheduler()
	 * @generated
	 */
	int FSM_CONTROL_SCHEDULER = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_SCHEDULER__NAME = FSM_CONTROL__NAME;

	/**
	 * The feature id for the '<em><b>Schedulers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_SCHEDULER__SCHEDULERS = FSM_CONTROL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Control Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_SCHEDULER_FEATURE_COUNT = FSM_CONTROL_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_SCHEDULER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_CONTROL___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_SCHEDULER_OPERATION_COUNT = FSM_CONTROL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlPeripheralImpl <em>FSM Control Peripheral</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlPeripheralImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlPeripheral()
	 * @generated
	 */
	int FSM_CONTROL_PERIPHERAL = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_PERIPHERAL__NAME = FSM_CONTROL__NAME;

	/**
	 * The feature id for the '<em><b>Line Displays</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_PERIPHERAL__LINE_DISPLAYS = FSM_CONTROL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Displays</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_PERIPHERAL__DISPLAYS = FSM_CONTROL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Pos Printers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_PERIPHERAL__POS_PRINTERS = FSM_CONTROL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Cash Drawers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_PERIPHERAL__CASH_DRAWERS = FSM_CONTROL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Payment Terminals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_PERIPHERAL__PAYMENT_TERMINALS = FSM_CONTROL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Signature Pads</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_PERIPHERAL__SIGNATURE_PADS = FSM_CONTROL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Scales</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_PERIPHERAL__SCALES = FSM_CONTROL_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>FSM Control Peripheral</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_PERIPHERAL_FEATURE_COUNT = FSM_CONTROL_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_PERIPHERAL___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_CONTROL___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Peripheral</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_PERIPHERAL_OPERATION_COUNT = FSM_CONTROL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlVisibility <em>FSM Control Visibility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlVisibility
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlVisibility()
	 * @generated
	 */
	int FSM_CONTROL_VISIBILITY = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_VISIBILITY__NAME = FSM_BASE__NAME;

	/**
	 * The number of structural features of the '<em>FSM Control Visibility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_VISIBILITY_FEATURE_COUNT = FSM_BASE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_VISIBILITY___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Visibility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_VISIBILITY_OPERATION_COUNT = FSM_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeImpl <em>FSM Control Button Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonAttribute()
	 * @generated
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE__NAME = FSM_CONTROL_VISIBILITY__NAME;

	/**
	 * The feature id for the '<em><b>Has Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE__HAS_IMAGE = FSM_CONTROL_VISIBILITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE__IMAGE = FSM_CONTROL_VISIBILITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT = FSM_CONTROL_VISIBILITY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Control Button Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_FEATURE_COUNT = FSM_CONTROL_VISIBILITY_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_CONTROL_VISIBILITY___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Button Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_OPERATION_COUNT = FSM_CONTROL_VISIBILITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEvent <em>FSM Control Button Attribute Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEvent
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonAttributeEvent()
	 * @generated
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT = 12;

	/**
	 * The number of structural features of the '<em>FSM Control Button Attribute Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_FEATURE_COUNT = FSM_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Button Attribute Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_OPERATION_COUNT = FSM_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventKeyboardImpl <em>FSM Control Button Attribute Event Keyboard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventKeyboardImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonAttributeEventKeyboard()
	 * @generated
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD = 13;

	/**
	 * The feature id for the '<em><b>Keystroke</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD__KEYSTROKE = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Control Button Attribute Event Keyboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD_FEATURE_COUNT = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Button Attribute Event Keyboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD_OPERATION_COUNT = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventIdentityImpl <em>FSM Control Button Attribute Event Identity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventIdentityImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonAttributeEventIdentity()
	 * @generated
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_IDENTITY = 14;

	/**
	 * The feature id for the '<em><b>Identity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_IDENTITY__IDENTITY = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Control Button Attribute Event Identity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_IDENTITY_FEATURE_COUNT = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_IDENTITY___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Button Attribute Event Identity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_IDENTITY_OPERATION_COUNT = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventEventImpl <em>FSM Control Button Attribute Event Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventEventImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonAttributeEventEvent()
	 * @generated
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_EVENT = 15;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_EVENT__EVENT = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Control Button Attribute Event Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_EVENT_FEATURE_COUNT = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_EVENT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Button Attribute Event Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_EVENT_OPERATION_COUNT = FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldAttributeImpl <em>FSM Control Field Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldAttributeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlFieldAttribute()
	 * @generated
	 */
	int FSM_CONTROL_FIELD_ATTRIBUTE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD_ATTRIBUTE__NAME = FSM_CONTROL_VISIBILITY__NAME;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD_ATTRIBUTE__ATTRIBUTE_TYPE = FSM_CONTROL_VISIBILITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Control Field Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD_ATTRIBUTE_FEATURE_COUNT = FSM_CONTROL_VISIBILITY_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD_ATTRIBUTE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_CONTROL_VISIBILITY___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Field Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD_ATTRIBUTE_OPERATION_COUNT = FSM_CONTROL_VISIBILITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldLayoutImpl <em>FSM Control Field Layout</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldLayoutImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlFieldLayout()
	 * @generated
	 */
	int FSM_CONTROL_FIELD_LAYOUT = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD_LAYOUT__NAME = FSM_CONTROL_VISIBILITY__NAME;

	/**
	 * The number of structural features of the '<em>FSM Control Field Layout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD_LAYOUT_FEATURE_COUNT = FSM_CONTROL_VISIBILITY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD_LAYOUT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_CONTROL_VISIBILITY___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Field Layout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FIELD_LAYOUT_OPERATION_COUNT = FSM_CONTROL_VISIBILITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOAttributeImpl <em>FSM Control DTO Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOAttributeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlDTOAttribute()
	 * @generated
	 */
	int FSM_CONTROL_DTO_ATTRIBUTE = 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO_ATTRIBUTE__NAME = FSM_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE = FSM_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO_ATTRIBUTE__HAS_EVENT = FSM_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO_ATTRIBUTE__EVENT = FSM_BASE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Is Attached</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO_ATTRIBUTE__IS_ATTACHED = FSM_BASE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Display</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO_ATTRIBUTE__DISPLAY = FSM_BASE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>FSM Control DTO Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO_ATTRIBUTE_FEATURE_COUNT = FSM_BASE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO_ATTRIBUTE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control DTO Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_DTO_ATTRIBUTE_OPERATION_COUNT = FSM_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevice <em>FSM Peripheral Device</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevice
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDevice()
	 * @generated
	 */
	int FSM_PERIPHERAL_DEVICE = 20;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE__NAME = FSM_BASE__NAME;

	/**
	 * The number of structural features of the '<em>FSM Peripheral Device</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_FEATURE_COUNT = FSM_BASE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Peripheral Device</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_OPERATION_COUNT = FSM_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceDisplayImpl <em>FSM Peripheral Device Display</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceDisplayImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDeviceDisplay()
	 * @generated
	 */
	int FSM_PERIPHERAL_DEVICE_DISPLAY = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_DISPLAY__NAME = FSM_PERIPHERAL_DEVICE__NAME;

	/**
	 * The feature id for the '<em><b>Output</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_DISPLAY__OUTPUT = FSM_PERIPHERAL_DEVICE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Peripheral Device Display</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_DISPLAY_FEATURE_COUNT = FSM_PERIPHERAL_DEVICE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_DISPLAY___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_PERIPHERAL_DEVICE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Peripheral Device Display</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_DISPLAY_OPERATION_COUNT = FSM_PERIPHERAL_DEVICE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceLineDisplayImpl <em>FSM Peripheral Device Line Display</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceLineDisplayImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDeviceLineDisplay()
	 * @generated
	 */
	int FSM_PERIPHERAL_DEVICE_LINE_DISPLAY = 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_LINE_DISPLAY__NAME = FSM_PERIPHERAL_DEVICE__NAME;

	/**
	 * The number of structural features of the '<em>FSM Peripheral Device Line Display</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_LINE_DISPLAY_FEATURE_COUNT = FSM_PERIPHERAL_DEVICE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_LINE_DISPLAY___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_PERIPHERAL_DEVICE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Peripheral Device Line Display</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_LINE_DISPLAY_OPERATION_COUNT = FSM_PERIPHERAL_DEVICE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDevicePOSPrinterImpl <em>FSM Peripheral Device POS Printer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDevicePOSPrinterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDevicePOSPrinter()
	 * @generated
	 */
	int FSM_PERIPHERAL_DEVICE_POS_PRINTER = 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_POS_PRINTER__NAME = FSM_PERIPHERAL_DEVICE__NAME;

	/**
	 * The number of structural features of the '<em>FSM Peripheral Device POS Printer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_POS_PRINTER_FEATURE_COUNT = FSM_PERIPHERAL_DEVICE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_POS_PRINTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_PERIPHERAL_DEVICE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Peripheral Device POS Printer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_POS_PRINTER_OPERATION_COUNT = FSM_PERIPHERAL_DEVICE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceCashDrawerImpl <em>FSM Peripheral Device Cash Drawer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceCashDrawerImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDeviceCashDrawer()
	 * @generated
	 */
	int FSM_PERIPHERAL_DEVICE_CASH_DRAWER = 24;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_CASH_DRAWER__NAME = FSM_PERIPHERAL_DEVICE__NAME;

	/**
	 * The number of structural features of the '<em>FSM Peripheral Device Cash Drawer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_CASH_DRAWER_FEATURE_COUNT = FSM_PERIPHERAL_DEVICE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_CASH_DRAWER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_PERIPHERAL_DEVICE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Peripheral Device Cash Drawer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_CASH_DRAWER_OPERATION_COUNT = FSM_PERIPHERAL_DEVICE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDevicePTImpl <em>FSM Peripheral Device PT</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDevicePTImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDevicePT()
	 * @generated
	 */
	int FSM_PERIPHERAL_DEVICE_PT = 25;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_PT__NAME = FSM_PERIPHERAL_DEVICE__NAME;

	/**
	 * The number of structural features of the '<em>FSM Peripheral Device PT</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_PT_FEATURE_COUNT = FSM_PERIPHERAL_DEVICE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_PT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_PERIPHERAL_DEVICE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Peripheral Device PT</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_PT_OPERATION_COUNT = FSM_PERIPHERAL_DEVICE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceSignatureImpl <em>FSM Peripheral Device Signature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceSignatureImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDeviceSignature()
	 * @generated
	 */
	int FSM_PERIPHERAL_DEVICE_SIGNATURE = 26;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_SIGNATURE__NAME = FSM_PERIPHERAL_DEVICE__NAME;

	/**
	 * The number of structural features of the '<em>FSM Peripheral Device Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_SIGNATURE_FEATURE_COUNT = FSM_PERIPHERAL_DEVICE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_SIGNATURE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_PERIPHERAL_DEVICE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Peripheral Device Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_SIGNATURE_OPERATION_COUNT = FSM_PERIPHERAL_DEVICE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceScaleImpl <em>FSM Peripheral Device Scale</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceScaleImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDeviceScale()
	 * @generated
	 */
	int FSM_PERIPHERAL_DEVICE_SCALE = 27;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_SCALE__NAME = FSM_PERIPHERAL_DEVICE__NAME;

	/**
	 * The number of structural features of the '<em>FSM Peripheral Device Scale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_SCALE_FEATURE_COUNT = FSM_PERIPHERAL_DEVICE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_SCALE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_PERIPHERAL_DEVICE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Peripheral Device Scale</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_PERIPHERAL_DEVICE_SCALE_OPERATION_COUNT = FSM_PERIPHERAL_DEVICE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlSchedulerAttributeImpl <em>FSM Control Scheduler Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlSchedulerAttributeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlSchedulerAttribute()
	 * @generated
	 */
	int FSM_CONTROL_SCHEDULER_ATTRIBUTE = 28;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_SCHEDULER_ATTRIBUTE__NAME = FSM_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_SCHEDULER_ATTRIBUTE__DELAY = FSM_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_SCHEDULER_ATTRIBUTE__EVENT = FSM_BASE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Control Scheduler Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_SCHEDULER_ATTRIBUTE_FEATURE_COUNT = FSM_BASE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_SCHEDULER_ATTRIBUTE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Scheduler Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_SCHEDULER_ATTRIBUTE_OPERATION_COUNT = FSM_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMEventImpl <em>FSM Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMEventImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMEvent()
	 * @generated
	 */
	int FSM_EVENT = 29;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_EVENT__NAME = FSM_BASE__NAME;

	/**
	 * The number of structural features of the '<em>FSM Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_EVENT_FEATURE_COUNT = FSM_BASE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_EVENT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_EVENT_OPERATION_COUNT = FSM_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStateImpl <em>FSM State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMStateImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMState()
	 * @generated
	 */
	int FSM_STATE = 30;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__NAME = FSM_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Triggers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__TRIGGERS = FSM_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__CONDITIONS = FSM_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Identity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__IDENTITY = FSM_BASE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Keystroke</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__KEYSTROKE = FSM_BASE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Has Key Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__HAS_KEY_OPERATION = FSM_BASE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Key Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__KEY_OPERATION = FSM_BASE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Key Mapper</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__KEY_MAPPER = FSM_BASE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>FSM State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE_FEATURE_COUNT = FSM_BASE_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE_OPERATION_COUNT = FSM_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMKeyMapperImpl <em>FSM Key Mapper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMKeyMapperImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMKeyMapper()
	 * @generated
	 */
	int FSM_KEY_MAPPER = 31;

	/**
	 * The feature id for the '<em><b>Key Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_KEY_MAPPER__KEY_CODE = FSM_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Key Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_KEY_MAPPER__KEY_EVENT = FSM_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Key Mapper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_KEY_MAPPER_FEATURE_COUNT = FSM_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_KEY_MAPPER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Key Mapper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_KEY_MAPPER_OPERATION_COUNT = FSM_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMTriggerImpl <em>FSM Trigger</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMTriggerImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMTrigger()
	 * @generated
	 */
	int FSM_TRIGGER = 32;

	/**
	 * The feature id for the '<em><b>Has Transition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRIGGER__HAS_TRANSITION = FSM_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRIGGER__TRANSITION = FSM_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Triggers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRIGGER__TRIGGERS = FSM_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Guards</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRIGGER__GUARDS = FSM_LAZY_RESOLVER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRIGGER__ACTIONS = FSM_LAZY_RESOLVER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>FSM Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRIGGER_FEATURE_COUNT = FSM_LAZY_RESOLVER_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRIGGER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRIGGER_OPERATION_COUNT = FSM_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMAction <em>FSM Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMAction
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMAction()
	 * @generated
	 */
	int FSM_ACTION = 33;

	/**
	 * The number of structural features of the '<em>FSM Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FEATURE_COUNT = FSM_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_OPERATION_COUNT = FSM_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralBlinkRateImpl <em>FSM Action Peripheral Blink Rate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralBlinkRateImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralBlinkRate()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_BLINK_RATE = 34;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_BLINK_RATE__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Blink Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_BLINK_RATE__BLINK_RATE = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Blink Rate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_BLINK_RATE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_BLINK_RATE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Blink Rate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_BLINK_RATE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralClearImpl <em>FSM Action Peripheral Clear</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralClearImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralClear()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_CLEAR = 35;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CLEAR__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Clear</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CLEAR_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CLEAR___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Clear</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CLEAR_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCreateWindowImpl <em>FSM Action Peripheral Create Window</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCreateWindowImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralCreateWindow()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_CREATE_WINDOW = 36;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CREATE_WINDOW__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Viewport Row</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_ROW = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Viewport Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_COLUMN = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Viewport Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_HEIGHT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Viewport Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_WIDTH = FSM_ACTION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Window Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_HEIGHT = FSM_ACTION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Window Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_WIDTH = FSM_ACTION_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Create Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CREATE_WINDOW_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CREATE_WINDOW___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Create Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CREATE_WINDOW_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCursorTypeImpl <em>FSM Action Peripheral Cursor Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCursorTypeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralCursorType()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_CURSOR_TYPE = 37;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CURSOR_TYPE__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cursor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CURSOR_TYPE__CURSOR_TYPE = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Cursor Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CURSOR_TYPE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CURSOR_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Cursor Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_CURSOR_TYPE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDestroyWindowImpl <em>FSM Action Peripheral Destroy Window</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDestroyWindowImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralDestroyWindow()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_DESTROY_WINDOW = 38;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DESTROY_WINDOW__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Destroy Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DESTROY_WINDOW_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DESTROY_WINDOW___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Destroy Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DESTROY_WINDOW_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDeviceBrightnessImpl <em>FSM Action Peripheral Device Brightness</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDeviceBrightnessImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralDeviceBrightness()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS = 39;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Brightness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS__BRIGHTNESS = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Device Brightness</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Device Brightness</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDisplayTextImpl <em>FSM Action Peripheral Display Text</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDisplayTextImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralDisplayText()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_DISPLAY_TEXT = 40;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__ATTRIBUTE = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Text</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__TEXT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Display Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DISPLAY_TEXT_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DISPLAY_TEXT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Display Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_DISPLAY_TEXT_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralLineDisplayTextImpl <em>FSM Action Peripheral Line Display Text</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralLineDisplayTextImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralLineDisplayText()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT = 41;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Text</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__TEXT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Has Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__HAS_TYPE = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Text Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__TEXT_TYPE = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Line Display Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Line Display Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralLineDisplayTextAtImpl <em>FSM Action Peripheral Line Display Text At</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralLineDisplayTextAtImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralLineDisplayTextAt()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT = 42;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Row</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__ROW = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__COLUMN = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Text</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__TEXT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Has Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__HAS_TYPE = FSM_ACTION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Text Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__TEXT_TYPE = FSM_ACTION_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Line Display Text At</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Line Display Text At</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralInterCharacterWaitImpl <em>FSM Action Peripheral Inter Character Wait</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralInterCharacterWaitImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralInterCharacterWait()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT = 43;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Wait</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT__WAIT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Inter Character Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Inter Character Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeFormatImpl <em>FSM Action Peripheral Marquee Format</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeFormatImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralMarqueeFormat()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT = 44;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT__FORMAT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Marquee Format</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Marquee Format</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeRepeatWaitImpl <em>FSM Action Peripheral Marquee Repeat Wait</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeRepeatWaitImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralMarqueeRepeatWait()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT = 45;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Wait</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT__WAIT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Marquee Repeat Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Marquee Repeat Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeTypeImpl <em>FSM Action Peripheral Marquee Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeTypeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralMarqueeType()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_TYPE = 46;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Marquee Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__MARQUEE_TYPE = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Marquee Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_TYPE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Marquee Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_TYPE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeUnitWaitImpl <em>FSM Action Peripheral Marquee Unit Wait</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeUnitWaitImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralMarqueeUnitWait()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT = 47;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Wait</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT__WAIT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Marquee Unit Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Marquee Unit Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScrollImpl <em>FSM Action Peripheral Scroll</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScrollImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScroll()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SCROLL = 48;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCROLL__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCROLL__DIRECTION = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Units</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCROLL__UNITS = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Scroll</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCROLL_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCROLL___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Scroll</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCROLL_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralOpenDrawerImpl <em>FSM Action Peripheral Open Drawer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralOpenDrawerImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralOpenDrawer()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_OPEN_DRAWER = 49;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_OPEN_DRAWER__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Open Drawer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_OPEN_DRAWER_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_OPEN_DRAWER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Open Drawer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_OPEN_DRAWER_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintBarcodeImpl <em>FSM Action Peripheral Print Barcode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintBarcodeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintBarcode()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BARCODE = 50;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BARCODE__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BARCODE__DATA = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Barcode Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BARCODE__BARCODE_TYPE = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Print Barcode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BARCODE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BARCODE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Print Barcode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BARCODE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintBitmapImpl <em>FSM Action Peripheral Print Bitmap</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintBitmapImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintBitmap()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BITMAP = 51;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BITMAP__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bitmap Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BITMAP__BITMAP_ID = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Print Bitmap</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BITMAP_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BITMAP___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Print Bitmap</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_BITMAP_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintCutImpl <em>FSM Action Peripheral Print Cut</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintCutImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintCut()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_CUT = 52;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_CUT__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Text</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_CUT__TEXT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Print Cut</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_CUT_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_CUT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Print Cut</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_CUT_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintRegisterBitmapImpl <em>FSM Action Peripheral Print Register Bitmap</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintRegisterBitmapImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintRegisterBitmap()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP = 53;

	/**
	 * The feature id for the '<em><b>Bitmap Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP__BITMAP_ID = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP__NAME = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Print Register Bitmap</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Print Register Bitmap</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintNormalImpl <em>FSM Action Peripheral Print Normal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintNormalImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintNormal()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_NORMAL = 54;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_NORMAL__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Text</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_NORMAL__TEXT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Barcode Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_NORMAL__BARCODE_TYPE = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Print Normal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_NORMAL_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_NORMAL___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Print Normal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_NORMAL_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTOpenImpl <em>FSM Action Peripheral PT Open</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTOpenImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTOpen()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PT_OPEN = 55;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_OPEN__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_OPEN__HOST = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_OPEN__PORT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral PT Open</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_OPEN_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_OPEN___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral PT Open</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_OPEN_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTCloseImpl <em>FSM Action Peripheral PT Close</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTCloseImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTClose()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PT_CLOSE = 56;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_CLOSE__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral PT Close</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_CLOSE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_CLOSE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral PT Close</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_CLOSE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTReversalImpl <em>FSM Action Peripheral PT Reversal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTReversalImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTReversal()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PT_REVERSAL = 57;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REVERSAL__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Password</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Receipt</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral PT Reversal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REVERSAL_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REVERSAL___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral PT Reversal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REVERSAL_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTAcknowledgeImpl <em>FSM Action Peripheral PT Acknowledge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTAcknowledgeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTAcknowledge()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE = 58;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral PT Acknowledge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral PT Acknowledge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTRegistrationImpl <em>FSM Action Peripheral PT Registration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTRegistrationImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTRegistration()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PT_REGISTRATION = 59;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REGISTRATION__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Password</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REGISTRATION__PASSWORD = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REGISTRATION__CONFIGURATION = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral PT Registration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REGISTRATION_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REGISTRATION___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral PT Registration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_REGISTRATION_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTAuthorizationImpl <em>FSM Action Peripheral PT Authorization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTAuthorizationImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTAuthorization()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION = 60;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Amount</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION__AMOUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral PT Authorization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral PT Authorization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralBeeperImpl <em>FSM Action Peripheral Beeper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralBeeperImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralBeeper()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_BEEPER = 61;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_BEEPER__DURATION = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_BEEPER__FREQUENCY = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Beeper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_BEEPER_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_BEEPER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Beeper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_BEEPER_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPlayerImpl <em>FSM Action Peripheral Player</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPlayerImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPlayer()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PLAYER = 62;

	/**
	 * The feature id for the '<em><b>Tune</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PLAYER__TUNE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Player</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PLAYER_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PLAYER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Player</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PLAYER_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSoundImpl <em>FSM Action Peripheral Sound</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSoundImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSound()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SOUND = 63;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SOUND__NAME = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Sound</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SOUND_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SOUND___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Sound</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SOUND_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSource <em>FSM Action Field Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSource
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSource()
	 * @generated
	 */
	int FSM_ACTION_FIELD_SOURCE = 79;

	/**
	 * The number of structural features of the '<em>FSM Action Field Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTResponseImpl <em>FSM Action Peripheral PT Response</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTResponseImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTResponse()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PT_RESPONSE = 64;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_RESPONSE__DEVICE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral PT Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_RESPONSE_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_RESPONSE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral PT Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PT_RESPONSE_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintReportImpl <em>FSM Action Peripheral Print Report</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintReportImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintReport()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REPORT = 65;

	/**
	 * The feature id for the '<em><b>Report</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REPORT__REPORT = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REPORT__KEY = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Has Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REPORT__HAS_FILTER = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Has Print Service</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REPORT__HAS_PRINT_SERVICE = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Print Service</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REPORT__PRINT_SERVICE = FSM_ACTION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Print Report</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REPORT_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REPORT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Print Report</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_PRINT_REPORT_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureOpenImpl <em>FSM Action Peripheral Signature Open</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureOpenImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureOpen()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN = 66;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Signature Open</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Signature Open</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureCloseImpl <em>FSM Action Peripheral Signature Close</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureCloseImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureClose()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE = 67;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Signature Close</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Signature Close</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureClearImpl <em>FSM Action Peripheral Signature Clear</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureClearImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureClear()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR = 68;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Signature Clear</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Signature Clear</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureCaptureImpl <em>FSM Action Peripheral Signature Capture</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureCaptureImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureCapture()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE = 69;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Signature Capture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Signature Capture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureIdleImpl <em>FSM Action Peripheral Signature Idle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureIdleImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureIdle()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE = 70;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE__DEVICE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Signature Idle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Signature Idle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureLabelImpl <em>FSM Action Peripheral Signature Label</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureLabelImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureLabel()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL = 71;

	/**
	 * The feature id for the '<em><b>Ok Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__OK_LABEL = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Clear Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__CLEAR_LABEL = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Cancel Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__CANCEL_LABEL = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__DEVICE = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Signature Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Signature Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMSignatureRetrieveImpl <em>FSM Signature Retrieve</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMSignatureRetrieveImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMSignatureRetrieve()
	 * @generated
	 */
	int FSM_SIGNATURE_RETRIEVE = 72;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_SIGNATURE_RETRIEVE__DEVICE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Signature Retrieve</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_SIGNATURE_RETRIEVE_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_SIGNATURE_RETRIEVE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Signature Retrieve</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_SIGNATURE_RETRIEVE_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleReadWeightImpl <em>FSM Action Peripheral Scale Read Weight</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleReadWeightImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleReadWeight()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT = 73;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT__DEVICE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Scale Read Weight</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Scale Read Weight</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleReadTareWeightImpl <em>FSM Action Peripheral Scale Read Tare Weight</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleReadTareWeightImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleReadTareWeight()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT = 74;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT__DEVICE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Scale Read Tare Weight</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Scale Read Tare Weight</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleTareWeightImpl <em>FSM Action Peripheral Scale Tare Weight</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleTareWeightImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleTareWeight()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT = 75;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT__DEVICE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT__VALUE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Scale Tare Weight</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Scale Tare Weight</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleZeroImpl <em>FSM Action Peripheral Scale Zero</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleZeroImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleZero()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_ZERO = 76;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_ZERO__DEVICE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Scale Zero</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_ZERO_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_ZERO___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Scale Zero</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_ZERO_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleDisplayTextImpl <em>FSM Action Peripheral Scale Display Text</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleDisplayTextImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleDisplayText()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT = 77;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT__DEVICE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Text</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT__TEXT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Scale Display Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Scale Display Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleWeightUnitImpl <em>FSM Action Peripheral Scale Weight Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleWeightUnitImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleWeightUnit()
	 * @generated
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT = 78;

	/**
	 * The feature id for the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT__DEVICE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Peripheral Scale Weight Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Peripheral Scale Weight Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceStringImpl <em>FSM Action Field Source String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceStringImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceString()
	 * @generated
	 */
	int FSM_ACTION_FIELD_SOURCE_STRING = 80;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_STRING__TEXT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Field Source String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_STRING_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_STRING___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Source String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_STRING_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceNumberImpl <em>FSM Action Field Source Number</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceNumberImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceNumber()
	 * @generated
	 */
	int FSM_ACTION_FIELD_SOURCE_NUMBER = 81;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_NUMBER__VALUE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Field Source Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_NUMBER_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_NUMBER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Source Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_NUMBER_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceIntegerImpl <em>FSM Action Field Source Integer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceIntegerImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceInteger()
	 * @generated
	 */
	int FSM_ACTION_FIELD_SOURCE_INTEGER = 82;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_INTEGER__VALUE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Field Source Integer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_INTEGER_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_INTEGER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Source Integer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_INTEGER_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceBooleanImpl <em>FSM Action Field Source Boolean</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceBooleanImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceBoolean()
	 * @generated
	 */
	int FSM_ACTION_FIELD_SOURCE_BOOLEAN = 83;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_BOOLEAN__VALUE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Field Source Boolean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_BOOLEAN_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_BOOLEAN___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Source Boolean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_BOOLEAN_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceEvaluateImpl <em>FSM Action Field Source Evaluate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceEvaluateImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceEvaluate()
	 * @generated
	 */
	int FSM_ACTION_FIELD_SOURCE_EVALUATE = 84;

	/**
	 * The feature id for the '<em><b>Evaluationtype</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_EVALUATE__EVALUATIONTYPE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Field Source Evaluate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_EVALUATE_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_EVALUATE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Source Evaluate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_EVALUATE_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceTranslateImpl <em>FSM Action Field Source Translate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceTranslateImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceTranslate()
	 * @generated
	 */
	int FSM_ACTION_FIELD_SOURCE_TRANSLATE = 85;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_TRANSLATE__TEXT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Field Source Translate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_TRANSLATE_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_TRANSLATE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Source Translate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_TRANSLATE_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMRef <em>FSM Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMRef
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMRef()
	 * @generated
	 */
	int FSM_REF = 86;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_REF__NAME = FSM_BASE__NAME;

	/**
	 * The number of structural features of the '<em>FSM Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_REF_FEATURE_COUNT = FSM_BASE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_REF___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_REF_OPERATION_COUNT = FSM_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMDotExpressionImpl <em>FSM Dot Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMDotExpressionImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMDotExpression()
	 * @generated
	 */
	int FSM_DOT_EXPRESSION = 87;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_DOT_EXPRESSION__NAME = FSM_REF__NAME;

	/**
	 * The feature id for the '<em><b>Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_DOT_EXPRESSION__REF = FSM_REF_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_DOT_EXPRESSION__TAIL = FSM_REF_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Dot Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_DOT_EXPRESSION_FEATURE_COUNT = FSM_REF_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_DOT_EXPRESSION___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_REF___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Dot Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_DOT_EXPRESSION_OPERATION_COUNT = FSM_REF_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMDtoRefImpl <em>FSM Dto Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMDtoRefImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMDtoRef()
	 * @generated
	 */
	int FSM_DTO_REF = 88;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_DTO_REF__NAME = FSM_REF__NAME;

	/**
	 * The feature id for the '<em><b>Dto</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_DTO_REF__DTO = FSM_REF_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Dto Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_DTO_REF_FEATURE_COUNT = FSM_REF_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_DTO_REF___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_REF___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Dto Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_DTO_REF_OPERATION_COUNT = FSM_REF_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceDtoAttributeImpl <em>FSM Action Field Source Dto Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceDtoAttributeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceDtoAttribute()
	 * @generated
	 */
	int FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE = 89;

	/**
	 * The feature id for the '<em><b>Dto</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE__DTO = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE__ATTRIBUTE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Field Source Dto Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Source Dto Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceEventImpl <em>FSM Action Field Source Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceEventImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceEvent()
	 * @generated
	 */
	int FSM_ACTION_FIELD_SOURCE_EVENT = 90;

	/**
	 * The number of structural features of the '<em>FSM Action Field Source Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_EVENT_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_EVENT___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Source Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SOURCE_EVENT_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionConditionalTransitionImpl <em>FSM Action Conditional Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionConditionalTransitionImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionConditionalTransition()
	 * @generated
	 */
	int FSM_ACTION_CONDITIONAL_TRANSITION = 91;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_CONDITIONAL_TRANSITION__TRANSITION = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_CONDITIONAL_TRANSITION__GUARD = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_CONDITIONAL_TRANSITION__ACTIONS = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Action Conditional Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_CONDITIONAL_TRANSITION_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_CONDITIONAL_TRANSITION___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Conditional Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_CONDITIONAL_TRANSITION_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMOperationParameterImpl <em>FSM Operation Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMOperationParameterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMOperationParameter()
	 * @generated
	 */
	int FSM_OPERATION_PARAMETER = 92;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION_PARAMETER__SOURCE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Operation Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION_PARAMETER_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION_PARAMETER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Operation Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION_PARAMETER_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMOperationImpl <em>FSM Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMOperationImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMOperation()
	 * @generated
	 */
	int FSM_OPERATION = 93;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION__GROUP = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION__OPERATION = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION__FIRST = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>More</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION__MORE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>FSM Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMGuardImpl <em>FSM Guard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMGuardImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMGuard()
	 * @generated
	 */
	int FSM_GUARD = 94;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_GUARD__GROUP = FSM_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_GUARD__GUARD = FSM_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Has On Fail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_GUARD__HAS_ON_FAIL = FSM_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>On Fail Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_GUARD__ON_FAIL_DESCRIPTION = FSM_LAZY_RESOLVER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>On Fail Caption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_GUARD__ON_FAIL_CAPTION = FSM_LAZY_RESOLVER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>On Fail Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_GUARD__ON_FAIL_TYPE = FSM_LAZY_RESOLVER_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>FSM Guard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_GUARD_FEATURE_COUNT = FSM_LAZY_RESOLVER_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_GUARD___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Guard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_GUARD_OPERATION_COUNT = FSM_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMFunctionImpl <em>FSM Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMFunctionImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMFunction()
	 * @generated
	 */
	int FSM_FUNCTION = 95;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FUNCTION__GROUP = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FUNCTION__FUNCTION = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FUNCTION__FIRST = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>More</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FUNCTION__MORE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>FSM Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FUNCTION_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FUNCTION___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FUNCTION_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStorageRetrieveImpl <em>FSM Storage Retrieve</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMStorageRetrieveImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMStorageRetrieve()
	 * @generated
	 */
	int FSM_STORAGE_RETRIEVE = 96;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STORAGE_RETRIEVE__KEY = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STORAGE_RETRIEVE__ATTRIBUTE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Storage Retrieve</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STORAGE_RETRIEVE_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STORAGE_RETRIEVE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Storage Retrieve</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STORAGE_RETRIEVE_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStorageImpl <em>FSM Storage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMStorageImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMStorage()
	 * @generated
	 */
	int FSM_STORAGE = 97;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STORAGE__KEY = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STORAGE__ATTRIBUTE = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STORAGE__CONTENT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Storage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STORAGE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STORAGE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Storage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STORAGE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldConcatenationImpl <em>FSM Action Field Concatenation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldConcatenationImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldConcatenation()
	 * @generated
	 */
	int FSM_ACTION_FIELD_CONCATENATION = 98;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_CONCATENATION__FIRST = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>More</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_CONCATENATION__MORE = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Field Concatenation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_CONCATENATION_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_CONCATENATION___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Concatenation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_CONCATENATION_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSetImpl <em>FSM Action Field Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSetImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSet()
	 * @generated
	 */
	int FSM_ACTION_FIELD_SET = 99;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SET__ATTRIBUTE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SET__SOURCE = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Field Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SET_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SET___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_SET_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldKeystrokeImpl <em>FSM Action Field Keystroke</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldKeystrokeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldKeystroke()
	 * @generated
	 */
	int FSM_ACTION_FIELD_KEYSTROKE = 100;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_KEYSTROKE__ATTRIBUTE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Keystroke</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_KEYSTROKE__KEYSTROKE = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Field Keystroke</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_KEYSTROKE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_KEYSTROKE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Keystroke</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_KEYSTROKE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldClearImpl <em>FSM Action Field Clear</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldClearImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldClear()
	 * @generated
	 */
	int FSM_ACTION_FIELD_CLEAR = 101;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_CLEAR__ATTRIBUTE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Field Clear</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_CLEAR_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_CLEAR___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Clear</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_CLEAR_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldGetImpl <em>FSM Action Field Get</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldGetImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldGet()
	 * @generated
	 */
	int FSM_ACTION_FIELD_GET = 102;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_GET__ATTRIBUTE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Field Get</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_GET_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_GET___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Get</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_GET_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldFilterToggleImpl <em>FSM Action Field Filter Toggle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldFilterToggleImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldFilterToggle()
	 * @generated
	 */
	int FSM_ACTION_FIELD_FILTER_TOGGLE = 103;

	/**
	 * The feature id for the '<em><b>Filter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_FILTER_TOGGLE__FILTER = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Field Filter Toggle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_FILTER_TOGGLE_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_FILTER_TOGGLE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Filter Toggle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_FILTER_TOGGLE_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldRemoveImpl <em>FSM Action Field Remove</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldRemoveImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldRemove()
	 * @generated
	 */
	int FSM_ACTION_FIELD_REMOVE = 104;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_REMOVE__ATTRIBUTE = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Field Remove</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_REMOVE_FEATURE_COUNT = FSM_ACTION_FIELD_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_REMOVE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION_FIELD_SOURCE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Field Remove</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_FIELD_REMOVE_OPERATION_COUNT = FSM_ACTION_FIELD_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionItemVisibleImpl <em>FSM Action Item Visible</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionItemVisibleImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionItemVisible()
	 * @generated
	 */
	int FSM_ACTION_ITEM_VISIBLE = 105;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_ITEM_VISIBLE__ATTRIBUTE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Item Visible</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_ITEM_VISIBLE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_ITEM_VISIBLE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Item Visible</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_ITEM_VISIBLE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionItemInvisibleImpl <em>FSM Action Item Invisible</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionItemInvisibleImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionItemInvisible()
	 * @generated
	 */
	int FSM_ACTION_ITEM_INVISIBLE = 106;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_ITEM_INVISIBLE__ATTRIBUTE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Item Invisible</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_ITEM_INVISIBLE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_ITEM_INVISIBLE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Item Invisible</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_ITEM_INVISIBLE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionButtonCaptionImpl <em>FSM Action Button Caption</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionButtonCaptionImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionButtonCaption()
	 * @generated
	 */
	int FSM_ACTION_BUTTON_CAPTION = 107;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_BUTTON_CAPTION__ATTRIBUTE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Caption</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_BUTTON_CAPTION__CAPTION = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Button Caption</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_BUTTON_CAPTION_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_BUTTON_CAPTION___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Button Caption</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_BUTTON_CAPTION_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionButtonImageImpl <em>FSM Action Button Image</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionButtonImageImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionButtonImage()
	 * @generated
	 */
	int FSM_ACTION_BUTTON_IMAGE = 108;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_BUTTON_IMAGE__ATTRIBUTE = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_BUTTON_IMAGE__IMAGE = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Action Button Image</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_BUTTON_IMAGE_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_BUTTON_IMAGE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Button Image</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_BUTTON_IMAGE_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionDTOFindImpl <em>FSM Action DTO Find</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionDTOFindImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionDTOFind()
	 * @generated
	 */
	int FSM_ACTION_DTO_FIND = 109;

	/**
	 * The feature id for the '<em><b>Dto</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_DTO_FIND__DTO = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_DTO_FIND__ATTRIBUTE = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Search</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_DTO_FIND__SEARCH = FSM_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Action DTO Find</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_DTO_FIND_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_DTO_FIND___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action DTO Find</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_DTO_FIND_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionDTOClearImpl <em>FSM Action DTO Clear</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionDTOClearImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionDTOClear()
	 * @generated
	 */
	int FSM_ACTION_DTO_CLEAR = 110;

	/**
	 * The feature id for the '<em><b>Dto</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_DTO_CLEAR__DTO = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action DTO Clear</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_DTO_CLEAR_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_DTO_CLEAR___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action DTO Clear</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_DTO_CLEAR_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionSchedulerImpl <em>FSM Action Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionSchedulerImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionScheduler()
	 * @generated
	 */
	int FSM_ACTION_SCHEDULER = 111;

	/**
	 * The feature id for the '<em><b>Scheduler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_SCHEDULER__SCHEDULER = FSM_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Action Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_SCHEDULER_FEATURE_COUNT = FSM_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_SCHEDULER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ACTION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Action Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ACTION_SCHEDULER_OPERATION_COUNT = FSM_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMDTOTypeImpl <em>FSMDTO Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMDTOTypeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMDTOType()
	 * @generated
	 */
	int FSMDTO_TYPE = 112;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSMDTO_TYPE__ATTRIBUTE_TYPE = FSM_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSMDTO Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSMDTO_TYPE_FEATURE_COUNT = FSM_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSMDTO_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSMDTO Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSMDTO_TYPE_OPERATION_COUNT = FSM_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMFieldTypeImpl <em>FSM Field Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMFieldTypeImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMFieldType()
	 * @generated
	 */
	int FSM_FIELD_TYPE = 113;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FIELD_TYPE__ATTRIBUTE_TYPE = FSM_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Field Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FIELD_TYPE_FEATURE_COUNT = FSM_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FIELD_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Field Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FIELD_TYPE_OPERATION_COUNT = FSM_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMAbstractFilter <em>FSM Abstract Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMAbstractFilter
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMAbstractFilter()
	 * @generated
	 */
	int FSM_ABSTRACT_FILTER = 114;

	/**
	 * The number of structural features of the '<em>FSM Abstract Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ABSTRACT_FILTER_FEATURE_COUNT = FSM_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ABSTRACT_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Abstract Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_ABSTRACT_FILTER_OPERATION_COUNT = FSM_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMFilterPropertyImpl <em>FSM Filter Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMFilterPropertyImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMFilterProperty()
	 * @generated
	 */
	int FSM_FILTER_PROPERTY = 115;

	/**
	 * The feature id for the '<em><b>Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FILTER_PROPERTY__PATH = FSM_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Filter Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FILTER_PROPERTY_FEATURE_COUNT = FSM_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FILTER_PROPERTY___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Filter Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FILTER_PROPERTY_OPERATION_COUNT = FSM_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMFilterImpl <em>FSM Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMFilterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMFilter()
	 * @generated
	 */
	int FSM_FILTER = 116;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FILTER__SOURCE = 0;

	/**
	 * The number of structural features of the '<em>FSM Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FILTER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>FSM Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FILTER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMJunctionFilterImpl <em>FSM Junction Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMJunctionFilterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMJunctionFilter()
	 * @generated
	 */
	int FSM_JUNCTION_FILTER = 117;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_JUNCTION_FILTER__FIRST = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>More</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_JUNCTION_FILTER__MORE = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM Junction Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_JUNCTION_FILTER_FEATURE_COUNT = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_JUNCTION_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ABSTRACT_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Junction Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_JUNCTION_FILTER_OPERATION_COUNT = FSM_ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMAndFilterImpl <em>FSM And Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMAndFilterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMAndFilter()
	 * @generated
	 */
	int FSM_AND_FILTER = 118;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_AND_FILTER__FIRST = FSM_JUNCTION_FILTER__FIRST;

	/**
	 * The feature id for the '<em><b>More</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_AND_FILTER__MORE = FSM_JUNCTION_FILTER__MORE;

	/**
	 * The number of structural features of the '<em>FSM And Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_AND_FILTER_FEATURE_COUNT = FSM_JUNCTION_FILTER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_AND_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_JUNCTION_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM And Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_AND_FILTER_OPERATION_COUNT = FSM_JUNCTION_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMOrFilterImpl <em>FSM Or Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMOrFilterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMOrFilter()
	 * @generated
	 */
	int FSM_OR_FILTER = 119;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OR_FILTER__FIRST = FSM_JUNCTION_FILTER__FIRST;

	/**
	 * The feature id for the '<em><b>More</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OR_FILTER__MORE = FSM_JUNCTION_FILTER__MORE;

	/**
	 * The number of structural features of the '<em>FSM Or Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OR_FILTER_FEATURE_COUNT = FSM_JUNCTION_FILTER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OR_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_JUNCTION_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Or Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OR_FILTER_OPERATION_COUNT = FSM_JUNCTION_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMBetweenFilterImpl <em>FSM Between Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMBetweenFilterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMBetweenFilter()
	 * @generated
	 */
	int FSM_BETWEEN_FILTER = 120;

	/**
	 * The feature id for the '<em><b>Property Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_BETWEEN_FILTER__PROPERTY_ID = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_BETWEEN_FILTER__START = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>End</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_BETWEEN_FILTER__END = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Between Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_BETWEEN_FILTER_FEATURE_COUNT = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_BETWEEN_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ABSTRACT_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Between Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_BETWEEN_FILTER_OPERATION_COUNT = FSM_ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMCompareFilterImpl <em>FSM Compare Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMCompareFilterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMCompareFilter()
	 * @generated
	 */
	int FSM_COMPARE_FILTER = 121;

	/**
	 * The feature id for the '<em><b>Property Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_COMPARE_FILTER__PROPERTY_ID = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_COMPARE_FILTER__OPERAND = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_COMPARE_FILTER__OPERATION = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Compare Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_COMPARE_FILTER_FEATURE_COUNT = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_COMPARE_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ABSTRACT_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Compare Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_COMPARE_FILTER_OPERATION_COUNT = FSM_ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMIsNullFilterImpl <em>FSM Is Null Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMIsNullFilterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMIsNullFilter()
	 * @generated
	 */
	int FSM_IS_NULL_FILTER = 122;

	/**
	 * The feature id for the '<em><b>Property Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_IS_NULL_FILTER__PROPERTY_ID = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Is Null Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_IS_NULL_FILTER_FEATURE_COUNT = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_IS_NULL_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ABSTRACT_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Is Null Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_IS_NULL_FILTER_OPERATION_COUNT = FSM_ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMLikeFilterImpl <em>FSM Like Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMLikeFilterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLikeFilter()
	 * @generated
	 */
	int FSM_LIKE_FILTER = 123;

	/**
	 * The feature id for the '<em><b>Property Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_LIKE_FILTER__PROPERTY_ID = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_LIKE_FILTER__VALUE = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ignore Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_LIKE_FILTER__IGNORE_CASE = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>FSM Like Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_LIKE_FILTER_FEATURE_COUNT = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_LIKE_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ABSTRACT_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Like Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_LIKE_FILTER_OPERATION_COUNT = FSM_ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMNotFilterImpl <em>FSM Not Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMNotFilterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMNotFilter()
	 * @generated
	 */
	int FSM_NOT_FILTER = 124;

	/**
	 * The feature id for the '<em><b>Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_NOT_FILTER__FILTER = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Not Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_NOT_FILTER_FEATURE_COUNT = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_NOT_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ABSTRACT_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Not Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_NOT_FILTER_OPERATION_COUNT = FSM_ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStringFilterImpl <em>FSM String Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMStringFilterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMStringFilter()
	 * @generated
	 */
	int FSM_STRING_FILTER = 125;

	/**
	 * The feature id for the '<em><b>Property Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STRING_FILTER__PROPERTY_ID = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Filter String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STRING_FILTER__FILTER_STRING = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Only Match Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STRING_FILTER__ONLY_MATCH_PREFIX = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Ignore Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STRING_FILTER__IGNORE_CASE = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>FSM String Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STRING_FILTER_FEATURE_COUNT = FSM_ABSTRACT_FILTER_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STRING_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_ABSTRACT_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM String Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STRING_FILTER_OPERATION_COUNT = FSM_ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlFilterImpl <em>FSM Control Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlFilterImpl
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlFilter()
	 * @generated
	 */
	int FSM_CONTROL_FILTER = 126;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FILTER__NAME = FSM_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FILTER__FILTER = FSM_BASE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Control Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FILTER_FEATURE_COUNT = FSM_BASE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = FSM_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>FSM Control Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_CONTROL_FILTER_OPERATION_COUNT = FSM_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMInternalType <em>FSM Internal Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMInternalType
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMInternalType()
	 * @generated
	 */
	int FSM_INTERNAL_TYPE = 127;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType <em>FSM Control Button Event Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonEventType()
	 * @generated
	 */
	int FSM_CONTROL_BUTTON_EVENT_TYPE = 128;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMCompareOperationEnum <em>FSM Compare Operation Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMCompareOperationEnum
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMCompareOperationEnum()
	 * @generated
	 */
	int FSM_COMPARE_OPERATION_ENUM = 129;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMEvaluationType <em>FSM Evaluation Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMEvaluationType
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMEvaluationType()
	 * @generated
	 */
	int FSM_EVALUATION_TYPE = 130;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMUserMessageType <em>FSM User Message Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMUserMessageType
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMUserMessageType()
	 * @generated
	 */
	int FSM_USER_MESSAGE_TYPE = 131;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayCursorType <em>FSM Line Display Cursor Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayCursorType
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLineDisplayCursorType()
	 * @generated
	 */
	int FSM_LINE_DISPLAY_CURSOR_TYPE = 132;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeType <em>FSM Line Display Marquee Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeType
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLineDisplayMarqueeType()
	 * @generated
	 */
	int FSM_LINE_DISPLAY_MARQUEE_TYPE = 133;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeFormat <em>FSM Line Display Marquee Format</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeFormat
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLineDisplayMarqueeFormat()
	 * @generated
	 */
	int FSM_LINE_DISPLAY_MARQUEE_FORMAT = 134;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayTextType <em>FSM Line Display Text Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayTextType
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLineDisplayTextType()
	 * @generated
	 */
	int FSM_LINE_DISPLAY_TEXT_TYPE = 135;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayScrollTextType <em>FSM Line Display Scroll Text Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayScrollTextType
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLineDisplayScrollTextType()
	 * @generated
	 */
	int FSM_LINE_DISPLAY_SCROLL_TEXT_TYPE = 136;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMPOSPrinterBarcodeType <em>FSMPOS Printer Barcode Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPOSPrinterBarcodeType
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPOSPrinterBarcodeType()
	 * @generated
	 */
	int FSMPOS_PRINTER_BARCODE_TYPE = 137;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.statemachine.FSMFunctionalKeyCodes <em>FSM Functional Key Codes</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFunctionalKeyCodes
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMFunctionalKeyCodes()
	 * @generated
	 */
	int FSM_FUNCTIONAL_KEY_CODES = 138;

	/**
	 * The meta object id for the '<em>Internal EObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getInternalEObject()
	 * @generated
	 */
	int INTERNAL_EOBJECT = 139;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMModel <em>FSM Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Model</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMModel
	 * @generated
	 */
	EClass getFSMModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMModel#getImportSection <em>Import Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import Section</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMModel#getImportSection()
	 * @see #getFSMModel()
	 * @generated
	 */
	EReference getFSMModel_ImportSection();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMModel#getPackages <em>Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Packages</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMModel#getPackages()
	 * @see #getFSMModel()
	 * @generated
	 */
	EReference getFSMModel_Packages();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMLazyResolver <em>FSM Lazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Lazy Resolver</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLazyResolver
	 * @generated
	 */
	EClass getFSMLazyResolver();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.xtext.statemachine.FSMLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject) <em>EResolve Proxy</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>EResolve Proxy</em>' operation.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject)
	 * @generated
	 */
	EOperation getFSMLazyResolver__EResolveProxy__InternalEObject();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMBase <em>FSM Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Base</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMBase
	 * @generated
	 */
	EClass getFSMBase();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMBase#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMBase#getName()
	 * @see #getFSMBase()
	 * @generated
	 */
	EAttribute getFSMBase_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMPackage <em>FSM Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Package</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPackage
	 * @generated
	 */
	EClass getFSMPackage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMPackage#getStatemachines <em>Statemachines</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statemachines</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPackage#getStatemachines()
	 * @see #getFSMPackage()
	 * @generated
	 */
	EReference getFSMPackage_Statemachines();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSM <em>FSM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSM
	 * @generated
	 */
	EClass getFSM();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSM#isDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSM#isDescription()
	 * @see #getFSM()
	 * @generated
	 */
	EAttribute getFSM_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSM#getDescriptionValue <em>Description Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Value</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSM#getDescriptionValue()
	 * @see #getFSM()
	 * @generated
	 */
	EAttribute getFSM_DescriptionValue();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSM#getInitialEvent <em>Initial Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Initial Event</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSM#getInitialEvent()
	 * @see #getFSM()
	 * @generated
	 */
	EReference getFSM_InitialEvent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSM#getInitialState <em>Initial State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Initial State</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSM#getInitialState()
	 * @see #getFSM()
	 * @generated
	 */
	EReference getFSM_InitialState();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSM#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSM#getEvents()
	 * @see #getFSM()
	 * @generated
	 */
	EReference getFSM_Events();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSM#getControls <em>Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Controls</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSM#getControls()
	 * @see #getFSM()
	 * @generated
	 */
	EReference getFSM_Controls();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSM#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSM#getStates()
	 * @see #getFSM()
	 * @generated
	 */
	EReference getFSM_States();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControl <em>FSM Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControl
	 * @generated
	 */
	EClass getFSMControl();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton <em>FSM Control Button</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Button</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButton
	 * @generated
	 */
	EClass getFSMControlButton();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getEventType <em>Event Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButton#getEventType()
	 * @see #getFSMControlButton()
	 * @generated
	 */
	EAttribute getFSMControlButton_EventType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getButtons <em>Buttons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Buttons</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButton#getButtons()
	 * @see #getFSMControlButton()
	 * @generated
	 */
	EReference getFSMControlButton_Buttons();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#isHasRange <em>Has Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Range</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButton#isHasRange()
	 * @see #getFSMControlButton()
	 * @generated
	 */
	EAttribute getFSMControlButton_HasRange();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButton#getStart()
	 * @see #getFSMControlButton()
	 * @generated
	 */
	EAttribute getFSMControlButton_Start();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButton#getEnd()
	 * @see #getFSMControlButton()
	 * @generated
	 */
	EAttribute getFSMControlButton_End();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getRangedName <em>Ranged Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ranged Name</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButton#getRangedName()
	 * @see #getFSMControlButton()
	 * @generated
	 */
	EAttribute getFSMControlButton_RangedName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlField <em>FSM Control Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Field</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlField
	 * @generated
	 */
	EClass getFSMControlField();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlField#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlField#getFields()
	 * @see #getFSMControlField()
	 * @generated
	 */
	EReference getFSMControlField_Fields();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlField#getLayouts <em>Layouts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Layouts</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlField#getLayouts()
	 * @see #getFSMControlField()
	 * @generated
	 */
	EReference getFSMControlField_Layouts();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlDTO <em>FSM Control DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control DTO</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlDTO
	 * @generated
	 */
	EClass getFSMControlDTO();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlDTO#getDtos <em>Dtos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dtos</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlDTO#getDtos()
	 * @see #getFSMControlDTO()
	 * @generated
	 */
	EReference getFSMControlDTO_Dtos();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlDTO#getFilters <em>Filters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Filters</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlDTO#getFilters()
	 * @see #getFSMControlDTO()
	 * @generated
	 */
	EReference getFSMControlDTO_Filters();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlScheduler <em>FSM Control Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Scheduler</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlScheduler
	 * @generated
	 */
	EClass getFSMControlScheduler();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlScheduler#getSchedulers <em>Schedulers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Schedulers</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlScheduler#getSchedulers()
	 * @see #getFSMControlScheduler()
	 * @generated
	 */
	EReference getFSMControlScheduler_Schedulers();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral <em>FSM Control Peripheral</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Peripheral</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral
	 * @generated
	 */
	EClass getFSMControlPeripheral();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getLineDisplays <em>Line Displays</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Line Displays</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getLineDisplays()
	 * @see #getFSMControlPeripheral()
	 * @generated
	 */
	EReference getFSMControlPeripheral_LineDisplays();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getDisplays <em>Displays</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Displays</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getDisplays()
	 * @see #getFSMControlPeripheral()
	 * @generated
	 */
	EReference getFSMControlPeripheral_Displays();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getPosPrinters <em>Pos Printers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pos Printers</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getPosPrinters()
	 * @see #getFSMControlPeripheral()
	 * @generated
	 */
	EReference getFSMControlPeripheral_PosPrinters();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getCashDrawers <em>Cash Drawers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cash Drawers</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getCashDrawers()
	 * @see #getFSMControlPeripheral()
	 * @generated
	 */
	EReference getFSMControlPeripheral_CashDrawers();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getPaymentTerminals <em>Payment Terminals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Payment Terminals</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getPaymentTerminals()
	 * @see #getFSMControlPeripheral()
	 * @generated
	 */
	EReference getFSMControlPeripheral_PaymentTerminals();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getSignaturePads <em>Signature Pads</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Signature Pads</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getSignaturePads()
	 * @see #getFSMControlPeripheral()
	 * @generated
	 */
	EReference getFSMControlPeripheral_SignaturePads();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getScales <em>Scales</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Scales</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral#getScales()
	 * @see #getFSMControlPeripheral()
	 * @generated
	 */
	EReference getFSMControlPeripheral_Scales();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute <em>FSM Control Button Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Button Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute
	 * @generated
	 */
	EClass getFSMControlButtonAttribute();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#isHasImage <em>Has Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Image</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#isHasImage()
	 * @see #getFSMControlButtonAttribute()
	 * @generated
	 */
	EAttribute getFSMControlButtonAttribute_HasImage();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#getImage()
	 * @see #getFSMControlButtonAttribute()
	 * @generated
	 */
	EAttribute getFSMControlButtonAttribute_Image();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Event</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#getEvent()
	 * @see #getFSMControlButtonAttribute()
	 * @generated
	 */
	EReference getFSMControlButtonAttribute_Event();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEvent <em>FSM Control Button Attribute Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Button Attribute Event</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEvent
	 * @generated
	 */
	EClass getFSMControlButtonAttributeEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard <em>FSM Control Button Attribute Event Keyboard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Button Attribute Event Keyboard</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard
	 * @generated
	 */
	EClass getFSMControlButtonAttributeEventKeyboard();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard#getKeystroke <em>Keystroke</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Keystroke</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard#getKeystroke()
	 * @see #getFSMControlButtonAttributeEventKeyboard()
	 * @generated
	 */
	EAttribute getFSMControlButtonAttributeEventKeyboard_Keystroke();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventIdentity <em>FSM Control Button Attribute Event Identity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Button Attribute Event Identity</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventIdentity
	 * @generated
	 */
	EClass getFSMControlButtonAttributeEventIdentity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventIdentity#getIdentity <em>Identity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Identity</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventIdentity#getIdentity()
	 * @see #getFSMControlButtonAttributeEventIdentity()
	 * @generated
	 */
	EAttribute getFSMControlButtonAttributeEventIdentity_Identity();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventEvent <em>FSM Control Button Attribute Event Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Button Attribute Event Event</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventEvent
	 * @generated
	 */
	EClass getFSMControlButtonAttributeEventEvent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventEvent#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventEvent#getEvent()
	 * @see #getFSMControlButtonAttributeEventEvent()
	 * @generated
	 */
	EReference getFSMControlButtonAttributeEventEvent_Event();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlVisibility <em>FSM Control Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Visibility</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlVisibility
	 * @generated
	 */
	EClass getFSMControlVisibility();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlFieldAttribute <em>FSM Control Field Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Field Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlFieldAttribute
	 * @generated
	 */
	EClass getFSMControlFieldAttribute();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMControlFieldAttribute#getAttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Attribute Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlFieldAttribute#getAttributeType()
	 * @see #getFSMControlFieldAttribute()
	 * @generated
	 */
	EReference getFSMControlFieldAttribute_AttributeType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlFieldLayout <em>FSM Control Field Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Field Layout</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlFieldLayout
	 * @generated
	 */
	EClass getFSMControlFieldLayout();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute <em>FSM Control DTO Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control DTO Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute
	 * @generated
	 */
	EClass getFSMControlDTOAttribute();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute#getAttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Attribute Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute#getAttributeType()
	 * @see #getFSMControlDTOAttribute()
	 * @generated
	 */
	EReference getFSMControlDTOAttribute_AttributeType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute#isHasEvent <em>Has Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Event</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute#isHasEvent()
	 * @see #getFSMControlDTOAttribute()
	 * @generated
	 */
	EAttribute getFSMControlDTOAttribute_HasEvent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute#getEvent()
	 * @see #getFSMControlDTOAttribute()
	 * @generated
	 */
	EReference getFSMControlDTOAttribute_Event();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute#isIsAttached <em>Is Attached</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Attached</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute#isIsAttached()
	 * @see #getFSMControlDTOAttribute()
	 * @generated
	 */
	EAttribute getFSMControlDTOAttribute_IsAttached();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute#getDisplay <em>Display</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Display</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute#getDisplay()
	 * @see #getFSMControlDTOAttribute()
	 * @generated
	 */
	EReference getFSMControlDTOAttribute_Display();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevice <em>FSM Peripheral Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Peripheral Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevice
	 * @generated
	 */
	EClass getFSMPeripheralDevice();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay <em>FSM Peripheral Device Display</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Peripheral Device Display</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay
	 * @generated
	 */
	EClass getFSMPeripheralDeviceDisplay();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Output</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay#getOutput()
	 * @see #getFSMPeripheralDeviceDisplay()
	 * @generated
	 */
	EReference getFSMPeripheralDeviceDisplay_Output();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceLineDisplay <em>FSM Peripheral Device Line Display</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Peripheral Device Line Display</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceLineDisplay
	 * @generated
	 */
	EClass getFSMPeripheralDeviceLineDisplay();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePOSPrinter <em>FSM Peripheral Device POS Printer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Peripheral Device POS Printer</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePOSPrinter
	 * @generated
	 */
	EClass getFSMPeripheralDevicePOSPrinter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceCashDrawer <em>FSM Peripheral Device Cash Drawer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Peripheral Device Cash Drawer</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceCashDrawer
	 * @generated
	 */
	EClass getFSMPeripheralDeviceCashDrawer();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePT <em>FSM Peripheral Device PT</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Peripheral Device PT</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePT
	 * @generated
	 */
	EClass getFSMPeripheralDevicePT();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceSignature <em>FSM Peripheral Device Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Peripheral Device Signature</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceSignature
	 * @generated
	 */
	EClass getFSMPeripheralDeviceSignature();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceScale <em>FSM Peripheral Device Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Peripheral Device Scale</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceScale
	 * @generated
	 */
	EClass getFSMPeripheralDeviceScale();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute <em>FSM Control Scheduler Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Scheduler Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute
	 * @generated
	 */
	EClass getFSMControlSchedulerAttribute();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute#getDelay()
	 * @see #getFSMControlSchedulerAttribute()
	 * @generated
	 */
	EAttribute getFSMControlSchedulerAttribute_Delay();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute#getEvent()
	 * @see #getFSMControlSchedulerAttribute()
	 * @generated
	 */
	EReference getFSMControlSchedulerAttribute_Event();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMEvent <em>FSM Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Event</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMEvent
	 * @generated
	 */
	EClass getFSMEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMState <em>FSM State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM State</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMState
	 * @generated
	 */
	EClass getFSMState();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMState#getTriggers <em>Triggers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Triggers</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMState#getTriggers()
	 * @see #getFSMState()
	 * @generated
	 */
	EReference getFSMState_Triggers();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMState#getConditions <em>Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Conditions</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMState#getConditions()
	 * @see #getFSMState()
	 * @generated
	 */
	EReference getFSMState_Conditions();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMState#getIdentity <em>Identity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Identity</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMState#getIdentity()
	 * @see #getFSMState()
	 * @generated
	 */
	EReference getFSMState_Identity();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMState#getKeystroke <em>Keystroke</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Keystroke</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMState#getKeystroke()
	 * @see #getFSMState()
	 * @generated
	 */
	EReference getFSMState_Keystroke();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMState#isHasKeyOperation <em>Has Key Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Key Operation</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMState#isHasKeyOperation()
	 * @see #getFSMState()
	 * @generated
	 */
	EAttribute getFSMState_HasKeyOperation();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMState#getKeyOperation <em>Key Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Key Operation</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMState#getKeyOperation()
	 * @see #getFSMState()
	 * @generated
	 */
	EReference getFSMState_KeyOperation();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMState#getKeyMapper <em>Key Mapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Key Mapper</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMState#getKeyMapper()
	 * @see #getFSMState()
	 * @generated
	 */
	EReference getFSMState_KeyMapper();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMKeyMapper <em>FSM Key Mapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Key Mapper</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMKeyMapper
	 * @generated
	 */
	EClass getFSMKeyMapper();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMKeyMapper#getKeyCode <em>Key Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key Code</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMKeyMapper#getKeyCode()
	 * @see #getFSMKeyMapper()
	 * @generated
	 */
	EAttribute getFSMKeyMapper_KeyCode();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMKeyMapper#getKeyEvent <em>Key Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key Event</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMKeyMapper#getKeyEvent()
	 * @see #getFSMKeyMapper()
	 * @generated
	 */
	EReference getFSMKeyMapper_KeyEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger <em>FSM Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Trigger</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMTrigger
	 * @generated
	 */
	EClass getFSMTrigger();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#isHasTransition <em>Has Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Transition</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMTrigger#isHasTransition()
	 * @see #getFSMTrigger()
	 * @generated
	 */
	EAttribute getFSMTrigger_HasTransition();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transition</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMTrigger#getTransition()
	 * @see #getFSMTrigger()
	 * @generated
	 */
	EReference getFSMTrigger_Transition();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#getTriggers <em>Triggers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Triggers</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMTrigger#getTriggers()
	 * @see #getFSMTrigger()
	 * @generated
	 */
	EReference getFSMTrigger_Triggers();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#getGuards <em>Guards</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Guards</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMTrigger#getGuards()
	 * @see #getFSMTrigger()
	 * @generated
	 */
	EReference getFSMTrigger_Guards();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMTrigger#getActions()
	 * @see #getFSMTrigger()
	 * @generated
	 */
	EReference getFSMTrigger_Actions();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMAction <em>FSM Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMAction
	 * @generated
	 */
	EClass getFSMAction();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBlinkRate <em>FSM Action Peripheral Blink Rate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Blink Rate</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBlinkRate
	 * @generated
	 */
	EClass getFSMActionPeripheralBlinkRate();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBlinkRate#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBlinkRate#getDevice()
	 * @see #getFSMActionPeripheralBlinkRate()
	 * @generated
	 */
	EReference getFSMActionPeripheralBlinkRate_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBlinkRate#getBlinkRate <em>Blink Rate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Blink Rate</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBlinkRate#getBlinkRate()
	 * @see #getFSMActionPeripheralBlinkRate()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralBlinkRate_BlinkRate();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralClear <em>FSM Action Peripheral Clear</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Clear</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralClear
	 * @generated
	 */
	EClass getFSMActionPeripheralClear();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralClear#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralClear#getDevice()
	 * @see #getFSMActionPeripheralClear()
	 * @generated
	 */
	EReference getFSMActionPeripheralClear_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow <em>FSM Action Peripheral Create Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Create Window</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow
	 * @generated
	 */
	EClass getFSMActionPeripheralCreateWindow();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getDevice()
	 * @see #getFSMActionPeripheralCreateWindow()
	 * @generated
	 */
	EReference getFSMActionPeripheralCreateWindow_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportRow <em>Viewport Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Viewport Row</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportRow()
	 * @see #getFSMActionPeripheralCreateWindow()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralCreateWindow_ViewportRow();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportColumn <em>Viewport Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Viewport Column</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportColumn()
	 * @see #getFSMActionPeripheralCreateWindow()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralCreateWindow_ViewportColumn();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportHeight <em>Viewport Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Viewport Height</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportHeight()
	 * @see #getFSMActionPeripheralCreateWindow()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralCreateWindow_ViewportHeight();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportWidth <em>Viewport Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Viewport Width</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportWidth()
	 * @see #getFSMActionPeripheralCreateWindow()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralCreateWindow_ViewportWidth();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getWindowHeight <em>Window Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Window Height</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getWindowHeight()
	 * @see #getFSMActionPeripheralCreateWindow()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralCreateWindow_WindowHeight();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getWindowWidth <em>Window Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Window Width</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getWindowWidth()
	 * @see #getFSMActionPeripheralCreateWindow()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralCreateWindow_WindowWidth();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCursorType <em>FSM Action Peripheral Cursor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Cursor Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCursorType
	 * @generated
	 */
	EClass getFSMActionPeripheralCursorType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCursorType#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCursorType#getDevice()
	 * @see #getFSMActionPeripheralCursorType()
	 * @generated
	 */
	EReference getFSMActionPeripheralCursorType_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCursorType#getCursorType <em>Cursor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cursor Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCursorType#getCursorType()
	 * @see #getFSMActionPeripheralCursorType()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralCursorType_CursorType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDestroyWindow <em>FSM Action Peripheral Destroy Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Destroy Window</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDestroyWindow
	 * @generated
	 */
	EClass getFSMActionPeripheralDestroyWindow();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDestroyWindow#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDestroyWindow#getDevice()
	 * @see #getFSMActionPeripheralDestroyWindow()
	 * @generated
	 */
	EReference getFSMActionPeripheralDestroyWindow_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDeviceBrightness <em>FSM Action Peripheral Device Brightness</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Device Brightness</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDeviceBrightness
	 * @generated
	 */
	EClass getFSMActionPeripheralDeviceBrightness();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDeviceBrightness#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDeviceBrightness#getDevice()
	 * @see #getFSMActionPeripheralDeviceBrightness()
	 * @generated
	 */
	EReference getFSMActionPeripheralDeviceBrightness_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDeviceBrightness#getBrightness <em>Brightness</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Brightness</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDeviceBrightness#getBrightness()
	 * @see #getFSMActionPeripheralDeviceBrightness()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralDeviceBrightness_Brightness();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText <em>FSM Action Peripheral Display Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Display Text</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText
	 * @generated
	 */
	EClass getFSMActionPeripheralDisplayText();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText#getDevice()
	 * @see #getFSMActionPeripheralDisplayText()
	 * @generated
	 */
	EReference getFSMActionPeripheralDisplayText_Device();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText#getAttribute()
	 * @see #getFSMActionPeripheralDisplayText()
	 * @generated
	 */
	EReference getFSMActionPeripheralDisplayText_Attribute();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Text</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText#getText()
	 * @see #getFSMActionPeripheralDisplayText()
	 * @generated
	 */
	EReference getFSMActionPeripheralDisplayText_Text();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText <em>FSM Action Peripheral Line Display Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Line Display Text</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText
	 * @generated
	 */
	EClass getFSMActionPeripheralLineDisplayText();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText#getDevice()
	 * @see #getFSMActionPeripheralLineDisplayText()
	 * @generated
	 */
	EReference getFSMActionPeripheralLineDisplayText_Device();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Text</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText#getText()
	 * @see #getFSMActionPeripheralLineDisplayText()
	 * @generated
	 */
	EReference getFSMActionPeripheralLineDisplayText_Text();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText#isHasType <em>Has Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText#isHasType()
	 * @see #getFSMActionPeripheralLineDisplayText()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralLineDisplayText_HasType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText#getTextType <em>Text Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText#getTextType()
	 * @see #getFSMActionPeripheralLineDisplayText()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralLineDisplayText_TextType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt <em>FSM Action Peripheral Line Display Text At</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Line Display Text At</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt
	 * @generated
	 */
	EClass getFSMActionPeripheralLineDisplayTextAt();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#getDevice()
	 * @see #getFSMActionPeripheralLineDisplayTextAt()
	 * @generated
	 */
	EReference getFSMActionPeripheralLineDisplayTextAt_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#getRow <em>Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Row</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#getRow()
	 * @see #getFSMActionPeripheralLineDisplayTextAt()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralLineDisplayTextAt_Row();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#getColumn <em>Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Column</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#getColumn()
	 * @see #getFSMActionPeripheralLineDisplayTextAt()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralLineDisplayTextAt_Column();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Text</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#getText()
	 * @see #getFSMActionPeripheralLineDisplayTextAt()
	 * @generated
	 */
	EReference getFSMActionPeripheralLineDisplayTextAt_Text();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#isHasType <em>Has Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#isHasType()
	 * @see #getFSMActionPeripheralLineDisplayTextAt()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralLineDisplayTextAt_HasType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#getTextType <em>Text Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt#getTextType()
	 * @see #getFSMActionPeripheralLineDisplayTextAt()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralLineDisplayTextAt_TextType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralInterCharacterWait <em>FSM Action Peripheral Inter Character Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Inter Character Wait</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralInterCharacterWait
	 * @generated
	 */
	EClass getFSMActionPeripheralInterCharacterWait();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralInterCharacterWait#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralInterCharacterWait#getDevice()
	 * @see #getFSMActionPeripheralInterCharacterWait()
	 * @generated
	 */
	EReference getFSMActionPeripheralInterCharacterWait_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralInterCharacterWait#getWait <em>Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wait</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralInterCharacterWait#getWait()
	 * @see #getFSMActionPeripheralInterCharacterWait()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralInterCharacterWait_Wait();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeFormat <em>FSM Action Peripheral Marquee Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Marquee Format</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeFormat
	 * @generated
	 */
	EClass getFSMActionPeripheralMarqueeFormat();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeFormat#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeFormat#getDevice()
	 * @see #getFSMActionPeripheralMarqueeFormat()
	 * @generated
	 */
	EReference getFSMActionPeripheralMarqueeFormat_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeFormat#getFormat <em>Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Format</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeFormat#getFormat()
	 * @see #getFSMActionPeripheralMarqueeFormat()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralMarqueeFormat_Format();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeRepeatWait <em>FSM Action Peripheral Marquee Repeat Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Marquee Repeat Wait</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeRepeatWait
	 * @generated
	 */
	EClass getFSMActionPeripheralMarqueeRepeatWait();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeRepeatWait#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeRepeatWait#getDevice()
	 * @see #getFSMActionPeripheralMarqueeRepeatWait()
	 * @generated
	 */
	EReference getFSMActionPeripheralMarqueeRepeatWait_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeRepeatWait#getWait <em>Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wait</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeRepeatWait#getWait()
	 * @see #getFSMActionPeripheralMarqueeRepeatWait()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralMarqueeRepeatWait_Wait();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeType <em>FSM Action Peripheral Marquee Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Marquee Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeType
	 * @generated
	 */
	EClass getFSMActionPeripheralMarqueeType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeType#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeType#getDevice()
	 * @see #getFSMActionPeripheralMarqueeType()
	 * @generated
	 */
	EReference getFSMActionPeripheralMarqueeType_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeType#getMarqueeType <em>Marquee Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Marquee Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeType#getMarqueeType()
	 * @see #getFSMActionPeripheralMarqueeType()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralMarqueeType_MarqueeType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeUnitWait <em>FSM Action Peripheral Marquee Unit Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Marquee Unit Wait</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeUnitWait
	 * @generated
	 */
	EClass getFSMActionPeripheralMarqueeUnitWait();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeUnitWait#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeUnitWait#getDevice()
	 * @see #getFSMActionPeripheralMarqueeUnitWait()
	 * @generated
	 */
	EReference getFSMActionPeripheralMarqueeUnitWait_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeUnitWait#getWait <em>Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wait</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeUnitWait#getWait()
	 * @see #getFSMActionPeripheralMarqueeUnitWait()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralMarqueeUnitWait_Wait();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll <em>FSM Action Peripheral Scroll</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Scroll</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll
	 * @generated
	 */
	EClass getFSMActionPeripheralScroll();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll#getDevice()
	 * @see #getFSMActionPeripheralScroll()
	 * @generated
	 */
	EReference getFSMActionPeripheralScroll_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll#getDirection()
	 * @see #getFSMActionPeripheralScroll()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralScroll_Direction();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll#getUnits <em>Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Units</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll#getUnits()
	 * @see #getFSMActionPeripheralScroll()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralScroll_Units();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralOpenDrawer <em>FSM Action Peripheral Open Drawer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Open Drawer</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralOpenDrawer
	 * @generated
	 */
	EClass getFSMActionPeripheralOpenDrawer();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralOpenDrawer#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralOpenDrawer#getDevice()
	 * @see #getFSMActionPeripheralOpenDrawer()
	 * @generated
	 */
	EReference getFSMActionPeripheralOpenDrawer_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode <em>FSM Action Peripheral Print Barcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Print Barcode</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode
	 * @generated
	 */
	EClass getFSMActionPeripheralPrintBarcode();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode#getDevice()
	 * @see #getFSMActionPeripheralPrintBarcode()
	 * @generated
	 */
	EReference getFSMActionPeripheralPrintBarcode_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode#getData()
	 * @see #getFSMActionPeripheralPrintBarcode()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralPrintBarcode_Data();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode#getBarcodeType <em>Barcode Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Barcode Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode#getBarcodeType()
	 * @see #getFSMActionPeripheralPrintBarcode()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralPrintBarcode_BarcodeType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBitmap <em>FSM Action Peripheral Print Bitmap</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Print Bitmap</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBitmap
	 * @generated
	 */
	EClass getFSMActionPeripheralPrintBitmap();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBitmap#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBitmap#getDevice()
	 * @see #getFSMActionPeripheralPrintBitmap()
	 * @generated
	 */
	EReference getFSMActionPeripheralPrintBitmap_Device();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBitmap#getBitmapId <em>Bitmap Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bitmap Id</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBitmap#getBitmapId()
	 * @see #getFSMActionPeripheralPrintBitmap()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralPrintBitmap_BitmapId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintCut <em>FSM Action Peripheral Print Cut</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Print Cut</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintCut
	 * @generated
	 */
	EClass getFSMActionPeripheralPrintCut();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintCut#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintCut#getDevice()
	 * @see #getFSMActionPeripheralPrintCut()
	 * @generated
	 */
	EReference getFSMActionPeripheralPrintCut_Device();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintCut#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Text</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintCut#getText()
	 * @see #getFSMActionPeripheralPrintCut()
	 * @generated
	 */
	EReference getFSMActionPeripheralPrintCut_Text();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintRegisterBitmap <em>FSM Action Peripheral Print Register Bitmap</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Print Register Bitmap</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintRegisterBitmap
	 * @generated
	 */
	EClass getFSMActionPeripheralPrintRegisterBitmap();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintRegisterBitmap#getBitmapId <em>Bitmap Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bitmap Id</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintRegisterBitmap#getBitmapId()
	 * @see #getFSMActionPeripheralPrintRegisterBitmap()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralPrintRegisterBitmap_BitmapId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintRegisterBitmap#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintRegisterBitmap#getName()
	 * @see #getFSMActionPeripheralPrintRegisterBitmap()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralPrintRegisterBitmap_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal <em>FSM Action Peripheral Print Normal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Print Normal</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal
	 * @generated
	 */
	EClass getFSMActionPeripheralPrintNormal();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal#getDevice()
	 * @see #getFSMActionPeripheralPrintNormal()
	 * @generated
	 */
	EReference getFSMActionPeripheralPrintNormal_Device();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Text</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal#getText()
	 * @see #getFSMActionPeripheralPrintNormal()
	 * @generated
	 */
	EReference getFSMActionPeripheralPrintNormal_Text();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal#getBarcodeType <em>Barcode Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Barcode Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal#getBarcodeType()
	 * @see #getFSMActionPeripheralPrintNormal()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralPrintNormal_BarcodeType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen <em>FSM Action Peripheral PT Open</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral PT Open</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen
	 * @generated
	 */
	EClass getFSMActionPeripheralPTOpen();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen#getDevice()
	 * @see #getFSMActionPeripheralPTOpen()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTOpen_Device();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen#getHost <em>Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Host</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen#getHost()
	 * @see #getFSMActionPeripheralPTOpen()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTOpen_Host();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen#getPort()
	 * @see #getFSMActionPeripheralPTOpen()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTOpen_Port();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTClose <em>FSM Action Peripheral PT Close</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral PT Close</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTClose
	 * @generated
	 */
	EClass getFSMActionPeripheralPTClose();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTClose#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTClose#getDevice()
	 * @see #getFSMActionPeripheralPTClose()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTClose_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal <em>FSM Action Peripheral PT Reversal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral PT Reversal</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal
	 * @generated
	 */
	EClass getFSMActionPeripheralPTReversal();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal#getDevice()
	 * @see #getFSMActionPeripheralPTReversal()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTReversal_Device();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Password</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal#getPassword()
	 * @see #getFSMActionPeripheralPTReversal()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTReversal_Password();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal#getReceipt <em>Receipt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Receipt</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal#getReceipt()
	 * @see #getFSMActionPeripheralPTReversal()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTReversal_Receipt();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAcknowledge <em>FSM Action Peripheral PT Acknowledge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral PT Acknowledge</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAcknowledge
	 * @generated
	 */
	EClass getFSMActionPeripheralPTAcknowledge();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAcknowledge#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAcknowledge#getDevice()
	 * @see #getFSMActionPeripheralPTAcknowledge()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTAcknowledge_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration <em>FSM Action Peripheral PT Registration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral PT Registration</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration
	 * @generated
	 */
	EClass getFSMActionPeripheralPTRegistration();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration#getDevice()
	 * @see #getFSMActionPeripheralPTRegistration()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTRegistration_Device();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Password</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration#getPassword()
	 * @see #getFSMActionPeripheralPTRegistration()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTRegistration_Password();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Configuration</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration#getConfiguration()
	 * @see #getFSMActionPeripheralPTRegistration()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralPTRegistration_Configuration();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization <em>FSM Action Peripheral PT Authorization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral PT Authorization</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization
	 * @generated
	 */
	EClass getFSMActionPeripheralPTAuthorization();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization#getDevice()
	 * @see #getFSMActionPeripheralPTAuthorization()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTAuthorization_Device();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization#getAmount <em>Amount</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Amount</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization#getAmount()
	 * @see #getFSMActionPeripheralPTAuthorization()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTAuthorization_Amount();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBeeper <em>FSM Action Peripheral Beeper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Beeper</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBeeper
	 * @generated
	 */
	EClass getFSMActionPeripheralBeeper();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBeeper#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBeeper#getDuration()
	 * @see #getFSMActionPeripheralBeeper()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralBeeper_Duration();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBeeper#getFrequency <em>Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frequency</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBeeper#getFrequency()
	 * @see #getFSMActionPeripheralBeeper()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralBeeper_Frequency();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPlayer <em>FSM Action Peripheral Player</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Player</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPlayer
	 * @generated
	 */
	EClass getFSMActionPeripheralPlayer();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPlayer#getTune <em>Tune</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tune</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPlayer#getTune()
	 * @see #getFSMActionPeripheralPlayer()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralPlayer_Tune();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSound <em>FSM Action Peripheral Sound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Sound</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSound
	 * @generated
	 */
	EClass getFSMActionPeripheralSound();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSound#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSound#getName()
	 * @see #getFSMActionPeripheralSound()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralSound_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTResponse <em>FSM Action Peripheral PT Response</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral PT Response</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTResponse
	 * @generated
	 */
	EClass getFSMActionPeripheralPTResponse();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTResponse#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTResponse#getDevice()
	 * @see #getFSMActionPeripheralPTResponse()
	 * @generated
	 */
	EReference getFSMActionPeripheralPTResponse_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport <em>FSM Action Peripheral Print Report</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Print Report</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport
	 * @generated
	 */
	EClass getFSMActionPeripheralPrintReport();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport#getReport <em>Report</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Report</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport#getReport()
	 * @see #getFSMActionPeripheralPrintReport()
	 * @generated
	 */
	EReference getFSMActionPeripheralPrintReport_Report();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport#getKey()
	 * @see #getFSMActionPeripheralPrintReport()
	 * @generated
	 */
	EReference getFSMActionPeripheralPrintReport_Key();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport#isHasFilter <em>Has Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport#isHasFilter()
	 * @see #getFSMActionPeripheralPrintReport()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralPrintReport_HasFilter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport#isHasPrintService <em>Has Print Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Print Service</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport#isHasPrintService()
	 * @see #getFSMActionPeripheralPrintReport()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralPrintReport_HasPrintService();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport#getPrintService <em>Print Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Print Service</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport#getPrintService()
	 * @see #getFSMActionPeripheralPrintReport()
	 * @generated
	 */
	EReference getFSMActionPeripheralPrintReport_PrintService();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureOpen <em>FSM Action Peripheral Signature Open</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Signature Open</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureOpen
	 * @generated
	 */
	EClass getFSMActionPeripheralSignatureOpen();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureOpen#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureOpen#getDevice()
	 * @see #getFSMActionPeripheralSignatureOpen()
	 * @generated
	 */
	EReference getFSMActionPeripheralSignatureOpen_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClose <em>FSM Action Peripheral Signature Close</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Signature Close</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClose
	 * @generated
	 */
	EClass getFSMActionPeripheralSignatureClose();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClose#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClose#getDevice()
	 * @see #getFSMActionPeripheralSignatureClose()
	 * @generated
	 */
	EReference getFSMActionPeripheralSignatureClose_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClear <em>FSM Action Peripheral Signature Clear</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Signature Clear</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClear
	 * @generated
	 */
	EClass getFSMActionPeripheralSignatureClear();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClear#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClear#getDevice()
	 * @see #getFSMActionPeripheralSignatureClear()
	 * @generated
	 */
	EReference getFSMActionPeripheralSignatureClear_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureCapture <em>FSM Action Peripheral Signature Capture</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Signature Capture</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureCapture
	 * @generated
	 */
	EClass getFSMActionPeripheralSignatureCapture();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureCapture#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureCapture#getDevice()
	 * @see #getFSMActionPeripheralSignatureCapture()
	 * @generated
	 */
	EReference getFSMActionPeripheralSignatureCapture_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureIdle <em>FSM Action Peripheral Signature Idle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Signature Idle</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureIdle
	 * @generated
	 */
	EClass getFSMActionPeripheralSignatureIdle();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureIdle#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureIdle#getDevice()
	 * @see #getFSMActionPeripheralSignatureIdle()
	 * @generated
	 */
	EReference getFSMActionPeripheralSignatureIdle_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel <em>FSM Action Peripheral Signature Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Signature Label</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel
	 * @generated
	 */
	EClass getFSMActionPeripheralSignatureLabel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel#getOkLabel <em>Ok Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ok Label</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel#getOkLabel()
	 * @see #getFSMActionPeripheralSignatureLabel()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralSignatureLabel_OkLabel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel#getClearLabel <em>Clear Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Clear Label</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel#getClearLabel()
	 * @see #getFSMActionPeripheralSignatureLabel()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralSignatureLabel_ClearLabel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel#getCancelLabel <em>Cancel Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cancel Label</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel#getCancelLabel()
	 * @see #getFSMActionPeripheralSignatureLabel()
	 * @generated
	 */
	EAttribute getFSMActionPeripheralSignatureLabel_CancelLabel();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel#getDevice()
	 * @see #getFSMActionPeripheralSignatureLabel()
	 * @generated
	 */
	EReference getFSMActionPeripheralSignatureLabel_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMSignatureRetrieve <em>FSM Signature Retrieve</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Signature Retrieve</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMSignatureRetrieve
	 * @generated
	 */
	EClass getFSMSignatureRetrieve();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMSignatureRetrieve#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMSignatureRetrieve#getDevice()
	 * @see #getFSMSignatureRetrieve()
	 * @generated
	 */
	EReference getFSMSignatureRetrieve_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadWeight <em>FSM Action Peripheral Scale Read Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Scale Read Weight</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadWeight
	 * @generated
	 */
	EClass getFSMActionPeripheralScaleReadWeight();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadWeight#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadWeight#getDevice()
	 * @see #getFSMActionPeripheralScaleReadWeight()
	 * @generated
	 */
	EReference getFSMActionPeripheralScaleReadWeight_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadTareWeight <em>FSM Action Peripheral Scale Read Tare Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Scale Read Tare Weight</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadTareWeight
	 * @generated
	 */
	EClass getFSMActionPeripheralScaleReadTareWeight();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadTareWeight#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadTareWeight#getDevice()
	 * @see #getFSMActionPeripheralScaleReadTareWeight()
	 * @generated
	 */
	EReference getFSMActionPeripheralScaleReadTareWeight_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleTareWeight <em>FSM Action Peripheral Scale Tare Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Scale Tare Weight</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleTareWeight
	 * @generated
	 */
	EClass getFSMActionPeripheralScaleTareWeight();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleTareWeight#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleTareWeight#getDevice()
	 * @see #getFSMActionPeripheralScaleTareWeight()
	 * @generated
	 */
	EReference getFSMActionPeripheralScaleTareWeight_Device();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleTareWeight#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleTareWeight#getValue()
	 * @see #getFSMActionPeripheralScaleTareWeight()
	 * @generated
	 */
	EReference getFSMActionPeripheralScaleTareWeight_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleZero <em>FSM Action Peripheral Scale Zero</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Scale Zero</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleZero
	 * @generated
	 */
	EClass getFSMActionPeripheralScaleZero();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleZero#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleZero#getDevice()
	 * @see #getFSMActionPeripheralScaleZero()
	 * @generated
	 */
	EReference getFSMActionPeripheralScaleZero_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleDisplayText <em>FSM Action Peripheral Scale Display Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Scale Display Text</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleDisplayText
	 * @generated
	 */
	EClass getFSMActionPeripheralScaleDisplayText();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleDisplayText#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleDisplayText#getDevice()
	 * @see #getFSMActionPeripheralScaleDisplayText()
	 * @generated
	 */
	EReference getFSMActionPeripheralScaleDisplayText_Device();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleDisplayText#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Text</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleDisplayText#getText()
	 * @see #getFSMActionPeripheralScaleDisplayText()
	 * @generated
	 */
	EReference getFSMActionPeripheralScaleDisplayText_Text();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleWeightUnit <em>FSM Action Peripheral Scale Weight Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Peripheral Scale Weight Unit</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleWeightUnit
	 * @generated
	 */
	EClass getFSMActionPeripheralScaleWeightUnit();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleWeightUnit#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Device</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleWeightUnit#getDevice()
	 * @see #getFSMActionPeripheralScaleWeightUnit()
	 * @generated
	 */
	EReference getFSMActionPeripheralScaleWeightUnit_Device();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSource <em>FSM Action Field Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Source</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSource
	 * @generated
	 */
	EClass getFSMActionFieldSource();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceString <em>FSM Action Field Source String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Source String</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceString
	 * @generated
	 */
	EClass getFSMActionFieldSourceString();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceString#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceString#getText()
	 * @see #getFSMActionFieldSourceString()
	 * @generated
	 */
	EAttribute getFSMActionFieldSourceString_Text();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceNumber <em>FSM Action Field Source Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Source Number</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceNumber
	 * @generated
	 */
	EClass getFSMActionFieldSourceNumber();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceNumber#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceNumber#getValue()
	 * @see #getFSMActionFieldSourceNumber()
	 * @generated
	 */
	EAttribute getFSMActionFieldSourceNumber_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceInteger <em>FSM Action Field Source Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Source Integer</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceInteger
	 * @generated
	 */
	EClass getFSMActionFieldSourceInteger();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceInteger#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceInteger#getValue()
	 * @see #getFSMActionFieldSourceInteger()
	 * @generated
	 */
	EAttribute getFSMActionFieldSourceInteger_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceBoolean <em>FSM Action Field Source Boolean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Source Boolean</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceBoolean
	 * @generated
	 */
	EClass getFSMActionFieldSourceBoolean();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceBoolean#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceBoolean#isValue()
	 * @see #getFSMActionFieldSourceBoolean()
	 * @generated
	 */
	EAttribute getFSMActionFieldSourceBoolean_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvaluate <em>FSM Action Field Source Evaluate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Source Evaluate</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvaluate
	 * @generated
	 */
	EClass getFSMActionFieldSourceEvaluate();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvaluate#getEvaluationtype <em>Evaluationtype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Evaluationtype</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvaluate#getEvaluationtype()
	 * @see #getFSMActionFieldSourceEvaluate()
	 * @generated
	 */
	EAttribute getFSMActionFieldSourceEvaluate_Evaluationtype();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceTranslate <em>FSM Action Field Source Translate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Source Translate</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceTranslate
	 * @generated
	 */
	EClass getFSMActionFieldSourceTranslate();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceTranslate#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceTranslate#getText()
	 * @see #getFSMActionFieldSourceTranslate()
	 * @generated
	 */
	EAttribute getFSMActionFieldSourceTranslate_Text();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMRef <em>FSM Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Ref</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMRef
	 * @generated
	 */
	EClass getFSMRef();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMDotExpression <em>FSM Dot Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Dot Expression</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMDotExpression
	 * @generated
	 */
	EClass getFSMDotExpression();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMDotExpression#getRef <em>Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Ref</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMDotExpression#getRef()
	 * @see #getFSMDotExpression()
	 * @generated
	 */
	EReference getFSMDotExpression_Ref();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMDotExpression#getTail <em>Tail</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tail</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMDotExpression#getTail()
	 * @see #getFSMDotExpression()
	 * @generated
	 */
	EReference getFSMDotExpression_Tail();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMDtoRef <em>FSM Dto Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Dto Ref</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMDtoRef
	 * @generated
	 */
	EClass getFSMDtoRef();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMDtoRef#getDto <em>Dto</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dto</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMDtoRef#getDto()
	 * @see #getFSMDtoRef()
	 * @generated
	 */
	EReference getFSMDtoRef_Dto();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute <em>FSM Action Field Source Dto Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Source Dto Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute
	 * @generated
	 */
	EClass getFSMActionFieldSourceDtoAttribute();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute#getDto <em>Dto</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dto</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute#getDto()
	 * @see #getFSMActionFieldSourceDtoAttribute()
	 * @generated
	 */
	EReference getFSMActionFieldSourceDtoAttribute_Dto();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute#getAttribute()
	 * @see #getFSMActionFieldSourceDtoAttribute()
	 * @generated
	 */
	EReference getFSMActionFieldSourceDtoAttribute_Attribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvent <em>FSM Action Field Source Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Source Event</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvent
	 * @generated
	 */
	EClass getFSMActionFieldSourceEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition <em>FSM Action Conditional Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Conditional Transition</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition
	 * @generated
	 */
	EClass getFSMActionConditionalTransition();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transition</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition#getTransition()
	 * @see #getFSMActionConditionalTransition()
	 * @generated
	 */
	EReference getFSMActionConditionalTransition_Transition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition#getGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Guard</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition#getGuard()
	 * @see #getFSMActionConditionalTransition()
	 * @generated
	 */
	EReference getFSMActionConditionalTransition_Guard();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition#getActions()
	 * @see #getFSMActionConditionalTransition()
	 * @generated
	 */
	EReference getFSMActionConditionalTransition_Actions();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMOperationParameter <em>FSM Operation Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Operation Parameter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMOperationParameter
	 * @generated
	 */
	EClass getFSMOperationParameter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMOperationParameter#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMOperationParameter#getSource()
	 * @see #getFSMOperationParameter()
	 * @generated
	 */
	EReference getFSMOperationParameter_Source();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMOperation <em>FSM Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Operation</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMOperation
	 * @generated
	 */
	EClass getFSMOperation();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMOperation#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Group</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMOperation#getGroup()
	 * @see #getFSMOperation()
	 * @generated
	 */
	EReference getFSMOperation_Group();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMOperation#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMOperation#getOperation()
	 * @see #getFSMOperation()
	 * @generated
	 */
	EReference getFSMOperation_Operation();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMOperation#getFirst <em>First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>First</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMOperation#getFirst()
	 * @see #getFSMOperation()
	 * @generated
	 */
	EReference getFSMOperation_First();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMOperation#getMore <em>More</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>More</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMOperation#getMore()
	 * @see #getFSMOperation()
	 * @generated
	 */
	EReference getFSMOperation_More();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard <em>FSM Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Guard</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMGuard
	 * @generated
	 */
	EClass getFSMGuard();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Group</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMGuard#getGroup()
	 * @see #getFSMGuard()
	 * @generated
	 */
	EReference getFSMGuard_Group();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Guard</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMGuard#getGuard()
	 * @see #getFSMGuard()
	 * @generated
	 */
	EReference getFSMGuard_Guard();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#isHasOnFail <em>Has On Fail</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has On Fail</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMGuard#isHasOnFail()
	 * @see #getFSMGuard()
	 * @generated
	 */
	EAttribute getFSMGuard_HasOnFail();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailDescription <em>On Fail Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>On Fail Description</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailDescription()
	 * @see #getFSMGuard()
	 * @generated
	 */
	EAttribute getFSMGuard_OnFailDescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailCaption <em>On Fail Caption</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>On Fail Caption</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailCaption()
	 * @see #getFSMGuard()
	 * @generated
	 */
	EAttribute getFSMGuard_OnFailCaption();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailType <em>On Fail Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>On Fail Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMGuard#getOnFailType()
	 * @see #getFSMGuard()
	 * @generated
	 */
	EAttribute getFSMGuard_OnFailType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMFunction <em>FSM Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Function</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFunction
	 * @generated
	 */
	EClass getFSMFunction();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMFunction#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Group</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFunction#getGroup()
	 * @see #getFSMFunction()
	 * @generated
	 */
	EReference getFSMFunction_Group();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMFunction#getFunction <em>Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Function</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFunction#getFunction()
	 * @see #getFSMFunction()
	 * @generated
	 */
	EReference getFSMFunction_Function();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMFunction#getFirst <em>First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>First</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFunction#getFirst()
	 * @see #getFSMFunction()
	 * @generated
	 */
	EReference getFSMFunction_First();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMFunction#getMore <em>More</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>More</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFunction#getMore()
	 * @see #getFSMFunction()
	 * @generated
	 */
	EReference getFSMFunction_More();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMStorageRetrieve <em>FSM Storage Retrieve</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Storage Retrieve</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStorageRetrieve
	 * @generated
	 */
	EClass getFSMStorageRetrieve();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMStorageRetrieve#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStorageRetrieve#getKey()
	 * @see #getFSMStorageRetrieve()
	 * @generated
	 */
	EAttribute getFSMStorageRetrieve_Key();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMStorageRetrieve#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStorageRetrieve#getAttribute()
	 * @see #getFSMStorageRetrieve()
	 * @generated
	 */
	EAttribute getFSMStorageRetrieve_Attribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMStorage <em>FSM Storage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Storage</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStorage
	 * @generated
	 */
	EClass getFSMStorage();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMStorage#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStorage#getKey()
	 * @see #getFSMStorage()
	 * @generated
	 */
	EAttribute getFSMStorage_Key();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMStorage#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStorage#getAttribute()
	 * @see #getFSMStorage()
	 * @generated
	 */
	EAttribute getFSMStorage_Attribute();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMStorage#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Content</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStorage#getContent()
	 * @see #getFSMStorage()
	 * @generated
	 */
	EReference getFSMStorage_Content();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldConcatenation <em>FSM Action Field Concatenation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Concatenation</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldConcatenation
	 * @generated
	 */
	EClass getFSMActionFieldConcatenation();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldConcatenation#getFirst <em>First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>First</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldConcatenation#getFirst()
	 * @see #getFSMActionFieldConcatenation()
	 * @generated
	 */
	EReference getFSMActionFieldConcatenation_First();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldConcatenation#getMore <em>More</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>More</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldConcatenation#getMore()
	 * @see #getFSMActionFieldConcatenation()
	 * @generated
	 */
	EReference getFSMActionFieldConcatenation_More();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSet <em>FSM Action Field Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Set</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSet
	 * @generated
	 */
	EClass getFSMActionFieldSet();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSet#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSet#getAttribute()
	 * @see #getFSMActionFieldSet()
	 * @generated
	 */
	EReference getFSMActionFieldSet_Attribute();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSet#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSet#getSource()
	 * @see #getFSMActionFieldSet()
	 * @generated
	 */
	EReference getFSMActionFieldSet_Source();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldKeystroke <em>FSM Action Field Keystroke</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Keystroke</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldKeystroke
	 * @generated
	 */
	EClass getFSMActionFieldKeystroke();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldKeystroke#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldKeystroke#getAttribute()
	 * @see #getFSMActionFieldKeystroke()
	 * @generated
	 */
	EReference getFSMActionFieldKeystroke_Attribute();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldKeystroke#getKeystroke <em>Keystroke</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Keystroke</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldKeystroke#getKeystroke()
	 * @see #getFSMActionFieldKeystroke()
	 * @generated
	 */
	EAttribute getFSMActionFieldKeystroke_Keystroke();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldClear <em>FSM Action Field Clear</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Clear</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldClear
	 * @generated
	 */
	EClass getFSMActionFieldClear();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldClear#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldClear#getAttribute()
	 * @see #getFSMActionFieldClear()
	 * @generated
	 */
	EReference getFSMActionFieldClear_Attribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldGet <em>FSM Action Field Get</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Get</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldGet
	 * @generated
	 */
	EClass getFSMActionFieldGet();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldGet#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldGet#getAttribute()
	 * @see #getFSMActionFieldGet()
	 * @generated
	 */
	EReference getFSMActionFieldGet_Attribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldFilterToggle <em>FSM Action Field Filter Toggle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Filter Toggle</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldFilterToggle
	 * @generated
	 */
	EClass getFSMActionFieldFilterToggle();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldFilterToggle#getFilter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldFilterToggle#getFilter()
	 * @see #getFSMActionFieldFilterToggle()
	 * @generated
	 */
	EReference getFSMActionFieldFilterToggle_Filter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldRemove <em>FSM Action Field Remove</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Field Remove</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldRemove
	 * @generated
	 */
	EClass getFSMActionFieldRemove();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldRemove#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldRemove#getAttribute()
	 * @see #getFSMActionFieldRemove()
	 * @generated
	 */
	EReference getFSMActionFieldRemove_Attribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionItemVisible <em>FSM Action Item Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Item Visible</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionItemVisible
	 * @generated
	 */
	EClass getFSMActionItemVisible();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionItemVisible#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionItemVisible#getAttribute()
	 * @see #getFSMActionItemVisible()
	 * @generated
	 */
	EReference getFSMActionItemVisible_Attribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionItemInvisible <em>FSM Action Item Invisible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Item Invisible</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionItemInvisible
	 * @generated
	 */
	EClass getFSMActionItemInvisible();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionItemInvisible#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionItemInvisible#getAttribute()
	 * @see #getFSMActionItemInvisible()
	 * @generated
	 */
	EReference getFSMActionItemInvisible_Attribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionButtonCaption <em>FSM Action Button Caption</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Button Caption</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionButtonCaption
	 * @generated
	 */
	EClass getFSMActionButtonCaption();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionButtonCaption#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionButtonCaption#getAttribute()
	 * @see #getFSMActionButtonCaption()
	 * @generated
	 */
	EReference getFSMActionButtonCaption_Attribute();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionButtonCaption#getCaption <em>Caption</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Caption</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionButtonCaption#getCaption()
	 * @see #getFSMActionButtonCaption()
	 * @generated
	 */
	EReference getFSMActionButtonCaption_Caption();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionButtonImage <em>FSM Action Button Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Button Image</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionButtonImage
	 * @generated
	 */
	EClass getFSMActionButtonImage();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionButtonImage#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionButtonImage#getAttribute()
	 * @see #getFSMActionButtonImage()
	 * @generated
	 */
	EReference getFSMActionButtonImage_Attribute();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMActionButtonImage#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionButtonImage#getImage()
	 * @see #getFSMActionButtonImage()
	 * @generated
	 */
	EAttribute getFSMActionButtonImage_Image();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind <em>FSM Action DTO Find</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action DTO Find</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind
	 * @generated
	 */
	EClass getFSMActionDTOFind();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind#getDto <em>Dto</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dto</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind#getDto()
	 * @see #getFSMActionDTOFind()
	 * @generated
	 */
	EReference getFSMActionDTOFind_Dto();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind#getAttribute()
	 * @see #getFSMActionDTOFind()
	 * @generated
	 */
	EReference getFSMActionDTOFind_Attribute();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind#getSearch <em>Search</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Search</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind#getSearch()
	 * @see #getFSMActionDTOFind()
	 * @generated
	 */
	EReference getFSMActionDTOFind_Search();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionDTOClear <em>FSM Action DTO Clear</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action DTO Clear</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionDTOClear
	 * @generated
	 */
	EClass getFSMActionDTOClear();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionDTOClear#getDto <em>Dto</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dto</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionDTOClear#getDto()
	 * @see #getFSMActionDTOClear()
	 * @generated
	 */
	EReference getFSMActionDTOClear_Dto();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionScheduler <em>FSM Action Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Action Scheduler</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionScheduler
	 * @generated
	 */
	EClass getFSMActionScheduler();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMActionScheduler#getScheduler <em>Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scheduler</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionScheduler#getScheduler()
	 * @see #getFSMActionScheduler()
	 * @generated
	 */
	EReference getFSMActionScheduler_Scheduler();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMDTOType <em>FSMDTO Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSMDTO Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMDTOType
	 * @generated
	 */
	EClass getFSMDTOType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.statemachine.FSMDTOType#getAttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMDTOType#getAttributeType()
	 * @see #getFSMDTOType()
	 * @generated
	 */
	EReference getFSMDTOType_AttributeType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMFieldType <em>FSM Field Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Field Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFieldType
	 * @generated
	 */
	EClass getFSMFieldType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMFieldType#getAttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFieldType#getAttributeType()
	 * @see #getFSMFieldType()
	 * @generated
	 */
	EAttribute getFSMFieldType_AttributeType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMAbstractFilter <em>FSM Abstract Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Abstract Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMAbstractFilter
	 * @generated
	 */
	EClass getFSMAbstractFilter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMFilterProperty <em>FSM Filter Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Filter Property</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFilterProperty
	 * @generated
	 */
	EClass getFSMFilterProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMFilterProperty#getPath <em>Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Path</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFilterProperty#getPath()
	 * @see #getFSMFilterProperty()
	 * @generated
	 */
	EReference getFSMFilterProperty_Path();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMFilter <em>FSM Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFilter
	 * @generated
	 */
	EClass getFSMFilter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMFilter#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFilter#getSource()
	 * @see #getFSMFilter()
	 * @generated
	 */
	EReference getFSMFilter_Source();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMJunctionFilter <em>FSM Junction Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Junction Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMJunctionFilter
	 * @generated
	 */
	EClass getFSMJunctionFilter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMJunctionFilter#getFirst <em>First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>First</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMJunctionFilter#getFirst()
	 * @see #getFSMJunctionFilter()
	 * @generated
	 */
	EReference getFSMJunctionFilter_First();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.statemachine.FSMJunctionFilter#getMore <em>More</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>More</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMJunctionFilter#getMore()
	 * @see #getFSMJunctionFilter()
	 * @generated
	 */
	EReference getFSMJunctionFilter_More();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMAndFilter <em>FSM And Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM And Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMAndFilter
	 * @generated
	 */
	EClass getFSMAndFilter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMOrFilter <em>FSM Or Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Or Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMOrFilter
	 * @generated
	 */
	EClass getFSMOrFilter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter <em>FSM Between Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Between Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter
	 * @generated
	 */
	EClass getFSMBetweenFilter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter#getPropertyId <em>Property Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property Id</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter#getPropertyId()
	 * @see #getFSMBetweenFilter()
	 * @generated
	 */
	EReference getFSMBetweenFilter_PropertyId();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Start</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter#getStart()
	 * @see #getFSMBetweenFilter()
	 * @generated
	 */
	EReference getFSMBetweenFilter_Start();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>End</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter#getEnd()
	 * @see #getFSMBetweenFilter()
	 * @generated
	 */
	EReference getFSMBetweenFilter_End();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMCompareFilter <em>FSM Compare Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Compare Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMCompareFilter
	 * @generated
	 */
	EClass getFSMCompareFilter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getPropertyId <em>Property Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property Id</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getPropertyId()
	 * @see #getFSMCompareFilter()
	 * @generated
	 */
	EReference getFSMCompareFilter_PropertyId();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getOperand()
	 * @see #getFSMCompareFilter()
	 * @generated
	 */
	EReference getFSMCompareFilter_Operand();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getOperation()
	 * @see #getFSMCompareFilter()
	 * @generated
	 */
	EAttribute getFSMCompareFilter_Operation();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMIsNullFilter <em>FSM Is Null Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Is Null Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMIsNullFilter
	 * @generated
	 */
	EClass getFSMIsNullFilter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMIsNullFilter#getPropertyId <em>Property Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property Id</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMIsNullFilter#getPropertyId()
	 * @see #getFSMIsNullFilter()
	 * @generated
	 */
	EReference getFSMIsNullFilter_PropertyId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMLikeFilter <em>FSM Like Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Like Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLikeFilter
	 * @generated
	 */
	EClass getFSMLikeFilter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMLikeFilter#getPropertyId <em>Property Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property Id</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLikeFilter#getPropertyId()
	 * @see #getFSMLikeFilter()
	 * @generated
	 */
	EReference getFSMLikeFilter_PropertyId();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMLikeFilter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLikeFilter#getValue()
	 * @see #getFSMLikeFilter()
	 * @generated
	 */
	EReference getFSMLikeFilter_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMLikeFilter#isIgnoreCase <em>Ignore Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ignore Case</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLikeFilter#isIgnoreCase()
	 * @see #getFSMLikeFilter()
	 * @generated
	 */
	EAttribute getFSMLikeFilter_IgnoreCase();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMNotFilter <em>FSM Not Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Not Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMNotFilter
	 * @generated
	 */
	EClass getFSMNotFilter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMNotFilter#getFilter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMNotFilter#getFilter()
	 * @see #getFSMNotFilter()
	 * @generated
	 */
	EReference getFSMNotFilter_Filter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter <em>FSM String Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM String Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStringFilter
	 * @generated
	 */
	EClass getFSMStringFilter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#getPropertyId <em>Property Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Property Id</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStringFilter#getPropertyId()
	 * @see #getFSMStringFilter()
	 * @generated
	 */
	EReference getFSMStringFilter_PropertyId();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#getFilterString <em>Filter String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter String</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStringFilter#getFilterString()
	 * @see #getFSMStringFilter()
	 * @generated
	 */
	EAttribute getFSMStringFilter_FilterString();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#isOnlyMatchPrefix <em>Only Match Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Only Match Prefix</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStringFilter#isOnlyMatchPrefix()
	 * @see #getFSMStringFilter()
	 * @generated
	 */
	EAttribute getFSMStringFilter_OnlyMatchPrefix();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#isIgnoreCase <em>Ignore Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ignore Case</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStringFilter#isIgnoreCase()
	 * @see #getFSMStringFilter()
	 * @generated
	 */
	EAttribute getFSMStringFilter_IgnoreCase();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlFilter <em>FSM Control Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Control Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlFilter
	 * @generated
	 */
	EClass getFSMControlFilter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.statemachine.FSMControlFilter#getFilter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Filter</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlFilter#getFilter()
	 * @see #getFSMControlFilter()
	 * @generated
	 */
	EReference getFSMControlFilter_Filter();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMInternalType <em>FSM Internal Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM Internal Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMInternalType
	 * @generated
	 */
	EEnum getFSMInternalType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType <em>FSM Control Button Event Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM Control Button Event Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType
	 * @generated
	 */
	EEnum getFSMControlButtonEventType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMCompareOperationEnum <em>FSM Compare Operation Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM Compare Operation Enum</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMCompareOperationEnum
	 * @generated
	 */
	EEnum getFSMCompareOperationEnum();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMEvaluationType <em>FSM Evaluation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM Evaluation Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMEvaluationType
	 * @generated
	 */
	EEnum getFSMEvaluationType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMUserMessageType <em>FSM User Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM User Message Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMUserMessageType
	 * @generated
	 */
	EEnum getFSMUserMessageType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayCursorType <em>FSM Line Display Cursor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM Line Display Cursor Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayCursorType
	 * @generated
	 */
	EEnum getFSMLineDisplayCursorType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeType <em>FSM Line Display Marquee Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM Line Display Marquee Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeType
	 * @generated
	 */
	EEnum getFSMLineDisplayMarqueeType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeFormat <em>FSM Line Display Marquee Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM Line Display Marquee Format</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeFormat
	 * @generated
	 */
	EEnum getFSMLineDisplayMarqueeFormat();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayTextType <em>FSM Line Display Text Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM Line Display Text Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayTextType
	 * @generated
	 */
	EEnum getFSMLineDisplayTextType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayScrollTextType <em>FSM Line Display Scroll Text Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM Line Display Scroll Text Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayScrollTextType
	 * @generated
	 */
	EEnum getFSMLineDisplayScrollTextType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMPOSPrinterBarcodeType <em>FSMPOS Printer Barcode Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSMPOS Printer Barcode Type</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPOSPrinterBarcodeType
	 * @generated
	 */
	EEnum getFSMPOSPrinterBarcodeType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.statemachine.FSMFunctionalKeyCodes <em>FSM Functional Key Codes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM Functional Key Codes</em>'.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFunctionalKeyCodes
	 * @generated
	 */
	EEnum getFSMFunctionalKeyCodes();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.InternalEObject <em>Internal EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Internal EObject</em>'.
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @model instanceClass="org.eclipse.emf.ecore.InternalEObject"
	 * @generated
	 */
	EDataType getInternalEObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StatemachineDSLFactory getStatemachineDSLFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMModelImpl <em>FSM Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMModelImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMModel()
		 * @generated
		 */
		EClass FSM_MODEL = eINSTANCE.getFSMModel();

		/**
		 * The meta object literal for the '<em><b>Import Section</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_MODEL__IMPORT_SECTION = eINSTANCE.getFSMModel_ImportSection();

		/**
		 * The meta object literal for the '<em><b>Packages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_MODEL__PACKAGES = eINSTANCE.getFSMModel_Packages();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMLazyResolverImpl <em>FSM Lazy Resolver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMLazyResolverImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLazyResolver()
		 * @generated
		 */
		EClass FSM_LAZY_RESOLVER = eINSTANCE.getFSMLazyResolver();

		/**
		 * The meta object literal for the '<em><b>EResolve Proxy</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = eINSTANCE.getFSMLazyResolver__EResolveProxy__InternalEObject();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMBaseImpl <em>FSM Base</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMBaseImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMBase()
		 * @generated
		 */
		EClass FSM_BASE = eINSTANCE.getFSMBase();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_BASE__NAME = eINSTANCE.getFSMBase_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPackageImpl <em>FSM Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPackageImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPackage()
		 * @generated
		 */
		EClass FSM_PACKAGE = eINSTANCE.getFSMPackage();

		/**
		 * The meta object literal for the '<em><b>Statemachines</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_PACKAGE__STATEMACHINES = eINSTANCE.getFSMPackage_Statemachines();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMImpl <em>FSM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSM()
		 * @generated
		 */
		EClass FSM = eINSTANCE.getFSM();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM__DESCRIPTION = eINSTANCE.getFSM_Description();

		/**
		 * The meta object literal for the '<em><b>Description Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM__DESCRIPTION_VALUE = eINSTANCE.getFSM_DescriptionValue();

		/**
		 * The meta object literal for the '<em><b>Initial Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM__INITIAL_EVENT = eINSTANCE.getFSM_InitialEvent();

		/**
		 * The meta object literal for the '<em><b>Initial State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM__INITIAL_STATE = eINSTANCE.getFSM_InitialState();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM__EVENTS = eINSTANCE.getFSM_Events();

		/**
		 * The meta object literal for the '<em><b>Controls</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM__CONTROLS = eINSTANCE.getFSM_Controls();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM__STATES = eINSTANCE.getFSM_States();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMControl <em>FSM Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMControl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControl()
		 * @generated
		 */
		EClass FSM_CONTROL = eINSTANCE.getFSMControl();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonImpl <em>FSM Control Button</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButton()
		 * @generated
		 */
		EClass FSM_CONTROL_BUTTON = eINSTANCE.getFSMControlButton();

		/**
		 * The meta object literal for the '<em><b>Event Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_BUTTON__EVENT_TYPE = eINSTANCE.getFSMControlButton_EventType();

		/**
		 * The meta object literal for the '<em><b>Buttons</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_BUTTON__BUTTONS = eINSTANCE.getFSMControlButton_Buttons();

		/**
		 * The meta object literal for the '<em><b>Has Range</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_BUTTON__HAS_RANGE = eINSTANCE.getFSMControlButton_HasRange();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_BUTTON__START = eINSTANCE.getFSMControlButton_Start();

		/**
		 * The meta object literal for the '<em><b>End</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_BUTTON__END = eINSTANCE.getFSMControlButton_End();

		/**
		 * The meta object literal for the '<em><b>Ranged Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_BUTTON__RANGED_NAME = eINSTANCE.getFSMControlButton_RangedName();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldImpl <em>FSM Control Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlField()
		 * @generated
		 */
		EClass FSM_CONTROL_FIELD = eINSTANCE.getFSMControlField();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_FIELD__FIELDS = eINSTANCE.getFSMControlField_Fields();

		/**
		 * The meta object literal for the '<em><b>Layouts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_FIELD__LAYOUTS = eINSTANCE.getFSMControlField_Layouts();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOImpl <em>FSM Control DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlDTO()
		 * @generated
		 */
		EClass FSM_CONTROL_DTO = eINSTANCE.getFSMControlDTO();

		/**
		 * The meta object literal for the '<em><b>Dtos</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_DTO__DTOS = eINSTANCE.getFSMControlDTO_Dtos();

		/**
		 * The meta object literal for the '<em><b>Filters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_DTO__FILTERS = eINSTANCE.getFSMControlDTO_Filters();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlSchedulerImpl <em>FSM Control Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlSchedulerImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlScheduler()
		 * @generated
		 */
		EClass FSM_CONTROL_SCHEDULER = eINSTANCE.getFSMControlScheduler();

		/**
		 * The meta object literal for the '<em><b>Schedulers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_SCHEDULER__SCHEDULERS = eINSTANCE.getFSMControlScheduler_Schedulers();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlPeripheralImpl <em>FSM Control Peripheral</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlPeripheralImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlPeripheral()
		 * @generated
		 */
		EClass FSM_CONTROL_PERIPHERAL = eINSTANCE.getFSMControlPeripheral();

		/**
		 * The meta object literal for the '<em><b>Line Displays</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_PERIPHERAL__LINE_DISPLAYS = eINSTANCE.getFSMControlPeripheral_LineDisplays();

		/**
		 * The meta object literal for the '<em><b>Displays</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_PERIPHERAL__DISPLAYS = eINSTANCE.getFSMControlPeripheral_Displays();

		/**
		 * The meta object literal for the '<em><b>Pos Printers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_PERIPHERAL__POS_PRINTERS = eINSTANCE.getFSMControlPeripheral_PosPrinters();

		/**
		 * The meta object literal for the '<em><b>Cash Drawers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_PERIPHERAL__CASH_DRAWERS = eINSTANCE.getFSMControlPeripheral_CashDrawers();

		/**
		 * The meta object literal for the '<em><b>Payment Terminals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_PERIPHERAL__PAYMENT_TERMINALS = eINSTANCE.getFSMControlPeripheral_PaymentTerminals();

		/**
		 * The meta object literal for the '<em><b>Signature Pads</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_PERIPHERAL__SIGNATURE_PADS = eINSTANCE.getFSMControlPeripheral_SignaturePads();

		/**
		 * The meta object literal for the '<em><b>Scales</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_PERIPHERAL__SCALES = eINSTANCE.getFSMControlPeripheral_Scales();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeImpl <em>FSM Control Button Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonAttribute()
		 * @generated
		 */
		EClass FSM_CONTROL_BUTTON_ATTRIBUTE = eINSTANCE.getFSMControlButtonAttribute();

		/**
		 * The meta object literal for the '<em><b>Has Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_BUTTON_ATTRIBUTE__HAS_IMAGE = eINSTANCE.getFSMControlButtonAttribute_HasImage();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_BUTTON_ATTRIBUTE__IMAGE = eINSTANCE.getFSMControlButtonAttribute_Image();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT = eINSTANCE.getFSMControlButtonAttribute_Event();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEvent <em>FSM Control Button Attribute Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEvent
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonAttributeEvent()
		 * @generated
		 */
		EClass FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT = eINSTANCE.getFSMControlButtonAttributeEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventKeyboardImpl <em>FSM Control Button Attribute Event Keyboard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventKeyboardImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonAttributeEventKeyboard()
		 * @generated
		 */
		EClass FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD = eINSTANCE.getFSMControlButtonAttributeEventKeyboard();

		/**
		 * The meta object literal for the '<em><b>Keystroke</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD__KEYSTROKE = eINSTANCE.getFSMControlButtonAttributeEventKeyboard_Keystroke();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventIdentityImpl <em>FSM Control Button Attribute Event Identity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventIdentityImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonAttributeEventIdentity()
		 * @generated
		 */
		EClass FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_IDENTITY = eINSTANCE.getFSMControlButtonAttributeEventIdentity();

		/**
		 * The meta object literal for the '<em><b>Identity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_IDENTITY__IDENTITY = eINSTANCE.getFSMControlButtonAttributeEventIdentity_Identity();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventEventImpl <em>FSM Control Button Attribute Event Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventEventImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonAttributeEventEvent()
		 * @generated
		 */
		EClass FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_EVENT = eINSTANCE.getFSMControlButtonAttributeEventEvent();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_EVENT__EVENT = eINSTANCE.getFSMControlButtonAttributeEventEvent_Event();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlVisibility <em>FSM Control Visibility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMControlVisibility
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlVisibility()
		 * @generated
		 */
		EClass FSM_CONTROL_VISIBILITY = eINSTANCE.getFSMControlVisibility();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldAttributeImpl <em>FSM Control Field Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldAttributeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlFieldAttribute()
		 * @generated
		 */
		EClass FSM_CONTROL_FIELD_ATTRIBUTE = eINSTANCE.getFSMControlFieldAttribute();

		/**
		 * The meta object literal for the '<em><b>Attribute Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_FIELD_ATTRIBUTE__ATTRIBUTE_TYPE = eINSTANCE.getFSMControlFieldAttribute_AttributeType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldLayoutImpl <em>FSM Control Field Layout</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlFieldLayoutImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlFieldLayout()
		 * @generated
		 */
		EClass FSM_CONTROL_FIELD_LAYOUT = eINSTANCE.getFSMControlFieldLayout();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOAttributeImpl <em>FSM Control DTO Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOAttributeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlDTOAttribute()
		 * @generated
		 */
		EClass FSM_CONTROL_DTO_ATTRIBUTE = eINSTANCE.getFSMControlDTOAttribute();

		/**
		 * The meta object literal for the '<em><b>Attribute Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE = eINSTANCE.getFSMControlDTOAttribute_AttributeType();

		/**
		 * The meta object literal for the '<em><b>Has Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_DTO_ATTRIBUTE__HAS_EVENT = eINSTANCE.getFSMControlDTOAttribute_HasEvent();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_DTO_ATTRIBUTE__EVENT = eINSTANCE.getFSMControlDTOAttribute_Event();

		/**
		 * The meta object literal for the '<em><b>Is Attached</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_DTO_ATTRIBUTE__IS_ATTACHED = eINSTANCE.getFSMControlDTOAttribute_IsAttached();

		/**
		 * The meta object literal for the '<em><b>Display</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_DTO_ATTRIBUTE__DISPLAY = eINSTANCE.getFSMControlDTOAttribute_Display();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevice <em>FSM Peripheral Device</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevice
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDevice()
		 * @generated
		 */
		EClass FSM_PERIPHERAL_DEVICE = eINSTANCE.getFSMPeripheralDevice();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceDisplayImpl <em>FSM Peripheral Device Display</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceDisplayImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDeviceDisplay()
		 * @generated
		 */
		EClass FSM_PERIPHERAL_DEVICE_DISPLAY = eINSTANCE.getFSMPeripheralDeviceDisplay();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_PERIPHERAL_DEVICE_DISPLAY__OUTPUT = eINSTANCE.getFSMPeripheralDeviceDisplay_Output();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceLineDisplayImpl <em>FSM Peripheral Device Line Display</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceLineDisplayImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDeviceLineDisplay()
		 * @generated
		 */
		EClass FSM_PERIPHERAL_DEVICE_LINE_DISPLAY = eINSTANCE.getFSMPeripheralDeviceLineDisplay();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDevicePOSPrinterImpl <em>FSM Peripheral Device POS Printer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDevicePOSPrinterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDevicePOSPrinter()
		 * @generated
		 */
		EClass FSM_PERIPHERAL_DEVICE_POS_PRINTER = eINSTANCE.getFSMPeripheralDevicePOSPrinter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceCashDrawerImpl <em>FSM Peripheral Device Cash Drawer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceCashDrawerImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDeviceCashDrawer()
		 * @generated
		 */
		EClass FSM_PERIPHERAL_DEVICE_CASH_DRAWER = eINSTANCE.getFSMPeripheralDeviceCashDrawer();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDevicePTImpl <em>FSM Peripheral Device PT</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDevicePTImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDevicePT()
		 * @generated
		 */
		EClass FSM_PERIPHERAL_DEVICE_PT = eINSTANCE.getFSMPeripheralDevicePT();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceSignatureImpl <em>FSM Peripheral Device Signature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceSignatureImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDeviceSignature()
		 * @generated
		 */
		EClass FSM_PERIPHERAL_DEVICE_SIGNATURE = eINSTANCE.getFSMPeripheralDeviceSignature();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceScaleImpl <em>FSM Peripheral Device Scale</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMPeripheralDeviceScaleImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPeripheralDeviceScale()
		 * @generated
		 */
		EClass FSM_PERIPHERAL_DEVICE_SCALE = eINSTANCE.getFSMPeripheralDeviceScale();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlSchedulerAttributeImpl <em>FSM Control Scheduler Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlSchedulerAttributeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlSchedulerAttribute()
		 * @generated
		 */
		EClass FSM_CONTROL_SCHEDULER_ATTRIBUTE = eINSTANCE.getFSMControlSchedulerAttribute();

		/**
		 * The meta object literal for the '<em><b>Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_CONTROL_SCHEDULER_ATTRIBUTE__DELAY = eINSTANCE.getFSMControlSchedulerAttribute_Delay();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_SCHEDULER_ATTRIBUTE__EVENT = eINSTANCE.getFSMControlSchedulerAttribute_Event();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMEventImpl <em>FSM Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMEventImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMEvent()
		 * @generated
		 */
		EClass FSM_EVENT = eINSTANCE.getFSMEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStateImpl <em>FSM State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMStateImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMState()
		 * @generated
		 */
		EClass FSM_STATE = eINSTANCE.getFSMState();

		/**
		 * The meta object literal for the '<em><b>Triggers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_STATE__TRIGGERS = eINSTANCE.getFSMState_Triggers();

		/**
		 * The meta object literal for the '<em><b>Conditions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_STATE__CONDITIONS = eINSTANCE.getFSMState_Conditions();

		/**
		 * The meta object literal for the '<em><b>Identity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_STATE__IDENTITY = eINSTANCE.getFSMState_Identity();

		/**
		 * The meta object literal for the '<em><b>Keystroke</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_STATE__KEYSTROKE = eINSTANCE.getFSMState_Keystroke();

		/**
		 * The meta object literal for the '<em><b>Has Key Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_STATE__HAS_KEY_OPERATION = eINSTANCE.getFSMState_HasKeyOperation();

		/**
		 * The meta object literal for the '<em><b>Key Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_STATE__KEY_OPERATION = eINSTANCE.getFSMState_KeyOperation();

		/**
		 * The meta object literal for the '<em><b>Key Mapper</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_STATE__KEY_MAPPER = eINSTANCE.getFSMState_KeyMapper();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMKeyMapperImpl <em>FSM Key Mapper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMKeyMapperImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMKeyMapper()
		 * @generated
		 */
		EClass FSM_KEY_MAPPER = eINSTANCE.getFSMKeyMapper();

		/**
		 * The meta object literal for the '<em><b>Key Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_KEY_MAPPER__KEY_CODE = eINSTANCE.getFSMKeyMapper_KeyCode();

		/**
		 * The meta object literal for the '<em><b>Key Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_KEY_MAPPER__KEY_EVENT = eINSTANCE.getFSMKeyMapper_KeyEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMTriggerImpl <em>FSM Trigger</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMTriggerImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMTrigger()
		 * @generated
		 */
		EClass FSM_TRIGGER = eINSTANCE.getFSMTrigger();

		/**
		 * The meta object literal for the '<em><b>Has Transition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_TRIGGER__HAS_TRANSITION = eINSTANCE.getFSMTrigger_HasTransition();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_TRIGGER__TRANSITION = eINSTANCE.getFSMTrigger_Transition();

		/**
		 * The meta object literal for the '<em><b>Triggers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_TRIGGER__TRIGGERS = eINSTANCE.getFSMTrigger_Triggers();

		/**
		 * The meta object literal for the '<em><b>Guards</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_TRIGGER__GUARDS = eINSTANCE.getFSMTrigger_Guards();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_TRIGGER__ACTIONS = eINSTANCE.getFSMTrigger_Actions();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMAction <em>FSM Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMAction
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMAction()
		 * @generated
		 */
		EClass FSM_ACTION = eINSTANCE.getFSMAction();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralBlinkRateImpl <em>FSM Action Peripheral Blink Rate</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralBlinkRateImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralBlinkRate()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_BLINK_RATE = eINSTANCE.getFSMActionPeripheralBlinkRate();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_BLINK_RATE__DEVICE = eINSTANCE.getFSMActionPeripheralBlinkRate_Device();

		/**
		 * The meta object literal for the '<em><b>Blink Rate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_BLINK_RATE__BLINK_RATE = eINSTANCE.getFSMActionPeripheralBlinkRate_BlinkRate();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralClearImpl <em>FSM Action Peripheral Clear</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralClearImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralClear()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_CLEAR = eINSTANCE.getFSMActionPeripheralClear();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_CLEAR__DEVICE = eINSTANCE.getFSMActionPeripheralClear_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCreateWindowImpl <em>FSM Action Peripheral Create Window</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCreateWindowImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralCreateWindow()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_CREATE_WINDOW = eINSTANCE.getFSMActionPeripheralCreateWindow();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_CREATE_WINDOW__DEVICE = eINSTANCE.getFSMActionPeripheralCreateWindow_Device();

		/**
		 * The meta object literal for the '<em><b>Viewport Row</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_ROW = eINSTANCE.getFSMActionPeripheralCreateWindow_ViewportRow();

		/**
		 * The meta object literal for the '<em><b>Viewport Column</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_COLUMN = eINSTANCE.getFSMActionPeripheralCreateWindow_ViewportColumn();

		/**
		 * The meta object literal for the '<em><b>Viewport Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_HEIGHT = eINSTANCE.getFSMActionPeripheralCreateWindow_ViewportHeight();

		/**
		 * The meta object literal for the '<em><b>Viewport Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_WIDTH = eINSTANCE.getFSMActionPeripheralCreateWindow_ViewportWidth();

		/**
		 * The meta object literal for the '<em><b>Window Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_HEIGHT = eINSTANCE.getFSMActionPeripheralCreateWindow_WindowHeight();

		/**
		 * The meta object literal for the '<em><b>Window Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_WIDTH = eINSTANCE.getFSMActionPeripheralCreateWindow_WindowWidth();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCursorTypeImpl <em>FSM Action Peripheral Cursor Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCursorTypeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralCursorType()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_CURSOR_TYPE = eINSTANCE.getFSMActionPeripheralCursorType();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_CURSOR_TYPE__DEVICE = eINSTANCE.getFSMActionPeripheralCursorType_Device();

		/**
		 * The meta object literal for the '<em><b>Cursor Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_CURSOR_TYPE__CURSOR_TYPE = eINSTANCE.getFSMActionPeripheralCursorType_CursorType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDestroyWindowImpl <em>FSM Action Peripheral Destroy Window</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDestroyWindowImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralDestroyWindow()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_DESTROY_WINDOW = eINSTANCE.getFSMActionPeripheralDestroyWindow();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_DESTROY_WINDOW__DEVICE = eINSTANCE.getFSMActionPeripheralDestroyWindow_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDeviceBrightnessImpl <em>FSM Action Peripheral Device Brightness</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDeviceBrightnessImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralDeviceBrightness()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS = eINSTANCE.getFSMActionPeripheralDeviceBrightness();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS__DEVICE = eINSTANCE.getFSMActionPeripheralDeviceBrightness_Device();

		/**
		 * The meta object literal for the '<em><b>Brightness</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS__BRIGHTNESS = eINSTANCE.getFSMActionPeripheralDeviceBrightness_Brightness();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDisplayTextImpl <em>FSM Action Peripheral Display Text</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralDisplayTextImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralDisplayText()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_DISPLAY_TEXT = eINSTANCE.getFSMActionPeripheralDisplayText();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__DEVICE = eINSTANCE.getFSMActionPeripheralDisplayText_Device();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__ATTRIBUTE = eINSTANCE.getFSMActionPeripheralDisplayText_Attribute();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__TEXT = eINSTANCE.getFSMActionPeripheralDisplayText_Text();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralLineDisplayTextImpl <em>FSM Action Peripheral Line Display Text</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralLineDisplayTextImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralLineDisplayText()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT = eINSTANCE.getFSMActionPeripheralLineDisplayText();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__DEVICE = eINSTANCE.getFSMActionPeripheralLineDisplayText_Device();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__TEXT = eINSTANCE.getFSMActionPeripheralLineDisplayText_Text();

		/**
		 * The meta object literal for the '<em><b>Has Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__HAS_TYPE = eINSTANCE.getFSMActionPeripheralLineDisplayText_HasType();

		/**
		 * The meta object literal for the '<em><b>Text Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__TEXT_TYPE = eINSTANCE.getFSMActionPeripheralLineDisplayText_TextType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralLineDisplayTextAtImpl <em>FSM Action Peripheral Line Display Text At</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralLineDisplayTextAtImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralLineDisplayTextAt()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT = eINSTANCE.getFSMActionPeripheralLineDisplayTextAt();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__DEVICE = eINSTANCE.getFSMActionPeripheralLineDisplayTextAt_Device();

		/**
		 * The meta object literal for the '<em><b>Row</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__ROW = eINSTANCE.getFSMActionPeripheralLineDisplayTextAt_Row();

		/**
		 * The meta object literal for the '<em><b>Column</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__COLUMN = eINSTANCE.getFSMActionPeripheralLineDisplayTextAt_Column();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__TEXT = eINSTANCE.getFSMActionPeripheralLineDisplayTextAt_Text();

		/**
		 * The meta object literal for the '<em><b>Has Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__HAS_TYPE = eINSTANCE.getFSMActionPeripheralLineDisplayTextAt_HasType();

		/**
		 * The meta object literal for the '<em><b>Text Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__TEXT_TYPE = eINSTANCE.getFSMActionPeripheralLineDisplayTextAt_TextType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralInterCharacterWaitImpl <em>FSM Action Peripheral Inter Character Wait</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralInterCharacterWaitImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralInterCharacterWait()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT = eINSTANCE.getFSMActionPeripheralInterCharacterWait();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT__DEVICE = eINSTANCE.getFSMActionPeripheralInterCharacterWait_Device();

		/**
		 * The meta object literal for the '<em><b>Wait</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT__WAIT = eINSTANCE.getFSMActionPeripheralInterCharacterWait_Wait();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeFormatImpl <em>FSM Action Peripheral Marquee Format</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeFormatImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralMarqueeFormat()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT = eINSTANCE.getFSMActionPeripheralMarqueeFormat();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT__DEVICE = eINSTANCE.getFSMActionPeripheralMarqueeFormat_Device();

		/**
		 * The meta object literal for the '<em><b>Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT__FORMAT = eINSTANCE.getFSMActionPeripheralMarqueeFormat_Format();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeRepeatWaitImpl <em>FSM Action Peripheral Marquee Repeat Wait</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeRepeatWaitImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralMarqueeRepeatWait()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT = eINSTANCE.getFSMActionPeripheralMarqueeRepeatWait();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT__DEVICE = eINSTANCE.getFSMActionPeripheralMarqueeRepeatWait_Device();

		/**
		 * The meta object literal for the '<em><b>Wait</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT__WAIT = eINSTANCE.getFSMActionPeripheralMarqueeRepeatWait_Wait();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeTypeImpl <em>FSM Action Peripheral Marquee Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeTypeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralMarqueeType()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_MARQUEE_TYPE = eINSTANCE.getFSMActionPeripheralMarqueeType();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__DEVICE = eINSTANCE.getFSMActionPeripheralMarqueeType_Device();

		/**
		 * The meta object literal for the '<em><b>Marquee Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__MARQUEE_TYPE = eINSTANCE.getFSMActionPeripheralMarqueeType_MarqueeType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeUnitWaitImpl <em>FSM Action Peripheral Marquee Unit Wait</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeUnitWaitImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralMarqueeUnitWait()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT = eINSTANCE.getFSMActionPeripheralMarqueeUnitWait();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT__DEVICE = eINSTANCE.getFSMActionPeripheralMarqueeUnitWait_Device();

		/**
		 * The meta object literal for the '<em><b>Wait</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT__WAIT = eINSTANCE.getFSMActionPeripheralMarqueeUnitWait_Wait();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScrollImpl <em>FSM Action Peripheral Scroll</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScrollImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScroll()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SCROLL = eINSTANCE.getFSMActionPeripheralScroll();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SCROLL__DEVICE = eINSTANCE.getFSMActionPeripheralScroll_Device();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_SCROLL__DIRECTION = eINSTANCE.getFSMActionPeripheralScroll_Direction();

		/**
		 * The meta object literal for the '<em><b>Units</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_SCROLL__UNITS = eINSTANCE.getFSMActionPeripheralScroll_Units();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralOpenDrawerImpl <em>FSM Action Peripheral Open Drawer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralOpenDrawerImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralOpenDrawer()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_OPEN_DRAWER = eINSTANCE.getFSMActionPeripheralOpenDrawer();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_OPEN_DRAWER__DEVICE = eINSTANCE.getFSMActionPeripheralOpenDrawer_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintBarcodeImpl <em>FSM Action Peripheral Print Barcode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintBarcodeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintBarcode()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PRINT_BARCODE = eINSTANCE.getFSMActionPeripheralPrintBarcode();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PRINT_BARCODE__DEVICE = eINSTANCE.getFSMActionPeripheralPrintBarcode_Device();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_PRINT_BARCODE__DATA = eINSTANCE.getFSMActionPeripheralPrintBarcode_Data();

		/**
		 * The meta object literal for the '<em><b>Barcode Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_PRINT_BARCODE__BARCODE_TYPE = eINSTANCE.getFSMActionPeripheralPrintBarcode_BarcodeType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintBitmapImpl <em>FSM Action Peripheral Print Bitmap</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintBitmapImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintBitmap()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PRINT_BITMAP = eINSTANCE.getFSMActionPeripheralPrintBitmap();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PRINT_BITMAP__DEVICE = eINSTANCE.getFSMActionPeripheralPrintBitmap_Device();

		/**
		 * The meta object literal for the '<em><b>Bitmap Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_PRINT_BITMAP__BITMAP_ID = eINSTANCE.getFSMActionPeripheralPrintBitmap_BitmapId();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintCutImpl <em>FSM Action Peripheral Print Cut</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintCutImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintCut()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PRINT_CUT = eINSTANCE.getFSMActionPeripheralPrintCut();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PRINT_CUT__DEVICE = eINSTANCE.getFSMActionPeripheralPrintCut_Device();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PRINT_CUT__TEXT = eINSTANCE.getFSMActionPeripheralPrintCut_Text();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintRegisterBitmapImpl <em>FSM Action Peripheral Print Register Bitmap</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintRegisterBitmapImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintRegisterBitmap()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP = eINSTANCE.getFSMActionPeripheralPrintRegisterBitmap();

		/**
		 * The meta object literal for the '<em><b>Bitmap Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP__BITMAP_ID = eINSTANCE.getFSMActionPeripheralPrintRegisterBitmap_BitmapId();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP__NAME = eINSTANCE.getFSMActionPeripheralPrintRegisterBitmap_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintNormalImpl <em>FSM Action Peripheral Print Normal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintNormalImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintNormal()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PRINT_NORMAL = eINSTANCE.getFSMActionPeripheralPrintNormal();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PRINT_NORMAL__DEVICE = eINSTANCE.getFSMActionPeripheralPrintNormal_Device();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PRINT_NORMAL__TEXT = eINSTANCE.getFSMActionPeripheralPrintNormal_Text();

		/**
		 * The meta object literal for the '<em><b>Barcode Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_PRINT_NORMAL__BARCODE_TYPE = eINSTANCE.getFSMActionPeripheralPrintNormal_BarcodeType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTOpenImpl <em>FSM Action Peripheral PT Open</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTOpenImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTOpen()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PT_OPEN = eINSTANCE.getFSMActionPeripheralPTOpen();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_OPEN__DEVICE = eINSTANCE.getFSMActionPeripheralPTOpen_Device();

		/**
		 * The meta object literal for the '<em><b>Host</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_OPEN__HOST = eINSTANCE.getFSMActionPeripheralPTOpen_Host();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_OPEN__PORT = eINSTANCE.getFSMActionPeripheralPTOpen_Port();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTCloseImpl <em>FSM Action Peripheral PT Close</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTCloseImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTClose()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PT_CLOSE = eINSTANCE.getFSMActionPeripheralPTClose();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_CLOSE__DEVICE = eINSTANCE.getFSMActionPeripheralPTClose_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTReversalImpl <em>FSM Action Peripheral PT Reversal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTReversalImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTReversal()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PT_REVERSAL = eINSTANCE.getFSMActionPeripheralPTReversal();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_REVERSAL__DEVICE = eINSTANCE.getFSMActionPeripheralPTReversal_Device();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD = eINSTANCE.getFSMActionPeripheralPTReversal_Password();

		/**
		 * The meta object literal for the '<em><b>Receipt</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT = eINSTANCE.getFSMActionPeripheralPTReversal_Receipt();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTAcknowledgeImpl <em>FSM Action Peripheral PT Acknowledge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTAcknowledgeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTAcknowledge()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE = eINSTANCE.getFSMActionPeripheralPTAcknowledge();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE__DEVICE = eINSTANCE.getFSMActionPeripheralPTAcknowledge_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTRegistrationImpl <em>FSM Action Peripheral PT Registration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTRegistrationImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTRegistration()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PT_REGISTRATION = eINSTANCE.getFSMActionPeripheralPTRegistration();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_REGISTRATION__DEVICE = eINSTANCE.getFSMActionPeripheralPTRegistration_Device();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_REGISTRATION__PASSWORD = eINSTANCE.getFSMActionPeripheralPTRegistration_Password();

		/**
		 * The meta object literal for the '<em><b>Configuration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_PT_REGISTRATION__CONFIGURATION = eINSTANCE.getFSMActionPeripheralPTRegistration_Configuration();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTAuthorizationImpl <em>FSM Action Peripheral PT Authorization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTAuthorizationImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTAuthorization()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION = eINSTANCE.getFSMActionPeripheralPTAuthorization();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION__DEVICE = eINSTANCE.getFSMActionPeripheralPTAuthorization_Device();

		/**
		 * The meta object literal for the '<em><b>Amount</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION__AMOUNT = eINSTANCE.getFSMActionPeripheralPTAuthorization_Amount();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralBeeperImpl <em>FSM Action Peripheral Beeper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralBeeperImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralBeeper()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_BEEPER = eINSTANCE.getFSMActionPeripheralBeeper();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_BEEPER__DURATION = eINSTANCE.getFSMActionPeripheralBeeper_Duration();

		/**
		 * The meta object literal for the '<em><b>Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_BEEPER__FREQUENCY = eINSTANCE.getFSMActionPeripheralBeeper_Frequency();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPlayerImpl <em>FSM Action Peripheral Player</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPlayerImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPlayer()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PLAYER = eINSTANCE.getFSMActionPeripheralPlayer();

		/**
		 * The meta object literal for the '<em><b>Tune</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_PLAYER__TUNE = eINSTANCE.getFSMActionPeripheralPlayer_Tune();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSoundImpl <em>FSM Action Peripheral Sound</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSoundImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSound()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SOUND = eINSTANCE.getFSMActionPeripheralSound();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_SOUND__NAME = eINSTANCE.getFSMActionPeripheralSound_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTResponseImpl <em>FSM Action Peripheral PT Response</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTResponseImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPTResponse()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PT_RESPONSE = eINSTANCE.getFSMActionPeripheralPTResponse();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PT_RESPONSE__DEVICE = eINSTANCE.getFSMActionPeripheralPTResponse_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintReportImpl <em>FSM Action Peripheral Print Report</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPrintReportImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralPrintReport()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_PRINT_REPORT = eINSTANCE.getFSMActionPeripheralPrintReport();

		/**
		 * The meta object literal for the '<em><b>Report</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PRINT_REPORT__REPORT = eINSTANCE.getFSMActionPeripheralPrintReport_Report();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PRINT_REPORT__KEY = eINSTANCE.getFSMActionPeripheralPrintReport_Key();

		/**
		 * The meta object literal for the '<em><b>Has Filter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_PRINT_REPORT__HAS_FILTER = eINSTANCE.getFSMActionPeripheralPrintReport_HasFilter();

		/**
		 * The meta object literal for the '<em><b>Has Print Service</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_PRINT_REPORT__HAS_PRINT_SERVICE = eINSTANCE.getFSMActionPeripheralPrintReport_HasPrintService();

		/**
		 * The meta object literal for the '<em><b>Print Service</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_PRINT_REPORT__PRINT_SERVICE = eINSTANCE.getFSMActionPeripheralPrintReport_PrintService();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureOpenImpl <em>FSM Action Peripheral Signature Open</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureOpenImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureOpen()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN = eINSTANCE.getFSMActionPeripheralSignatureOpen();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN__DEVICE = eINSTANCE.getFSMActionPeripheralSignatureOpen_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureCloseImpl <em>FSM Action Peripheral Signature Close</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureCloseImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureClose()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE = eINSTANCE.getFSMActionPeripheralSignatureClose();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE__DEVICE = eINSTANCE.getFSMActionPeripheralSignatureClose_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureClearImpl <em>FSM Action Peripheral Signature Clear</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureClearImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureClear()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR = eINSTANCE.getFSMActionPeripheralSignatureClear();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR__DEVICE = eINSTANCE.getFSMActionPeripheralSignatureClear_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureCaptureImpl <em>FSM Action Peripheral Signature Capture</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureCaptureImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureCapture()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE = eINSTANCE.getFSMActionPeripheralSignatureCapture();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE__DEVICE = eINSTANCE.getFSMActionPeripheralSignatureCapture_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureIdleImpl <em>FSM Action Peripheral Signature Idle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureIdleImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureIdle()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE = eINSTANCE.getFSMActionPeripheralSignatureIdle();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE__DEVICE = eINSTANCE.getFSMActionPeripheralSignatureIdle_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureLabelImpl <em>FSM Action Peripheral Signature Label</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralSignatureLabelImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralSignatureLabel()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL = eINSTANCE.getFSMActionPeripheralSignatureLabel();

		/**
		 * The meta object literal for the '<em><b>Ok Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__OK_LABEL = eINSTANCE.getFSMActionPeripheralSignatureLabel_OkLabel();

		/**
		 * The meta object literal for the '<em><b>Clear Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__CLEAR_LABEL = eINSTANCE.getFSMActionPeripheralSignatureLabel_ClearLabel();

		/**
		 * The meta object literal for the '<em><b>Cancel Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__CANCEL_LABEL = eINSTANCE.getFSMActionPeripheralSignatureLabel_CancelLabel();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__DEVICE = eINSTANCE.getFSMActionPeripheralSignatureLabel_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMSignatureRetrieveImpl <em>FSM Signature Retrieve</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMSignatureRetrieveImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMSignatureRetrieve()
		 * @generated
		 */
		EClass FSM_SIGNATURE_RETRIEVE = eINSTANCE.getFSMSignatureRetrieve();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_SIGNATURE_RETRIEVE__DEVICE = eINSTANCE.getFSMSignatureRetrieve_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleReadWeightImpl <em>FSM Action Peripheral Scale Read Weight</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleReadWeightImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleReadWeight()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT = eINSTANCE.getFSMActionPeripheralScaleReadWeight();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT__DEVICE = eINSTANCE.getFSMActionPeripheralScaleReadWeight_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleReadTareWeightImpl <em>FSM Action Peripheral Scale Read Tare Weight</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleReadTareWeightImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleReadTareWeight()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT = eINSTANCE.getFSMActionPeripheralScaleReadTareWeight();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT__DEVICE = eINSTANCE.getFSMActionPeripheralScaleReadTareWeight_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleTareWeightImpl <em>FSM Action Peripheral Scale Tare Weight</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleTareWeightImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleTareWeight()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT = eINSTANCE.getFSMActionPeripheralScaleTareWeight();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT__DEVICE = eINSTANCE.getFSMActionPeripheralScaleTareWeight_Device();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT__VALUE = eINSTANCE.getFSMActionPeripheralScaleTareWeight_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleZeroImpl <em>FSM Action Peripheral Scale Zero</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleZeroImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleZero()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SCALE_ZERO = eINSTANCE.getFSMActionPeripheralScaleZero();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SCALE_ZERO__DEVICE = eINSTANCE.getFSMActionPeripheralScaleZero_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleDisplayTextImpl <em>FSM Action Peripheral Scale Display Text</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleDisplayTextImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleDisplayText()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT = eINSTANCE.getFSMActionPeripheralScaleDisplayText();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT__DEVICE = eINSTANCE.getFSMActionPeripheralScaleDisplayText_Device();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT__TEXT = eINSTANCE.getFSMActionPeripheralScaleDisplayText_Text();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleWeightUnitImpl <em>FSM Action Peripheral Scale Weight Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralScaleWeightUnitImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionPeripheralScaleWeightUnit()
		 * @generated
		 */
		EClass FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT = eINSTANCE.getFSMActionPeripheralScaleWeightUnit();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT__DEVICE = eINSTANCE.getFSMActionPeripheralScaleWeightUnit_Device();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSource <em>FSM Action Field Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSource
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSource()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_SOURCE = eINSTANCE.getFSMActionFieldSource();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceStringImpl <em>FSM Action Field Source String</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceStringImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceString()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_SOURCE_STRING = eINSTANCE.getFSMActionFieldSourceString();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_FIELD_SOURCE_STRING__TEXT = eINSTANCE.getFSMActionFieldSourceString_Text();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceNumberImpl <em>FSM Action Field Source Number</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceNumberImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceNumber()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_SOURCE_NUMBER = eINSTANCE.getFSMActionFieldSourceNumber();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_FIELD_SOURCE_NUMBER__VALUE = eINSTANCE.getFSMActionFieldSourceNumber_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceIntegerImpl <em>FSM Action Field Source Integer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceIntegerImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceInteger()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_SOURCE_INTEGER = eINSTANCE.getFSMActionFieldSourceInteger();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_FIELD_SOURCE_INTEGER__VALUE = eINSTANCE.getFSMActionFieldSourceInteger_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceBooleanImpl <em>FSM Action Field Source Boolean</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceBooleanImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceBoolean()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_SOURCE_BOOLEAN = eINSTANCE.getFSMActionFieldSourceBoolean();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_FIELD_SOURCE_BOOLEAN__VALUE = eINSTANCE.getFSMActionFieldSourceBoolean_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceEvaluateImpl <em>FSM Action Field Source Evaluate</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceEvaluateImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceEvaluate()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_SOURCE_EVALUATE = eINSTANCE.getFSMActionFieldSourceEvaluate();

		/**
		 * The meta object literal for the '<em><b>Evaluationtype</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_FIELD_SOURCE_EVALUATE__EVALUATIONTYPE = eINSTANCE.getFSMActionFieldSourceEvaluate_Evaluationtype();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceTranslateImpl <em>FSM Action Field Source Translate</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceTranslateImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceTranslate()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_SOURCE_TRANSLATE = eINSTANCE.getFSMActionFieldSourceTranslate();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_FIELD_SOURCE_TRANSLATE__TEXT = eINSTANCE.getFSMActionFieldSourceTranslate_Text();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMRef <em>FSM Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMRef
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMRef()
		 * @generated
		 */
		EClass FSM_REF = eINSTANCE.getFSMRef();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMDotExpressionImpl <em>FSM Dot Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMDotExpressionImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMDotExpression()
		 * @generated
		 */
		EClass FSM_DOT_EXPRESSION = eINSTANCE.getFSMDotExpression();

		/**
		 * The meta object literal for the '<em><b>Ref</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_DOT_EXPRESSION__REF = eINSTANCE.getFSMDotExpression_Ref();

		/**
		 * The meta object literal for the '<em><b>Tail</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_DOT_EXPRESSION__TAIL = eINSTANCE.getFSMDotExpression_Tail();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMDtoRefImpl <em>FSM Dto Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMDtoRefImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMDtoRef()
		 * @generated
		 */
		EClass FSM_DTO_REF = eINSTANCE.getFSMDtoRef();

		/**
		 * The meta object literal for the '<em><b>Dto</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_DTO_REF__DTO = eINSTANCE.getFSMDtoRef_Dto();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceDtoAttributeImpl <em>FSM Action Field Source Dto Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceDtoAttributeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceDtoAttribute()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE = eINSTANCE.getFSMActionFieldSourceDtoAttribute();

		/**
		 * The meta object literal for the '<em><b>Dto</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE__DTO = eINSTANCE.getFSMActionFieldSourceDtoAttribute_Dto();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE__ATTRIBUTE = eINSTANCE.getFSMActionFieldSourceDtoAttribute_Attribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceEventImpl <em>FSM Action Field Source Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceEventImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSourceEvent()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_SOURCE_EVENT = eINSTANCE.getFSMActionFieldSourceEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionConditionalTransitionImpl <em>FSM Action Conditional Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionConditionalTransitionImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionConditionalTransition()
		 * @generated
		 */
		EClass FSM_ACTION_CONDITIONAL_TRANSITION = eINSTANCE.getFSMActionConditionalTransition();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_CONDITIONAL_TRANSITION__TRANSITION = eINSTANCE.getFSMActionConditionalTransition_Transition();

		/**
		 * The meta object literal for the '<em><b>Guard</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_CONDITIONAL_TRANSITION__GUARD = eINSTANCE.getFSMActionConditionalTransition_Guard();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_CONDITIONAL_TRANSITION__ACTIONS = eINSTANCE.getFSMActionConditionalTransition_Actions();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMOperationParameterImpl <em>FSM Operation Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMOperationParameterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMOperationParameter()
		 * @generated
		 */
		EClass FSM_OPERATION_PARAMETER = eINSTANCE.getFSMOperationParameter();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_OPERATION_PARAMETER__SOURCE = eINSTANCE.getFSMOperationParameter_Source();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMOperationImpl <em>FSM Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMOperationImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMOperation()
		 * @generated
		 */
		EClass FSM_OPERATION = eINSTANCE.getFSMOperation();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_OPERATION__GROUP = eINSTANCE.getFSMOperation_Group();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_OPERATION__OPERATION = eINSTANCE.getFSMOperation_Operation();

		/**
		 * The meta object literal for the '<em><b>First</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_OPERATION__FIRST = eINSTANCE.getFSMOperation_First();

		/**
		 * The meta object literal for the '<em><b>More</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_OPERATION__MORE = eINSTANCE.getFSMOperation_More();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMGuardImpl <em>FSM Guard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMGuardImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMGuard()
		 * @generated
		 */
		EClass FSM_GUARD = eINSTANCE.getFSMGuard();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_GUARD__GROUP = eINSTANCE.getFSMGuard_Group();

		/**
		 * The meta object literal for the '<em><b>Guard</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_GUARD__GUARD = eINSTANCE.getFSMGuard_Guard();

		/**
		 * The meta object literal for the '<em><b>Has On Fail</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_GUARD__HAS_ON_FAIL = eINSTANCE.getFSMGuard_HasOnFail();

		/**
		 * The meta object literal for the '<em><b>On Fail Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_GUARD__ON_FAIL_DESCRIPTION = eINSTANCE.getFSMGuard_OnFailDescription();

		/**
		 * The meta object literal for the '<em><b>On Fail Caption</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_GUARD__ON_FAIL_CAPTION = eINSTANCE.getFSMGuard_OnFailCaption();

		/**
		 * The meta object literal for the '<em><b>On Fail Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_GUARD__ON_FAIL_TYPE = eINSTANCE.getFSMGuard_OnFailType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMFunctionImpl <em>FSM Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMFunctionImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMFunction()
		 * @generated
		 */
		EClass FSM_FUNCTION = eINSTANCE.getFSMFunction();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_FUNCTION__GROUP = eINSTANCE.getFSMFunction_Group();

		/**
		 * The meta object literal for the '<em><b>Function</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_FUNCTION__FUNCTION = eINSTANCE.getFSMFunction_Function();

		/**
		 * The meta object literal for the '<em><b>First</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_FUNCTION__FIRST = eINSTANCE.getFSMFunction_First();

		/**
		 * The meta object literal for the '<em><b>More</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_FUNCTION__MORE = eINSTANCE.getFSMFunction_More();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStorageRetrieveImpl <em>FSM Storage Retrieve</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMStorageRetrieveImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMStorageRetrieve()
		 * @generated
		 */
		EClass FSM_STORAGE_RETRIEVE = eINSTANCE.getFSMStorageRetrieve();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_STORAGE_RETRIEVE__KEY = eINSTANCE.getFSMStorageRetrieve_Key();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_STORAGE_RETRIEVE__ATTRIBUTE = eINSTANCE.getFSMStorageRetrieve_Attribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStorageImpl <em>FSM Storage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMStorageImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMStorage()
		 * @generated
		 */
		EClass FSM_STORAGE = eINSTANCE.getFSMStorage();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_STORAGE__KEY = eINSTANCE.getFSMStorage_Key();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_STORAGE__ATTRIBUTE = eINSTANCE.getFSMStorage_Attribute();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_STORAGE__CONTENT = eINSTANCE.getFSMStorage_Content();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldConcatenationImpl <em>FSM Action Field Concatenation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldConcatenationImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldConcatenation()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_CONCATENATION = eINSTANCE.getFSMActionFieldConcatenation();

		/**
		 * The meta object literal for the '<em><b>First</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_FIELD_CONCATENATION__FIRST = eINSTANCE.getFSMActionFieldConcatenation_First();

		/**
		 * The meta object literal for the '<em><b>More</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_FIELD_CONCATENATION__MORE = eINSTANCE.getFSMActionFieldConcatenation_More();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSetImpl <em>FSM Action Field Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSetImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldSet()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_SET = eINSTANCE.getFSMActionFieldSet();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_FIELD_SET__ATTRIBUTE = eINSTANCE.getFSMActionFieldSet_Attribute();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_FIELD_SET__SOURCE = eINSTANCE.getFSMActionFieldSet_Source();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldKeystrokeImpl <em>FSM Action Field Keystroke</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldKeystrokeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldKeystroke()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_KEYSTROKE = eINSTANCE.getFSMActionFieldKeystroke();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_FIELD_KEYSTROKE__ATTRIBUTE = eINSTANCE.getFSMActionFieldKeystroke_Attribute();

		/**
		 * The meta object literal for the '<em><b>Keystroke</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_FIELD_KEYSTROKE__KEYSTROKE = eINSTANCE.getFSMActionFieldKeystroke_Keystroke();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldClearImpl <em>FSM Action Field Clear</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldClearImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldClear()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_CLEAR = eINSTANCE.getFSMActionFieldClear();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_FIELD_CLEAR__ATTRIBUTE = eINSTANCE.getFSMActionFieldClear_Attribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldGetImpl <em>FSM Action Field Get</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldGetImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldGet()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_GET = eINSTANCE.getFSMActionFieldGet();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_FIELD_GET__ATTRIBUTE = eINSTANCE.getFSMActionFieldGet_Attribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldFilterToggleImpl <em>FSM Action Field Filter Toggle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldFilterToggleImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldFilterToggle()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_FILTER_TOGGLE = eINSTANCE.getFSMActionFieldFilterToggle();

		/**
		 * The meta object literal for the '<em><b>Filter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_FIELD_FILTER_TOGGLE__FILTER = eINSTANCE.getFSMActionFieldFilterToggle_Filter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldRemoveImpl <em>FSM Action Field Remove</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldRemoveImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionFieldRemove()
		 * @generated
		 */
		EClass FSM_ACTION_FIELD_REMOVE = eINSTANCE.getFSMActionFieldRemove();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_FIELD_REMOVE__ATTRIBUTE = eINSTANCE.getFSMActionFieldRemove_Attribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionItemVisibleImpl <em>FSM Action Item Visible</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionItemVisibleImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionItemVisible()
		 * @generated
		 */
		EClass FSM_ACTION_ITEM_VISIBLE = eINSTANCE.getFSMActionItemVisible();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_ITEM_VISIBLE__ATTRIBUTE = eINSTANCE.getFSMActionItemVisible_Attribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionItemInvisibleImpl <em>FSM Action Item Invisible</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionItemInvisibleImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionItemInvisible()
		 * @generated
		 */
		EClass FSM_ACTION_ITEM_INVISIBLE = eINSTANCE.getFSMActionItemInvisible();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_ITEM_INVISIBLE__ATTRIBUTE = eINSTANCE.getFSMActionItemInvisible_Attribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionButtonCaptionImpl <em>FSM Action Button Caption</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionButtonCaptionImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionButtonCaption()
		 * @generated
		 */
		EClass FSM_ACTION_BUTTON_CAPTION = eINSTANCE.getFSMActionButtonCaption();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_BUTTON_CAPTION__ATTRIBUTE = eINSTANCE.getFSMActionButtonCaption_Attribute();

		/**
		 * The meta object literal for the '<em><b>Caption</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_BUTTON_CAPTION__CAPTION = eINSTANCE.getFSMActionButtonCaption_Caption();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionButtonImageImpl <em>FSM Action Button Image</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionButtonImageImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionButtonImage()
		 * @generated
		 */
		EClass FSM_ACTION_BUTTON_IMAGE = eINSTANCE.getFSMActionButtonImage();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_BUTTON_IMAGE__ATTRIBUTE = eINSTANCE.getFSMActionButtonImage_Attribute();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_ACTION_BUTTON_IMAGE__IMAGE = eINSTANCE.getFSMActionButtonImage_Image();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionDTOFindImpl <em>FSM Action DTO Find</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionDTOFindImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionDTOFind()
		 * @generated
		 */
		EClass FSM_ACTION_DTO_FIND = eINSTANCE.getFSMActionDTOFind();

		/**
		 * The meta object literal for the '<em><b>Dto</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_DTO_FIND__DTO = eINSTANCE.getFSMActionDTOFind_Dto();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_DTO_FIND__ATTRIBUTE = eINSTANCE.getFSMActionDTOFind_Attribute();

		/**
		 * The meta object literal for the '<em><b>Search</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_DTO_FIND__SEARCH = eINSTANCE.getFSMActionDTOFind_Search();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionDTOClearImpl <em>FSM Action DTO Clear</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionDTOClearImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionDTOClear()
		 * @generated
		 */
		EClass FSM_ACTION_DTO_CLEAR = eINSTANCE.getFSMActionDTOClear();

		/**
		 * The meta object literal for the '<em><b>Dto</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_DTO_CLEAR__DTO = eINSTANCE.getFSMActionDTOClear_Dto();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionSchedulerImpl <em>FSM Action Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMActionSchedulerImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMActionScheduler()
		 * @generated
		 */
		EClass FSM_ACTION_SCHEDULER = eINSTANCE.getFSMActionScheduler();

		/**
		 * The meta object literal for the '<em><b>Scheduler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_ACTION_SCHEDULER__SCHEDULER = eINSTANCE.getFSMActionScheduler_Scheduler();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMDTOTypeImpl <em>FSMDTO Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMDTOTypeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMDTOType()
		 * @generated
		 */
		EClass FSMDTO_TYPE = eINSTANCE.getFSMDTOType();

		/**
		 * The meta object literal for the '<em><b>Attribute Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSMDTO_TYPE__ATTRIBUTE_TYPE = eINSTANCE.getFSMDTOType_AttributeType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMFieldTypeImpl <em>FSM Field Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMFieldTypeImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMFieldType()
		 * @generated
		 */
		EClass FSM_FIELD_TYPE = eINSTANCE.getFSMFieldType();

		/**
		 * The meta object literal for the '<em><b>Attribute Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_FIELD_TYPE__ATTRIBUTE_TYPE = eINSTANCE.getFSMFieldType_AttributeType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMAbstractFilter <em>FSM Abstract Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMAbstractFilter
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMAbstractFilter()
		 * @generated
		 */
		EClass FSM_ABSTRACT_FILTER = eINSTANCE.getFSMAbstractFilter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMFilterPropertyImpl <em>FSM Filter Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMFilterPropertyImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMFilterProperty()
		 * @generated
		 */
		EClass FSM_FILTER_PROPERTY = eINSTANCE.getFSMFilterProperty();

		/**
		 * The meta object literal for the '<em><b>Path</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_FILTER_PROPERTY__PATH = eINSTANCE.getFSMFilterProperty_Path();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMFilterImpl <em>FSM Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMFilterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMFilter()
		 * @generated
		 */
		EClass FSM_FILTER = eINSTANCE.getFSMFilter();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_FILTER__SOURCE = eINSTANCE.getFSMFilter_Source();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMJunctionFilterImpl <em>FSM Junction Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMJunctionFilterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMJunctionFilter()
		 * @generated
		 */
		EClass FSM_JUNCTION_FILTER = eINSTANCE.getFSMJunctionFilter();

		/**
		 * The meta object literal for the '<em><b>First</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_JUNCTION_FILTER__FIRST = eINSTANCE.getFSMJunctionFilter_First();

		/**
		 * The meta object literal for the '<em><b>More</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_JUNCTION_FILTER__MORE = eINSTANCE.getFSMJunctionFilter_More();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMAndFilterImpl <em>FSM And Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMAndFilterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMAndFilter()
		 * @generated
		 */
		EClass FSM_AND_FILTER = eINSTANCE.getFSMAndFilter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMOrFilterImpl <em>FSM Or Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMOrFilterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMOrFilter()
		 * @generated
		 */
		EClass FSM_OR_FILTER = eINSTANCE.getFSMOrFilter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMBetweenFilterImpl <em>FSM Between Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMBetweenFilterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMBetweenFilter()
		 * @generated
		 */
		EClass FSM_BETWEEN_FILTER = eINSTANCE.getFSMBetweenFilter();

		/**
		 * The meta object literal for the '<em><b>Property Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_BETWEEN_FILTER__PROPERTY_ID = eINSTANCE.getFSMBetweenFilter_PropertyId();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_BETWEEN_FILTER__START = eINSTANCE.getFSMBetweenFilter_Start();

		/**
		 * The meta object literal for the '<em><b>End</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_BETWEEN_FILTER__END = eINSTANCE.getFSMBetweenFilter_End();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMCompareFilterImpl <em>FSM Compare Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMCompareFilterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMCompareFilter()
		 * @generated
		 */
		EClass FSM_COMPARE_FILTER = eINSTANCE.getFSMCompareFilter();

		/**
		 * The meta object literal for the '<em><b>Property Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_COMPARE_FILTER__PROPERTY_ID = eINSTANCE.getFSMCompareFilter_PropertyId();

		/**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_COMPARE_FILTER__OPERAND = eINSTANCE.getFSMCompareFilter_Operand();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_COMPARE_FILTER__OPERATION = eINSTANCE.getFSMCompareFilter_Operation();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMIsNullFilterImpl <em>FSM Is Null Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMIsNullFilterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMIsNullFilter()
		 * @generated
		 */
		EClass FSM_IS_NULL_FILTER = eINSTANCE.getFSMIsNullFilter();

		/**
		 * The meta object literal for the '<em><b>Property Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_IS_NULL_FILTER__PROPERTY_ID = eINSTANCE.getFSMIsNullFilter_PropertyId();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMLikeFilterImpl <em>FSM Like Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMLikeFilterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLikeFilter()
		 * @generated
		 */
		EClass FSM_LIKE_FILTER = eINSTANCE.getFSMLikeFilter();

		/**
		 * The meta object literal for the '<em><b>Property Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_LIKE_FILTER__PROPERTY_ID = eINSTANCE.getFSMLikeFilter_PropertyId();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_LIKE_FILTER__VALUE = eINSTANCE.getFSMLikeFilter_Value();

		/**
		 * The meta object literal for the '<em><b>Ignore Case</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_LIKE_FILTER__IGNORE_CASE = eINSTANCE.getFSMLikeFilter_IgnoreCase();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMNotFilterImpl <em>FSM Not Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMNotFilterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMNotFilter()
		 * @generated
		 */
		EClass FSM_NOT_FILTER = eINSTANCE.getFSMNotFilter();

		/**
		 * The meta object literal for the '<em><b>Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_NOT_FILTER__FILTER = eINSTANCE.getFSMNotFilter_Filter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStringFilterImpl <em>FSM String Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMStringFilterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMStringFilter()
		 * @generated
		 */
		EClass FSM_STRING_FILTER = eINSTANCE.getFSMStringFilter();

		/**
		 * The meta object literal for the '<em><b>Property Id</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_STRING_FILTER__PROPERTY_ID = eINSTANCE.getFSMStringFilter_PropertyId();

		/**
		 * The meta object literal for the '<em><b>Filter String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_STRING_FILTER__FILTER_STRING = eINSTANCE.getFSMStringFilter_FilterString();

		/**
		 * The meta object literal for the '<em><b>Only Match Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_STRING_FILTER__ONLY_MATCH_PREFIX = eINSTANCE.getFSMStringFilter_OnlyMatchPrefix();

		/**
		 * The meta object literal for the '<em><b>Ignore Case</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_STRING_FILTER__IGNORE_CASE = eINSTANCE.getFSMStringFilter_IgnoreCase();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlFilterImpl <em>FSM Control Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.impl.FSMControlFilterImpl
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlFilter()
		 * @generated
		 */
		EClass FSM_CONTROL_FILTER = eINSTANCE.getFSMControlFilter();

		/**
		 * The meta object literal for the '<em><b>Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_CONTROL_FILTER__FILTER = eINSTANCE.getFSMControlFilter_Filter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMInternalType <em>FSM Internal Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMInternalType
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMInternalType()
		 * @generated
		 */
		EEnum FSM_INTERNAL_TYPE = eINSTANCE.getFSMInternalType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType <em>FSM Control Button Event Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMControlButtonEventType()
		 * @generated
		 */
		EEnum FSM_CONTROL_BUTTON_EVENT_TYPE = eINSTANCE.getFSMControlButtonEventType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMCompareOperationEnum <em>FSM Compare Operation Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMCompareOperationEnum
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMCompareOperationEnum()
		 * @generated
		 */
		EEnum FSM_COMPARE_OPERATION_ENUM = eINSTANCE.getFSMCompareOperationEnum();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMEvaluationType <em>FSM Evaluation Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMEvaluationType
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMEvaluationType()
		 * @generated
		 */
		EEnum FSM_EVALUATION_TYPE = eINSTANCE.getFSMEvaluationType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMUserMessageType <em>FSM User Message Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMUserMessageType
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMUserMessageType()
		 * @generated
		 */
		EEnum FSM_USER_MESSAGE_TYPE = eINSTANCE.getFSMUserMessageType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayCursorType <em>FSM Line Display Cursor Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayCursorType
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLineDisplayCursorType()
		 * @generated
		 */
		EEnum FSM_LINE_DISPLAY_CURSOR_TYPE = eINSTANCE.getFSMLineDisplayCursorType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeType <em>FSM Line Display Marquee Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeType
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLineDisplayMarqueeType()
		 * @generated
		 */
		EEnum FSM_LINE_DISPLAY_MARQUEE_TYPE = eINSTANCE.getFSMLineDisplayMarqueeType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeFormat <em>FSM Line Display Marquee Format</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeFormat
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLineDisplayMarqueeFormat()
		 * @generated
		 */
		EEnum FSM_LINE_DISPLAY_MARQUEE_FORMAT = eINSTANCE.getFSMLineDisplayMarqueeFormat();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayTextType <em>FSM Line Display Text Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayTextType
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLineDisplayTextType()
		 * @generated
		 */
		EEnum FSM_LINE_DISPLAY_TEXT_TYPE = eINSTANCE.getFSMLineDisplayTextType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMLineDisplayScrollTextType <em>FSM Line Display Scroll Text Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMLineDisplayScrollTextType
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMLineDisplayScrollTextType()
		 * @generated
		 */
		EEnum FSM_LINE_DISPLAY_SCROLL_TEXT_TYPE = eINSTANCE.getFSMLineDisplayScrollTextType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMPOSPrinterBarcodeType <em>FSMPOS Printer Barcode Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMPOSPrinterBarcodeType
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMPOSPrinterBarcodeType()
		 * @generated
		 */
		EEnum FSMPOS_PRINTER_BARCODE_TYPE = eINSTANCE.getFSMPOSPrinterBarcodeType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.statemachine.FSMFunctionalKeyCodes <em>FSM Functional Key Codes</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.statemachine.FSMFunctionalKeyCodes
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getFSMFunctionalKeyCodes()
		 * @generated
		 */
		EEnum FSM_FUNCTIONAL_KEY_CODES = eINSTANCE.getFSMFunctionalKeyCodes();

		/**
		 * The meta object literal for the '<em>Internal EObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.InternalEObject
		 * @see org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLPackageImpl#getInternalEObject()
		 * @generated
		 */
		EDataType INTERNAL_EOBJECT = eINSTANCE.getInternalEObject();

	}

} //StatemachineDSLPackage
