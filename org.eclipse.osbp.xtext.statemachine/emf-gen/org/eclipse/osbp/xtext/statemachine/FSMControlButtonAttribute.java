/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Control Button Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#isHasImage <em>Has Image</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#getImage <em>Image</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#getEvent <em>Event</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButtonAttribute()
 * @model
 * @generated
 */
public interface FSMControlButtonAttribute extends FSMControlVisibility {
	/**
	 * Returns the value of the '<em><b>Has Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Image</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Image</em>' attribute.
	 * @see #setHasImage(boolean)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButtonAttribute_HasImage()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasImage();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#isHasImage <em>Has Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Image</em>' attribute.
	 * @see #isHasImage()
	 * @generated
	 */
	void setHasImage(boolean value);

	/**
	 * Returns the value of the '<em><b>Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image</em>' attribute.
	 * @see #setImage(String)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButtonAttribute_Image()
	 * @model unique="false"
	 * @generated
	 */
	String getImage();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#getImage <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image</em>' attribute.
	 * @see #getImage()
	 * @generated
	 */
	void setImage(String value);

	/**
	 * Returns the value of the '<em><b>Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' containment reference.
	 * @see #setEvent(FSMControlButtonAttributeEvent)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButtonAttribute_Event()
	 * @model containment="true"
	 * @generated
	 */
	FSMControlButtonAttributeEvent getEvent();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute#getEvent <em>Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' containment reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(FSMControlButtonAttributeEvent value);

} // FSMControlButtonAttribute
