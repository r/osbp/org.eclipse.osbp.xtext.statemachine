/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Control Scheduler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlScheduler#getSchedulers <em>Schedulers</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlScheduler()
 * @model
 * @generated
 */
public interface FSMControlScheduler extends FSMControl {
	/**
	 * Returns the value of the '<em><b>Schedulers</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schedulers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schedulers</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlScheduler_Schedulers()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMControlSchedulerAttribute> getSchedulers();

} // FSMControlScheduler
