/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>FSM Line Display Cursor Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMLineDisplayCursorType()
 * @model
 * @generated
 */
public enum FSMLineDisplayCursorType implements Enumerator {
	/**
	 * The '<em><b>DISP CT NONE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_NONE_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_CT_NONE(0, "DISP_CT_NONE", "none"),

	/**
	 * The '<em><b>DISP CT FIXED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_FIXED_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_CT_FIXED(0, "DISP_CT_FIXED", "fixed"),

	/**
	 * The '<em><b>DISP CT BLOCK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_BLOCK_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_CT_BLOCK(0, "DISP_CT_BLOCK", "block"),

	/**
	 * The '<em><b>DISP CT HALFBLOCK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_HALFBLOCK_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_CT_HALFBLOCK(0, "DISP_CT_HALFBLOCK", "halfblock"),

	/**
	 * The '<em><b>DISP CT UNDERLINE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_UNDERLINE_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_CT_UNDERLINE(0, "DISP_CT_UNDERLINE", "underline"),

	/**
	 * The '<em><b>DISP CT REVERSE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_REVERSE_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_CT_REVERSE(0, "DISP_CT_REVERSE", "reverse"),

	/**
	 * The '<em><b>DISP CT OTHER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_CT_OTHER(0, "DISP_CT_OTHER", "other"),

	/**
	 * The '<em><b>DISP CT BLINK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_BLINK_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_CT_BLINK(0, "DISP_CT_BLINK", "blink");

	/**
	 * The '<em><b>DISP CT NONE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP CT NONE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_NONE
	 * @model literal="none"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_CT_NONE_VALUE = 0;

	/**
	 * The '<em><b>DISP CT FIXED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP CT FIXED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_FIXED
	 * @model literal="fixed"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_CT_FIXED_VALUE = 0;

	/**
	 * The '<em><b>DISP CT BLOCK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP CT BLOCK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_BLOCK
	 * @model literal="block"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_CT_BLOCK_VALUE = 0;

	/**
	 * The '<em><b>DISP CT HALFBLOCK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP CT HALFBLOCK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_HALFBLOCK
	 * @model literal="halfblock"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_CT_HALFBLOCK_VALUE = 0;

	/**
	 * The '<em><b>DISP CT UNDERLINE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP CT UNDERLINE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_UNDERLINE
	 * @model literal="underline"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_CT_UNDERLINE_VALUE = 0;

	/**
	 * The '<em><b>DISP CT REVERSE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP CT REVERSE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_REVERSE
	 * @model literal="reverse"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_CT_REVERSE_VALUE = 0;

	/**
	 * The '<em><b>DISP CT OTHER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP CT OTHER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_OTHER
	 * @model literal="other"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_CT_OTHER_VALUE = 0;

	/**
	 * The '<em><b>DISP CT BLINK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP CT BLINK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_CT_BLINK
	 * @model literal="blink"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_CT_BLINK_VALUE = 0;

	/**
	 * An array of all the '<em><b>FSM Line Display Cursor Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final FSMLineDisplayCursorType[] VALUES_ARRAY =
		new FSMLineDisplayCursorType[] {
			DISP_CT_NONE,
			DISP_CT_FIXED,
			DISP_CT_BLOCK,
			DISP_CT_HALFBLOCK,
			DISP_CT_UNDERLINE,
			DISP_CT_REVERSE,
			DISP_CT_OTHER,
			DISP_CT_BLINK,
		};

	/**
	 * A public read-only list of all the '<em><b>FSM Line Display Cursor Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<FSMLineDisplayCursorType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>FSM Line Display Cursor Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMLineDisplayCursorType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMLineDisplayCursorType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM Line Display Cursor Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMLineDisplayCursorType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMLineDisplayCursorType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM Line Display Cursor Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMLineDisplayCursorType get(int value) {
		switch (value) {
			case DISP_CT_NONE_VALUE: return DISP_CT_NONE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FSMLineDisplayCursorType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //FSMLineDisplayCursorType
