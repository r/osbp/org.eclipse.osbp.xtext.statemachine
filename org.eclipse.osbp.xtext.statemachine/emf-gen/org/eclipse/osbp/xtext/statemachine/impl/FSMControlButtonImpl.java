/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.statemachine.FSMControlButton;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Control Button</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonImpl#getEventType <em>Event Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonImpl#getButtons <em>Buttons</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonImpl#isHasRange <em>Has Range</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonImpl#getStart <em>Start</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonImpl#getEnd <em>End</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonImpl#getRangedName <em>Ranged Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMControlButtonImpl extends FSMBaseImpl implements FSMControlButton {
	/**
	 * The default value of the '{@link #getEventType() <em>Event Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventType()
	 * @generated
	 * @ordered
	 */
	protected static final FSMControlButtonEventType EVENT_TYPE_EDEFAULT = FSMControlButtonEventType.KEYBOARD;

	/**
	 * The cached value of the '{@link #getEventType() <em>Event Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventType()
	 * @generated
	 * @ordered
	 */
	protected FSMControlButtonEventType eventType = EVENT_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getButtons() <em>Buttons</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getButtons()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMControlButtonAttribute> buttons;

	/**
	 * The default value of the '{@link #isHasRange() <em>Has Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasRange()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_RANGE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasRange() <em>Has Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasRange()
	 * @generated
	 * @ordered
	 */
	protected boolean hasRange = HAS_RANGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStart() <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart()
	 * @generated
	 * @ordered
	 */
	protected static final int START_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStart() <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart()
	 * @generated
	 * @ordered
	 */
	protected int start = START_EDEFAULT;

	/**
	 * The default value of the '{@link #getEnd() <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected static final int END_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEnd() <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected int end = END_EDEFAULT;

	/**
	 * The default value of the '{@link #getRangedName() <em>Ranged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRangedName()
	 * @generated
	 * @ordered
	 */
	protected static final String RANGED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRangedName() <em>Ranged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRangedName()
	 * @generated
	 * @ordered
	 */
	protected String rangedName = RANGED_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMControlButtonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_CONTROL_BUTTON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlButtonEventType getEventType() {
		return eventType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEventType(FSMControlButtonEventType newEventType) {
		FSMControlButtonEventType oldEventType = eventType;
		eventType = newEventType == null ? EVENT_TYPE_EDEFAULT : newEventType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_BUTTON__EVENT_TYPE, oldEventType, eventType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMControlButtonAttribute> getButtons() {
		if (buttons == null) {
			buttons = new EObjectContainmentEList<FSMControlButtonAttribute>(FSMControlButtonAttribute.class, this, StatemachineDSLPackage.FSM_CONTROL_BUTTON__BUTTONS);
		}
		return buttons;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasRange() {
		return hasRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasRange(boolean newHasRange) {
		boolean oldHasRange = hasRange;
		hasRange = newHasRange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_BUTTON__HAS_RANGE, oldHasRange, hasRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getStart() {
		return start;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStart(int newStart) {
		int oldStart = start;
		start = newStart;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_BUTTON__START, oldStart, start));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEnd() {
		return end;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnd(int newEnd) {
		int oldEnd = end;
		end = newEnd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_BUTTON__END, oldEnd, end));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRangedName() {
		return rangedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRangedName(String newRangedName) {
		String oldRangedName = rangedName;
		rangedName = newRangedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_BUTTON__RANGED_NAME, oldRangedName, rangedName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__BUTTONS:
				return ((InternalEList<?>)getButtons()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__EVENT_TYPE:
				return getEventType();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__BUTTONS:
				return getButtons();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__HAS_RANGE:
				return isHasRange();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__START:
				return getStart();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__END:
				return getEnd();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__RANGED_NAME:
				return getRangedName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__EVENT_TYPE:
				setEventType((FSMControlButtonEventType)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__BUTTONS:
				getButtons().clear();
				getButtons().addAll((Collection<? extends FSMControlButtonAttribute>)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__HAS_RANGE:
				setHasRange((Boolean)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__START:
				setStart((Integer)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__END:
				setEnd((Integer)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__RANGED_NAME:
				setRangedName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__EVENT_TYPE:
				setEventType(EVENT_TYPE_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__BUTTONS:
				getButtons().clear();
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__HAS_RANGE:
				setHasRange(HAS_RANGE_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__START:
				setStart(START_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__END:
				setEnd(END_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__RANGED_NAME:
				setRangedName(RANGED_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__EVENT_TYPE:
				return eventType != EVENT_TYPE_EDEFAULT;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__BUTTONS:
				return buttons != null && !buttons.isEmpty();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__HAS_RANGE:
				return hasRange != HAS_RANGE_EDEFAULT;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__START:
				return start != START_EDEFAULT;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__END:
				return end != END_EDEFAULT;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON__RANGED_NAME:
				return RANGED_NAME_EDEFAULT == null ? rangedName != null : !RANGED_NAME_EDEFAULT.equals(rangedName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (eventType: ");
		result.append(eventType);
		result.append(", hasRange: ");
		result.append(hasRange);
		result.append(", start: ");
		result.append(start);
		result.append(", end: ");
		result.append(end);
		result.append(", rangedName: ");
		result.append(rangedName);
		result.append(')');
		return result.toString();
	}

} //FSMControlButtonImpl
