/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMDTOType;
import org.eclipse.osbp.xtext.statemachine.FSMEvent;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Control DTO Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOAttributeImpl#getAttributeType <em>Attribute Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOAttributeImpl#isHasEvent <em>Has Event</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOAttributeImpl#getEvent <em>Event</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOAttributeImpl#isIsAttached <em>Is Attached</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlDTOAttributeImpl#getDisplay <em>Display</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMControlDTOAttributeImpl extends FSMBaseImpl implements FSMControlDTOAttribute {
	/**
	 * The cached value of the '{@link #getAttributeType() <em>Attribute Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeType()
	 * @generated
	 * @ordered
	 */
	protected FSMDTOType attributeType;

	/**
	 * The default value of the '{@link #isHasEvent() <em>Has Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasEvent()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_EVENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasEvent() <em>Has Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasEvent()
	 * @generated
	 * @ordered
	 */
	protected boolean hasEvent = HAS_EVENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEvent() <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected FSMEvent event;

	/**
	 * The default value of the '{@link #isIsAttached() <em>Is Attached</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsAttached()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ATTACHED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsAttached() <em>Is Attached</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsAttached()
	 * @generated
	 * @ordered
	 */
	protected boolean isAttached = IS_ATTACHED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDisplay() <em>Display</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplay()
	 * @generated
	 * @ordered
	 */
	protected FSMPeripheralDeviceDisplay display;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMControlDTOAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_CONTROL_DTO_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMDTOType getAttributeType() {
		return attributeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttributeType(FSMDTOType newAttributeType, NotificationChain msgs) {
		FSMDTOType oldAttributeType = attributeType;
		attributeType = newAttributeType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE, oldAttributeType, newAttributeType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeType(FSMDTOType newAttributeType) {
		if (newAttributeType != attributeType) {
			NotificationChain msgs = null;
			if (attributeType != null)
				msgs = ((InternalEObject)attributeType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE, null, msgs);
			if (newAttributeType != null)
				msgs = ((InternalEObject)newAttributeType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE, null, msgs);
			msgs = basicSetAttributeType(newAttributeType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE, newAttributeType, newAttributeType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasEvent() {
		return hasEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasEvent(boolean newHasEvent) {
		boolean oldHasEvent = hasEvent;
		hasEvent = newHasEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__HAS_EVENT, oldHasEvent, hasEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMEvent getEvent() {
		if (event != null && event.eIsProxy()) {
			InternalEObject oldEvent = (InternalEObject)event;
			event = (FSMEvent)eResolveProxy(oldEvent);
			if (event != oldEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__EVENT, oldEvent, event));
			}
		}
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMEvent basicGetEvent() {
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(FSMEvent newEvent) {
		FSMEvent oldEvent = event;
		event = newEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__EVENT, oldEvent, event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsAttached() {
		return isAttached;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsAttached(boolean newIsAttached) {
		boolean oldIsAttached = isAttached;
		isAttached = newIsAttached;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__IS_ATTACHED, oldIsAttached, isAttached));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDeviceDisplay getDisplay() {
		if (display != null && display.eIsProxy()) {
			InternalEObject oldDisplay = (InternalEObject)display;
			display = (FSMPeripheralDeviceDisplay)eResolveProxy(oldDisplay);
			if (display != oldDisplay) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__DISPLAY, oldDisplay, display));
			}
		}
		return display;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDeviceDisplay basicGetDisplay() {
		return display;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisplay(FSMPeripheralDeviceDisplay newDisplay) {
		FSMPeripheralDeviceDisplay oldDisplay = display;
		display = newDisplay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__DISPLAY, oldDisplay, display));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE:
				return basicSetAttributeType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE:
				return getAttributeType();
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__HAS_EVENT:
				return isHasEvent();
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__EVENT:
				if (resolve) return getEvent();
				return basicGetEvent();
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__IS_ATTACHED:
				return isIsAttached();
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__DISPLAY:
				if (resolve) return getDisplay();
				return basicGetDisplay();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE:
				setAttributeType((FSMDTOType)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__HAS_EVENT:
				setHasEvent((Boolean)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__EVENT:
				setEvent((FSMEvent)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__IS_ATTACHED:
				setIsAttached((Boolean)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__DISPLAY:
				setDisplay((FSMPeripheralDeviceDisplay)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE:
				setAttributeType((FSMDTOType)null);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__HAS_EVENT:
				setHasEvent(HAS_EVENT_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__EVENT:
				setEvent((FSMEvent)null);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__IS_ATTACHED:
				setIsAttached(IS_ATTACHED_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__DISPLAY:
				setDisplay((FSMPeripheralDeviceDisplay)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE:
				return attributeType != null;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__HAS_EVENT:
				return hasEvent != HAS_EVENT_EDEFAULT;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__EVENT:
				return event != null;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__IS_ATTACHED:
				return isAttached != IS_ATTACHED_EDEFAULT;
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE__DISPLAY:
				return display != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (hasEvent: ");
		result.append(hasEvent);
		result.append(", isAttached: ");
		result.append(isAttached);
		result.append(')');
		return result.toString();
	}

} //FSMControlDTOAttributeImpl
