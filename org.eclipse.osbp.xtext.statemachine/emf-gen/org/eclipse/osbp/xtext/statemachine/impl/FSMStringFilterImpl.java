/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.statemachine.FSMFilterProperty;
import org.eclipse.osbp.xtext.statemachine.FSMStringFilter;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM String Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStringFilterImpl#getPropertyId <em>Property Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStringFilterImpl#getFilterString <em>Filter String</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStringFilterImpl#isOnlyMatchPrefix <em>Only Match Prefix</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStringFilterImpl#isIgnoreCase <em>Ignore Case</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMStringFilterImpl extends FSMLazyResolverImpl implements FSMStringFilter {
	/**
	 * The cached value of the '{@link #getPropertyId() <em>Property Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyId()
	 * @generated
	 * @ordered
	 */
	protected FSMFilterProperty propertyId;

	/**
	 * The default value of the '{@link #getFilterString() <em>Filter String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterString()
	 * @generated
	 * @ordered
	 */
	protected static final String FILTER_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFilterString() <em>Filter String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterString()
	 * @generated
	 * @ordered
	 */
	protected String filterString = FILTER_STRING_EDEFAULT;

	/**
	 * The default value of the '{@link #isOnlyMatchPrefix() <em>Only Match Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnlyMatchPrefix()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ONLY_MATCH_PREFIX_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOnlyMatchPrefix() <em>Only Match Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnlyMatchPrefix()
	 * @generated
	 * @ordered
	 */
	protected boolean onlyMatchPrefix = ONLY_MATCH_PREFIX_EDEFAULT;

	/**
	 * The default value of the '{@link #isIgnoreCase() <em>Ignore Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIgnoreCase()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IGNORE_CASE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIgnoreCase() <em>Ignore Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIgnoreCase()
	 * @generated
	 * @ordered
	 */
	protected boolean ignoreCase = IGNORE_CASE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMStringFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_STRING_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMFilterProperty getPropertyId() {
		return propertyId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPropertyId(FSMFilterProperty newPropertyId, NotificationChain msgs) {
		FSMFilterProperty oldPropertyId = propertyId;
		propertyId = newPropertyId;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_STRING_FILTER__PROPERTY_ID, oldPropertyId, newPropertyId);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyId(FSMFilterProperty newPropertyId) {
		if (newPropertyId != propertyId) {
			NotificationChain msgs = null;
			if (propertyId != null)
				msgs = ((InternalEObject)propertyId).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_STRING_FILTER__PROPERTY_ID, null, msgs);
			if (newPropertyId != null)
				msgs = ((InternalEObject)newPropertyId).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_STRING_FILTER__PROPERTY_ID, null, msgs);
			msgs = basicSetPropertyId(newPropertyId, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_STRING_FILTER__PROPERTY_ID, newPropertyId, newPropertyId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFilterString() {
		return filterString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilterString(String newFilterString) {
		String oldFilterString = filterString;
		filterString = newFilterString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_STRING_FILTER__FILTER_STRING, oldFilterString, filterString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOnlyMatchPrefix() {
		return onlyMatchPrefix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnlyMatchPrefix(boolean newOnlyMatchPrefix) {
		boolean oldOnlyMatchPrefix = onlyMatchPrefix;
		onlyMatchPrefix = newOnlyMatchPrefix;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_STRING_FILTER__ONLY_MATCH_PREFIX, oldOnlyMatchPrefix, onlyMatchPrefix));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIgnoreCase() {
		return ignoreCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIgnoreCase(boolean newIgnoreCase) {
		boolean oldIgnoreCase = ignoreCase;
		ignoreCase = newIgnoreCase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_STRING_FILTER__IGNORE_CASE, oldIgnoreCase, ignoreCase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_STRING_FILTER__PROPERTY_ID:
				return basicSetPropertyId(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_STRING_FILTER__PROPERTY_ID:
				return getPropertyId();
			case StatemachineDSLPackage.FSM_STRING_FILTER__FILTER_STRING:
				return getFilterString();
			case StatemachineDSLPackage.FSM_STRING_FILTER__ONLY_MATCH_PREFIX:
				return isOnlyMatchPrefix();
			case StatemachineDSLPackage.FSM_STRING_FILTER__IGNORE_CASE:
				return isIgnoreCase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_STRING_FILTER__PROPERTY_ID:
				setPropertyId((FSMFilterProperty)newValue);
				return;
			case StatemachineDSLPackage.FSM_STRING_FILTER__FILTER_STRING:
				setFilterString((String)newValue);
				return;
			case StatemachineDSLPackage.FSM_STRING_FILTER__ONLY_MATCH_PREFIX:
				setOnlyMatchPrefix((Boolean)newValue);
				return;
			case StatemachineDSLPackage.FSM_STRING_FILTER__IGNORE_CASE:
				setIgnoreCase((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_STRING_FILTER__PROPERTY_ID:
				setPropertyId((FSMFilterProperty)null);
				return;
			case StatemachineDSLPackage.FSM_STRING_FILTER__FILTER_STRING:
				setFilterString(FILTER_STRING_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_STRING_FILTER__ONLY_MATCH_PREFIX:
				setOnlyMatchPrefix(ONLY_MATCH_PREFIX_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_STRING_FILTER__IGNORE_CASE:
				setIgnoreCase(IGNORE_CASE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_STRING_FILTER__PROPERTY_ID:
				return propertyId != null;
			case StatemachineDSLPackage.FSM_STRING_FILTER__FILTER_STRING:
				return FILTER_STRING_EDEFAULT == null ? filterString != null : !FILTER_STRING_EDEFAULT.equals(filterString);
			case StatemachineDSLPackage.FSM_STRING_FILTER__ONLY_MATCH_PREFIX:
				return onlyMatchPrefix != ONLY_MATCH_PREFIX_EDEFAULT;
			case StatemachineDSLPackage.FSM_STRING_FILTER__IGNORE_CASE:
				return ignoreCase != IGNORE_CASE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (filterString: ");
		result.append(filterString);
		result.append(", onlyMatchPrefix: ");
		result.append(onlyMatchPrefix);
		result.append(", ignoreCase: ");
		result.append(ignoreCase);
		result.append(')');
		return result.toString();
	}

} //FSMStringFilterImpl
