/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvaluate;
import org.eclipse.osbp.xtext.statemachine.FSMEvaluationType;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Action Field Source Evaluate</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldSourceEvaluateImpl#getEvaluationtype <em>Evaluationtype</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMActionFieldSourceEvaluateImpl extends FSMLazyResolverImpl implements FSMActionFieldSourceEvaluate {
	/**
	 * The default value of the '{@link #getEvaluationtype() <em>Evaluationtype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluationtype()
	 * @generated
	 * @ordered
	 */
	protected static final FSMEvaluationType EVALUATIONTYPE_EDEFAULT = FSMEvaluationType.IP_ADDRESS;

	/**
	 * The cached value of the '{@link #getEvaluationtype() <em>Evaluationtype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluationtype()
	 * @generated
	 * @ordered
	 */
	protected FSMEvaluationType evaluationtype = EVALUATIONTYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMActionFieldSourceEvaluateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_SOURCE_EVALUATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMEvaluationType getEvaluationtype() {
		return evaluationtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvaluationtype(FSMEvaluationType newEvaluationtype) {
		FSMEvaluationType oldEvaluationtype = evaluationtype;
		evaluationtype = newEvaluationtype == null ? EVALUATIONTYPE_EDEFAULT : newEvaluationtype;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_EVALUATE__EVALUATIONTYPE, oldEvaluationtype, evaluationtype));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_EVALUATE__EVALUATIONTYPE:
				return getEvaluationtype();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_EVALUATE__EVALUATIONTYPE:
				setEvaluationtype((FSMEvaluationType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_EVALUATE__EVALUATIONTYPE:
				setEvaluationtype(EVALUATIONTYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_EVALUATE__EVALUATIONTYPE:
				return evaluationtype != EVALUATIONTYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (evaluationtype: ");
		result.append(evaluationtype);
		result.append(')');
		return result.toString();
	}

} //FSMActionFieldSourceEvaluateImpl
