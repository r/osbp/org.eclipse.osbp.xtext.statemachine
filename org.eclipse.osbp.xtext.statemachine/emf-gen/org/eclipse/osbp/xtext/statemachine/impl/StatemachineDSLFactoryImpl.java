/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.osbp.xtext.statemachine.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StatemachineDSLFactoryImpl extends EFactoryImpl implements StatemachineDSLFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StatemachineDSLFactory init() {
		try {
			StatemachineDSLFactory theStatemachineDSLFactory = (StatemachineDSLFactory)EPackage.Registry.INSTANCE.getEFactory(StatemachineDSLPackage.eNS_URI);
			if (theStatemachineDSLFactory != null) {
				return theStatemachineDSLFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StatemachineDSLFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachineDSLFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StatemachineDSLPackage.FSM_MODEL: return createFSMModel();
			case StatemachineDSLPackage.FSM_LAZY_RESOLVER: return createFSMLazyResolver();
			case StatemachineDSLPackage.FSM_BASE: return createFSMBase();
			case StatemachineDSLPackage.FSM_PACKAGE: return createFSMPackage();
			case StatemachineDSLPackage.FSM: return createFSM();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON: return createFSMControlButton();
			case StatemachineDSLPackage.FSM_CONTROL_FIELD: return createFSMControlField();
			case StatemachineDSLPackage.FSM_CONTROL_DTO: return createFSMControlDTO();
			case StatemachineDSLPackage.FSM_CONTROL_SCHEDULER: return createFSMControlScheduler();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL: return createFSMControlPeripheral();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE: return createFSMControlButtonAttribute();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD: return createFSMControlButtonAttributeEventKeyboard();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_IDENTITY: return createFSMControlButtonAttributeEventIdentity();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_EVENT: return createFSMControlButtonAttributeEventEvent();
			case StatemachineDSLPackage.FSM_CONTROL_FIELD_ATTRIBUTE: return createFSMControlFieldAttribute();
			case StatemachineDSLPackage.FSM_CONTROL_FIELD_LAYOUT: return createFSMControlFieldLayout();
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE: return createFSMControlDTOAttribute();
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_DISPLAY: return createFSMPeripheralDeviceDisplay();
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_LINE_DISPLAY: return createFSMPeripheralDeviceLineDisplay();
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_POS_PRINTER: return createFSMPeripheralDevicePOSPrinter();
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_CASH_DRAWER: return createFSMPeripheralDeviceCashDrawer();
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_PT: return createFSMPeripheralDevicePT();
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_SIGNATURE: return createFSMPeripheralDeviceSignature();
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_SCALE: return createFSMPeripheralDeviceScale();
			case StatemachineDSLPackage.FSM_CONTROL_SCHEDULER_ATTRIBUTE: return createFSMControlSchedulerAttribute();
			case StatemachineDSLPackage.FSM_EVENT: return createFSMEvent();
			case StatemachineDSLPackage.FSM_STATE: return createFSMState();
			case StatemachineDSLPackage.FSM_KEY_MAPPER: return createFSMKeyMapper();
			case StatemachineDSLPackage.FSM_TRIGGER: return createFSMTrigger();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_BLINK_RATE: return createFSMActionPeripheralBlinkRate();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CLEAR: return createFSMActionPeripheralClear();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW: return createFSMActionPeripheralCreateWindow();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CURSOR_TYPE: return createFSMActionPeripheralCursorType();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_DESTROY_WINDOW: return createFSMActionPeripheralDestroyWindow();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS: return createFSMActionPeripheralDeviceBrightness();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_DISPLAY_TEXT: return createFSMActionPeripheralDisplayText();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT: return createFSMActionPeripheralLineDisplayText();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT: return createFSMActionPeripheralLineDisplayTextAt();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT: return createFSMActionPeripheralInterCharacterWait();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT: return createFSMActionPeripheralMarqueeFormat();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT: return createFSMActionPeripheralMarqueeRepeatWait();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE: return createFSMActionPeripheralMarqueeType();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT: return createFSMActionPeripheralMarqueeUnitWait();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCROLL: return createFSMActionPeripheralScroll();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_OPEN_DRAWER: return createFSMActionPeripheralOpenDrawer();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_BARCODE: return createFSMActionPeripheralPrintBarcode();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_BITMAP: return createFSMActionPeripheralPrintBitmap();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_CUT: return createFSMActionPeripheralPrintCut();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP: return createFSMActionPeripheralPrintRegisterBitmap();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_NORMAL: return createFSMActionPeripheralPrintNormal();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_OPEN: return createFSMActionPeripheralPTOpen();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_CLOSE: return createFSMActionPeripheralPTClose();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL: return createFSMActionPeripheralPTReversal();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE: return createFSMActionPeripheralPTAcknowledge();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REGISTRATION: return createFSMActionPeripheralPTRegistration();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION: return createFSMActionPeripheralPTAuthorization();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_BEEPER: return createFSMActionPeripheralBeeper();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PLAYER: return createFSMActionPeripheralPlayer();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SOUND: return createFSMActionPeripheralSound();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_RESPONSE: return createFSMActionPeripheralPTResponse();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_REPORT: return createFSMActionPeripheralPrintReport();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN: return createFSMActionPeripheralSignatureOpen();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE: return createFSMActionPeripheralSignatureClose();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR: return createFSMActionPeripheralSignatureClear();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE: return createFSMActionPeripheralSignatureCapture();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE: return createFSMActionPeripheralSignatureIdle();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL: return createFSMActionPeripheralSignatureLabel();
			case StatemachineDSLPackage.FSM_SIGNATURE_RETRIEVE: return createFSMSignatureRetrieve();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT: return createFSMActionPeripheralScaleReadWeight();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT: return createFSMActionPeripheralScaleReadTareWeight();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT: return createFSMActionPeripheralScaleTareWeight();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_ZERO: return createFSMActionPeripheralScaleZero();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT: return createFSMActionPeripheralScaleDisplayText();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT: return createFSMActionPeripheralScaleWeightUnit();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_STRING: return createFSMActionFieldSourceString();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_NUMBER: return createFSMActionFieldSourceNumber();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_INTEGER: return createFSMActionFieldSourceInteger();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_BOOLEAN: return createFSMActionFieldSourceBoolean();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_EVALUATE: return createFSMActionFieldSourceEvaluate();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_TRANSLATE: return createFSMActionFieldSourceTranslate();
			case StatemachineDSLPackage.FSM_DOT_EXPRESSION: return createFSMDotExpression();
			case StatemachineDSLPackage.FSM_DTO_REF: return createFSMDtoRef();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE: return createFSMActionFieldSourceDtoAttribute();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_EVENT: return createFSMActionFieldSourceEvent();
			case StatemachineDSLPackage.FSM_ACTION_CONDITIONAL_TRANSITION: return createFSMActionConditionalTransition();
			case StatemachineDSLPackage.FSM_OPERATION_PARAMETER: return createFSMOperationParameter();
			case StatemachineDSLPackage.FSM_OPERATION: return createFSMOperation();
			case StatemachineDSLPackage.FSM_GUARD: return createFSMGuard();
			case StatemachineDSLPackage.FSM_FUNCTION: return createFSMFunction();
			case StatemachineDSLPackage.FSM_STORAGE_RETRIEVE: return createFSMStorageRetrieve();
			case StatemachineDSLPackage.FSM_STORAGE: return createFSMStorage();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_CONCATENATION: return createFSMActionFieldConcatenation();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SET: return createFSMActionFieldSet();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE: return createFSMActionFieldKeystroke();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_CLEAR: return createFSMActionFieldClear();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_GET: return createFSMActionFieldGet();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_FILTER_TOGGLE: return createFSMActionFieldFilterToggle();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_REMOVE: return createFSMActionFieldRemove();
			case StatemachineDSLPackage.FSM_ACTION_ITEM_VISIBLE: return createFSMActionItemVisible();
			case StatemachineDSLPackage.FSM_ACTION_ITEM_INVISIBLE: return createFSMActionItemInvisible();
			case StatemachineDSLPackage.FSM_ACTION_BUTTON_CAPTION: return createFSMActionButtonCaption();
			case StatemachineDSLPackage.FSM_ACTION_BUTTON_IMAGE: return createFSMActionButtonImage();
			case StatemachineDSLPackage.FSM_ACTION_DTO_FIND: return createFSMActionDTOFind();
			case StatemachineDSLPackage.FSM_ACTION_DTO_CLEAR: return createFSMActionDTOClear();
			case StatemachineDSLPackage.FSM_ACTION_SCHEDULER: return createFSMActionScheduler();
			case StatemachineDSLPackage.FSMDTO_TYPE: return createFSMDTOType();
			case StatemachineDSLPackage.FSM_FIELD_TYPE: return createFSMFieldType();
			case StatemachineDSLPackage.FSM_FILTER_PROPERTY: return createFSMFilterProperty();
			case StatemachineDSLPackage.FSM_FILTER: return createFSMFilter();
			case StatemachineDSLPackage.FSM_JUNCTION_FILTER: return createFSMJunctionFilter();
			case StatemachineDSLPackage.FSM_AND_FILTER: return createFSMAndFilter();
			case StatemachineDSLPackage.FSM_OR_FILTER: return createFSMOrFilter();
			case StatemachineDSLPackage.FSM_BETWEEN_FILTER: return createFSMBetweenFilter();
			case StatemachineDSLPackage.FSM_COMPARE_FILTER: return createFSMCompareFilter();
			case StatemachineDSLPackage.FSM_IS_NULL_FILTER: return createFSMIsNullFilter();
			case StatemachineDSLPackage.FSM_LIKE_FILTER: return createFSMLikeFilter();
			case StatemachineDSLPackage.FSM_NOT_FILTER: return createFSMNotFilter();
			case StatemachineDSLPackage.FSM_STRING_FILTER: return createFSMStringFilter();
			case StatemachineDSLPackage.FSM_CONTROL_FILTER: return createFSMControlFilter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case StatemachineDSLPackage.FSM_INTERNAL_TYPE:
				return createFSMInternalTypeFromString(eDataType, initialValue);
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_EVENT_TYPE:
				return createFSMControlButtonEventTypeFromString(eDataType, initialValue);
			case StatemachineDSLPackage.FSM_COMPARE_OPERATION_ENUM:
				return createFSMCompareOperationEnumFromString(eDataType, initialValue);
			case StatemachineDSLPackage.FSM_EVALUATION_TYPE:
				return createFSMEvaluationTypeFromString(eDataType, initialValue);
			case StatemachineDSLPackage.FSM_USER_MESSAGE_TYPE:
				return createFSMUserMessageTypeFromString(eDataType, initialValue);
			case StatemachineDSLPackage.FSM_LINE_DISPLAY_CURSOR_TYPE:
				return createFSMLineDisplayCursorTypeFromString(eDataType, initialValue);
			case StatemachineDSLPackage.FSM_LINE_DISPLAY_MARQUEE_TYPE:
				return createFSMLineDisplayMarqueeTypeFromString(eDataType, initialValue);
			case StatemachineDSLPackage.FSM_LINE_DISPLAY_MARQUEE_FORMAT:
				return createFSMLineDisplayMarqueeFormatFromString(eDataType, initialValue);
			case StatemachineDSLPackage.FSM_LINE_DISPLAY_TEXT_TYPE:
				return createFSMLineDisplayTextTypeFromString(eDataType, initialValue);
			case StatemachineDSLPackage.FSM_LINE_DISPLAY_SCROLL_TEXT_TYPE:
				return createFSMLineDisplayScrollTextTypeFromString(eDataType, initialValue);
			case StatemachineDSLPackage.FSMPOS_PRINTER_BARCODE_TYPE:
				return createFSMPOSPrinterBarcodeTypeFromString(eDataType, initialValue);
			case StatemachineDSLPackage.FSM_FUNCTIONAL_KEY_CODES:
				return createFSMFunctionalKeyCodesFromString(eDataType, initialValue);
			case StatemachineDSLPackage.INTERNAL_EOBJECT:
				return createInternalEObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case StatemachineDSLPackage.FSM_INTERNAL_TYPE:
				return convertFSMInternalTypeToString(eDataType, instanceValue);
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_EVENT_TYPE:
				return convertFSMControlButtonEventTypeToString(eDataType, instanceValue);
			case StatemachineDSLPackage.FSM_COMPARE_OPERATION_ENUM:
				return convertFSMCompareOperationEnumToString(eDataType, instanceValue);
			case StatemachineDSLPackage.FSM_EVALUATION_TYPE:
				return convertFSMEvaluationTypeToString(eDataType, instanceValue);
			case StatemachineDSLPackage.FSM_USER_MESSAGE_TYPE:
				return convertFSMUserMessageTypeToString(eDataType, instanceValue);
			case StatemachineDSLPackage.FSM_LINE_DISPLAY_CURSOR_TYPE:
				return convertFSMLineDisplayCursorTypeToString(eDataType, instanceValue);
			case StatemachineDSLPackage.FSM_LINE_DISPLAY_MARQUEE_TYPE:
				return convertFSMLineDisplayMarqueeTypeToString(eDataType, instanceValue);
			case StatemachineDSLPackage.FSM_LINE_DISPLAY_MARQUEE_FORMAT:
				return convertFSMLineDisplayMarqueeFormatToString(eDataType, instanceValue);
			case StatemachineDSLPackage.FSM_LINE_DISPLAY_TEXT_TYPE:
				return convertFSMLineDisplayTextTypeToString(eDataType, instanceValue);
			case StatemachineDSLPackage.FSM_LINE_DISPLAY_SCROLL_TEXT_TYPE:
				return convertFSMLineDisplayScrollTextTypeToString(eDataType, instanceValue);
			case StatemachineDSLPackage.FSMPOS_PRINTER_BARCODE_TYPE:
				return convertFSMPOSPrinterBarcodeTypeToString(eDataType, instanceValue);
			case StatemachineDSLPackage.FSM_FUNCTIONAL_KEY_CODES:
				return convertFSMFunctionalKeyCodesToString(eDataType, instanceValue);
			case StatemachineDSLPackage.INTERNAL_EOBJECT:
				return convertInternalEObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMModel createFSMModel() {
		FSMModelImpl fsmModel = new FSMModelImpl();
		return fsmModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMLazyResolver createFSMLazyResolver() {
		FSMLazyResolverImpl fsmLazyResolver = new FSMLazyResolverImpl();
		return fsmLazyResolver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMBase createFSMBase() {
		FSMBaseImpl fsmBase = new FSMBaseImpl();
		return fsmBase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPackage createFSMPackage() {
		FSMPackageImpl fsmPackage = new FSMPackageImpl();
		return fsmPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSM createFSM() {
		FSMImpl fsm = new FSMImpl();
		return fsm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlButton createFSMControlButton() {
		FSMControlButtonImpl fsmControlButton = new FSMControlButtonImpl();
		return fsmControlButton;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlField createFSMControlField() {
		FSMControlFieldImpl fsmControlField = new FSMControlFieldImpl();
		return fsmControlField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlDTO createFSMControlDTO() {
		FSMControlDTOImpl fsmControlDTO = new FSMControlDTOImpl();
		return fsmControlDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlScheduler createFSMControlScheduler() {
		FSMControlSchedulerImpl fsmControlScheduler = new FSMControlSchedulerImpl();
		return fsmControlScheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlPeripheral createFSMControlPeripheral() {
		FSMControlPeripheralImpl fsmControlPeripheral = new FSMControlPeripheralImpl();
		return fsmControlPeripheral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlButtonAttribute createFSMControlButtonAttribute() {
		FSMControlButtonAttributeImpl fsmControlButtonAttribute = new FSMControlButtonAttributeImpl();
		return fsmControlButtonAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlButtonAttributeEventKeyboard createFSMControlButtonAttributeEventKeyboard() {
		FSMControlButtonAttributeEventKeyboardImpl fsmControlButtonAttributeEventKeyboard = new FSMControlButtonAttributeEventKeyboardImpl();
		return fsmControlButtonAttributeEventKeyboard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlButtonAttributeEventIdentity createFSMControlButtonAttributeEventIdentity() {
		FSMControlButtonAttributeEventIdentityImpl fsmControlButtonAttributeEventIdentity = new FSMControlButtonAttributeEventIdentityImpl();
		return fsmControlButtonAttributeEventIdentity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlButtonAttributeEventEvent createFSMControlButtonAttributeEventEvent() {
		FSMControlButtonAttributeEventEventImpl fsmControlButtonAttributeEventEvent = new FSMControlButtonAttributeEventEventImpl();
		return fsmControlButtonAttributeEventEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlFieldAttribute createFSMControlFieldAttribute() {
		FSMControlFieldAttributeImpl fsmControlFieldAttribute = new FSMControlFieldAttributeImpl();
		return fsmControlFieldAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlFieldLayout createFSMControlFieldLayout() {
		FSMControlFieldLayoutImpl fsmControlFieldLayout = new FSMControlFieldLayoutImpl();
		return fsmControlFieldLayout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlDTOAttribute createFSMControlDTOAttribute() {
		FSMControlDTOAttributeImpl fsmControlDTOAttribute = new FSMControlDTOAttributeImpl();
		return fsmControlDTOAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDeviceDisplay createFSMPeripheralDeviceDisplay() {
		FSMPeripheralDeviceDisplayImpl fsmPeripheralDeviceDisplay = new FSMPeripheralDeviceDisplayImpl();
		return fsmPeripheralDeviceDisplay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDeviceLineDisplay createFSMPeripheralDeviceLineDisplay() {
		FSMPeripheralDeviceLineDisplayImpl fsmPeripheralDeviceLineDisplay = new FSMPeripheralDeviceLineDisplayImpl();
		return fsmPeripheralDeviceLineDisplay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDevicePOSPrinter createFSMPeripheralDevicePOSPrinter() {
		FSMPeripheralDevicePOSPrinterImpl fsmPeripheralDevicePOSPrinter = new FSMPeripheralDevicePOSPrinterImpl();
		return fsmPeripheralDevicePOSPrinter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDeviceCashDrawer createFSMPeripheralDeviceCashDrawer() {
		FSMPeripheralDeviceCashDrawerImpl fsmPeripheralDeviceCashDrawer = new FSMPeripheralDeviceCashDrawerImpl();
		return fsmPeripheralDeviceCashDrawer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDevicePT createFSMPeripheralDevicePT() {
		FSMPeripheralDevicePTImpl fsmPeripheralDevicePT = new FSMPeripheralDevicePTImpl();
		return fsmPeripheralDevicePT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDeviceSignature createFSMPeripheralDeviceSignature() {
		FSMPeripheralDeviceSignatureImpl fsmPeripheralDeviceSignature = new FSMPeripheralDeviceSignatureImpl();
		return fsmPeripheralDeviceSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDeviceScale createFSMPeripheralDeviceScale() {
		FSMPeripheralDeviceScaleImpl fsmPeripheralDeviceScale = new FSMPeripheralDeviceScaleImpl();
		return fsmPeripheralDeviceScale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlSchedulerAttribute createFSMControlSchedulerAttribute() {
		FSMControlSchedulerAttributeImpl fsmControlSchedulerAttribute = new FSMControlSchedulerAttributeImpl();
		return fsmControlSchedulerAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMEvent createFSMEvent() {
		FSMEventImpl fsmEvent = new FSMEventImpl();
		return fsmEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMState createFSMState() {
		FSMStateImpl fsmState = new FSMStateImpl();
		return fsmState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMKeyMapper createFSMKeyMapper() {
		FSMKeyMapperImpl fsmKeyMapper = new FSMKeyMapperImpl();
		return fsmKeyMapper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMTrigger createFSMTrigger() {
		FSMTriggerImpl fsmTrigger = new FSMTriggerImpl();
		return fsmTrigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralBlinkRate createFSMActionPeripheralBlinkRate() {
		FSMActionPeripheralBlinkRateImpl fsmActionPeripheralBlinkRate = new FSMActionPeripheralBlinkRateImpl();
		return fsmActionPeripheralBlinkRate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralClear createFSMActionPeripheralClear() {
		FSMActionPeripheralClearImpl fsmActionPeripheralClear = new FSMActionPeripheralClearImpl();
		return fsmActionPeripheralClear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralCreateWindow createFSMActionPeripheralCreateWindow() {
		FSMActionPeripheralCreateWindowImpl fsmActionPeripheralCreateWindow = new FSMActionPeripheralCreateWindowImpl();
		return fsmActionPeripheralCreateWindow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralCursorType createFSMActionPeripheralCursorType() {
		FSMActionPeripheralCursorTypeImpl fsmActionPeripheralCursorType = new FSMActionPeripheralCursorTypeImpl();
		return fsmActionPeripheralCursorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralDestroyWindow createFSMActionPeripheralDestroyWindow() {
		FSMActionPeripheralDestroyWindowImpl fsmActionPeripheralDestroyWindow = new FSMActionPeripheralDestroyWindowImpl();
		return fsmActionPeripheralDestroyWindow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralDeviceBrightness createFSMActionPeripheralDeviceBrightness() {
		FSMActionPeripheralDeviceBrightnessImpl fsmActionPeripheralDeviceBrightness = new FSMActionPeripheralDeviceBrightnessImpl();
		return fsmActionPeripheralDeviceBrightness;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralDisplayText createFSMActionPeripheralDisplayText() {
		FSMActionPeripheralDisplayTextImpl fsmActionPeripheralDisplayText = new FSMActionPeripheralDisplayTextImpl();
		return fsmActionPeripheralDisplayText;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralLineDisplayText createFSMActionPeripheralLineDisplayText() {
		FSMActionPeripheralLineDisplayTextImpl fsmActionPeripheralLineDisplayText = new FSMActionPeripheralLineDisplayTextImpl();
		return fsmActionPeripheralLineDisplayText;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralLineDisplayTextAt createFSMActionPeripheralLineDisplayTextAt() {
		FSMActionPeripheralLineDisplayTextAtImpl fsmActionPeripheralLineDisplayTextAt = new FSMActionPeripheralLineDisplayTextAtImpl();
		return fsmActionPeripheralLineDisplayTextAt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralInterCharacterWait createFSMActionPeripheralInterCharacterWait() {
		FSMActionPeripheralInterCharacterWaitImpl fsmActionPeripheralInterCharacterWait = new FSMActionPeripheralInterCharacterWaitImpl();
		return fsmActionPeripheralInterCharacterWait;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralMarqueeFormat createFSMActionPeripheralMarqueeFormat() {
		FSMActionPeripheralMarqueeFormatImpl fsmActionPeripheralMarqueeFormat = new FSMActionPeripheralMarqueeFormatImpl();
		return fsmActionPeripheralMarqueeFormat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralMarqueeRepeatWait createFSMActionPeripheralMarqueeRepeatWait() {
		FSMActionPeripheralMarqueeRepeatWaitImpl fsmActionPeripheralMarqueeRepeatWait = new FSMActionPeripheralMarqueeRepeatWaitImpl();
		return fsmActionPeripheralMarqueeRepeatWait;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralMarqueeType createFSMActionPeripheralMarqueeType() {
		FSMActionPeripheralMarqueeTypeImpl fsmActionPeripheralMarqueeType = new FSMActionPeripheralMarqueeTypeImpl();
		return fsmActionPeripheralMarqueeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralMarqueeUnitWait createFSMActionPeripheralMarqueeUnitWait() {
		FSMActionPeripheralMarqueeUnitWaitImpl fsmActionPeripheralMarqueeUnitWait = new FSMActionPeripheralMarqueeUnitWaitImpl();
		return fsmActionPeripheralMarqueeUnitWait;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralScroll createFSMActionPeripheralScroll() {
		FSMActionPeripheralScrollImpl fsmActionPeripheralScroll = new FSMActionPeripheralScrollImpl();
		return fsmActionPeripheralScroll;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralOpenDrawer createFSMActionPeripheralOpenDrawer() {
		FSMActionPeripheralOpenDrawerImpl fsmActionPeripheralOpenDrawer = new FSMActionPeripheralOpenDrawerImpl();
		return fsmActionPeripheralOpenDrawer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPrintBarcode createFSMActionPeripheralPrintBarcode() {
		FSMActionPeripheralPrintBarcodeImpl fsmActionPeripheralPrintBarcode = new FSMActionPeripheralPrintBarcodeImpl();
		return fsmActionPeripheralPrintBarcode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPrintBitmap createFSMActionPeripheralPrintBitmap() {
		FSMActionPeripheralPrintBitmapImpl fsmActionPeripheralPrintBitmap = new FSMActionPeripheralPrintBitmapImpl();
		return fsmActionPeripheralPrintBitmap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPrintCut createFSMActionPeripheralPrintCut() {
		FSMActionPeripheralPrintCutImpl fsmActionPeripheralPrintCut = new FSMActionPeripheralPrintCutImpl();
		return fsmActionPeripheralPrintCut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPrintRegisterBitmap createFSMActionPeripheralPrintRegisterBitmap() {
		FSMActionPeripheralPrintRegisterBitmapImpl fsmActionPeripheralPrintRegisterBitmap = new FSMActionPeripheralPrintRegisterBitmapImpl();
		return fsmActionPeripheralPrintRegisterBitmap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPrintNormal createFSMActionPeripheralPrintNormal() {
		FSMActionPeripheralPrintNormalImpl fsmActionPeripheralPrintNormal = new FSMActionPeripheralPrintNormalImpl();
		return fsmActionPeripheralPrintNormal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPTOpen createFSMActionPeripheralPTOpen() {
		FSMActionPeripheralPTOpenImpl fsmActionPeripheralPTOpen = new FSMActionPeripheralPTOpenImpl();
		return fsmActionPeripheralPTOpen;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPTClose createFSMActionPeripheralPTClose() {
		FSMActionPeripheralPTCloseImpl fsmActionPeripheralPTClose = new FSMActionPeripheralPTCloseImpl();
		return fsmActionPeripheralPTClose;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPTReversal createFSMActionPeripheralPTReversal() {
		FSMActionPeripheralPTReversalImpl fsmActionPeripheralPTReversal = new FSMActionPeripheralPTReversalImpl();
		return fsmActionPeripheralPTReversal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPTAcknowledge createFSMActionPeripheralPTAcknowledge() {
		FSMActionPeripheralPTAcknowledgeImpl fsmActionPeripheralPTAcknowledge = new FSMActionPeripheralPTAcknowledgeImpl();
		return fsmActionPeripheralPTAcknowledge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPTRegistration createFSMActionPeripheralPTRegistration() {
		FSMActionPeripheralPTRegistrationImpl fsmActionPeripheralPTRegistration = new FSMActionPeripheralPTRegistrationImpl();
		return fsmActionPeripheralPTRegistration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPTAuthorization createFSMActionPeripheralPTAuthorization() {
		FSMActionPeripheralPTAuthorizationImpl fsmActionPeripheralPTAuthorization = new FSMActionPeripheralPTAuthorizationImpl();
		return fsmActionPeripheralPTAuthorization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralBeeper createFSMActionPeripheralBeeper() {
		FSMActionPeripheralBeeperImpl fsmActionPeripheralBeeper = new FSMActionPeripheralBeeperImpl();
		return fsmActionPeripheralBeeper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPlayer createFSMActionPeripheralPlayer() {
		FSMActionPeripheralPlayerImpl fsmActionPeripheralPlayer = new FSMActionPeripheralPlayerImpl();
		return fsmActionPeripheralPlayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralSound createFSMActionPeripheralSound() {
		FSMActionPeripheralSoundImpl fsmActionPeripheralSound = new FSMActionPeripheralSoundImpl();
		return fsmActionPeripheralSound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPTResponse createFSMActionPeripheralPTResponse() {
		FSMActionPeripheralPTResponseImpl fsmActionPeripheralPTResponse = new FSMActionPeripheralPTResponseImpl();
		return fsmActionPeripheralPTResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralPrintReport createFSMActionPeripheralPrintReport() {
		FSMActionPeripheralPrintReportImpl fsmActionPeripheralPrintReport = new FSMActionPeripheralPrintReportImpl();
		return fsmActionPeripheralPrintReport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralSignatureOpen createFSMActionPeripheralSignatureOpen() {
		FSMActionPeripheralSignatureOpenImpl fsmActionPeripheralSignatureOpen = new FSMActionPeripheralSignatureOpenImpl();
		return fsmActionPeripheralSignatureOpen;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralSignatureClose createFSMActionPeripheralSignatureClose() {
		FSMActionPeripheralSignatureCloseImpl fsmActionPeripheralSignatureClose = new FSMActionPeripheralSignatureCloseImpl();
		return fsmActionPeripheralSignatureClose;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralSignatureClear createFSMActionPeripheralSignatureClear() {
		FSMActionPeripheralSignatureClearImpl fsmActionPeripheralSignatureClear = new FSMActionPeripheralSignatureClearImpl();
		return fsmActionPeripheralSignatureClear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralSignatureCapture createFSMActionPeripheralSignatureCapture() {
		FSMActionPeripheralSignatureCaptureImpl fsmActionPeripheralSignatureCapture = new FSMActionPeripheralSignatureCaptureImpl();
		return fsmActionPeripheralSignatureCapture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralSignatureIdle createFSMActionPeripheralSignatureIdle() {
		FSMActionPeripheralSignatureIdleImpl fsmActionPeripheralSignatureIdle = new FSMActionPeripheralSignatureIdleImpl();
		return fsmActionPeripheralSignatureIdle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralSignatureLabel createFSMActionPeripheralSignatureLabel() {
		FSMActionPeripheralSignatureLabelImpl fsmActionPeripheralSignatureLabel = new FSMActionPeripheralSignatureLabelImpl();
		return fsmActionPeripheralSignatureLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMSignatureRetrieve createFSMSignatureRetrieve() {
		FSMSignatureRetrieveImpl fsmSignatureRetrieve = new FSMSignatureRetrieveImpl();
		return fsmSignatureRetrieve;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralScaleReadWeight createFSMActionPeripheralScaleReadWeight() {
		FSMActionPeripheralScaleReadWeightImpl fsmActionPeripheralScaleReadWeight = new FSMActionPeripheralScaleReadWeightImpl();
		return fsmActionPeripheralScaleReadWeight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralScaleReadTareWeight createFSMActionPeripheralScaleReadTareWeight() {
		FSMActionPeripheralScaleReadTareWeightImpl fsmActionPeripheralScaleReadTareWeight = new FSMActionPeripheralScaleReadTareWeightImpl();
		return fsmActionPeripheralScaleReadTareWeight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralScaleTareWeight createFSMActionPeripheralScaleTareWeight() {
		FSMActionPeripheralScaleTareWeightImpl fsmActionPeripheralScaleTareWeight = new FSMActionPeripheralScaleTareWeightImpl();
		return fsmActionPeripheralScaleTareWeight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralScaleZero createFSMActionPeripheralScaleZero() {
		FSMActionPeripheralScaleZeroImpl fsmActionPeripheralScaleZero = new FSMActionPeripheralScaleZeroImpl();
		return fsmActionPeripheralScaleZero;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralScaleDisplayText createFSMActionPeripheralScaleDisplayText() {
		FSMActionPeripheralScaleDisplayTextImpl fsmActionPeripheralScaleDisplayText = new FSMActionPeripheralScaleDisplayTextImpl();
		return fsmActionPeripheralScaleDisplayText;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionPeripheralScaleWeightUnit createFSMActionPeripheralScaleWeightUnit() {
		FSMActionPeripheralScaleWeightUnitImpl fsmActionPeripheralScaleWeightUnit = new FSMActionPeripheralScaleWeightUnitImpl();
		return fsmActionPeripheralScaleWeightUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldSourceString createFSMActionFieldSourceString() {
		FSMActionFieldSourceStringImpl fsmActionFieldSourceString = new FSMActionFieldSourceStringImpl();
		return fsmActionFieldSourceString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldSourceNumber createFSMActionFieldSourceNumber() {
		FSMActionFieldSourceNumberImpl fsmActionFieldSourceNumber = new FSMActionFieldSourceNumberImpl();
		return fsmActionFieldSourceNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldSourceInteger createFSMActionFieldSourceInteger() {
		FSMActionFieldSourceIntegerImpl fsmActionFieldSourceInteger = new FSMActionFieldSourceIntegerImpl();
		return fsmActionFieldSourceInteger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldSourceBoolean createFSMActionFieldSourceBoolean() {
		FSMActionFieldSourceBooleanImpl fsmActionFieldSourceBoolean = new FSMActionFieldSourceBooleanImpl();
		return fsmActionFieldSourceBoolean;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldSourceEvaluate createFSMActionFieldSourceEvaluate() {
		FSMActionFieldSourceEvaluateImpl fsmActionFieldSourceEvaluate = new FSMActionFieldSourceEvaluateImpl();
		return fsmActionFieldSourceEvaluate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldSourceTranslate createFSMActionFieldSourceTranslate() {
		FSMActionFieldSourceTranslateImpl fsmActionFieldSourceTranslate = new FSMActionFieldSourceTranslateImpl();
		return fsmActionFieldSourceTranslate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMDotExpression createFSMDotExpression() {
		FSMDotExpressionImpl fsmDotExpression = new FSMDotExpressionImpl();
		return fsmDotExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMDtoRef createFSMDtoRef() {
		FSMDtoRefImpl fsmDtoRef = new FSMDtoRefImpl();
		return fsmDtoRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldSourceDtoAttribute createFSMActionFieldSourceDtoAttribute() {
		FSMActionFieldSourceDtoAttributeImpl fsmActionFieldSourceDtoAttribute = new FSMActionFieldSourceDtoAttributeImpl();
		return fsmActionFieldSourceDtoAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldSourceEvent createFSMActionFieldSourceEvent() {
		FSMActionFieldSourceEventImpl fsmActionFieldSourceEvent = new FSMActionFieldSourceEventImpl();
		return fsmActionFieldSourceEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionConditionalTransition createFSMActionConditionalTransition() {
		FSMActionConditionalTransitionImpl fsmActionConditionalTransition = new FSMActionConditionalTransitionImpl();
		return fsmActionConditionalTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMOperationParameter createFSMOperationParameter() {
		FSMOperationParameterImpl fsmOperationParameter = new FSMOperationParameterImpl();
		return fsmOperationParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMOperation createFSMOperation() {
		FSMOperationImpl fsmOperation = new FSMOperationImpl();
		return fsmOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMGuard createFSMGuard() {
		FSMGuardImpl fsmGuard = new FSMGuardImpl();
		return fsmGuard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMFunction createFSMFunction() {
		FSMFunctionImpl fsmFunction = new FSMFunctionImpl();
		return fsmFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMStorageRetrieve createFSMStorageRetrieve() {
		FSMStorageRetrieveImpl fsmStorageRetrieve = new FSMStorageRetrieveImpl();
		return fsmStorageRetrieve;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMStorage createFSMStorage() {
		FSMStorageImpl fsmStorage = new FSMStorageImpl();
		return fsmStorage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldConcatenation createFSMActionFieldConcatenation() {
		FSMActionFieldConcatenationImpl fsmActionFieldConcatenation = new FSMActionFieldConcatenationImpl();
		return fsmActionFieldConcatenation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldSet createFSMActionFieldSet() {
		FSMActionFieldSetImpl fsmActionFieldSet = new FSMActionFieldSetImpl();
		return fsmActionFieldSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldKeystroke createFSMActionFieldKeystroke() {
		FSMActionFieldKeystrokeImpl fsmActionFieldKeystroke = new FSMActionFieldKeystrokeImpl();
		return fsmActionFieldKeystroke;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldClear createFSMActionFieldClear() {
		FSMActionFieldClearImpl fsmActionFieldClear = new FSMActionFieldClearImpl();
		return fsmActionFieldClear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldGet createFSMActionFieldGet() {
		FSMActionFieldGetImpl fsmActionFieldGet = new FSMActionFieldGetImpl();
		return fsmActionFieldGet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldFilterToggle createFSMActionFieldFilterToggle() {
		FSMActionFieldFilterToggleImpl fsmActionFieldFilterToggle = new FSMActionFieldFilterToggleImpl();
		return fsmActionFieldFilterToggle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldRemove createFSMActionFieldRemove() {
		FSMActionFieldRemoveImpl fsmActionFieldRemove = new FSMActionFieldRemoveImpl();
		return fsmActionFieldRemove;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionItemVisible createFSMActionItemVisible() {
		FSMActionItemVisibleImpl fsmActionItemVisible = new FSMActionItemVisibleImpl();
		return fsmActionItemVisible;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionItemInvisible createFSMActionItemInvisible() {
		FSMActionItemInvisibleImpl fsmActionItemInvisible = new FSMActionItemInvisibleImpl();
		return fsmActionItemInvisible;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionButtonCaption createFSMActionButtonCaption() {
		FSMActionButtonCaptionImpl fsmActionButtonCaption = new FSMActionButtonCaptionImpl();
		return fsmActionButtonCaption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionButtonImage createFSMActionButtonImage() {
		FSMActionButtonImageImpl fsmActionButtonImage = new FSMActionButtonImageImpl();
		return fsmActionButtonImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionDTOFind createFSMActionDTOFind() {
		FSMActionDTOFindImpl fsmActionDTOFind = new FSMActionDTOFindImpl();
		return fsmActionDTOFind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionDTOClear createFSMActionDTOClear() {
		FSMActionDTOClearImpl fsmActionDTOClear = new FSMActionDTOClearImpl();
		return fsmActionDTOClear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionScheduler createFSMActionScheduler() {
		FSMActionSchedulerImpl fsmActionScheduler = new FSMActionSchedulerImpl();
		return fsmActionScheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMDTOType createFSMDTOType() {
		FSMDTOTypeImpl fsmdtoType = new FSMDTOTypeImpl();
		return fsmdtoType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMFieldType createFSMFieldType() {
		FSMFieldTypeImpl fsmFieldType = new FSMFieldTypeImpl();
		return fsmFieldType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMFilterProperty createFSMFilterProperty() {
		FSMFilterPropertyImpl fsmFilterProperty = new FSMFilterPropertyImpl();
		return fsmFilterProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMFilter createFSMFilter() {
		FSMFilterImpl fsmFilter = new FSMFilterImpl();
		return fsmFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMJunctionFilter createFSMJunctionFilter() {
		FSMJunctionFilterImpl fsmJunctionFilter = new FSMJunctionFilterImpl();
		return fsmJunctionFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMAndFilter createFSMAndFilter() {
		FSMAndFilterImpl fsmAndFilter = new FSMAndFilterImpl();
		return fsmAndFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMOrFilter createFSMOrFilter() {
		FSMOrFilterImpl fsmOrFilter = new FSMOrFilterImpl();
		return fsmOrFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMBetweenFilter createFSMBetweenFilter() {
		FSMBetweenFilterImpl fsmBetweenFilter = new FSMBetweenFilterImpl();
		return fsmBetweenFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMCompareFilter createFSMCompareFilter() {
		FSMCompareFilterImpl fsmCompareFilter = new FSMCompareFilterImpl();
		return fsmCompareFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMIsNullFilter createFSMIsNullFilter() {
		FSMIsNullFilterImpl fsmIsNullFilter = new FSMIsNullFilterImpl();
		return fsmIsNullFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMLikeFilter createFSMLikeFilter() {
		FSMLikeFilterImpl fsmLikeFilter = new FSMLikeFilterImpl();
		return fsmLikeFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMNotFilter createFSMNotFilter() {
		FSMNotFilterImpl fsmNotFilter = new FSMNotFilterImpl();
		return fsmNotFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMStringFilter createFSMStringFilter() {
		FSMStringFilterImpl fsmStringFilter = new FSMStringFilterImpl();
		return fsmStringFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlFilter createFSMControlFilter() {
		FSMControlFilterImpl fsmControlFilter = new FSMControlFilterImpl();
		return fsmControlFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMInternalType createFSMInternalTypeFromString(EDataType eDataType, String initialValue) {
		FSMInternalType result = FSMInternalType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMInternalTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlButtonEventType createFSMControlButtonEventTypeFromString(EDataType eDataType, String initialValue) {
		FSMControlButtonEventType result = FSMControlButtonEventType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMControlButtonEventTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMCompareOperationEnum createFSMCompareOperationEnumFromString(EDataType eDataType, String initialValue) {
		FSMCompareOperationEnum result = FSMCompareOperationEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMCompareOperationEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMEvaluationType createFSMEvaluationTypeFromString(EDataType eDataType, String initialValue) {
		FSMEvaluationType result = FSMEvaluationType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMEvaluationTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMUserMessageType createFSMUserMessageTypeFromString(EDataType eDataType, String initialValue) {
		FSMUserMessageType result = FSMUserMessageType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMUserMessageTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMLineDisplayCursorType createFSMLineDisplayCursorTypeFromString(EDataType eDataType, String initialValue) {
		FSMLineDisplayCursorType result = FSMLineDisplayCursorType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMLineDisplayCursorTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMLineDisplayMarqueeType createFSMLineDisplayMarqueeTypeFromString(EDataType eDataType, String initialValue) {
		FSMLineDisplayMarqueeType result = FSMLineDisplayMarqueeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMLineDisplayMarqueeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMLineDisplayMarqueeFormat createFSMLineDisplayMarqueeFormatFromString(EDataType eDataType, String initialValue) {
		FSMLineDisplayMarqueeFormat result = FSMLineDisplayMarqueeFormat.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMLineDisplayMarqueeFormatToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMLineDisplayTextType createFSMLineDisplayTextTypeFromString(EDataType eDataType, String initialValue) {
		FSMLineDisplayTextType result = FSMLineDisplayTextType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMLineDisplayTextTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMLineDisplayScrollTextType createFSMLineDisplayScrollTextTypeFromString(EDataType eDataType, String initialValue) {
		FSMLineDisplayScrollTextType result = FSMLineDisplayScrollTextType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMLineDisplayScrollTextTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPOSPrinterBarcodeType createFSMPOSPrinterBarcodeTypeFromString(EDataType eDataType, String initialValue) {
		FSMPOSPrinterBarcodeType result = FSMPOSPrinterBarcodeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMPOSPrinterBarcodeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMFunctionalKeyCodes createFSMFunctionalKeyCodesFromString(EDataType eDataType, String initialValue) {
		FSMFunctionalKeyCodes result = FSMFunctionalKeyCodes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMFunctionalKeyCodesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalEObject createInternalEObjectFromString(EDataType eDataType, String initialValue) {
		return (InternalEObject)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInternalEObjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachineDSLPackage getStatemachineDSLPackage() {
		return (StatemachineDSLPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StatemachineDSLPackage getPackage() {
		return StatemachineDSLPackage.eINSTANCE;
	}

} //StatemachineDSLFactoryImpl
