/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.statemachine.FSMEvent;
import org.eclipse.osbp.xtext.statemachine.FSMFunctionalKeyCodes;
import org.eclipse.osbp.xtext.statemachine.FSMKeyMapper;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Key Mapper</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMKeyMapperImpl#getKeyCode <em>Key Code</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMKeyMapperImpl#getKeyEvent <em>Key Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMKeyMapperImpl extends FSMLazyResolverImpl implements FSMKeyMapper {
	/**
	 * The default value of the '{@link #getKeyCode() <em>Key Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyCode()
	 * @generated
	 * @ordered
	 */
	protected static final FSMFunctionalKeyCodes KEY_CODE_EDEFAULT = FSMFunctionalKeyCodes.BACKSPACE;

	/**
	 * The cached value of the '{@link #getKeyCode() <em>Key Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyCode()
	 * @generated
	 * @ordered
	 */
	protected FSMFunctionalKeyCodes keyCode = KEY_CODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getKeyEvent() <em>Key Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyEvent()
	 * @generated
	 * @ordered
	 */
	protected FSMEvent keyEvent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMKeyMapperImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_KEY_MAPPER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMFunctionalKeyCodes getKeyCode() {
		return keyCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKeyCode(FSMFunctionalKeyCodes newKeyCode) {
		FSMFunctionalKeyCodes oldKeyCode = keyCode;
		keyCode = newKeyCode == null ? KEY_CODE_EDEFAULT : newKeyCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_KEY_MAPPER__KEY_CODE, oldKeyCode, keyCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMEvent getKeyEvent() {
		if (keyEvent != null && keyEvent.eIsProxy()) {
			InternalEObject oldKeyEvent = (InternalEObject)keyEvent;
			keyEvent = (FSMEvent)eResolveProxy(oldKeyEvent);
			if (keyEvent != oldKeyEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_KEY_MAPPER__KEY_EVENT, oldKeyEvent, keyEvent));
			}
		}
		return keyEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMEvent basicGetKeyEvent() {
		return keyEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKeyEvent(FSMEvent newKeyEvent) {
		FSMEvent oldKeyEvent = keyEvent;
		keyEvent = newKeyEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_KEY_MAPPER__KEY_EVENT, oldKeyEvent, keyEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_KEY_MAPPER__KEY_CODE:
				return getKeyCode();
			case StatemachineDSLPackage.FSM_KEY_MAPPER__KEY_EVENT:
				if (resolve) return getKeyEvent();
				return basicGetKeyEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_KEY_MAPPER__KEY_CODE:
				setKeyCode((FSMFunctionalKeyCodes)newValue);
				return;
			case StatemachineDSLPackage.FSM_KEY_MAPPER__KEY_EVENT:
				setKeyEvent((FSMEvent)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_KEY_MAPPER__KEY_CODE:
				setKeyCode(KEY_CODE_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_KEY_MAPPER__KEY_EVENT:
				setKeyEvent((FSMEvent)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_KEY_MAPPER__KEY_CODE:
				return keyCode != KEY_CODE_EDEFAULT;
			case StatemachineDSLPackage.FSM_KEY_MAPPER__KEY_EVENT:
				return keyEvent != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (keyCode: ");
		result.append(keyCode);
		result.append(')');
		return result.toString();
	}

} //FSMKeyMapperImpl
