/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryOperation;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryStatemachineGroup;

import org.eclipse.osbp.xtext.statemachine.FSMOperation;
import org.eclipse.osbp.xtext.statemachine.FSMOperationParameter;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMOperationImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMOperationImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMOperationImpl#getFirst <em>First</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMOperationImpl#getMore <em>More</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMOperationImpl extends FSMLazyResolverImpl implements FSMOperation {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FunctionLibraryStatemachineGroup group;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected FunctionLibraryOperation operation;

	/**
	 * The cached value of the '{@link #getFirst() <em>First</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirst()
	 * @generated
	 * @ordered
	 */
	protected FSMOperationParameter first;

	/**
	 * The cached value of the '{@link #getMore() <em>More</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMore()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMOperationParameter> more;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryStatemachineGroup getGroup() {
		if (group != null && group.eIsProxy()) {
			InternalEObject oldGroup = (InternalEObject)group;
			group = (FunctionLibraryStatemachineGroup)eResolveProxy(oldGroup);
			if (group != oldGroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_OPERATION__GROUP, oldGroup, group));
			}
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryStatemachineGroup basicGetGroup() {
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroup(FunctionLibraryStatemachineGroup newGroup) {
		FunctionLibraryStatemachineGroup oldGroup = group;
		group = newGroup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_OPERATION__GROUP, oldGroup, group));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryOperation getOperation() {
		if (operation != null && operation.eIsProxy()) {
			InternalEObject oldOperation = (InternalEObject)operation;
			operation = (FunctionLibraryOperation)eResolveProxy(oldOperation);
			if (operation != oldOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_OPERATION__OPERATION, oldOperation, operation));
			}
		}
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryOperation basicGetOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(FunctionLibraryOperation newOperation) {
		FunctionLibraryOperation oldOperation = operation;
		operation = newOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_OPERATION__OPERATION, oldOperation, operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMOperationParameter getFirst() {
		return first;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFirst(FSMOperationParameter newFirst, NotificationChain msgs) {
		FSMOperationParameter oldFirst = first;
		first = newFirst;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_OPERATION__FIRST, oldFirst, newFirst);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirst(FSMOperationParameter newFirst) {
		if (newFirst != first) {
			NotificationChain msgs = null;
			if (first != null)
				msgs = ((InternalEObject)first).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_OPERATION__FIRST, null, msgs);
			if (newFirst != null)
				msgs = ((InternalEObject)newFirst).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_OPERATION__FIRST, null, msgs);
			msgs = basicSetFirst(newFirst, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_OPERATION__FIRST, newFirst, newFirst));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMOperationParameter> getMore() {
		if (more == null) {
			more = new EObjectContainmentEList<FSMOperationParameter>(FSMOperationParameter.class, this, StatemachineDSLPackage.FSM_OPERATION__MORE);
		}
		return more;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_OPERATION__FIRST:
				return basicSetFirst(null, msgs);
			case StatemachineDSLPackage.FSM_OPERATION__MORE:
				return ((InternalEList<?>)getMore()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_OPERATION__GROUP:
				if (resolve) return getGroup();
				return basicGetGroup();
			case StatemachineDSLPackage.FSM_OPERATION__OPERATION:
				if (resolve) return getOperation();
				return basicGetOperation();
			case StatemachineDSLPackage.FSM_OPERATION__FIRST:
				return getFirst();
			case StatemachineDSLPackage.FSM_OPERATION__MORE:
				return getMore();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_OPERATION__GROUP:
				setGroup((FunctionLibraryStatemachineGroup)newValue);
				return;
			case StatemachineDSLPackage.FSM_OPERATION__OPERATION:
				setOperation((FunctionLibraryOperation)newValue);
				return;
			case StatemachineDSLPackage.FSM_OPERATION__FIRST:
				setFirst((FSMOperationParameter)newValue);
				return;
			case StatemachineDSLPackage.FSM_OPERATION__MORE:
				getMore().clear();
				getMore().addAll((Collection<? extends FSMOperationParameter>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_OPERATION__GROUP:
				setGroup((FunctionLibraryStatemachineGroup)null);
				return;
			case StatemachineDSLPackage.FSM_OPERATION__OPERATION:
				setOperation((FunctionLibraryOperation)null);
				return;
			case StatemachineDSLPackage.FSM_OPERATION__FIRST:
				setFirst((FSMOperationParameter)null);
				return;
			case StatemachineDSLPackage.FSM_OPERATION__MORE:
				getMore().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_OPERATION__GROUP:
				return group != null;
			case StatemachineDSLPackage.FSM_OPERATION__OPERATION:
				return operation != null;
			case StatemachineDSLPackage.FSM_OPERATION__FIRST:
				return first != null;
			case StatemachineDSLPackage.FSM_OPERATION__MORE:
				return more != null && !more.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FSMOperationImpl
