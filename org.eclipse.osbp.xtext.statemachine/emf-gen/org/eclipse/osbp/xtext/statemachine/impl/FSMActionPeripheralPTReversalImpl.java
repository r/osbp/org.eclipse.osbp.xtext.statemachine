/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.statemachine.FSMActionFieldConcatenation;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePT;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Action Peripheral PT Reversal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTReversalImpl#getDevice <em>Device</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTReversalImpl#getPassword <em>Password</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralPTReversalImpl#getReceipt <em>Receipt</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMActionPeripheralPTReversalImpl extends FSMLazyResolverImpl implements FSMActionPeripheralPTReversal {
	/**
	 * The cached value of the '{@link #getDevice() <em>Device</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDevice()
	 * @generated
	 * @ordered
	 */
	protected FSMPeripheralDevicePT device;

	/**
	 * The cached value of the '{@link #getPassword() <em>Password</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPassword()
	 * @generated
	 * @ordered
	 */
	protected FSMActionFieldConcatenation password;

	/**
	 * The cached value of the '{@link #getReceipt() <em>Receipt</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceipt()
	 * @generated
	 * @ordered
	 */
	protected FSMActionFieldConcatenation receipt;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMActionPeripheralPTReversalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_PT_REVERSAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDevicePT getDevice() {
		if (device != null && device.eIsProxy()) {
			InternalEObject oldDevice = (InternalEObject)device;
			device = (FSMPeripheralDevicePT)eResolveProxy(oldDevice);
			if (device != oldDevice) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__DEVICE, oldDevice, device));
			}
		}
		return device;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDevicePT basicGetDevice() {
		return device;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDevice(FSMPeripheralDevicePT newDevice) {
		FSMPeripheralDevicePT oldDevice = device;
		device = newDevice;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__DEVICE, oldDevice, device));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldConcatenation getPassword() {
		return password;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPassword(FSMActionFieldConcatenation newPassword, NotificationChain msgs) {
		FSMActionFieldConcatenation oldPassword = password;
		password = newPassword;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD, oldPassword, newPassword);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPassword(FSMActionFieldConcatenation newPassword) {
		if (newPassword != password) {
			NotificationChain msgs = null;
			if (password != null)
				msgs = ((InternalEObject)password).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD, null, msgs);
			if (newPassword != null)
				msgs = ((InternalEObject)newPassword).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD, null, msgs);
			msgs = basicSetPassword(newPassword, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD, newPassword, newPassword));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMActionFieldConcatenation getReceipt() {
		return receipt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReceipt(FSMActionFieldConcatenation newReceipt, NotificationChain msgs) {
		FSMActionFieldConcatenation oldReceipt = receipt;
		receipt = newReceipt;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT, oldReceipt, newReceipt);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceipt(FSMActionFieldConcatenation newReceipt) {
		if (newReceipt != receipt) {
			NotificationChain msgs = null;
			if (receipt != null)
				msgs = ((InternalEObject)receipt).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT, null, msgs);
			if (newReceipt != null)
				msgs = ((InternalEObject)newReceipt).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT, null, msgs);
			msgs = basicSetReceipt(newReceipt, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT, newReceipt, newReceipt));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD:
				return basicSetPassword(null, msgs);
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT:
				return basicSetReceipt(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__DEVICE:
				if (resolve) return getDevice();
				return basicGetDevice();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD:
				return getPassword();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT:
				return getReceipt();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__DEVICE:
				setDevice((FSMPeripheralDevicePT)newValue);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD:
				setPassword((FSMActionFieldConcatenation)newValue);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT:
				setReceipt((FSMActionFieldConcatenation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__DEVICE:
				setDevice((FSMPeripheralDevicePT)null);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD:
				setPassword((FSMActionFieldConcatenation)null);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT:
				setReceipt((FSMActionFieldConcatenation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__DEVICE:
				return device != null;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD:
				return password != null;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT:
				return receipt != null;
		}
		return super.eIsSet(featureID);
	}

} //FSMActionPeripheralPTReversalImpl
