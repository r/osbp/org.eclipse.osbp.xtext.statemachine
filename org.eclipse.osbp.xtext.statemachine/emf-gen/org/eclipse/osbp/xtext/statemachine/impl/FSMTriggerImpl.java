/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.statemachine.FSMAction;
import org.eclipse.osbp.xtext.statemachine.FSMEvent;
import org.eclipse.osbp.xtext.statemachine.FSMGuard;
import org.eclipse.osbp.xtext.statemachine.FSMState;
import org.eclipse.osbp.xtext.statemachine.FSMTrigger;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Trigger</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMTriggerImpl#isHasTransition <em>Has Transition</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMTriggerImpl#getTransition <em>Transition</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMTriggerImpl#getTriggers <em>Triggers</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMTriggerImpl#getGuards <em>Guards</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMTriggerImpl#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMTriggerImpl extends FSMLazyResolverImpl implements FSMTrigger {
	/**
	 * The default value of the '{@link #isHasTransition() <em>Has Transition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasTransition()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_TRANSITION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasTransition() <em>Has Transition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasTransition()
	 * @generated
	 * @ordered
	 */
	protected boolean hasTransition = HAS_TRANSITION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTransition() <em>Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransition()
	 * @generated
	 * @ordered
	 */
	protected FSMState transition;

	/**
	 * The cached value of the '{@link #getTriggers() <em>Triggers</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggers()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMEvent> triggers;

	/**
	 * The cached value of the '{@link #getGuards() <em>Guards</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuards()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMGuard> guards;

	/**
	 * The cached value of the '{@link #getActions() <em>Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActions()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMAction> actions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMTriggerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_TRIGGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasTransition() {
		return hasTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasTransition(boolean newHasTransition) {
		boolean oldHasTransition = hasTransition;
		hasTransition = newHasTransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_TRIGGER__HAS_TRANSITION, oldHasTransition, hasTransition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMState getTransition() {
		if (transition != null && transition.eIsProxy()) {
			InternalEObject oldTransition = (InternalEObject)transition;
			transition = (FSMState)eResolveProxy(oldTransition);
			if (transition != oldTransition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_TRIGGER__TRANSITION, oldTransition, transition));
			}
		}
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMState basicGetTransition() {
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransition(FSMState newTransition) {
		FSMState oldTransition = transition;
		transition = newTransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_TRIGGER__TRANSITION, oldTransition, transition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMEvent> getTriggers() {
		if (triggers == null) {
			triggers = new EObjectResolvingEList<FSMEvent>(FSMEvent.class, this, StatemachineDSLPackage.FSM_TRIGGER__TRIGGERS);
		}
		return triggers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMGuard> getGuards() {
		if (guards == null) {
			guards = new EObjectContainmentEList<FSMGuard>(FSMGuard.class, this, StatemachineDSLPackage.FSM_TRIGGER__GUARDS);
		}
		return guards;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMAction> getActions() {
		if (actions == null) {
			actions = new EObjectContainmentEList<FSMAction>(FSMAction.class, this, StatemachineDSLPackage.FSM_TRIGGER__ACTIONS);
		}
		return actions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_TRIGGER__GUARDS:
				return ((InternalEList<?>)getGuards()).basicRemove(otherEnd, msgs);
			case StatemachineDSLPackage.FSM_TRIGGER__ACTIONS:
				return ((InternalEList<?>)getActions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_TRIGGER__HAS_TRANSITION:
				return isHasTransition();
			case StatemachineDSLPackage.FSM_TRIGGER__TRANSITION:
				if (resolve) return getTransition();
				return basicGetTransition();
			case StatemachineDSLPackage.FSM_TRIGGER__TRIGGERS:
				return getTriggers();
			case StatemachineDSLPackage.FSM_TRIGGER__GUARDS:
				return getGuards();
			case StatemachineDSLPackage.FSM_TRIGGER__ACTIONS:
				return getActions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_TRIGGER__HAS_TRANSITION:
				setHasTransition((Boolean)newValue);
				return;
			case StatemachineDSLPackage.FSM_TRIGGER__TRANSITION:
				setTransition((FSMState)newValue);
				return;
			case StatemachineDSLPackage.FSM_TRIGGER__TRIGGERS:
				getTriggers().clear();
				getTriggers().addAll((Collection<? extends FSMEvent>)newValue);
				return;
			case StatemachineDSLPackage.FSM_TRIGGER__GUARDS:
				getGuards().clear();
				getGuards().addAll((Collection<? extends FSMGuard>)newValue);
				return;
			case StatemachineDSLPackage.FSM_TRIGGER__ACTIONS:
				getActions().clear();
				getActions().addAll((Collection<? extends FSMAction>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_TRIGGER__HAS_TRANSITION:
				setHasTransition(HAS_TRANSITION_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_TRIGGER__TRANSITION:
				setTransition((FSMState)null);
				return;
			case StatemachineDSLPackage.FSM_TRIGGER__TRIGGERS:
				getTriggers().clear();
				return;
			case StatemachineDSLPackage.FSM_TRIGGER__GUARDS:
				getGuards().clear();
				return;
			case StatemachineDSLPackage.FSM_TRIGGER__ACTIONS:
				getActions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_TRIGGER__HAS_TRANSITION:
				return hasTransition != HAS_TRANSITION_EDEFAULT;
			case StatemachineDSLPackage.FSM_TRIGGER__TRANSITION:
				return transition != null;
			case StatemachineDSLPackage.FSM_TRIGGER__TRIGGERS:
				return triggers != null && !triggers.isEmpty();
			case StatemachineDSLPackage.FSM_TRIGGER__GUARDS:
				return guards != null && !guards.isEmpty();
			case StatemachineDSLPackage.FSM_TRIGGER__ACTIONS:
				return actions != null && !actions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (hasTransition: ");
		result.append(hasTransition);
		result.append(')');
		return result.toString();
	}

} //FSMTriggerImpl
