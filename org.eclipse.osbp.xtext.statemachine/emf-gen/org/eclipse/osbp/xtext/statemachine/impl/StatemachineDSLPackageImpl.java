/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryDSLPackage;

import org.eclipse.osbp.xtext.reportdsl.ReportDSLPackage;

import org.eclipse.osbp.xtext.statemachine.FSMAbstractFilter;
import org.eclipse.osbp.xtext.statemachine.FSMAction;
import org.eclipse.osbp.xtext.statemachine.FSMActionButtonCaption;
import org.eclipse.osbp.xtext.statemachine.FSMActionButtonImage;
import org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition;
import org.eclipse.osbp.xtext.statemachine.FSMActionDTOClear;
import org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldClear;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldConcatenation;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldFilterToggle;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldGet;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldKeystroke;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldRemove;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSet;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSource;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceBoolean;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvaluate;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvent;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceInteger;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceNumber;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceString;
import org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceTranslate;
import org.eclipse.osbp.xtext.statemachine.FSMActionItemInvisible;
import org.eclipse.osbp.xtext.statemachine.FSMActionItemVisible;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBeeper;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBlinkRate;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralClear;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCursorType;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDestroyWindow;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDeviceBrightness;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralInterCharacterWait;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeFormat;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeRepeatWait;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeType;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeUnitWait;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralOpenDrawer;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAcknowledge;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTClose;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTResponse;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPlayer;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBitmap;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintCut;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintRegisterBitmap;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleDisplayText;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadTareWeight;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadWeight;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleTareWeight;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleWeightUnit;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleZero;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureCapture;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClear;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClose;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureIdle;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureOpen;
import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSound;
import org.eclipse.osbp.xtext.statemachine.FSMActionScheduler;
import org.eclipse.osbp.xtext.statemachine.FSMAndFilter;
import org.eclipse.osbp.xtext.statemachine.FSMBase;
import org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter;
import org.eclipse.osbp.xtext.statemachine.FSMCompareFilter;
import org.eclipse.osbp.xtext.statemachine.FSMCompareOperationEnum;
import org.eclipse.osbp.xtext.statemachine.FSMControl;
import org.eclipse.osbp.xtext.statemachine.FSMControlButton;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEvent;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventEvent;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventIdentity;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType;
import org.eclipse.osbp.xtext.statemachine.FSMControlDTO;
import org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlField;
import org.eclipse.osbp.xtext.statemachine.FSMControlFieldAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlFieldLayout;
import org.eclipse.osbp.xtext.statemachine.FSMControlFilter;
import org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral;
import org.eclipse.osbp.xtext.statemachine.FSMControlScheduler;
import org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlVisibility;
import org.eclipse.osbp.xtext.statemachine.FSMDTOType;
import org.eclipse.osbp.xtext.statemachine.FSMDotExpression;
import org.eclipse.osbp.xtext.statemachine.FSMDtoRef;
import org.eclipse.osbp.xtext.statemachine.FSMEvaluationType;
import org.eclipse.osbp.xtext.statemachine.FSMEvent;
import org.eclipse.osbp.xtext.statemachine.FSMFieldType;
import org.eclipse.osbp.xtext.statemachine.FSMFilter;
import org.eclipse.osbp.xtext.statemachine.FSMFilterProperty;
import org.eclipse.osbp.xtext.statemachine.FSMFunction;
import org.eclipse.osbp.xtext.statemachine.FSMFunctionalKeyCodes;
import org.eclipse.osbp.xtext.statemachine.FSMGuard;
import org.eclipse.osbp.xtext.statemachine.FSMInternalType;
import org.eclipse.osbp.xtext.statemachine.FSMIsNullFilter;
import org.eclipse.osbp.xtext.statemachine.FSMJunctionFilter;
import org.eclipse.osbp.xtext.statemachine.FSMKeyMapper;
import org.eclipse.osbp.xtext.statemachine.FSMLazyResolver;
import org.eclipse.osbp.xtext.statemachine.FSMLikeFilter;
import org.eclipse.osbp.xtext.statemachine.FSMLineDisplayCursorType;
import org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeFormat;
import org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeType;
import org.eclipse.osbp.xtext.statemachine.FSMLineDisplayScrollTextType;
import org.eclipse.osbp.xtext.statemachine.FSMLineDisplayTextType;
import org.eclipse.osbp.xtext.statemachine.FSMModel;
import org.eclipse.osbp.xtext.statemachine.FSMNotFilter;
import org.eclipse.osbp.xtext.statemachine.FSMOperation;
import org.eclipse.osbp.xtext.statemachine.FSMOperationParameter;
import org.eclipse.osbp.xtext.statemachine.FSMOrFilter;
import org.eclipse.osbp.xtext.statemachine.FSMPOSPrinterBarcodeType;
import org.eclipse.osbp.xtext.statemachine.FSMPackage;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevice;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceCashDrawer;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceLineDisplay;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePOSPrinter;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePT;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceScale;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceSignature;
import org.eclipse.osbp.xtext.statemachine.FSMRef;
import org.eclipse.osbp.xtext.statemachine.FSMSignatureRetrieve;
import org.eclipse.osbp.xtext.statemachine.FSMState;
import org.eclipse.osbp.xtext.statemachine.FSMStorage;
import org.eclipse.osbp.xtext.statemachine.FSMStorageRetrieve;
import org.eclipse.osbp.xtext.statemachine.FSMStringFilter;
import org.eclipse.osbp.xtext.statemachine.FSMTrigger;
import org.eclipse.osbp.xtext.statemachine.FSMUserMessageType;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLFactory;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

import org.eclipse.xtext.xtype.XtypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StatemachineDSLPackageImpl extends EPackageImpl implements StatemachineDSLPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmLazyResolverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmBaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlButtonEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlDTOEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlSchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlPeripheralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlButtonAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlButtonAttributeEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlButtonAttributeEventKeyboardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlButtonAttributeEventIdentityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlButtonAttributeEventEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlVisibilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlFieldAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlFieldLayoutEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlDTOAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmPeripheralDeviceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmPeripheralDeviceDisplayEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmPeripheralDeviceLineDisplayEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmPeripheralDevicePOSPrinterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmPeripheralDeviceCashDrawerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmPeripheralDevicePTEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmPeripheralDeviceSignatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmPeripheralDeviceScaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlSchedulerAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmKeyMapperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmTriggerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralBlinkRateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralClearEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralCreateWindowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralCursorTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralDestroyWindowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralDeviceBrightnessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralDisplayTextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralLineDisplayTextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralLineDisplayTextAtEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralInterCharacterWaitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralMarqueeFormatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralMarqueeRepeatWaitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralMarqueeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralMarqueeUnitWaitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralScrollEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralOpenDrawerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPrintBarcodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPrintBitmapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPrintCutEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPrintRegisterBitmapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPrintNormalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPTOpenEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPTCloseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPTReversalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPTAcknowledgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPTRegistrationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPTAuthorizationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralBeeperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPlayerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralSoundEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPTResponseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralPrintReportEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralSignatureOpenEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralSignatureCloseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralSignatureClearEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralSignatureCaptureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralSignatureIdleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralSignatureLabelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmSignatureRetrieveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralScaleReadWeightEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralScaleReadTareWeightEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralScaleTareWeightEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralScaleZeroEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralScaleDisplayTextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionPeripheralScaleWeightUnitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldSourceStringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldSourceNumberEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldSourceIntegerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldSourceBooleanEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldSourceEvaluateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldSourceTranslateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmRefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmDotExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmDtoRefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldSourceDtoAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldSourceEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionConditionalTransitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmOperationParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmGuardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmFunctionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmStorageRetrieveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmStorageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldConcatenationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldKeystrokeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldClearEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldGetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldFilterToggleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionFieldRemoveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionItemVisibleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionItemInvisibleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionButtonCaptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionButtonImageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionDTOFindEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionDTOClearEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmActionSchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmdtoTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmFieldTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmAbstractFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmFilterPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmJunctionFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmAndFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmOrFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmBetweenFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmCompareFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmIsNullFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmLikeFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmNotFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmStringFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmControlFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmInternalTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmControlButtonEventTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmCompareOperationEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmEvaluationTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmUserMessageTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmLineDisplayCursorTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmLineDisplayMarqueeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmLineDisplayMarqueeFormatEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmLineDisplayTextTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmLineDisplayScrollTextTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmposPrinterBarcodeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum fsmFunctionalKeyCodesEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType internalEObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private StatemachineDSLPackageImpl() {
		super(eNS_URI, StatemachineDSLFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link StatemachineDSLPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static StatemachineDSLPackage init() {
		if (isInited) return (StatemachineDSLPackage)EPackage.Registry.INSTANCE.getEPackage(StatemachineDSLPackage.eNS_URI);

		// Obtain or create and register package
		StatemachineDSLPackageImpl theStatemachineDSLPackage = (StatemachineDSLPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof StatemachineDSLPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new StatemachineDSLPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		OSBPDtoPackage.eINSTANCE.eClass();
		ReportDSLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theStatemachineDSLPackage.createPackageContents();

		// Initialize created meta-data
		theStatemachineDSLPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theStatemachineDSLPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(StatemachineDSLPackage.eNS_URI, theStatemachineDSLPackage);
		return theStatemachineDSLPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMModel() {
		return fsmModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMModel_ImportSection() {
		return (EReference)fsmModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMModel_Packages() {
		return (EReference)fsmModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMLazyResolver() {
		return fsmLazyResolverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFSMLazyResolver__EResolveProxy__InternalEObject() {
		return fsmLazyResolverEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMBase() {
		return fsmBaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMBase_Name() {
		return (EAttribute)fsmBaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMPackage() {
		return fsmPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMPackage_Statemachines() {
		return (EReference)fsmPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSM() {
		return fsmEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSM_Description() {
		return (EAttribute)fsmEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSM_DescriptionValue() {
		return (EAttribute)fsmEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSM_InitialEvent() {
		return (EReference)fsmEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSM_InitialState() {
		return (EReference)fsmEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSM_Events() {
		return (EReference)fsmEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSM_Controls() {
		return (EReference)fsmEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSM_States() {
		return (EReference)fsmEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControl() {
		return fsmControlEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlButton() {
		return fsmControlButtonEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlButton_EventType() {
		return (EAttribute)fsmControlButtonEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlButton_Buttons() {
		return (EReference)fsmControlButtonEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlButton_HasRange() {
		return (EAttribute)fsmControlButtonEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlButton_Start() {
		return (EAttribute)fsmControlButtonEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlButton_End() {
		return (EAttribute)fsmControlButtonEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlButton_RangedName() {
		return (EAttribute)fsmControlButtonEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlField() {
		return fsmControlFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlField_Fields() {
		return (EReference)fsmControlFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlField_Layouts() {
		return (EReference)fsmControlFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlDTO() {
		return fsmControlDTOEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlDTO_Dtos() {
		return (EReference)fsmControlDTOEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlDTO_Filters() {
		return (EReference)fsmControlDTOEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlScheduler() {
		return fsmControlSchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlScheduler_Schedulers() {
		return (EReference)fsmControlSchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlPeripheral() {
		return fsmControlPeripheralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlPeripheral_LineDisplays() {
		return (EReference)fsmControlPeripheralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlPeripheral_Displays() {
		return (EReference)fsmControlPeripheralEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlPeripheral_PosPrinters() {
		return (EReference)fsmControlPeripheralEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlPeripheral_CashDrawers() {
		return (EReference)fsmControlPeripheralEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlPeripheral_PaymentTerminals() {
		return (EReference)fsmControlPeripheralEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlPeripheral_SignaturePads() {
		return (EReference)fsmControlPeripheralEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlPeripheral_Scales() {
		return (EReference)fsmControlPeripheralEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlButtonAttribute() {
		return fsmControlButtonAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlButtonAttribute_HasImage() {
		return (EAttribute)fsmControlButtonAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlButtonAttribute_Image() {
		return (EAttribute)fsmControlButtonAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlButtonAttribute_Event() {
		return (EReference)fsmControlButtonAttributeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlButtonAttributeEvent() {
		return fsmControlButtonAttributeEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlButtonAttributeEventKeyboard() {
		return fsmControlButtonAttributeEventKeyboardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlButtonAttributeEventKeyboard_Keystroke() {
		return (EAttribute)fsmControlButtonAttributeEventKeyboardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlButtonAttributeEventIdentity() {
		return fsmControlButtonAttributeEventIdentityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlButtonAttributeEventIdentity_Identity() {
		return (EAttribute)fsmControlButtonAttributeEventIdentityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlButtonAttributeEventEvent() {
		return fsmControlButtonAttributeEventEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlButtonAttributeEventEvent_Event() {
		return (EReference)fsmControlButtonAttributeEventEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlVisibility() {
		return fsmControlVisibilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlFieldAttribute() {
		return fsmControlFieldAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlFieldAttribute_AttributeType() {
		return (EReference)fsmControlFieldAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlFieldLayout() {
		return fsmControlFieldLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlDTOAttribute() {
		return fsmControlDTOAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlDTOAttribute_AttributeType() {
		return (EReference)fsmControlDTOAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlDTOAttribute_HasEvent() {
		return (EAttribute)fsmControlDTOAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlDTOAttribute_Event() {
		return (EReference)fsmControlDTOAttributeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlDTOAttribute_IsAttached() {
		return (EAttribute)fsmControlDTOAttributeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlDTOAttribute_Display() {
		return (EReference)fsmControlDTOAttributeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMPeripheralDevice() {
		return fsmPeripheralDeviceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMPeripheralDeviceDisplay() {
		return fsmPeripheralDeviceDisplayEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMPeripheralDeviceDisplay_Output() {
		return (EReference)fsmPeripheralDeviceDisplayEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMPeripheralDeviceLineDisplay() {
		return fsmPeripheralDeviceLineDisplayEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMPeripheralDevicePOSPrinter() {
		return fsmPeripheralDevicePOSPrinterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMPeripheralDeviceCashDrawer() {
		return fsmPeripheralDeviceCashDrawerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMPeripheralDevicePT() {
		return fsmPeripheralDevicePTEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMPeripheralDeviceSignature() {
		return fsmPeripheralDeviceSignatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMPeripheralDeviceScale() {
		return fsmPeripheralDeviceScaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlSchedulerAttribute() {
		return fsmControlSchedulerAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMControlSchedulerAttribute_Delay() {
		return (EAttribute)fsmControlSchedulerAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlSchedulerAttribute_Event() {
		return (EReference)fsmControlSchedulerAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMEvent() {
		return fsmEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMState() {
		return fsmStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMState_Triggers() {
		return (EReference)fsmStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMState_Conditions() {
		return (EReference)fsmStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMState_Identity() {
		return (EReference)fsmStateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMState_Keystroke() {
		return (EReference)fsmStateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMState_HasKeyOperation() {
		return (EAttribute)fsmStateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMState_KeyOperation() {
		return (EReference)fsmStateEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMState_KeyMapper() {
		return (EReference)fsmStateEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMKeyMapper() {
		return fsmKeyMapperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMKeyMapper_KeyCode() {
		return (EAttribute)fsmKeyMapperEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMKeyMapper_KeyEvent() {
		return (EReference)fsmKeyMapperEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMTrigger() {
		return fsmTriggerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMTrigger_HasTransition() {
		return (EAttribute)fsmTriggerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMTrigger_Transition() {
		return (EReference)fsmTriggerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMTrigger_Triggers() {
		return (EReference)fsmTriggerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMTrigger_Guards() {
		return (EReference)fsmTriggerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMTrigger_Actions() {
		return (EReference)fsmTriggerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMAction() {
		return fsmActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralBlinkRate() {
		return fsmActionPeripheralBlinkRateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralBlinkRate_Device() {
		return (EReference)fsmActionPeripheralBlinkRateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralBlinkRate_BlinkRate() {
		return (EAttribute)fsmActionPeripheralBlinkRateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralClear() {
		return fsmActionPeripheralClearEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralClear_Device() {
		return (EReference)fsmActionPeripheralClearEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralCreateWindow() {
		return fsmActionPeripheralCreateWindowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralCreateWindow_Device() {
		return (EReference)fsmActionPeripheralCreateWindowEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralCreateWindow_ViewportRow() {
		return (EAttribute)fsmActionPeripheralCreateWindowEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralCreateWindow_ViewportColumn() {
		return (EAttribute)fsmActionPeripheralCreateWindowEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralCreateWindow_ViewportHeight() {
		return (EAttribute)fsmActionPeripheralCreateWindowEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralCreateWindow_ViewportWidth() {
		return (EAttribute)fsmActionPeripheralCreateWindowEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralCreateWindow_WindowHeight() {
		return (EAttribute)fsmActionPeripheralCreateWindowEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralCreateWindow_WindowWidth() {
		return (EAttribute)fsmActionPeripheralCreateWindowEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralCursorType() {
		return fsmActionPeripheralCursorTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralCursorType_Device() {
		return (EReference)fsmActionPeripheralCursorTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralCursorType_CursorType() {
		return (EAttribute)fsmActionPeripheralCursorTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralDestroyWindow() {
		return fsmActionPeripheralDestroyWindowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralDestroyWindow_Device() {
		return (EReference)fsmActionPeripheralDestroyWindowEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralDeviceBrightness() {
		return fsmActionPeripheralDeviceBrightnessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralDeviceBrightness_Device() {
		return (EReference)fsmActionPeripheralDeviceBrightnessEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralDeviceBrightness_Brightness() {
		return (EAttribute)fsmActionPeripheralDeviceBrightnessEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralDisplayText() {
		return fsmActionPeripheralDisplayTextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralDisplayText_Device() {
		return (EReference)fsmActionPeripheralDisplayTextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralDisplayText_Attribute() {
		return (EReference)fsmActionPeripheralDisplayTextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralDisplayText_Text() {
		return (EReference)fsmActionPeripheralDisplayTextEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralLineDisplayText() {
		return fsmActionPeripheralLineDisplayTextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralLineDisplayText_Device() {
		return (EReference)fsmActionPeripheralLineDisplayTextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralLineDisplayText_Text() {
		return (EReference)fsmActionPeripheralLineDisplayTextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralLineDisplayText_HasType() {
		return (EAttribute)fsmActionPeripheralLineDisplayTextEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralLineDisplayText_TextType() {
		return (EAttribute)fsmActionPeripheralLineDisplayTextEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralLineDisplayTextAt() {
		return fsmActionPeripheralLineDisplayTextAtEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralLineDisplayTextAt_Device() {
		return (EReference)fsmActionPeripheralLineDisplayTextAtEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralLineDisplayTextAt_Row() {
		return (EAttribute)fsmActionPeripheralLineDisplayTextAtEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralLineDisplayTextAt_Column() {
		return (EAttribute)fsmActionPeripheralLineDisplayTextAtEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralLineDisplayTextAt_Text() {
		return (EReference)fsmActionPeripheralLineDisplayTextAtEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralLineDisplayTextAt_HasType() {
		return (EAttribute)fsmActionPeripheralLineDisplayTextAtEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralLineDisplayTextAt_TextType() {
		return (EAttribute)fsmActionPeripheralLineDisplayTextAtEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralInterCharacterWait() {
		return fsmActionPeripheralInterCharacterWaitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralInterCharacterWait_Device() {
		return (EReference)fsmActionPeripheralInterCharacterWaitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralInterCharacterWait_Wait() {
		return (EAttribute)fsmActionPeripheralInterCharacterWaitEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralMarqueeFormat() {
		return fsmActionPeripheralMarqueeFormatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralMarqueeFormat_Device() {
		return (EReference)fsmActionPeripheralMarqueeFormatEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralMarqueeFormat_Format() {
		return (EAttribute)fsmActionPeripheralMarqueeFormatEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralMarqueeRepeatWait() {
		return fsmActionPeripheralMarqueeRepeatWaitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralMarqueeRepeatWait_Device() {
		return (EReference)fsmActionPeripheralMarqueeRepeatWaitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralMarqueeRepeatWait_Wait() {
		return (EAttribute)fsmActionPeripheralMarqueeRepeatWaitEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralMarqueeType() {
		return fsmActionPeripheralMarqueeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralMarqueeType_Device() {
		return (EReference)fsmActionPeripheralMarqueeTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralMarqueeType_MarqueeType() {
		return (EAttribute)fsmActionPeripheralMarqueeTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralMarqueeUnitWait() {
		return fsmActionPeripheralMarqueeUnitWaitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralMarqueeUnitWait_Device() {
		return (EReference)fsmActionPeripheralMarqueeUnitWaitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralMarqueeUnitWait_Wait() {
		return (EAttribute)fsmActionPeripheralMarqueeUnitWaitEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralScroll() {
		return fsmActionPeripheralScrollEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralScroll_Device() {
		return (EReference)fsmActionPeripheralScrollEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralScroll_Direction() {
		return (EAttribute)fsmActionPeripheralScrollEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralScroll_Units() {
		return (EAttribute)fsmActionPeripheralScrollEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralOpenDrawer() {
		return fsmActionPeripheralOpenDrawerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralOpenDrawer_Device() {
		return (EReference)fsmActionPeripheralOpenDrawerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPrintBarcode() {
		return fsmActionPeripheralPrintBarcodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPrintBarcode_Device() {
		return (EReference)fsmActionPeripheralPrintBarcodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralPrintBarcode_Data() {
		return (EAttribute)fsmActionPeripheralPrintBarcodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralPrintBarcode_BarcodeType() {
		return (EAttribute)fsmActionPeripheralPrintBarcodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPrintBitmap() {
		return fsmActionPeripheralPrintBitmapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPrintBitmap_Device() {
		return (EReference)fsmActionPeripheralPrintBitmapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralPrintBitmap_BitmapId() {
		return (EAttribute)fsmActionPeripheralPrintBitmapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPrintCut() {
		return fsmActionPeripheralPrintCutEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPrintCut_Device() {
		return (EReference)fsmActionPeripheralPrintCutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPrintCut_Text() {
		return (EReference)fsmActionPeripheralPrintCutEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPrintRegisterBitmap() {
		return fsmActionPeripheralPrintRegisterBitmapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralPrintRegisterBitmap_BitmapId() {
		return (EAttribute)fsmActionPeripheralPrintRegisterBitmapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralPrintRegisterBitmap_Name() {
		return (EAttribute)fsmActionPeripheralPrintRegisterBitmapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPrintNormal() {
		return fsmActionPeripheralPrintNormalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPrintNormal_Device() {
		return (EReference)fsmActionPeripheralPrintNormalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPrintNormal_Text() {
		return (EReference)fsmActionPeripheralPrintNormalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralPrintNormal_BarcodeType() {
		return (EAttribute)fsmActionPeripheralPrintNormalEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPTOpen() {
		return fsmActionPeripheralPTOpenEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTOpen_Device() {
		return (EReference)fsmActionPeripheralPTOpenEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTOpen_Host() {
		return (EReference)fsmActionPeripheralPTOpenEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTOpen_Port() {
		return (EReference)fsmActionPeripheralPTOpenEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPTClose() {
		return fsmActionPeripheralPTCloseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTClose_Device() {
		return (EReference)fsmActionPeripheralPTCloseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPTReversal() {
		return fsmActionPeripheralPTReversalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTReversal_Device() {
		return (EReference)fsmActionPeripheralPTReversalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTReversal_Password() {
		return (EReference)fsmActionPeripheralPTReversalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTReversal_Receipt() {
		return (EReference)fsmActionPeripheralPTReversalEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPTAcknowledge() {
		return fsmActionPeripheralPTAcknowledgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTAcknowledge_Device() {
		return (EReference)fsmActionPeripheralPTAcknowledgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPTRegistration() {
		return fsmActionPeripheralPTRegistrationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTRegistration_Device() {
		return (EReference)fsmActionPeripheralPTRegistrationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTRegistration_Password() {
		return (EReference)fsmActionPeripheralPTRegistrationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralPTRegistration_Configuration() {
		return (EAttribute)fsmActionPeripheralPTRegistrationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPTAuthorization() {
		return fsmActionPeripheralPTAuthorizationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTAuthorization_Device() {
		return (EReference)fsmActionPeripheralPTAuthorizationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTAuthorization_Amount() {
		return (EReference)fsmActionPeripheralPTAuthorizationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralBeeper() {
		return fsmActionPeripheralBeeperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralBeeper_Duration() {
		return (EAttribute)fsmActionPeripheralBeeperEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralBeeper_Frequency() {
		return (EAttribute)fsmActionPeripheralBeeperEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPlayer() {
		return fsmActionPeripheralPlayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralPlayer_Tune() {
		return (EAttribute)fsmActionPeripheralPlayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralSound() {
		return fsmActionPeripheralSoundEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralSound_Name() {
		return (EAttribute)fsmActionPeripheralSoundEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPTResponse() {
		return fsmActionPeripheralPTResponseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPTResponse_Device() {
		return (EReference)fsmActionPeripheralPTResponseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralPrintReport() {
		return fsmActionPeripheralPrintReportEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPrintReport_Report() {
		return (EReference)fsmActionPeripheralPrintReportEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPrintReport_Key() {
		return (EReference)fsmActionPeripheralPrintReportEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralPrintReport_HasFilter() {
		return (EAttribute)fsmActionPeripheralPrintReportEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralPrintReport_HasPrintService() {
		return (EAttribute)fsmActionPeripheralPrintReportEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralPrintReport_PrintService() {
		return (EReference)fsmActionPeripheralPrintReportEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralSignatureOpen() {
		return fsmActionPeripheralSignatureOpenEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralSignatureOpen_Device() {
		return (EReference)fsmActionPeripheralSignatureOpenEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralSignatureClose() {
		return fsmActionPeripheralSignatureCloseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralSignatureClose_Device() {
		return (EReference)fsmActionPeripheralSignatureCloseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralSignatureClear() {
		return fsmActionPeripheralSignatureClearEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralSignatureClear_Device() {
		return (EReference)fsmActionPeripheralSignatureClearEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralSignatureCapture() {
		return fsmActionPeripheralSignatureCaptureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralSignatureCapture_Device() {
		return (EReference)fsmActionPeripheralSignatureCaptureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralSignatureIdle() {
		return fsmActionPeripheralSignatureIdleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralSignatureIdle_Device() {
		return (EReference)fsmActionPeripheralSignatureIdleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralSignatureLabel() {
		return fsmActionPeripheralSignatureLabelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralSignatureLabel_OkLabel() {
		return (EAttribute)fsmActionPeripheralSignatureLabelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralSignatureLabel_ClearLabel() {
		return (EAttribute)fsmActionPeripheralSignatureLabelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionPeripheralSignatureLabel_CancelLabel() {
		return (EAttribute)fsmActionPeripheralSignatureLabelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralSignatureLabel_Device() {
		return (EReference)fsmActionPeripheralSignatureLabelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMSignatureRetrieve() {
		return fsmSignatureRetrieveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMSignatureRetrieve_Device() {
		return (EReference)fsmSignatureRetrieveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralScaleReadWeight() {
		return fsmActionPeripheralScaleReadWeightEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralScaleReadWeight_Device() {
		return (EReference)fsmActionPeripheralScaleReadWeightEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralScaleReadTareWeight() {
		return fsmActionPeripheralScaleReadTareWeightEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralScaleReadTareWeight_Device() {
		return (EReference)fsmActionPeripheralScaleReadTareWeightEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralScaleTareWeight() {
		return fsmActionPeripheralScaleTareWeightEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralScaleTareWeight_Device() {
		return (EReference)fsmActionPeripheralScaleTareWeightEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralScaleTareWeight_Value() {
		return (EReference)fsmActionPeripheralScaleTareWeightEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralScaleZero() {
		return fsmActionPeripheralScaleZeroEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralScaleZero_Device() {
		return (EReference)fsmActionPeripheralScaleZeroEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralScaleDisplayText() {
		return fsmActionPeripheralScaleDisplayTextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralScaleDisplayText_Device() {
		return (EReference)fsmActionPeripheralScaleDisplayTextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralScaleDisplayText_Text() {
		return (EReference)fsmActionPeripheralScaleDisplayTextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionPeripheralScaleWeightUnit() {
		return fsmActionPeripheralScaleWeightUnitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionPeripheralScaleWeightUnit_Device() {
		return (EReference)fsmActionPeripheralScaleWeightUnitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldSource() {
		return fsmActionFieldSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldSourceString() {
		return fsmActionFieldSourceStringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionFieldSourceString_Text() {
		return (EAttribute)fsmActionFieldSourceStringEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldSourceNumber() {
		return fsmActionFieldSourceNumberEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionFieldSourceNumber_Value() {
		return (EAttribute)fsmActionFieldSourceNumberEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldSourceInteger() {
		return fsmActionFieldSourceIntegerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionFieldSourceInteger_Value() {
		return (EAttribute)fsmActionFieldSourceIntegerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldSourceBoolean() {
		return fsmActionFieldSourceBooleanEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionFieldSourceBoolean_Value() {
		return (EAttribute)fsmActionFieldSourceBooleanEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldSourceEvaluate() {
		return fsmActionFieldSourceEvaluateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionFieldSourceEvaluate_Evaluationtype() {
		return (EAttribute)fsmActionFieldSourceEvaluateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldSourceTranslate() {
		return fsmActionFieldSourceTranslateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionFieldSourceTranslate_Text() {
		return (EAttribute)fsmActionFieldSourceTranslateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMRef() {
		return fsmRefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMDotExpression() {
		return fsmDotExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMDotExpression_Ref() {
		return (EReference)fsmDotExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMDotExpression_Tail() {
		return (EReference)fsmDotExpressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMDtoRef() {
		return fsmDtoRefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMDtoRef_Dto() {
		return (EReference)fsmDtoRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldSourceDtoAttribute() {
		return fsmActionFieldSourceDtoAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionFieldSourceDtoAttribute_Dto() {
		return (EReference)fsmActionFieldSourceDtoAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionFieldSourceDtoAttribute_Attribute() {
		return (EReference)fsmActionFieldSourceDtoAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldSourceEvent() {
		return fsmActionFieldSourceEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionConditionalTransition() {
		return fsmActionConditionalTransitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionConditionalTransition_Transition() {
		return (EReference)fsmActionConditionalTransitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionConditionalTransition_Guard() {
		return (EReference)fsmActionConditionalTransitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionConditionalTransition_Actions() {
		return (EReference)fsmActionConditionalTransitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMOperationParameter() {
		return fsmOperationParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMOperationParameter_Source() {
		return (EReference)fsmOperationParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMOperation() {
		return fsmOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMOperation_Group() {
		return (EReference)fsmOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMOperation_Operation() {
		return (EReference)fsmOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMOperation_First() {
		return (EReference)fsmOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMOperation_More() {
		return (EReference)fsmOperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMGuard() {
		return fsmGuardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMGuard_Group() {
		return (EReference)fsmGuardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMGuard_Guard() {
		return (EReference)fsmGuardEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMGuard_HasOnFail() {
		return (EAttribute)fsmGuardEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMGuard_OnFailDescription() {
		return (EAttribute)fsmGuardEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMGuard_OnFailCaption() {
		return (EAttribute)fsmGuardEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMGuard_OnFailType() {
		return (EAttribute)fsmGuardEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMFunction() {
		return fsmFunctionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMFunction_Group() {
		return (EReference)fsmFunctionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMFunction_Function() {
		return (EReference)fsmFunctionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMFunction_First() {
		return (EReference)fsmFunctionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMFunction_More() {
		return (EReference)fsmFunctionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMStorageRetrieve() {
		return fsmStorageRetrieveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMStorageRetrieve_Key() {
		return (EAttribute)fsmStorageRetrieveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMStorageRetrieve_Attribute() {
		return (EAttribute)fsmStorageRetrieveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMStorage() {
		return fsmStorageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMStorage_Key() {
		return (EAttribute)fsmStorageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMStorage_Attribute() {
		return (EAttribute)fsmStorageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMStorage_Content() {
		return (EReference)fsmStorageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldConcatenation() {
		return fsmActionFieldConcatenationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionFieldConcatenation_First() {
		return (EReference)fsmActionFieldConcatenationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionFieldConcatenation_More() {
		return (EReference)fsmActionFieldConcatenationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldSet() {
		return fsmActionFieldSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionFieldSet_Attribute() {
		return (EReference)fsmActionFieldSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionFieldSet_Source() {
		return (EReference)fsmActionFieldSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldKeystroke() {
		return fsmActionFieldKeystrokeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionFieldKeystroke_Attribute() {
		return (EReference)fsmActionFieldKeystrokeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionFieldKeystroke_Keystroke() {
		return (EAttribute)fsmActionFieldKeystrokeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldClear() {
		return fsmActionFieldClearEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionFieldClear_Attribute() {
		return (EReference)fsmActionFieldClearEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldGet() {
		return fsmActionFieldGetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionFieldGet_Attribute() {
		return (EReference)fsmActionFieldGetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldFilterToggle() {
		return fsmActionFieldFilterToggleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionFieldFilterToggle_Filter() {
		return (EReference)fsmActionFieldFilterToggleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionFieldRemove() {
		return fsmActionFieldRemoveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionFieldRemove_Attribute() {
		return (EReference)fsmActionFieldRemoveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionItemVisible() {
		return fsmActionItemVisibleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionItemVisible_Attribute() {
		return (EReference)fsmActionItemVisibleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionItemInvisible() {
		return fsmActionItemInvisibleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionItemInvisible_Attribute() {
		return (EReference)fsmActionItemInvisibleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionButtonCaption() {
		return fsmActionButtonCaptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionButtonCaption_Attribute() {
		return (EReference)fsmActionButtonCaptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionButtonCaption_Caption() {
		return (EReference)fsmActionButtonCaptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionButtonImage() {
		return fsmActionButtonImageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionButtonImage_Attribute() {
		return (EReference)fsmActionButtonImageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMActionButtonImage_Image() {
		return (EAttribute)fsmActionButtonImageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionDTOFind() {
		return fsmActionDTOFindEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionDTOFind_Dto() {
		return (EReference)fsmActionDTOFindEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionDTOFind_Attribute() {
		return (EReference)fsmActionDTOFindEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionDTOFind_Search() {
		return (EReference)fsmActionDTOFindEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionDTOClear() {
		return fsmActionDTOClearEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionDTOClear_Dto() {
		return (EReference)fsmActionDTOClearEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMActionScheduler() {
		return fsmActionSchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMActionScheduler_Scheduler() {
		return (EReference)fsmActionSchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMDTOType() {
		return fsmdtoTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMDTOType_AttributeType() {
		return (EReference)fsmdtoTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMFieldType() {
		return fsmFieldTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMFieldType_AttributeType() {
		return (EAttribute)fsmFieldTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMAbstractFilter() {
		return fsmAbstractFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMFilterProperty() {
		return fsmFilterPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMFilterProperty_Path() {
		return (EReference)fsmFilterPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMFilter() {
		return fsmFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMFilter_Source() {
		return (EReference)fsmFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMJunctionFilter() {
		return fsmJunctionFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMJunctionFilter_First() {
		return (EReference)fsmJunctionFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMJunctionFilter_More() {
		return (EReference)fsmJunctionFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMAndFilter() {
		return fsmAndFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMOrFilter() {
		return fsmOrFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMBetweenFilter() {
		return fsmBetweenFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMBetweenFilter_PropertyId() {
		return (EReference)fsmBetweenFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMBetweenFilter_Start() {
		return (EReference)fsmBetweenFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMBetweenFilter_End() {
		return (EReference)fsmBetweenFilterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMCompareFilter() {
		return fsmCompareFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMCompareFilter_PropertyId() {
		return (EReference)fsmCompareFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMCompareFilter_Operand() {
		return (EReference)fsmCompareFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMCompareFilter_Operation() {
		return (EAttribute)fsmCompareFilterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMIsNullFilter() {
		return fsmIsNullFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMIsNullFilter_PropertyId() {
		return (EReference)fsmIsNullFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMLikeFilter() {
		return fsmLikeFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMLikeFilter_PropertyId() {
		return (EReference)fsmLikeFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMLikeFilter_Value() {
		return (EReference)fsmLikeFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMLikeFilter_IgnoreCase() {
		return (EAttribute)fsmLikeFilterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMNotFilter() {
		return fsmNotFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMNotFilter_Filter() {
		return (EReference)fsmNotFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMStringFilter() {
		return fsmStringFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMStringFilter_PropertyId() {
		return (EReference)fsmStringFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMStringFilter_FilterString() {
		return (EAttribute)fsmStringFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMStringFilter_OnlyMatchPrefix() {
		return (EAttribute)fsmStringFilterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFSMStringFilter_IgnoreCase() {
		return (EAttribute)fsmStringFilterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSMControlFilter() {
		return fsmControlFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSMControlFilter_Filter() {
		return (EReference)fsmControlFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMInternalType() {
		return fsmInternalTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMControlButtonEventType() {
		return fsmControlButtonEventTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMCompareOperationEnum() {
		return fsmCompareOperationEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMEvaluationType() {
		return fsmEvaluationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMUserMessageType() {
		return fsmUserMessageTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMLineDisplayCursorType() {
		return fsmLineDisplayCursorTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMLineDisplayMarqueeType() {
		return fsmLineDisplayMarqueeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMLineDisplayMarqueeFormat() {
		return fsmLineDisplayMarqueeFormatEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMLineDisplayTextType() {
		return fsmLineDisplayTextTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMLineDisplayScrollTextType() {
		return fsmLineDisplayScrollTextTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMPOSPrinterBarcodeType() {
		return fsmposPrinterBarcodeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFSMFunctionalKeyCodes() {
		return fsmFunctionalKeyCodesEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInternalEObject() {
		return internalEObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachineDSLFactory getStatemachineDSLFactory() {
		return (StatemachineDSLFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		fsmModelEClass = createEClass(FSM_MODEL);
		createEReference(fsmModelEClass, FSM_MODEL__IMPORT_SECTION);
		createEReference(fsmModelEClass, FSM_MODEL__PACKAGES);

		fsmLazyResolverEClass = createEClass(FSM_LAZY_RESOLVER);
		createEOperation(fsmLazyResolverEClass, FSM_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT);

		fsmBaseEClass = createEClass(FSM_BASE);
		createEAttribute(fsmBaseEClass, FSM_BASE__NAME);

		fsmPackageEClass = createEClass(FSM_PACKAGE);
		createEReference(fsmPackageEClass, FSM_PACKAGE__STATEMACHINES);

		fsmEClass = createEClass(FSM);
		createEAttribute(fsmEClass, FSM__DESCRIPTION);
		createEAttribute(fsmEClass, FSM__DESCRIPTION_VALUE);
		createEReference(fsmEClass, FSM__INITIAL_EVENT);
		createEReference(fsmEClass, FSM__INITIAL_STATE);
		createEReference(fsmEClass, FSM__EVENTS);
		createEReference(fsmEClass, FSM__CONTROLS);
		createEReference(fsmEClass, FSM__STATES);

		fsmControlEClass = createEClass(FSM_CONTROL);

		fsmControlButtonEClass = createEClass(FSM_CONTROL_BUTTON);
		createEAttribute(fsmControlButtonEClass, FSM_CONTROL_BUTTON__EVENT_TYPE);
		createEReference(fsmControlButtonEClass, FSM_CONTROL_BUTTON__BUTTONS);
		createEAttribute(fsmControlButtonEClass, FSM_CONTROL_BUTTON__HAS_RANGE);
		createEAttribute(fsmControlButtonEClass, FSM_CONTROL_BUTTON__START);
		createEAttribute(fsmControlButtonEClass, FSM_CONTROL_BUTTON__END);
		createEAttribute(fsmControlButtonEClass, FSM_CONTROL_BUTTON__RANGED_NAME);

		fsmControlFieldEClass = createEClass(FSM_CONTROL_FIELD);
		createEReference(fsmControlFieldEClass, FSM_CONTROL_FIELD__FIELDS);
		createEReference(fsmControlFieldEClass, FSM_CONTROL_FIELD__LAYOUTS);

		fsmControlDTOEClass = createEClass(FSM_CONTROL_DTO);
		createEReference(fsmControlDTOEClass, FSM_CONTROL_DTO__DTOS);
		createEReference(fsmControlDTOEClass, FSM_CONTROL_DTO__FILTERS);

		fsmControlSchedulerEClass = createEClass(FSM_CONTROL_SCHEDULER);
		createEReference(fsmControlSchedulerEClass, FSM_CONTROL_SCHEDULER__SCHEDULERS);

		fsmControlPeripheralEClass = createEClass(FSM_CONTROL_PERIPHERAL);
		createEReference(fsmControlPeripheralEClass, FSM_CONTROL_PERIPHERAL__LINE_DISPLAYS);
		createEReference(fsmControlPeripheralEClass, FSM_CONTROL_PERIPHERAL__DISPLAYS);
		createEReference(fsmControlPeripheralEClass, FSM_CONTROL_PERIPHERAL__POS_PRINTERS);
		createEReference(fsmControlPeripheralEClass, FSM_CONTROL_PERIPHERAL__CASH_DRAWERS);
		createEReference(fsmControlPeripheralEClass, FSM_CONTROL_PERIPHERAL__PAYMENT_TERMINALS);
		createEReference(fsmControlPeripheralEClass, FSM_CONTROL_PERIPHERAL__SIGNATURE_PADS);
		createEReference(fsmControlPeripheralEClass, FSM_CONTROL_PERIPHERAL__SCALES);

		fsmControlButtonAttributeEClass = createEClass(FSM_CONTROL_BUTTON_ATTRIBUTE);
		createEAttribute(fsmControlButtonAttributeEClass, FSM_CONTROL_BUTTON_ATTRIBUTE__HAS_IMAGE);
		createEAttribute(fsmControlButtonAttributeEClass, FSM_CONTROL_BUTTON_ATTRIBUTE__IMAGE);
		createEReference(fsmControlButtonAttributeEClass, FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT);

		fsmControlButtonAttributeEventEClass = createEClass(FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT);

		fsmControlButtonAttributeEventKeyboardEClass = createEClass(FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD);
		createEAttribute(fsmControlButtonAttributeEventKeyboardEClass, FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD__KEYSTROKE);

		fsmControlButtonAttributeEventIdentityEClass = createEClass(FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_IDENTITY);
		createEAttribute(fsmControlButtonAttributeEventIdentityEClass, FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_IDENTITY__IDENTITY);

		fsmControlButtonAttributeEventEventEClass = createEClass(FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_EVENT);
		createEReference(fsmControlButtonAttributeEventEventEClass, FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_EVENT__EVENT);

		fsmControlVisibilityEClass = createEClass(FSM_CONTROL_VISIBILITY);

		fsmControlFieldAttributeEClass = createEClass(FSM_CONTROL_FIELD_ATTRIBUTE);
		createEReference(fsmControlFieldAttributeEClass, FSM_CONTROL_FIELD_ATTRIBUTE__ATTRIBUTE_TYPE);

		fsmControlFieldLayoutEClass = createEClass(FSM_CONTROL_FIELD_LAYOUT);

		fsmControlDTOAttributeEClass = createEClass(FSM_CONTROL_DTO_ATTRIBUTE);
		createEReference(fsmControlDTOAttributeEClass, FSM_CONTROL_DTO_ATTRIBUTE__ATTRIBUTE_TYPE);
		createEAttribute(fsmControlDTOAttributeEClass, FSM_CONTROL_DTO_ATTRIBUTE__HAS_EVENT);
		createEReference(fsmControlDTOAttributeEClass, FSM_CONTROL_DTO_ATTRIBUTE__EVENT);
		createEAttribute(fsmControlDTOAttributeEClass, FSM_CONTROL_DTO_ATTRIBUTE__IS_ATTACHED);
		createEReference(fsmControlDTOAttributeEClass, FSM_CONTROL_DTO_ATTRIBUTE__DISPLAY);

		fsmPeripheralDeviceEClass = createEClass(FSM_PERIPHERAL_DEVICE);

		fsmPeripheralDeviceDisplayEClass = createEClass(FSM_PERIPHERAL_DEVICE_DISPLAY);
		createEReference(fsmPeripheralDeviceDisplayEClass, FSM_PERIPHERAL_DEVICE_DISPLAY__OUTPUT);

		fsmPeripheralDeviceLineDisplayEClass = createEClass(FSM_PERIPHERAL_DEVICE_LINE_DISPLAY);

		fsmPeripheralDevicePOSPrinterEClass = createEClass(FSM_PERIPHERAL_DEVICE_POS_PRINTER);

		fsmPeripheralDeviceCashDrawerEClass = createEClass(FSM_PERIPHERAL_DEVICE_CASH_DRAWER);

		fsmPeripheralDevicePTEClass = createEClass(FSM_PERIPHERAL_DEVICE_PT);

		fsmPeripheralDeviceSignatureEClass = createEClass(FSM_PERIPHERAL_DEVICE_SIGNATURE);

		fsmPeripheralDeviceScaleEClass = createEClass(FSM_PERIPHERAL_DEVICE_SCALE);

		fsmControlSchedulerAttributeEClass = createEClass(FSM_CONTROL_SCHEDULER_ATTRIBUTE);
		createEAttribute(fsmControlSchedulerAttributeEClass, FSM_CONTROL_SCHEDULER_ATTRIBUTE__DELAY);
		createEReference(fsmControlSchedulerAttributeEClass, FSM_CONTROL_SCHEDULER_ATTRIBUTE__EVENT);

		fsmEventEClass = createEClass(FSM_EVENT);

		fsmStateEClass = createEClass(FSM_STATE);
		createEReference(fsmStateEClass, FSM_STATE__TRIGGERS);
		createEReference(fsmStateEClass, FSM_STATE__CONDITIONS);
		createEReference(fsmStateEClass, FSM_STATE__IDENTITY);
		createEReference(fsmStateEClass, FSM_STATE__KEYSTROKE);
		createEAttribute(fsmStateEClass, FSM_STATE__HAS_KEY_OPERATION);
		createEReference(fsmStateEClass, FSM_STATE__KEY_OPERATION);
		createEReference(fsmStateEClass, FSM_STATE__KEY_MAPPER);

		fsmKeyMapperEClass = createEClass(FSM_KEY_MAPPER);
		createEAttribute(fsmKeyMapperEClass, FSM_KEY_MAPPER__KEY_CODE);
		createEReference(fsmKeyMapperEClass, FSM_KEY_MAPPER__KEY_EVENT);

		fsmTriggerEClass = createEClass(FSM_TRIGGER);
		createEAttribute(fsmTriggerEClass, FSM_TRIGGER__HAS_TRANSITION);
		createEReference(fsmTriggerEClass, FSM_TRIGGER__TRANSITION);
		createEReference(fsmTriggerEClass, FSM_TRIGGER__TRIGGERS);
		createEReference(fsmTriggerEClass, FSM_TRIGGER__GUARDS);
		createEReference(fsmTriggerEClass, FSM_TRIGGER__ACTIONS);

		fsmActionEClass = createEClass(FSM_ACTION);

		fsmActionPeripheralBlinkRateEClass = createEClass(FSM_ACTION_PERIPHERAL_BLINK_RATE);
		createEReference(fsmActionPeripheralBlinkRateEClass, FSM_ACTION_PERIPHERAL_BLINK_RATE__DEVICE);
		createEAttribute(fsmActionPeripheralBlinkRateEClass, FSM_ACTION_PERIPHERAL_BLINK_RATE__BLINK_RATE);

		fsmActionPeripheralClearEClass = createEClass(FSM_ACTION_PERIPHERAL_CLEAR);
		createEReference(fsmActionPeripheralClearEClass, FSM_ACTION_PERIPHERAL_CLEAR__DEVICE);

		fsmActionPeripheralCreateWindowEClass = createEClass(FSM_ACTION_PERIPHERAL_CREATE_WINDOW);
		createEReference(fsmActionPeripheralCreateWindowEClass, FSM_ACTION_PERIPHERAL_CREATE_WINDOW__DEVICE);
		createEAttribute(fsmActionPeripheralCreateWindowEClass, FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_ROW);
		createEAttribute(fsmActionPeripheralCreateWindowEClass, FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_COLUMN);
		createEAttribute(fsmActionPeripheralCreateWindowEClass, FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_HEIGHT);
		createEAttribute(fsmActionPeripheralCreateWindowEClass, FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_WIDTH);
		createEAttribute(fsmActionPeripheralCreateWindowEClass, FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_HEIGHT);
		createEAttribute(fsmActionPeripheralCreateWindowEClass, FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_WIDTH);

		fsmActionPeripheralCursorTypeEClass = createEClass(FSM_ACTION_PERIPHERAL_CURSOR_TYPE);
		createEReference(fsmActionPeripheralCursorTypeEClass, FSM_ACTION_PERIPHERAL_CURSOR_TYPE__DEVICE);
		createEAttribute(fsmActionPeripheralCursorTypeEClass, FSM_ACTION_PERIPHERAL_CURSOR_TYPE__CURSOR_TYPE);

		fsmActionPeripheralDestroyWindowEClass = createEClass(FSM_ACTION_PERIPHERAL_DESTROY_WINDOW);
		createEReference(fsmActionPeripheralDestroyWindowEClass, FSM_ACTION_PERIPHERAL_DESTROY_WINDOW__DEVICE);

		fsmActionPeripheralDeviceBrightnessEClass = createEClass(FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS);
		createEReference(fsmActionPeripheralDeviceBrightnessEClass, FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS__DEVICE);
		createEAttribute(fsmActionPeripheralDeviceBrightnessEClass, FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS__BRIGHTNESS);

		fsmActionPeripheralDisplayTextEClass = createEClass(FSM_ACTION_PERIPHERAL_DISPLAY_TEXT);
		createEReference(fsmActionPeripheralDisplayTextEClass, FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__DEVICE);
		createEReference(fsmActionPeripheralDisplayTextEClass, FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__ATTRIBUTE);
		createEReference(fsmActionPeripheralDisplayTextEClass, FSM_ACTION_PERIPHERAL_DISPLAY_TEXT__TEXT);

		fsmActionPeripheralLineDisplayTextEClass = createEClass(FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT);
		createEReference(fsmActionPeripheralLineDisplayTextEClass, FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__DEVICE);
		createEReference(fsmActionPeripheralLineDisplayTextEClass, FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__TEXT);
		createEAttribute(fsmActionPeripheralLineDisplayTextEClass, FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__HAS_TYPE);
		createEAttribute(fsmActionPeripheralLineDisplayTextEClass, FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT__TEXT_TYPE);

		fsmActionPeripheralLineDisplayTextAtEClass = createEClass(FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT);
		createEReference(fsmActionPeripheralLineDisplayTextAtEClass, FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__DEVICE);
		createEAttribute(fsmActionPeripheralLineDisplayTextAtEClass, FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__ROW);
		createEAttribute(fsmActionPeripheralLineDisplayTextAtEClass, FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__COLUMN);
		createEReference(fsmActionPeripheralLineDisplayTextAtEClass, FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__TEXT);
		createEAttribute(fsmActionPeripheralLineDisplayTextAtEClass, FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__HAS_TYPE);
		createEAttribute(fsmActionPeripheralLineDisplayTextAtEClass, FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT__TEXT_TYPE);

		fsmActionPeripheralInterCharacterWaitEClass = createEClass(FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT);
		createEReference(fsmActionPeripheralInterCharacterWaitEClass, FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT__DEVICE);
		createEAttribute(fsmActionPeripheralInterCharacterWaitEClass, FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT__WAIT);

		fsmActionPeripheralMarqueeFormatEClass = createEClass(FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT);
		createEReference(fsmActionPeripheralMarqueeFormatEClass, FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT__DEVICE);
		createEAttribute(fsmActionPeripheralMarqueeFormatEClass, FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT__FORMAT);

		fsmActionPeripheralMarqueeRepeatWaitEClass = createEClass(FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT);
		createEReference(fsmActionPeripheralMarqueeRepeatWaitEClass, FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT__DEVICE);
		createEAttribute(fsmActionPeripheralMarqueeRepeatWaitEClass, FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT__WAIT);

		fsmActionPeripheralMarqueeTypeEClass = createEClass(FSM_ACTION_PERIPHERAL_MARQUEE_TYPE);
		createEReference(fsmActionPeripheralMarqueeTypeEClass, FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__DEVICE);
		createEAttribute(fsmActionPeripheralMarqueeTypeEClass, FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__MARQUEE_TYPE);

		fsmActionPeripheralMarqueeUnitWaitEClass = createEClass(FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT);
		createEReference(fsmActionPeripheralMarqueeUnitWaitEClass, FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT__DEVICE);
		createEAttribute(fsmActionPeripheralMarqueeUnitWaitEClass, FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT__WAIT);

		fsmActionPeripheralScrollEClass = createEClass(FSM_ACTION_PERIPHERAL_SCROLL);
		createEReference(fsmActionPeripheralScrollEClass, FSM_ACTION_PERIPHERAL_SCROLL__DEVICE);
		createEAttribute(fsmActionPeripheralScrollEClass, FSM_ACTION_PERIPHERAL_SCROLL__DIRECTION);
		createEAttribute(fsmActionPeripheralScrollEClass, FSM_ACTION_PERIPHERAL_SCROLL__UNITS);

		fsmActionPeripheralOpenDrawerEClass = createEClass(FSM_ACTION_PERIPHERAL_OPEN_DRAWER);
		createEReference(fsmActionPeripheralOpenDrawerEClass, FSM_ACTION_PERIPHERAL_OPEN_DRAWER__DEVICE);

		fsmActionPeripheralPrintBarcodeEClass = createEClass(FSM_ACTION_PERIPHERAL_PRINT_BARCODE);
		createEReference(fsmActionPeripheralPrintBarcodeEClass, FSM_ACTION_PERIPHERAL_PRINT_BARCODE__DEVICE);
		createEAttribute(fsmActionPeripheralPrintBarcodeEClass, FSM_ACTION_PERIPHERAL_PRINT_BARCODE__DATA);
		createEAttribute(fsmActionPeripheralPrintBarcodeEClass, FSM_ACTION_PERIPHERAL_PRINT_BARCODE__BARCODE_TYPE);

		fsmActionPeripheralPrintBitmapEClass = createEClass(FSM_ACTION_PERIPHERAL_PRINT_BITMAP);
		createEReference(fsmActionPeripheralPrintBitmapEClass, FSM_ACTION_PERIPHERAL_PRINT_BITMAP__DEVICE);
		createEAttribute(fsmActionPeripheralPrintBitmapEClass, FSM_ACTION_PERIPHERAL_PRINT_BITMAP__BITMAP_ID);

		fsmActionPeripheralPrintCutEClass = createEClass(FSM_ACTION_PERIPHERAL_PRINT_CUT);
		createEReference(fsmActionPeripheralPrintCutEClass, FSM_ACTION_PERIPHERAL_PRINT_CUT__DEVICE);
		createEReference(fsmActionPeripheralPrintCutEClass, FSM_ACTION_PERIPHERAL_PRINT_CUT__TEXT);

		fsmActionPeripheralPrintRegisterBitmapEClass = createEClass(FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP);
		createEAttribute(fsmActionPeripheralPrintRegisterBitmapEClass, FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP__BITMAP_ID);
		createEAttribute(fsmActionPeripheralPrintRegisterBitmapEClass, FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP__NAME);

		fsmActionPeripheralPrintNormalEClass = createEClass(FSM_ACTION_PERIPHERAL_PRINT_NORMAL);
		createEReference(fsmActionPeripheralPrintNormalEClass, FSM_ACTION_PERIPHERAL_PRINT_NORMAL__DEVICE);
		createEReference(fsmActionPeripheralPrintNormalEClass, FSM_ACTION_PERIPHERAL_PRINT_NORMAL__TEXT);
		createEAttribute(fsmActionPeripheralPrintNormalEClass, FSM_ACTION_PERIPHERAL_PRINT_NORMAL__BARCODE_TYPE);

		fsmActionPeripheralPTOpenEClass = createEClass(FSM_ACTION_PERIPHERAL_PT_OPEN);
		createEReference(fsmActionPeripheralPTOpenEClass, FSM_ACTION_PERIPHERAL_PT_OPEN__DEVICE);
		createEReference(fsmActionPeripheralPTOpenEClass, FSM_ACTION_PERIPHERAL_PT_OPEN__HOST);
		createEReference(fsmActionPeripheralPTOpenEClass, FSM_ACTION_PERIPHERAL_PT_OPEN__PORT);

		fsmActionPeripheralPTCloseEClass = createEClass(FSM_ACTION_PERIPHERAL_PT_CLOSE);
		createEReference(fsmActionPeripheralPTCloseEClass, FSM_ACTION_PERIPHERAL_PT_CLOSE__DEVICE);

		fsmActionPeripheralPTReversalEClass = createEClass(FSM_ACTION_PERIPHERAL_PT_REVERSAL);
		createEReference(fsmActionPeripheralPTReversalEClass, FSM_ACTION_PERIPHERAL_PT_REVERSAL__DEVICE);
		createEReference(fsmActionPeripheralPTReversalEClass, FSM_ACTION_PERIPHERAL_PT_REVERSAL__PASSWORD);
		createEReference(fsmActionPeripheralPTReversalEClass, FSM_ACTION_PERIPHERAL_PT_REVERSAL__RECEIPT);

		fsmActionPeripheralPTAcknowledgeEClass = createEClass(FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE);
		createEReference(fsmActionPeripheralPTAcknowledgeEClass, FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE__DEVICE);

		fsmActionPeripheralPTRegistrationEClass = createEClass(FSM_ACTION_PERIPHERAL_PT_REGISTRATION);
		createEReference(fsmActionPeripheralPTRegistrationEClass, FSM_ACTION_PERIPHERAL_PT_REGISTRATION__DEVICE);
		createEReference(fsmActionPeripheralPTRegistrationEClass, FSM_ACTION_PERIPHERAL_PT_REGISTRATION__PASSWORD);
		createEAttribute(fsmActionPeripheralPTRegistrationEClass, FSM_ACTION_PERIPHERAL_PT_REGISTRATION__CONFIGURATION);

		fsmActionPeripheralPTAuthorizationEClass = createEClass(FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION);
		createEReference(fsmActionPeripheralPTAuthorizationEClass, FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION__DEVICE);
		createEReference(fsmActionPeripheralPTAuthorizationEClass, FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION__AMOUNT);

		fsmActionPeripheralBeeperEClass = createEClass(FSM_ACTION_PERIPHERAL_BEEPER);
		createEAttribute(fsmActionPeripheralBeeperEClass, FSM_ACTION_PERIPHERAL_BEEPER__DURATION);
		createEAttribute(fsmActionPeripheralBeeperEClass, FSM_ACTION_PERIPHERAL_BEEPER__FREQUENCY);

		fsmActionPeripheralPlayerEClass = createEClass(FSM_ACTION_PERIPHERAL_PLAYER);
		createEAttribute(fsmActionPeripheralPlayerEClass, FSM_ACTION_PERIPHERAL_PLAYER__TUNE);

		fsmActionPeripheralSoundEClass = createEClass(FSM_ACTION_PERIPHERAL_SOUND);
		createEAttribute(fsmActionPeripheralSoundEClass, FSM_ACTION_PERIPHERAL_SOUND__NAME);

		fsmActionPeripheralPTResponseEClass = createEClass(FSM_ACTION_PERIPHERAL_PT_RESPONSE);
		createEReference(fsmActionPeripheralPTResponseEClass, FSM_ACTION_PERIPHERAL_PT_RESPONSE__DEVICE);

		fsmActionPeripheralPrintReportEClass = createEClass(FSM_ACTION_PERIPHERAL_PRINT_REPORT);
		createEReference(fsmActionPeripheralPrintReportEClass, FSM_ACTION_PERIPHERAL_PRINT_REPORT__REPORT);
		createEReference(fsmActionPeripheralPrintReportEClass, FSM_ACTION_PERIPHERAL_PRINT_REPORT__KEY);
		createEAttribute(fsmActionPeripheralPrintReportEClass, FSM_ACTION_PERIPHERAL_PRINT_REPORT__HAS_FILTER);
		createEAttribute(fsmActionPeripheralPrintReportEClass, FSM_ACTION_PERIPHERAL_PRINT_REPORT__HAS_PRINT_SERVICE);
		createEReference(fsmActionPeripheralPrintReportEClass, FSM_ACTION_PERIPHERAL_PRINT_REPORT__PRINT_SERVICE);

		fsmActionPeripheralSignatureOpenEClass = createEClass(FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN);
		createEReference(fsmActionPeripheralSignatureOpenEClass, FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN__DEVICE);

		fsmActionPeripheralSignatureCloseEClass = createEClass(FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE);
		createEReference(fsmActionPeripheralSignatureCloseEClass, FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE__DEVICE);

		fsmActionPeripheralSignatureClearEClass = createEClass(FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR);
		createEReference(fsmActionPeripheralSignatureClearEClass, FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR__DEVICE);

		fsmActionPeripheralSignatureCaptureEClass = createEClass(FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE);
		createEReference(fsmActionPeripheralSignatureCaptureEClass, FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE__DEVICE);

		fsmActionPeripheralSignatureIdleEClass = createEClass(FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE);
		createEReference(fsmActionPeripheralSignatureIdleEClass, FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE__DEVICE);

		fsmActionPeripheralSignatureLabelEClass = createEClass(FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL);
		createEAttribute(fsmActionPeripheralSignatureLabelEClass, FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__OK_LABEL);
		createEAttribute(fsmActionPeripheralSignatureLabelEClass, FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__CLEAR_LABEL);
		createEAttribute(fsmActionPeripheralSignatureLabelEClass, FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__CANCEL_LABEL);
		createEReference(fsmActionPeripheralSignatureLabelEClass, FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL__DEVICE);

		fsmSignatureRetrieveEClass = createEClass(FSM_SIGNATURE_RETRIEVE);
		createEReference(fsmSignatureRetrieveEClass, FSM_SIGNATURE_RETRIEVE__DEVICE);

		fsmActionPeripheralScaleReadWeightEClass = createEClass(FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT);
		createEReference(fsmActionPeripheralScaleReadWeightEClass, FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT__DEVICE);

		fsmActionPeripheralScaleReadTareWeightEClass = createEClass(FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT);
		createEReference(fsmActionPeripheralScaleReadTareWeightEClass, FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT__DEVICE);

		fsmActionPeripheralScaleTareWeightEClass = createEClass(FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT);
		createEReference(fsmActionPeripheralScaleTareWeightEClass, FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT__DEVICE);
		createEReference(fsmActionPeripheralScaleTareWeightEClass, FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT__VALUE);

		fsmActionPeripheralScaleZeroEClass = createEClass(FSM_ACTION_PERIPHERAL_SCALE_ZERO);
		createEReference(fsmActionPeripheralScaleZeroEClass, FSM_ACTION_PERIPHERAL_SCALE_ZERO__DEVICE);

		fsmActionPeripheralScaleDisplayTextEClass = createEClass(FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT);
		createEReference(fsmActionPeripheralScaleDisplayTextEClass, FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT__DEVICE);
		createEReference(fsmActionPeripheralScaleDisplayTextEClass, FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT__TEXT);

		fsmActionPeripheralScaleWeightUnitEClass = createEClass(FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT);
		createEReference(fsmActionPeripheralScaleWeightUnitEClass, FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT__DEVICE);

		fsmActionFieldSourceEClass = createEClass(FSM_ACTION_FIELD_SOURCE);

		fsmActionFieldSourceStringEClass = createEClass(FSM_ACTION_FIELD_SOURCE_STRING);
		createEAttribute(fsmActionFieldSourceStringEClass, FSM_ACTION_FIELD_SOURCE_STRING__TEXT);

		fsmActionFieldSourceNumberEClass = createEClass(FSM_ACTION_FIELD_SOURCE_NUMBER);
		createEAttribute(fsmActionFieldSourceNumberEClass, FSM_ACTION_FIELD_SOURCE_NUMBER__VALUE);

		fsmActionFieldSourceIntegerEClass = createEClass(FSM_ACTION_FIELD_SOURCE_INTEGER);
		createEAttribute(fsmActionFieldSourceIntegerEClass, FSM_ACTION_FIELD_SOURCE_INTEGER__VALUE);

		fsmActionFieldSourceBooleanEClass = createEClass(FSM_ACTION_FIELD_SOURCE_BOOLEAN);
		createEAttribute(fsmActionFieldSourceBooleanEClass, FSM_ACTION_FIELD_SOURCE_BOOLEAN__VALUE);

		fsmActionFieldSourceEvaluateEClass = createEClass(FSM_ACTION_FIELD_SOURCE_EVALUATE);
		createEAttribute(fsmActionFieldSourceEvaluateEClass, FSM_ACTION_FIELD_SOURCE_EVALUATE__EVALUATIONTYPE);

		fsmActionFieldSourceTranslateEClass = createEClass(FSM_ACTION_FIELD_SOURCE_TRANSLATE);
		createEAttribute(fsmActionFieldSourceTranslateEClass, FSM_ACTION_FIELD_SOURCE_TRANSLATE__TEXT);

		fsmRefEClass = createEClass(FSM_REF);

		fsmDotExpressionEClass = createEClass(FSM_DOT_EXPRESSION);
		createEReference(fsmDotExpressionEClass, FSM_DOT_EXPRESSION__REF);
		createEReference(fsmDotExpressionEClass, FSM_DOT_EXPRESSION__TAIL);

		fsmDtoRefEClass = createEClass(FSM_DTO_REF);
		createEReference(fsmDtoRefEClass, FSM_DTO_REF__DTO);

		fsmActionFieldSourceDtoAttributeEClass = createEClass(FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE);
		createEReference(fsmActionFieldSourceDtoAttributeEClass, FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE__DTO);
		createEReference(fsmActionFieldSourceDtoAttributeEClass, FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE__ATTRIBUTE);

		fsmActionFieldSourceEventEClass = createEClass(FSM_ACTION_FIELD_SOURCE_EVENT);

		fsmActionConditionalTransitionEClass = createEClass(FSM_ACTION_CONDITIONAL_TRANSITION);
		createEReference(fsmActionConditionalTransitionEClass, FSM_ACTION_CONDITIONAL_TRANSITION__TRANSITION);
		createEReference(fsmActionConditionalTransitionEClass, FSM_ACTION_CONDITIONAL_TRANSITION__GUARD);
		createEReference(fsmActionConditionalTransitionEClass, FSM_ACTION_CONDITIONAL_TRANSITION__ACTIONS);

		fsmOperationParameterEClass = createEClass(FSM_OPERATION_PARAMETER);
		createEReference(fsmOperationParameterEClass, FSM_OPERATION_PARAMETER__SOURCE);

		fsmOperationEClass = createEClass(FSM_OPERATION);
		createEReference(fsmOperationEClass, FSM_OPERATION__GROUP);
		createEReference(fsmOperationEClass, FSM_OPERATION__OPERATION);
		createEReference(fsmOperationEClass, FSM_OPERATION__FIRST);
		createEReference(fsmOperationEClass, FSM_OPERATION__MORE);

		fsmGuardEClass = createEClass(FSM_GUARD);
		createEReference(fsmGuardEClass, FSM_GUARD__GROUP);
		createEReference(fsmGuardEClass, FSM_GUARD__GUARD);
		createEAttribute(fsmGuardEClass, FSM_GUARD__HAS_ON_FAIL);
		createEAttribute(fsmGuardEClass, FSM_GUARD__ON_FAIL_DESCRIPTION);
		createEAttribute(fsmGuardEClass, FSM_GUARD__ON_FAIL_CAPTION);
		createEAttribute(fsmGuardEClass, FSM_GUARD__ON_FAIL_TYPE);

		fsmFunctionEClass = createEClass(FSM_FUNCTION);
		createEReference(fsmFunctionEClass, FSM_FUNCTION__GROUP);
		createEReference(fsmFunctionEClass, FSM_FUNCTION__FUNCTION);
		createEReference(fsmFunctionEClass, FSM_FUNCTION__FIRST);
		createEReference(fsmFunctionEClass, FSM_FUNCTION__MORE);

		fsmStorageRetrieveEClass = createEClass(FSM_STORAGE_RETRIEVE);
		createEAttribute(fsmStorageRetrieveEClass, FSM_STORAGE_RETRIEVE__KEY);
		createEAttribute(fsmStorageRetrieveEClass, FSM_STORAGE_RETRIEVE__ATTRIBUTE);

		fsmStorageEClass = createEClass(FSM_STORAGE);
		createEAttribute(fsmStorageEClass, FSM_STORAGE__KEY);
		createEAttribute(fsmStorageEClass, FSM_STORAGE__ATTRIBUTE);
		createEReference(fsmStorageEClass, FSM_STORAGE__CONTENT);

		fsmActionFieldConcatenationEClass = createEClass(FSM_ACTION_FIELD_CONCATENATION);
		createEReference(fsmActionFieldConcatenationEClass, FSM_ACTION_FIELD_CONCATENATION__FIRST);
		createEReference(fsmActionFieldConcatenationEClass, FSM_ACTION_FIELD_CONCATENATION__MORE);

		fsmActionFieldSetEClass = createEClass(FSM_ACTION_FIELD_SET);
		createEReference(fsmActionFieldSetEClass, FSM_ACTION_FIELD_SET__ATTRIBUTE);
		createEReference(fsmActionFieldSetEClass, FSM_ACTION_FIELD_SET__SOURCE);

		fsmActionFieldKeystrokeEClass = createEClass(FSM_ACTION_FIELD_KEYSTROKE);
		createEReference(fsmActionFieldKeystrokeEClass, FSM_ACTION_FIELD_KEYSTROKE__ATTRIBUTE);
		createEAttribute(fsmActionFieldKeystrokeEClass, FSM_ACTION_FIELD_KEYSTROKE__KEYSTROKE);

		fsmActionFieldClearEClass = createEClass(FSM_ACTION_FIELD_CLEAR);
		createEReference(fsmActionFieldClearEClass, FSM_ACTION_FIELD_CLEAR__ATTRIBUTE);

		fsmActionFieldGetEClass = createEClass(FSM_ACTION_FIELD_GET);
		createEReference(fsmActionFieldGetEClass, FSM_ACTION_FIELD_GET__ATTRIBUTE);

		fsmActionFieldFilterToggleEClass = createEClass(FSM_ACTION_FIELD_FILTER_TOGGLE);
		createEReference(fsmActionFieldFilterToggleEClass, FSM_ACTION_FIELD_FILTER_TOGGLE__FILTER);

		fsmActionFieldRemoveEClass = createEClass(FSM_ACTION_FIELD_REMOVE);
		createEReference(fsmActionFieldRemoveEClass, FSM_ACTION_FIELD_REMOVE__ATTRIBUTE);

		fsmActionItemVisibleEClass = createEClass(FSM_ACTION_ITEM_VISIBLE);
		createEReference(fsmActionItemVisibleEClass, FSM_ACTION_ITEM_VISIBLE__ATTRIBUTE);

		fsmActionItemInvisibleEClass = createEClass(FSM_ACTION_ITEM_INVISIBLE);
		createEReference(fsmActionItemInvisibleEClass, FSM_ACTION_ITEM_INVISIBLE__ATTRIBUTE);

		fsmActionButtonCaptionEClass = createEClass(FSM_ACTION_BUTTON_CAPTION);
		createEReference(fsmActionButtonCaptionEClass, FSM_ACTION_BUTTON_CAPTION__ATTRIBUTE);
		createEReference(fsmActionButtonCaptionEClass, FSM_ACTION_BUTTON_CAPTION__CAPTION);

		fsmActionButtonImageEClass = createEClass(FSM_ACTION_BUTTON_IMAGE);
		createEReference(fsmActionButtonImageEClass, FSM_ACTION_BUTTON_IMAGE__ATTRIBUTE);
		createEAttribute(fsmActionButtonImageEClass, FSM_ACTION_BUTTON_IMAGE__IMAGE);

		fsmActionDTOFindEClass = createEClass(FSM_ACTION_DTO_FIND);
		createEReference(fsmActionDTOFindEClass, FSM_ACTION_DTO_FIND__DTO);
		createEReference(fsmActionDTOFindEClass, FSM_ACTION_DTO_FIND__ATTRIBUTE);
		createEReference(fsmActionDTOFindEClass, FSM_ACTION_DTO_FIND__SEARCH);

		fsmActionDTOClearEClass = createEClass(FSM_ACTION_DTO_CLEAR);
		createEReference(fsmActionDTOClearEClass, FSM_ACTION_DTO_CLEAR__DTO);

		fsmActionSchedulerEClass = createEClass(FSM_ACTION_SCHEDULER);
		createEReference(fsmActionSchedulerEClass, FSM_ACTION_SCHEDULER__SCHEDULER);

		fsmdtoTypeEClass = createEClass(FSMDTO_TYPE);
		createEReference(fsmdtoTypeEClass, FSMDTO_TYPE__ATTRIBUTE_TYPE);

		fsmFieldTypeEClass = createEClass(FSM_FIELD_TYPE);
		createEAttribute(fsmFieldTypeEClass, FSM_FIELD_TYPE__ATTRIBUTE_TYPE);

		fsmAbstractFilterEClass = createEClass(FSM_ABSTRACT_FILTER);

		fsmFilterPropertyEClass = createEClass(FSM_FILTER_PROPERTY);
		createEReference(fsmFilterPropertyEClass, FSM_FILTER_PROPERTY__PATH);

		fsmFilterEClass = createEClass(FSM_FILTER);
		createEReference(fsmFilterEClass, FSM_FILTER__SOURCE);

		fsmJunctionFilterEClass = createEClass(FSM_JUNCTION_FILTER);
		createEReference(fsmJunctionFilterEClass, FSM_JUNCTION_FILTER__FIRST);
		createEReference(fsmJunctionFilterEClass, FSM_JUNCTION_FILTER__MORE);

		fsmAndFilterEClass = createEClass(FSM_AND_FILTER);

		fsmOrFilterEClass = createEClass(FSM_OR_FILTER);

		fsmBetweenFilterEClass = createEClass(FSM_BETWEEN_FILTER);
		createEReference(fsmBetweenFilterEClass, FSM_BETWEEN_FILTER__PROPERTY_ID);
		createEReference(fsmBetweenFilterEClass, FSM_BETWEEN_FILTER__START);
		createEReference(fsmBetweenFilterEClass, FSM_BETWEEN_FILTER__END);

		fsmCompareFilterEClass = createEClass(FSM_COMPARE_FILTER);
		createEReference(fsmCompareFilterEClass, FSM_COMPARE_FILTER__PROPERTY_ID);
		createEReference(fsmCompareFilterEClass, FSM_COMPARE_FILTER__OPERAND);
		createEAttribute(fsmCompareFilterEClass, FSM_COMPARE_FILTER__OPERATION);

		fsmIsNullFilterEClass = createEClass(FSM_IS_NULL_FILTER);
		createEReference(fsmIsNullFilterEClass, FSM_IS_NULL_FILTER__PROPERTY_ID);

		fsmLikeFilterEClass = createEClass(FSM_LIKE_FILTER);
		createEReference(fsmLikeFilterEClass, FSM_LIKE_FILTER__PROPERTY_ID);
		createEReference(fsmLikeFilterEClass, FSM_LIKE_FILTER__VALUE);
		createEAttribute(fsmLikeFilterEClass, FSM_LIKE_FILTER__IGNORE_CASE);

		fsmNotFilterEClass = createEClass(FSM_NOT_FILTER);
		createEReference(fsmNotFilterEClass, FSM_NOT_FILTER__FILTER);

		fsmStringFilterEClass = createEClass(FSM_STRING_FILTER);
		createEReference(fsmStringFilterEClass, FSM_STRING_FILTER__PROPERTY_ID);
		createEAttribute(fsmStringFilterEClass, FSM_STRING_FILTER__FILTER_STRING);
		createEAttribute(fsmStringFilterEClass, FSM_STRING_FILTER__ONLY_MATCH_PREFIX);
		createEAttribute(fsmStringFilterEClass, FSM_STRING_FILTER__IGNORE_CASE);

		fsmControlFilterEClass = createEClass(FSM_CONTROL_FILTER);
		createEReference(fsmControlFilterEClass, FSM_CONTROL_FILTER__FILTER);

		// Create enums
		fsmInternalTypeEEnum = createEEnum(FSM_INTERNAL_TYPE);
		fsmControlButtonEventTypeEEnum = createEEnum(FSM_CONTROL_BUTTON_EVENT_TYPE);
		fsmCompareOperationEnumEEnum = createEEnum(FSM_COMPARE_OPERATION_ENUM);
		fsmEvaluationTypeEEnum = createEEnum(FSM_EVALUATION_TYPE);
		fsmUserMessageTypeEEnum = createEEnum(FSM_USER_MESSAGE_TYPE);
		fsmLineDisplayCursorTypeEEnum = createEEnum(FSM_LINE_DISPLAY_CURSOR_TYPE);
		fsmLineDisplayMarqueeTypeEEnum = createEEnum(FSM_LINE_DISPLAY_MARQUEE_TYPE);
		fsmLineDisplayMarqueeFormatEEnum = createEEnum(FSM_LINE_DISPLAY_MARQUEE_FORMAT);
		fsmLineDisplayTextTypeEEnum = createEEnum(FSM_LINE_DISPLAY_TEXT_TYPE);
		fsmLineDisplayScrollTextTypeEEnum = createEEnum(FSM_LINE_DISPLAY_SCROLL_TEXT_TYPE);
		fsmposPrinterBarcodeTypeEEnum = createEEnum(FSMPOS_PRINTER_BARCODE_TYPE);
		fsmFunctionalKeyCodesEEnum = createEEnum(FSM_FUNCTIONAL_KEY_CODES);

		// Create data types
		internalEObjectEDataType = createEDataType(INTERNAL_EOBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XtypePackage theXtypePackage = (XtypePackage)EPackage.Registry.INSTANCE.getEPackage(XtypePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		OSBPTypesPackage theOSBPTypesPackage = (OSBPTypesPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPTypesPackage.eNS_URI);
		OSBPDtoPackage theOSBPDtoPackage = (OSBPDtoPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPDtoPackage.eNS_URI);
		ReportDSLPackage theReportDSLPackage = (ReportDSLPackage)EPackage.Registry.INSTANCE.getEPackage(ReportDSLPackage.eNS_URI);
		FunctionLibraryDSLPackage theFunctionLibraryDSLPackage = (FunctionLibraryDSLPackage)EPackage.Registry.INSTANCE.getEPackage(FunctionLibraryDSLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		fsmBaseEClass.getESuperTypes().add(this.getFSMLazyResolver());
		fsmPackageEClass.getESuperTypes().add(theOSBPTypesPackage.getLPackage());
		fsmEClass.getESuperTypes().add(this.getFSMBase());
		fsmControlEClass.getESuperTypes().add(this.getFSMBase());
		fsmControlButtonEClass.getESuperTypes().add(this.getFSMControl());
		fsmControlButtonEClass.getESuperTypes().add(this.getFSMControlVisibility());
		fsmControlFieldEClass.getESuperTypes().add(this.getFSMControl());
		fsmControlDTOEClass.getESuperTypes().add(this.getFSMControl());
		fsmControlSchedulerEClass.getESuperTypes().add(this.getFSMControl());
		fsmControlPeripheralEClass.getESuperTypes().add(this.getFSMControl());
		fsmControlButtonAttributeEClass.getESuperTypes().add(this.getFSMControlVisibility());
		fsmControlButtonAttributeEventEClass.getESuperTypes().add(this.getFSMLazyResolver());
		fsmControlButtonAttributeEventKeyboardEClass.getESuperTypes().add(this.getFSMControlButtonAttributeEvent());
		fsmControlButtonAttributeEventIdentityEClass.getESuperTypes().add(this.getFSMControlButtonAttributeEvent());
		fsmControlButtonAttributeEventEventEClass.getESuperTypes().add(this.getFSMControlButtonAttributeEvent());
		fsmControlVisibilityEClass.getESuperTypes().add(this.getFSMBase());
		fsmControlFieldAttributeEClass.getESuperTypes().add(this.getFSMControlVisibility());
		fsmControlFieldLayoutEClass.getESuperTypes().add(this.getFSMControlVisibility());
		fsmControlDTOAttributeEClass.getESuperTypes().add(this.getFSMBase());
		fsmPeripheralDeviceEClass.getESuperTypes().add(this.getFSMBase());
		fsmPeripheralDeviceDisplayEClass.getESuperTypes().add(this.getFSMPeripheralDevice());
		fsmPeripheralDeviceLineDisplayEClass.getESuperTypes().add(this.getFSMPeripheralDevice());
		fsmPeripheralDevicePOSPrinterEClass.getESuperTypes().add(this.getFSMPeripheralDevice());
		fsmPeripheralDeviceCashDrawerEClass.getESuperTypes().add(this.getFSMPeripheralDevice());
		fsmPeripheralDevicePTEClass.getESuperTypes().add(this.getFSMPeripheralDevice());
		fsmPeripheralDeviceSignatureEClass.getESuperTypes().add(this.getFSMPeripheralDevice());
		fsmPeripheralDeviceScaleEClass.getESuperTypes().add(this.getFSMPeripheralDevice());
		fsmControlSchedulerAttributeEClass.getESuperTypes().add(this.getFSMBase());
		fsmEventEClass.getESuperTypes().add(this.getFSMBase());
		fsmStateEClass.getESuperTypes().add(this.getFSMBase());
		fsmKeyMapperEClass.getESuperTypes().add(this.getFSMLazyResolver());
		fsmTriggerEClass.getESuperTypes().add(this.getFSMLazyResolver());
		fsmActionEClass.getESuperTypes().add(this.getFSMLazyResolver());
		fsmActionPeripheralBlinkRateEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralClearEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralCreateWindowEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralCursorTypeEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralDestroyWindowEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralDeviceBrightnessEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralDisplayTextEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralLineDisplayTextEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralLineDisplayTextAtEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralInterCharacterWaitEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralMarqueeFormatEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralMarqueeRepeatWaitEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralMarqueeTypeEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralMarqueeUnitWaitEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralScrollEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralOpenDrawerEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPrintBarcodeEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPrintBitmapEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPrintCutEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPrintRegisterBitmapEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPrintNormalEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPTOpenEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPTCloseEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPTReversalEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPTAcknowledgeEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPTRegistrationEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPTAuthorizationEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralBeeperEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPlayerEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralSoundEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralPTResponseEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionPeripheralPrintReportEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralSignatureOpenEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralSignatureCloseEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralSignatureClearEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralSignatureCaptureEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralSignatureIdleEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionPeripheralSignatureLabelEClass.getESuperTypes().add(this.getFSMAction());
		fsmSignatureRetrieveEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionPeripheralScaleReadWeightEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionPeripheralScaleReadTareWeightEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionPeripheralScaleTareWeightEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionPeripheralScaleZeroEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionPeripheralScaleDisplayTextEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionPeripheralScaleWeightUnitEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionFieldSourceEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionFieldSourceStringEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionFieldSourceNumberEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionFieldSourceIntegerEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionFieldSourceBooleanEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionFieldSourceEvaluateEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionFieldSourceTranslateEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmRefEClass.getESuperTypes().add(this.getFSMBase());
		fsmDotExpressionEClass.getESuperTypes().add(this.getFSMRef());
		fsmDtoRefEClass.getESuperTypes().add(this.getFSMRef());
		fsmActionFieldSourceDtoAttributeEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionFieldSourceEventEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionConditionalTransitionEClass.getESuperTypes().add(this.getFSMAction());
		fsmOperationParameterEClass.getESuperTypes().add(this.getFSMAction());
		fsmOperationEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmGuardEClass.getESuperTypes().add(this.getFSMLazyResolver());
		fsmFunctionEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmStorageRetrieveEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmStorageEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionFieldConcatenationEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionFieldSetEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionFieldKeystrokeEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionFieldClearEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionFieldGetEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionFieldFilterToggleEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionFieldRemoveEClass.getESuperTypes().add(this.getFSMActionFieldSource());
		fsmActionItemVisibleEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionItemInvisibleEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionButtonCaptionEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionButtonImageEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionDTOFindEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionDTOClearEClass.getESuperTypes().add(this.getFSMAction());
		fsmActionSchedulerEClass.getESuperTypes().add(this.getFSMAction());
		fsmdtoTypeEClass.getESuperTypes().add(this.getFSMLazyResolver());
		fsmFieldTypeEClass.getESuperTypes().add(this.getFSMLazyResolver());
		fsmAbstractFilterEClass.getESuperTypes().add(this.getFSMLazyResolver());
		fsmFilterPropertyEClass.getESuperTypes().add(this.getFSMLazyResolver());
		fsmJunctionFilterEClass.getESuperTypes().add(this.getFSMAbstractFilter());
		fsmAndFilterEClass.getESuperTypes().add(this.getFSMJunctionFilter());
		fsmOrFilterEClass.getESuperTypes().add(this.getFSMJunctionFilter());
		fsmBetweenFilterEClass.getESuperTypes().add(this.getFSMAbstractFilter());
		fsmCompareFilterEClass.getESuperTypes().add(this.getFSMAbstractFilter());
		fsmIsNullFilterEClass.getESuperTypes().add(this.getFSMAbstractFilter());
		fsmLikeFilterEClass.getESuperTypes().add(this.getFSMAbstractFilter());
		fsmNotFilterEClass.getESuperTypes().add(this.getFSMAbstractFilter());
		fsmStringFilterEClass.getESuperTypes().add(this.getFSMAbstractFilter());
		fsmControlFilterEClass.getESuperTypes().add(this.getFSMBase());

		// Initialize classes, features, and operations; add parameters
		initEClass(fsmModelEClass, FSMModel.class, "FSMModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMModel_ImportSection(), theXtypePackage.getXImportSection(), null, "importSection", null, 0, 1, FSMModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMModel_Packages(), this.getFSMPackage(), null, "packages", null, 0, -1, FSMModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmLazyResolverEClass, FSMLazyResolver.class, "FSMLazyResolver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getFSMLazyResolver__EResolveProxy__InternalEObject(), theEcorePackage.getEObject(), "eResolveProxy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInternalEObject(), "proxy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(fsmBaseEClass, FSMBase.class, "FSMBase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMBase_Name(), theEcorePackage.getEString(), "name", null, 0, 1, FSMBase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmPackageEClass, FSMPackage.class, "FSMPackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMPackage_Statemachines(), this.getFSM(), null, "statemachines", null, 0, -1, FSMPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmEClass, org.eclipse.osbp.xtext.statemachine.FSM.class, "FSM", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSM_Description(), theEcorePackage.getEBoolean(), "description", null, 0, 1, org.eclipse.osbp.xtext.statemachine.FSM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSM_DescriptionValue(), theEcorePackage.getEString(), "descriptionValue", null, 0, 1, org.eclipse.osbp.xtext.statemachine.FSM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSM_InitialEvent(), this.getFSMEvent(), null, "initialEvent", null, 0, 1, org.eclipse.osbp.xtext.statemachine.FSM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSM_InitialState(), this.getFSMState(), null, "initialState", null, 0, 1, org.eclipse.osbp.xtext.statemachine.FSM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSM_Events(), this.getFSMEvent(), null, "events", null, 0, -1, org.eclipse.osbp.xtext.statemachine.FSM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSM_Controls(), this.getFSMControl(), null, "controls", null, 0, -1, org.eclipse.osbp.xtext.statemachine.FSM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSM_States(), this.getFSMState(), null, "states", null, 0, -1, org.eclipse.osbp.xtext.statemachine.FSM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlEClass, FSMControl.class, "FSMControl", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmControlButtonEClass, FSMControlButton.class, "FSMControlButton", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMControlButton_EventType(), this.getFSMControlButtonEventType(), "eventType", null, 0, 1, FSMControlButton.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlButton_Buttons(), this.getFSMControlButtonAttribute(), null, "buttons", null, 0, -1, FSMControlButton.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMControlButton_HasRange(), theEcorePackage.getEBoolean(), "hasRange", null, 0, 1, FSMControlButton.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMControlButton_Start(), theEcorePackage.getEInt(), "start", null, 0, 1, FSMControlButton.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMControlButton_End(), theEcorePackage.getEInt(), "end", null, 0, 1, FSMControlButton.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMControlButton_RangedName(), theEcorePackage.getEString(), "rangedName", null, 0, 1, FSMControlButton.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlFieldEClass, FSMControlField.class, "FSMControlField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMControlField_Fields(), this.getFSMControlFieldAttribute(), null, "fields", null, 0, -1, FSMControlField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlField_Layouts(), this.getFSMControlFieldLayout(), null, "layouts", null, 0, -1, FSMControlField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlDTOEClass, FSMControlDTO.class, "FSMControlDTO", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMControlDTO_Dtos(), this.getFSMControlDTOAttribute(), null, "dtos", null, 0, -1, FSMControlDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlDTO_Filters(), this.getFSMControlFilter(), null, "filters", null, 0, -1, FSMControlDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlSchedulerEClass, FSMControlScheduler.class, "FSMControlScheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMControlScheduler_Schedulers(), this.getFSMControlSchedulerAttribute(), null, "schedulers", null, 0, -1, FSMControlScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlPeripheralEClass, FSMControlPeripheral.class, "FSMControlPeripheral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMControlPeripheral_LineDisplays(), this.getFSMPeripheralDeviceLineDisplay(), null, "lineDisplays", null, 0, -1, FSMControlPeripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlPeripheral_Displays(), this.getFSMPeripheralDeviceDisplay(), null, "displays", null, 0, -1, FSMControlPeripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlPeripheral_PosPrinters(), this.getFSMPeripheralDevicePOSPrinter(), null, "posPrinters", null, 0, -1, FSMControlPeripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlPeripheral_CashDrawers(), this.getFSMPeripheralDeviceCashDrawer(), null, "cashDrawers", null, 0, -1, FSMControlPeripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlPeripheral_PaymentTerminals(), this.getFSMPeripheralDevicePT(), null, "paymentTerminals", null, 0, -1, FSMControlPeripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlPeripheral_SignaturePads(), this.getFSMPeripheralDeviceSignature(), null, "signaturePads", null, 0, -1, FSMControlPeripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlPeripheral_Scales(), this.getFSMPeripheralDeviceScale(), null, "scales", null, 0, -1, FSMControlPeripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlButtonAttributeEClass, FSMControlButtonAttribute.class, "FSMControlButtonAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMControlButtonAttribute_HasImage(), theEcorePackage.getEBoolean(), "hasImage", null, 0, 1, FSMControlButtonAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMControlButtonAttribute_Image(), theEcorePackage.getEString(), "image", null, 0, 1, FSMControlButtonAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlButtonAttribute_Event(), this.getFSMControlButtonAttributeEvent(), null, "event", null, 0, 1, FSMControlButtonAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlButtonAttributeEventEClass, FSMControlButtonAttributeEvent.class, "FSMControlButtonAttributeEvent", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmControlButtonAttributeEventKeyboardEClass, FSMControlButtonAttributeEventKeyboard.class, "FSMControlButtonAttributeEventKeyboard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMControlButtonAttributeEventKeyboard_Keystroke(), theEcorePackage.getEString(), "keystroke", null, 0, 1, FSMControlButtonAttributeEventKeyboard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlButtonAttributeEventIdentityEClass, FSMControlButtonAttributeEventIdentity.class, "FSMControlButtonAttributeEventIdentity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMControlButtonAttributeEventIdentity_Identity(), theEcorePackage.getEInt(), "identity", null, 0, 1, FSMControlButtonAttributeEventIdentity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlButtonAttributeEventEventEClass, FSMControlButtonAttributeEventEvent.class, "FSMControlButtonAttributeEventEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMControlButtonAttributeEventEvent_Event(), this.getFSMEvent(), null, "event", null, 0, 1, FSMControlButtonAttributeEventEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlVisibilityEClass, FSMControlVisibility.class, "FSMControlVisibility", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmControlFieldAttributeEClass, FSMControlFieldAttribute.class, "FSMControlFieldAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMControlFieldAttribute_AttributeType(), this.getFSMFieldType(), null, "attributeType", null, 0, 1, FSMControlFieldAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlFieldLayoutEClass, FSMControlFieldLayout.class, "FSMControlFieldLayout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmControlDTOAttributeEClass, FSMControlDTOAttribute.class, "FSMControlDTOAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMControlDTOAttribute_AttributeType(), this.getFSMDTOType(), null, "attributeType", null, 0, 1, FSMControlDTOAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMControlDTOAttribute_HasEvent(), theEcorePackage.getEBoolean(), "hasEvent", null, 0, 1, FSMControlDTOAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlDTOAttribute_Event(), this.getFSMEvent(), null, "event", null, 0, 1, FSMControlDTOAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMControlDTOAttribute_IsAttached(), theEcorePackage.getEBoolean(), "isAttached", null, 0, 1, FSMControlDTOAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlDTOAttribute_Display(), this.getFSMPeripheralDeviceDisplay(), null, "display", null, 0, 1, FSMControlDTOAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmPeripheralDeviceEClass, FSMPeripheralDevice.class, "FSMPeripheralDevice", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmPeripheralDeviceDisplayEClass, FSMPeripheralDeviceDisplay.class, "FSMPeripheralDeviceDisplay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMPeripheralDeviceDisplay_Output(), this.getFSMControlDTOAttribute(), null, "output", null, 0, 1, FSMPeripheralDeviceDisplay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmPeripheralDeviceLineDisplayEClass, FSMPeripheralDeviceLineDisplay.class, "FSMPeripheralDeviceLineDisplay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmPeripheralDevicePOSPrinterEClass, FSMPeripheralDevicePOSPrinter.class, "FSMPeripheralDevicePOSPrinter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmPeripheralDeviceCashDrawerEClass, FSMPeripheralDeviceCashDrawer.class, "FSMPeripheralDeviceCashDrawer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmPeripheralDevicePTEClass, FSMPeripheralDevicePT.class, "FSMPeripheralDevicePT", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmPeripheralDeviceSignatureEClass, FSMPeripheralDeviceSignature.class, "FSMPeripheralDeviceSignature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmPeripheralDeviceScaleEClass, FSMPeripheralDeviceScale.class, "FSMPeripheralDeviceScale", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmControlSchedulerAttributeEClass, FSMControlSchedulerAttribute.class, "FSMControlSchedulerAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMControlSchedulerAttribute_Delay(), theEcorePackage.getEInt(), "delay", null, 0, 1, FSMControlSchedulerAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMControlSchedulerAttribute_Event(), this.getFSMEvent(), null, "event", null, 0, 1, FSMControlSchedulerAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmEventEClass, FSMEvent.class, "FSMEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmStateEClass, FSMState.class, "FSMState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMState_Triggers(), this.getFSMTrigger(), null, "triggers", null, 0, -1, FSMState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMState_Conditions(), this.getFSMAction(), null, "conditions", null, 0, -1, FSMState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMState_Identity(), this.getFSMOperation(), null, "identity", null, 0, 1, FSMState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMState_Keystroke(), this.getFSMControlFieldAttribute(), null, "keystroke", null, 0, 1, FSMState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMState_HasKeyOperation(), theEcorePackage.getEBoolean(), "hasKeyOperation", null, 0, 1, FSMState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMState_KeyOperation(), this.getFSMOperation(), null, "keyOperation", null, 0, 1, FSMState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMState_KeyMapper(), this.getFSMKeyMapper(), null, "keyMapper", null, 0, -1, FSMState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmKeyMapperEClass, FSMKeyMapper.class, "FSMKeyMapper", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMKeyMapper_KeyCode(), this.getFSMFunctionalKeyCodes(), "keyCode", null, 0, 1, FSMKeyMapper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMKeyMapper_KeyEvent(), this.getFSMEvent(), null, "keyEvent", null, 0, 1, FSMKeyMapper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmTriggerEClass, FSMTrigger.class, "FSMTrigger", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMTrigger_HasTransition(), theEcorePackage.getEBoolean(), "hasTransition", null, 0, 1, FSMTrigger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMTrigger_Transition(), this.getFSMState(), null, "transition", null, 0, 1, FSMTrigger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMTrigger_Triggers(), this.getFSMEvent(), null, "triggers", null, 0, -1, FSMTrigger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMTrigger_Guards(), this.getFSMGuard(), null, "guards", null, 0, -1, FSMTrigger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMTrigger_Actions(), this.getFSMAction(), null, "actions", null, 0, -1, FSMTrigger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionEClass, FSMAction.class, "FSMAction", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmActionPeripheralBlinkRateEClass, FSMActionPeripheralBlinkRate.class, "FSMActionPeripheralBlinkRate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralBlinkRate_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralBlinkRate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralBlinkRate_BlinkRate(), theEcorePackage.getEInt(), "blinkRate", null, 0, 1, FSMActionPeripheralBlinkRate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralClearEClass, FSMActionPeripheralClear.class, "FSMActionPeripheralClear", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralClear_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralClear.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralCreateWindowEClass, FSMActionPeripheralCreateWindow.class, "FSMActionPeripheralCreateWindow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralCreateWindow_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralCreateWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralCreateWindow_ViewportRow(), theEcorePackage.getEInt(), "viewportRow", null, 0, 1, FSMActionPeripheralCreateWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralCreateWindow_ViewportColumn(), theEcorePackage.getEInt(), "viewportColumn", null, 0, 1, FSMActionPeripheralCreateWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralCreateWindow_ViewportHeight(), theEcorePackage.getEInt(), "viewportHeight", null, 0, 1, FSMActionPeripheralCreateWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralCreateWindow_ViewportWidth(), theEcorePackage.getEInt(), "viewportWidth", null, 0, 1, FSMActionPeripheralCreateWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralCreateWindow_WindowHeight(), theEcorePackage.getEInt(), "windowHeight", null, 0, 1, FSMActionPeripheralCreateWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralCreateWindow_WindowWidth(), theEcorePackage.getEInt(), "windowWidth", null, 0, 1, FSMActionPeripheralCreateWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralCursorTypeEClass, FSMActionPeripheralCursorType.class, "FSMActionPeripheralCursorType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralCursorType_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralCursorType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralCursorType_CursorType(), this.getFSMLineDisplayCursorType(), "cursorType", null, 0, 1, FSMActionPeripheralCursorType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralDestroyWindowEClass, FSMActionPeripheralDestroyWindow.class, "FSMActionPeripheralDestroyWindow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralDestroyWindow_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralDestroyWindow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralDeviceBrightnessEClass, FSMActionPeripheralDeviceBrightness.class, "FSMActionPeripheralDeviceBrightness", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralDeviceBrightness_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralDeviceBrightness.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralDeviceBrightness_Brightness(), theEcorePackage.getEInt(), "brightness", null, 0, 1, FSMActionPeripheralDeviceBrightness.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralDisplayTextEClass, FSMActionPeripheralDisplayText.class, "FSMActionPeripheralDisplayText", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralDisplayText_Device(), this.getFSMPeripheralDeviceDisplay(), null, "device", null, 0, 1, FSMActionPeripheralDisplayText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralDisplayText_Attribute(), theOSBPDtoPackage.getLDtoAttribute(), null, "attribute", null, 0, 1, FSMActionPeripheralDisplayText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralDisplayText_Text(), this.getFSMActionFieldConcatenation(), null, "text", null, 0, 1, FSMActionPeripheralDisplayText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralLineDisplayTextEClass, FSMActionPeripheralLineDisplayText.class, "FSMActionPeripheralLineDisplayText", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralLineDisplayText_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralLineDisplayText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralLineDisplayText_Text(), this.getFSMActionFieldConcatenation(), null, "text", null, 0, 1, FSMActionPeripheralLineDisplayText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralLineDisplayText_HasType(), theEcorePackage.getEBoolean(), "hasType", null, 0, 1, FSMActionPeripheralLineDisplayText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralLineDisplayText_TextType(), this.getFSMLineDisplayTextType(), "textType", null, 0, 1, FSMActionPeripheralLineDisplayText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralLineDisplayTextAtEClass, FSMActionPeripheralLineDisplayTextAt.class, "FSMActionPeripheralLineDisplayTextAt", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralLineDisplayTextAt_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralLineDisplayTextAt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralLineDisplayTextAt_Row(), theEcorePackage.getEInt(), "row", null, 0, 1, FSMActionPeripheralLineDisplayTextAt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralLineDisplayTextAt_Column(), theEcorePackage.getEInt(), "column", null, 0, 1, FSMActionPeripheralLineDisplayTextAt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralLineDisplayTextAt_Text(), this.getFSMActionFieldConcatenation(), null, "text", null, 0, 1, FSMActionPeripheralLineDisplayTextAt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralLineDisplayTextAt_HasType(), theEcorePackage.getEBoolean(), "hasType", null, 0, 1, FSMActionPeripheralLineDisplayTextAt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralLineDisplayTextAt_TextType(), this.getFSMLineDisplayTextType(), "textType", null, 0, 1, FSMActionPeripheralLineDisplayTextAt.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralInterCharacterWaitEClass, FSMActionPeripheralInterCharacterWait.class, "FSMActionPeripheralInterCharacterWait", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralInterCharacterWait_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralInterCharacterWait.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralInterCharacterWait_Wait(), theEcorePackage.getEInt(), "wait", null, 0, 1, FSMActionPeripheralInterCharacterWait.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralMarqueeFormatEClass, FSMActionPeripheralMarqueeFormat.class, "FSMActionPeripheralMarqueeFormat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralMarqueeFormat_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralMarqueeFormat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralMarqueeFormat_Format(), this.getFSMLineDisplayMarqueeFormat(), "format", null, 0, 1, FSMActionPeripheralMarqueeFormat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralMarqueeRepeatWaitEClass, FSMActionPeripheralMarqueeRepeatWait.class, "FSMActionPeripheralMarqueeRepeatWait", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralMarqueeRepeatWait_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralMarqueeRepeatWait.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralMarqueeRepeatWait_Wait(), theEcorePackage.getEInt(), "wait", null, 0, 1, FSMActionPeripheralMarqueeRepeatWait.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralMarqueeTypeEClass, FSMActionPeripheralMarqueeType.class, "FSMActionPeripheralMarqueeType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralMarqueeType_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralMarqueeType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralMarqueeType_MarqueeType(), this.getFSMLineDisplayMarqueeType(), "marqueeType", null, 0, 1, FSMActionPeripheralMarqueeType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralMarqueeUnitWaitEClass, FSMActionPeripheralMarqueeUnitWait.class, "FSMActionPeripheralMarqueeUnitWait", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralMarqueeUnitWait_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralMarqueeUnitWait.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralMarqueeUnitWait_Wait(), theEcorePackage.getEInt(), "wait", null, 0, 1, FSMActionPeripheralMarqueeUnitWait.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralScrollEClass, FSMActionPeripheralScroll.class, "FSMActionPeripheralScroll", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralScroll_Device(), this.getFSMPeripheralDeviceLineDisplay(), null, "device", null, 0, 1, FSMActionPeripheralScroll.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralScroll_Direction(), this.getFSMLineDisplayScrollTextType(), "direction", null, 0, 1, FSMActionPeripheralScroll.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralScroll_Units(), theEcorePackage.getEInt(), "units", null, 0, 1, FSMActionPeripheralScroll.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralOpenDrawerEClass, FSMActionPeripheralOpenDrawer.class, "FSMActionPeripheralOpenDrawer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralOpenDrawer_Device(), this.getFSMPeripheralDeviceCashDrawer(), null, "device", null, 0, 1, FSMActionPeripheralOpenDrawer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPrintBarcodeEClass, FSMActionPeripheralPrintBarcode.class, "FSMActionPeripheralPrintBarcode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPrintBarcode_Device(), this.getFSMPeripheralDevicePOSPrinter(), null, "device", null, 0, 1, FSMActionPeripheralPrintBarcode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralPrintBarcode_Data(), theEcorePackage.getEString(), "data", null, 0, 1, FSMActionPeripheralPrintBarcode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralPrintBarcode_BarcodeType(), this.getFSMPOSPrinterBarcodeType(), "barcodeType", null, 0, 1, FSMActionPeripheralPrintBarcode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPrintBitmapEClass, FSMActionPeripheralPrintBitmap.class, "FSMActionPeripheralPrintBitmap", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPrintBitmap_Device(), this.getFSMPeripheralDevicePOSPrinter(), null, "device", null, 0, 1, FSMActionPeripheralPrintBitmap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralPrintBitmap_BitmapId(), theEcorePackage.getEInt(), "bitmapId", null, 0, 1, FSMActionPeripheralPrintBitmap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPrintCutEClass, FSMActionPeripheralPrintCut.class, "FSMActionPeripheralPrintCut", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPrintCut_Device(), this.getFSMPeripheralDevicePOSPrinter(), null, "device", null, 0, 1, FSMActionPeripheralPrintCut.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralPrintCut_Text(), this.getFSMActionFieldConcatenation(), null, "text", null, 0, 1, FSMActionPeripheralPrintCut.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPrintRegisterBitmapEClass, FSMActionPeripheralPrintRegisterBitmap.class, "FSMActionPeripheralPrintRegisterBitmap", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMActionPeripheralPrintRegisterBitmap_BitmapId(), theEcorePackage.getEInt(), "bitmapId", null, 0, 1, FSMActionPeripheralPrintRegisterBitmap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralPrintRegisterBitmap_Name(), theEcorePackage.getEString(), "name", null, 0, 1, FSMActionPeripheralPrintRegisterBitmap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPrintNormalEClass, FSMActionPeripheralPrintNormal.class, "FSMActionPeripheralPrintNormal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPrintNormal_Device(), this.getFSMPeripheralDevicePOSPrinter(), null, "device", null, 0, 1, FSMActionPeripheralPrintNormal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralPrintNormal_Text(), this.getFSMActionFieldConcatenation(), null, "text", null, 0, 1, FSMActionPeripheralPrintNormal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralPrintNormal_BarcodeType(), this.getFSMPOSPrinterBarcodeType(), "barcodeType", null, 0, 1, FSMActionPeripheralPrintNormal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPTOpenEClass, FSMActionPeripheralPTOpen.class, "FSMActionPeripheralPTOpen", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPTOpen_Device(), this.getFSMPeripheralDevicePT(), null, "device", null, 0, 1, FSMActionPeripheralPTOpen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralPTOpen_Host(), this.getFSMActionFieldConcatenation(), null, "host", null, 0, 1, FSMActionPeripheralPTOpen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralPTOpen_Port(), this.getFSMActionFieldConcatenation(), null, "port", null, 0, 1, FSMActionPeripheralPTOpen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPTCloseEClass, FSMActionPeripheralPTClose.class, "FSMActionPeripheralPTClose", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPTClose_Device(), this.getFSMPeripheralDevicePT(), null, "device", null, 0, 1, FSMActionPeripheralPTClose.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPTReversalEClass, FSMActionPeripheralPTReversal.class, "FSMActionPeripheralPTReversal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPTReversal_Device(), this.getFSMPeripheralDevicePT(), null, "device", null, 0, 1, FSMActionPeripheralPTReversal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralPTReversal_Password(), this.getFSMActionFieldConcatenation(), null, "password", null, 0, 1, FSMActionPeripheralPTReversal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralPTReversal_Receipt(), this.getFSMActionFieldConcatenation(), null, "receipt", null, 0, 1, FSMActionPeripheralPTReversal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPTAcknowledgeEClass, FSMActionPeripheralPTAcknowledge.class, "FSMActionPeripheralPTAcknowledge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPTAcknowledge_Device(), this.getFSMPeripheralDevicePT(), null, "device", null, 0, 1, FSMActionPeripheralPTAcknowledge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPTRegistrationEClass, FSMActionPeripheralPTRegistration.class, "FSMActionPeripheralPTRegistration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPTRegistration_Device(), this.getFSMPeripheralDevicePT(), null, "device", null, 0, 1, FSMActionPeripheralPTRegistration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralPTRegistration_Password(), this.getFSMActionFieldConcatenation(), null, "password", null, 0, 1, FSMActionPeripheralPTRegistration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralPTRegistration_Configuration(), theEcorePackage.getEString(), "configuration", null, 0, 1, FSMActionPeripheralPTRegistration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPTAuthorizationEClass, FSMActionPeripheralPTAuthorization.class, "FSMActionPeripheralPTAuthorization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPTAuthorization_Device(), this.getFSMPeripheralDevicePT(), null, "device", null, 0, 1, FSMActionPeripheralPTAuthorization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralPTAuthorization_Amount(), this.getFSMActionFieldConcatenation(), null, "amount", null, 0, 1, FSMActionPeripheralPTAuthorization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralBeeperEClass, FSMActionPeripheralBeeper.class, "FSMActionPeripheralBeeper", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMActionPeripheralBeeper_Duration(), theEcorePackage.getEInt(), "duration", null, 0, 1, FSMActionPeripheralBeeper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralBeeper_Frequency(), theEcorePackage.getEInt(), "frequency", null, 0, 1, FSMActionPeripheralBeeper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPlayerEClass, FSMActionPeripheralPlayer.class, "FSMActionPeripheralPlayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMActionPeripheralPlayer_Tune(), theEcorePackage.getEString(), "tune", null, 0, 1, FSMActionPeripheralPlayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralSoundEClass, FSMActionPeripheralSound.class, "FSMActionPeripheralSound", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMActionPeripheralSound_Name(), theEcorePackage.getEString(), "name", null, 0, 1, FSMActionPeripheralSound.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPTResponseEClass, FSMActionPeripheralPTResponse.class, "FSMActionPeripheralPTResponse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPTResponse_Device(), this.getFSMPeripheralDevicePT(), null, "device", null, 0, 1, FSMActionPeripheralPTResponse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralPrintReportEClass, FSMActionPeripheralPrintReport.class, "FSMActionPeripheralPrintReport", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralPrintReport_Report(), theReportDSLPackage.getReport(), null, "report", null, 0, 1, FSMActionPeripheralPrintReport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralPrintReport_Key(), this.getFSMStorage(), null, "key", null, 0, 1, FSMActionPeripheralPrintReport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralPrintReport_HasFilter(), theEcorePackage.getEBoolean(), "hasFilter", null, 0, 1, FSMActionPeripheralPrintReport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralPrintReport_HasPrintService(), theEcorePackage.getEBoolean(), "hasPrintService", null, 0, 1, FSMActionPeripheralPrintReport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralPrintReport_PrintService(), this.getFSMActionFieldConcatenation(), null, "printService", null, 0, 1, FSMActionPeripheralPrintReport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralSignatureOpenEClass, FSMActionPeripheralSignatureOpen.class, "FSMActionPeripheralSignatureOpen", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralSignatureOpen_Device(), this.getFSMPeripheralDeviceSignature(), null, "device", null, 0, 1, FSMActionPeripheralSignatureOpen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralSignatureCloseEClass, FSMActionPeripheralSignatureClose.class, "FSMActionPeripheralSignatureClose", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralSignatureClose_Device(), this.getFSMPeripheralDeviceSignature(), null, "device", null, 0, 1, FSMActionPeripheralSignatureClose.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralSignatureClearEClass, FSMActionPeripheralSignatureClear.class, "FSMActionPeripheralSignatureClear", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralSignatureClear_Device(), this.getFSMPeripheralDeviceSignature(), null, "device", null, 0, 1, FSMActionPeripheralSignatureClear.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralSignatureCaptureEClass, FSMActionPeripheralSignatureCapture.class, "FSMActionPeripheralSignatureCapture", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralSignatureCapture_Device(), this.getFSMPeripheralDeviceSignature(), null, "device", null, 0, 1, FSMActionPeripheralSignatureCapture.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralSignatureIdleEClass, FSMActionPeripheralSignatureIdle.class, "FSMActionPeripheralSignatureIdle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralSignatureIdle_Device(), this.getFSMPeripheralDeviceSignature(), null, "device", null, 0, 1, FSMActionPeripheralSignatureIdle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralSignatureLabelEClass, FSMActionPeripheralSignatureLabel.class, "FSMActionPeripheralSignatureLabel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMActionPeripheralSignatureLabel_OkLabel(), theEcorePackage.getEString(), "okLabel", null, 0, 1, FSMActionPeripheralSignatureLabel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralSignatureLabel_ClearLabel(), theEcorePackage.getEString(), "clearLabel", null, 0, 1, FSMActionPeripheralSignatureLabel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionPeripheralSignatureLabel_CancelLabel(), theEcorePackage.getEString(), "cancelLabel", null, 0, 1, FSMActionPeripheralSignatureLabel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralSignatureLabel_Device(), this.getFSMPeripheralDeviceSignature(), null, "device", null, 0, 1, FSMActionPeripheralSignatureLabel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmSignatureRetrieveEClass, FSMSignatureRetrieve.class, "FSMSignatureRetrieve", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMSignatureRetrieve_Device(), this.getFSMPeripheralDeviceSignature(), null, "device", null, 0, 1, FSMSignatureRetrieve.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralScaleReadWeightEClass, FSMActionPeripheralScaleReadWeight.class, "FSMActionPeripheralScaleReadWeight", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralScaleReadWeight_Device(), this.getFSMPeripheralDeviceScale(), null, "device", null, 0, 1, FSMActionPeripheralScaleReadWeight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralScaleReadTareWeightEClass, FSMActionPeripheralScaleReadTareWeight.class, "FSMActionPeripheralScaleReadTareWeight", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralScaleReadTareWeight_Device(), this.getFSMPeripheralDeviceScale(), null, "device", null, 0, 1, FSMActionPeripheralScaleReadTareWeight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralScaleTareWeightEClass, FSMActionPeripheralScaleTareWeight.class, "FSMActionPeripheralScaleTareWeight", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralScaleTareWeight_Device(), this.getFSMPeripheralDeviceScale(), null, "device", null, 0, 1, FSMActionPeripheralScaleTareWeight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralScaleTareWeight_Value(), this.getFSMActionFieldConcatenation(), null, "value", null, 0, 1, FSMActionPeripheralScaleTareWeight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralScaleZeroEClass, FSMActionPeripheralScaleZero.class, "FSMActionPeripheralScaleZero", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralScaleZero_Device(), this.getFSMPeripheralDeviceScale(), null, "device", null, 0, 1, FSMActionPeripheralScaleZero.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralScaleDisplayTextEClass, FSMActionPeripheralScaleDisplayText.class, "FSMActionPeripheralScaleDisplayText", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralScaleDisplayText_Device(), this.getFSMPeripheralDeviceScale(), null, "device", null, 0, 1, FSMActionPeripheralScaleDisplayText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionPeripheralScaleDisplayText_Text(), this.getFSMActionFieldConcatenation(), null, "text", null, 0, 1, FSMActionPeripheralScaleDisplayText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionPeripheralScaleWeightUnitEClass, FSMActionPeripheralScaleWeightUnit.class, "FSMActionPeripheralScaleWeightUnit", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionPeripheralScaleWeightUnit_Device(), this.getFSMPeripheralDeviceScale(), null, "device", null, 0, 1, FSMActionPeripheralScaleWeightUnit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldSourceEClass, FSMActionFieldSource.class, "FSMActionFieldSource", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmActionFieldSourceStringEClass, FSMActionFieldSourceString.class, "FSMActionFieldSourceString", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMActionFieldSourceString_Text(), theEcorePackage.getEString(), "text", null, 0, 1, FSMActionFieldSourceString.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldSourceNumberEClass, FSMActionFieldSourceNumber.class, "FSMActionFieldSourceNumber", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMActionFieldSourceNumber_Value(), theEcorePackage.getEDoubleObject(), "value", null, 0, 1, FSMActionFieldSourceNumber.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldSourceIntegerEClass, FSMActionFieldSourceInteger.class, "FSMActionFieldSourceInteger", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMActionFieldSourceInteger_Value(), theEcorePackage.getEInt(), "value", null, 0, 1, FSMActionFieldSourceInteger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldSourceBooleanEClass, FSMActionFieldSourceBoolean.class, "FSMActionFieldSourceBoolean", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMActionFieldSourceBoolean_Value(), theEcorePackage.getEBoolean(), "value", null, 0, 1, FSMActionFieldSourceBoolean.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldSourceEvaluateEClass, FSMActionFieldSourceEvaluate.class, "FSMActionFieldSourceEvaluate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMActionFieldSourceEvaluate_Evaluationtype(), this.getFSMEvaluationType(), "evaluationtype", null, 0, 1, FSMActionFieldSourceEvaluate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldSourceTranslateEClass, FSMActionFieldSourceTranslate.class, "FSMActionFieldSourceTranslate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMActionFieldSourceTranslate_Text(), theEcorePackage.getEString(), "text", null, 0, 1, FSMActionFieldSourceTranslate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmRefEClass, FSMRef.class, "FSMRef", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmDotExpressionEClass, FSMDotExpression.class, "FSMDotExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMDotExpression_Ref(), this.getFSMRef(), null, "ref", null, 0, 1, FSMDotExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMDotExpression_Tail(), theOSBPTypesPackage.getLFeature(), null, "tail", null, 0, 1, FSMDotExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmDtoRefEClass, FSMDtoRef.class, "FSMDtoRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMDtoRef_Dto(), this.getFSMControlDTOAttribute(), null, "dto", null, 0, 1, FSMDtoRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldSourceDtoAttributeEClass, FSMActionFieldSourceDtoAttribute.class, "FSMActionFieldSourceDtoAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionFieldSourceDtoAttribute_Dto(), this.getFSMControlDTOAttribute(), null, "dto", null, 0, 1, FSMActionFieldSourceDtoAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionFieldSourceDtoAttribute_Attribute(), theOSBPDtoPackage.getLDtoInheritedAttribute(), null, "attribute", null, 0, 1, FSMActionFieldSourceDtoAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldSourceEventEClass, FSMActionFieldSourceEvent.class, "FSMActionFieldSourceEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmActionConditionalTransitionEClass, FSMActionConditionalTransition.class, "FSMActionConditionalTransition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionConditionalTransition_Transition(), this.getFSMState(), null, "transition", null, 0, 1, FSMActionConditionalTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionConditionalTransition_Guard(), this.getFSMGuard(), null, "guard", null, 0, 1, FSMActionConditionalTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionConditionalTransition_Actions(), this.getFSMAction(), null, "actions", null, 0, -1, FSMActionConditionalTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmOperationParameterEClass, FSMOperationParameter.class, "FSMOperationParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMOperationParameter_Source(), this.getFSMActionFieldConcatenation(), null, "source", null, 0, 1, FSMOperationParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmOperationEClass, FSMOperation.class, "FSMOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMOperation_Group(), theFunctionLibraryDSLPackage.getFunctionLibraryStatemachineGroup(), null, "group", null, 0, 1, FSMOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMOperation_Operation(), theFunctionLibraryDSLPackage.getFunctionLibraryOperation(), null, "operation", null, 0, 1, FSMOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMOperation_First(), this.getFSMOperationParameter(), null, "first", null, 0, 1, FSMOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMOperation_More(), this.getFSMOperationParameter(), null, "more", null, 0, -1, FSMOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmGuardEClass, FSMGuard.class, "FSMGuard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMGuard_Group(), theFunctionLibraryDSLPackage.getFunctionLibraryStatemachineGroup(), null, "group", null, 0, 1, FSMGuard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMGuard_Guard(), theFunctionLibraryDSLPackage.getFunctionLibraryGuard(), null, "guard", null, 0, 1, FSMGuard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMGuard_HasOnFail(), theEcorePackage.getEBoolean(), "hasOnFail", null, 0, 1, FSMGuard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMGuard_OnFailDescription(), theEcorePackage.getEString(), "onFailDescription", null, 0, 1, FSMGuard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMGuard_OnFailCaption(), theEcorePackage.getEString(), "onFailCaption", null, 0, 1, FSMGuard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMGuard_OnFailType(), this.getFSMUserMessageType(), "onFailType", null, 0, 1, FSMGuard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmFunctionEClass, FSMFunction.class, "FSMFunction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMFunction_Group(), theFunctionLibraryDSLPackage.getFunctionLibraryStatemachineGroup(), null, "group", null, 0, 1, FSMFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMFunction_Function(), theFunctionLibraryDSLPackage.getFunctionLibraryFunction(), null, "function", null, 0, 1, FSMFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMFunction_First(), this.getFSMOperationParameter(), null, "first", null, 0, 1, FSMFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMFunction_More(), this.getFSMOperationParameter(), null, "more", null, 0, -1, FSMFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmStorageRetrieveEClass, FSMStorageRetrieve.class, "FSMStorageRetrieve", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMStorageRetrieve_Key(), theEcorePackage.getEString(), "key", null, 0, 1, FSMStorageRetrieve.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMStorageRetrieve_Attribute(), theEcorePackage.getEString(), "attribute", null, 0, 1, FSMStorageRetrieve.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmStorageEClass, FSMStorage.class, "FSMStorage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMStorage_Key(), theEcorePackage.getEString(), "key", null, 0, 1, FSMStorage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMStorage_Attribute(), theEcorePackage.getEString(), "attribute", null, 0, 1, FSMStorage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMStorage_Content(), this.getFSMActionFieldConcatenation(), null, "content", null, 0, 1, FSMStorage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldConcatenationEClass, FSMActionFieldConcatenation.class, "FSMActionFieldConcatenation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionFieldConcatenation_First(), this.getFSMActionFieldSource(), null, "first", null, 0, 1, FSMActionFieldConcatenation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionFieldConcatenation_More(), this.getFSMActionFieldSource(), null, "more", null, 0, -1, FSMActionFieldConcatenation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldSetEClass, FSMActionFieldSet.class, "FSMActionFieldSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionFieldSet_Attribute(), this.getFSMControlFieldAttribute(), null, "attribute", null, 0, 1, FSMActionFieldSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionFieldSet_Source(), this.getFSMActionFieldConcatenation(), null, "source", null, 0, 1, FSMActionFieldSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldKeystrokeEClass, FSMActionFieldKeystroke.class, "FSMActionFieldKeystroke", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionFieldKeystroke_Attribute(), this.getFSMControlFieldAttribute(), null, "attribute", null, 0, 1, FSMActionFieldKeystroke.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionFieldKeystroke_Keystroke(), theEcorePackage.getEString(), "keystroke", null, 0, 1, FSMActionFieldKeystroke.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldClearEClass, FSMActionFieldClear.class, "FSMActionFieldClear", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionFieldClear_Attribute(), this.getFSMControlFieldAttribute(), null, "attribute", null, 0, 1, FSMActionFieldClear.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldGetEClass, FSMActionFieldGet.class, "FSMActionFieldGet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionFieldGet_Attribute(), this.getFSMControlFieldAttribute(), null, "attribute", null, 0, 1, FSMActionFieldGet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldFilterToggleEClass, FSMActionFieldFilterToggle.class, "FSMActionFieldFilterToggle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionFieldFilterToggle_Filter(), this.getFSMControlFilter(), null, "filter", null, 0, 1, FSMActionFieldFilterToggle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionFieldRemoveEClass, FSMActionFieldRemove.class, "FSMActionFieldRemove", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionFieldRemove_Attribute(), this.getFSMControlFieldAttribute(), null, "attribute", null, 0, 1, FSMActionFieldRemove.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionItemVisibleEClass, FSMActionItemVisible.class, "FSMActionItemVisible", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionItemVisible_Attribute(), this.getFSMControlVisibility(), null, "attribute", null, 0, 1, FSMActionItemVisible.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionItemInvisibleEClass, FSMActionItemInvisible.class, "FSMActionItemInvisible", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionItemInvisible_Attribute(), this.getFSMControlVisibility(), null, "attribute", null, 0, 1, FSMActionItemInvisible.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionButtonCaptionEClass, FSMActionButtonCaption.class, "FSMActionButtonCaption", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionButtonCaption_Attribute(), this.getFSMControlButtonAttribute(), null, "attribute", null, 0, 1, FSMActionButtonCaption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionButtonCaption_Caption(), this.getFSMActionFieldConcatenation(), null, "caption", null, 0, 1, FSMActionButtonCaption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionButtonImageEClass, FSMActionButtonImage.class, "FSMActionButtonImage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionButtonImage_Attribute(), this.getFSMControlButtonAttribute(), null, "attribute", null, 0, 1, FSMActionButtonImage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMActionButtonImage_Image(), theEcorePackage.getEString(), "image", null, 0, 1, FSMActionButtonImage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionDTOFindEClass, FSMActionDTOFind.class, "FSMActionDTOFind", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionDTOFind_Dto(), this.getFSMControlDTOAttribute(), null, "dto", null, 0, 1, FSMActionDTOFind.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionDTOFind_Attribute(), theOSBPDtoPackage.getLDtoInheritedAttribute(), null, "attribute", null, 0, 1, FSMActionDTOFind.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMActionDTOFind_Search(), this.getFSMActionFieldConcatenation(), null, "search", null, 0, 1, FSMActionDTOFind.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionDTOClearEClass, FSMActionDTOClear.class, "FSMActionDTOClear", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionDTOClear_Dto(), this.getFSMControlDTOAttribute(), null, "dto", null, 0, 1, FSMActionDTOClear.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmActionSchedulerEClass, FSMActionScheduler.class, "FSMActionScheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMActionScheduler_Scheduler(), this.getFSMControlSchedulerAttribute(), null, "scheduler", null, 0, 1, FSMActionScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmdtoTypeEClass, FSMDTOType.class, "FSMDTOType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMDTOType_AttributeType(), theOSBPDtoPackage.getLDto(), null, "attributeType", null, 0, 1, FSMDTOType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmFieldTypeEClass, FSMFieldType.class, "FSMFieldType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFSMFieldType_AttributeType(), this.getFSMInternalType(), "attributeType", null, 0, 1, FSMFieldType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmAbstractFilterEClass, FSMAbstractFilter.class, "FSMAbstractFilter", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmFilterPropertyEClass, FSMFilterProperty.class, "FSMFilterProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMFilterProperty_Path(), this.getFSMRef(), null, "path", null, 0, 1, FSMFilterProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmFilterEClass, FSMFilter.class, "FSMFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMFilter_Source(), this.getFSMAbstractFilter(), null, "source", null, 0, 1, FSMFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmJunctionFilterEClass, FSMJunctionFilter.class, "FSMJunctionFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMJunctionFilter_First(), this.getFSMFilter(), null, "first", null, 0, 1, FSMJunctionFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMJunctionFilter_More(), this.getFSMFilter(), null, "more", null, 0, -1, FSMJunctionFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmAndFilterEClass, FSMAndFilter.class, "FSMAndFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmOrFilterEClass, FSMOrFilter.class, "FSMOrFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fsmBetweenFilterEClass, FSMBetweenFilter.class, "FSMBetweenFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMBetweenFilter_PropertyId(), this.getFSMFilterProperty(), null, "propertyId", null, 0, 1, FSMBetweenFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMBetweenFilter_Start(), this.getFSMActionFieldSource(), null, "start", null, 0, 1, FSMBetweenFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMBetweenFilter_End(), this.getFSMActionFieldSource(), null, "end", null, 0, 1, FSMBetweenFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmCompareFilterEClass, FSMCompareFilter.class, "FSMCompareFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMCompareFilter_PropertyId(), this.getFSMFilterProperty(), null, "propertyId", null, 0, 1, FSMCompareFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMCompareFilter_Operand(), this.getFSMActionFieldSource(), null, "operand", null, 0, 1, FSMCompareFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMCompareFilter_Operation(), this.getFSMCompareOperationEnum(), "operation", null, 0, 1, FSMCompareFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmIsNullFilterEClass, FSMIsNullFilter.class, "FSMIsNullFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMIsNullFilter_PropertyId(), this.getFSMFilterProperty(), null, "propertyId", null, 0, 1, FSMIsNullFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmLikeFilterEClass, FSMLikeFilter.class, "FSMLikeFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMLikeFilter_PropertyId(), this.getFSMFilterProperty(), null, "propertyId", null, 0, 1, FSMLikeFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSMLikeFilter_Value(), this.getFSMActionFieldSource(), null, "value", null, 0, 1, FSMLikeFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMLikeFilter_IgnoreCase(), theEcorePackage.getEBoolean(), "ignoreCase", null, 0, 1, FSMLikeFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmNotFilterEClass, FSMNotFilter.class, "FSMNotFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMNotFilter_Filter(), this.getFSMFilter(), null, "filter", null, 0, 1, FSMNotFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmStringFilterEClass, FSMStringFilter.class, "FSMStringFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMStringFilter_PropertyId(), this.getFSMFilterProperty(), null, "propertyId", null, 0, 1, FSMStringFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMStringFilter_FilterString(), theEcorePackage.getEString(), "filterString", null, 0, 1, FSMStringFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMStringFilter_OnlyMatchPrefix(), theEcorePackage.getEBoolean(), "onlyMatchPrefix", null, 0, 1, FSMStringFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFSMStringFilter_IgnoreCase(), theEcorePackage.getEBoolean(), "ignoreCase", null, 0, 1, FSMStringFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fsmControlFilterEClass, FSMControlFilter.class, "FSMControlFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSMControlFilter_Filter(), this.getFSMFilter(), null, "filter", null, 0, 1, FSMControlFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(fsmInternalTypeEEnum, FSMInternalType.class, "FSMInternalType");
		addEEnumLiteral(fsmInternalTypeEEnum, FSMInternalType.BOOLEAN);
		addEEnumLiteral(fsmInternalTypeEEnum, FSMInternalType.INTEGER);
		addEEnumLiteral(fsmInternalTypeEEnum, FSMInternalType.LONG);
		addEEnumLiteral(fsmInternalTypeEEnum, FSMInternalType.DOUBLE);
		addEEnumLiteral(fsmInternalTypeEEnum, FSMInternalType.STRING);
		addEEnumLiteral(fsmInternalTypeEEnum, FSMInternalType.DATE);
		addEEnumLiteral(fsmInternalTypeEEnum, FSMInternalType.SUGGESTTEXT);
		addEEnumLiteral(fsmInternalTypeEEnum, FSMInternalType.EMBEDDABLEEVENT);

		initEEnum(fsmControlButtonEventTypeEEnum, FSMControlButtonEventType.class, "FSMControlButtonEventType");
		addEEnumLiteral(fsmControlButtonEventTypeEEnum, FSMControlButtonEventType.KEYBOARD);
		addEEnumLiteral(fsmControlButtonEventTypeEEnum, FSMControlButtonEventType.TRIGGER);
		addEEnumLiteral(fsmControlButtonEventTypeEEnum, FSMControlButtonEventType.IDENTITY);

		initEEnum(fsmCompareOperationEnumEEnum, FSMCompareOperationEnum.class, "FSMCompareOperationEnum");
		addEEnumLiteral(fsmCompareOperationEnumEEnum, FSMCompareOperationEnum.EQUAL);
		addEEnumLiteral(fsmCompareOperationEnumEEnum, FSMCompareOperationEnum.GREATER);
		addEEnumLiteral(fsmCompareOperationEnumEEnum, FSMCompareOperationEnum.LESS);
		addEEnumLiteral(fsmCompareOperationEnumEEnum, FSMCompareOperationEnum.GREATER_OR_EQUAL);
		addEEnumLiteral(fsmCompareOperationEnumEEnum, FSMCompareOperationEnum.LESS_OR_EQUAL);

		initEEnum(fsmEvaluationTypeEEnum, FSMEvaluationType.class, "FSMEvaluationType");
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.IP_ADDRESS);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.HOSTNAME);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.NOW);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.USER_AGENT_INFO);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.IS_TOUCH_DEVICE);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.IS_HTTPS);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.BROWSER_LOCALE);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.USER_NAME);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.USER_PASSWORD);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.USER_EMAIL);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.USER_POSITION);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.USER_PRTSERVICE);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.SCREEN_WIDTH);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.SCREEN_HEIGHT);
		addEEnumLiteral(fsmEvaluationTypeEEnum, FSMEvaluationType.TRIGGER);

		initEEnum(fsmUserMessageTypeEEnum, FSMUserMessageType.class, "FSMUserMessageType");
		addEEnumLiteral(fsmUserMessageTypeEEnum, FSMUserMessageType.HUMANIZED_MESSAGE);
		addEEnumLiteral(fsmUserMessageTypeEEnum, FSMUserMessageType.WARNING_MESSAGE);
		addEEnumLiteral(fsmUserMessageTypeEEnum, FSMUserMessageType.ERROR_MESSAGE);
		addEEnumLiteral(fsmUserMessageTypeEEnum, FSMUserMessageType.TRAY_NOTIFICATION);
		addEEnumLiteral(fsmUserMessageTypeEEnum, FSMUserMessageType.ASSISTIVE_NOTIFICATION);

		initEEnum(fsmLineDisplayCursorTypeEEnum, FSMLineDisplayCursorType.class, "FSMLineDisplayCursorType");
		addEEnumLiteral(fsmLineDisplayCursorTypeEEnum, FSMLineDisplayCursorType.DISP_CT_NONE);
		addEEnumLiteral(fsmLineDisplayCursorTypeEEnum, FSMLineDisplayCursorType.DISP_CT_FIXED);
		addEEnumLiteral(fsmLineDisplayCursorTypeEEnum, FSMLineDisplayCursorType.DISP_CT_BLOCK);
		addEEnumLiteral(fsmLineDisplayCursorTypeEEnum, FSMLineDisplayCursorType.DISP_CT_HALFBLOCK);
		addEEnumLiteral(fsmLineDisplayCursorTypeEEnum, FSMLineDisplayCursorType.DISP_CT_UNDERLINE);
		addEEnumLiteral(fsmLineDisplayCursorTypeEEnum, FSMLineDisplayCursorType.DISP_CT_REVERSE);
		addEEnumLiteral(fsmLineDisplayCursorTypeEEnum, FSMLineDisplayCursorType.DISP_CT_OTHER);
		addEEnumLiteral(fsmLineDisplayCursorTypeEEnum, FSMLineDisplayCursorType.DISP_CT_BLINK);

		initEEnum(fsmLineDisplayMarqueeTypeEEnum, FSMLineDisplayMarqueeType.class, "FSMLineDisplayMarqueeType");
		addEEnumLiteral(fsmLineDisplayMarqueeTypeEEnum, FSMLineDisplayMarqueeType.DISP_MT_NONE);
		addEEnumLiteral(fsmLineDisplayMarqueeTypeEEnum, FSMLineDisplayMarqueeType.DISP_MT_UP);
		addEEnumLiteral(fsmLineDisplayMarqueeTypeEEnum, FSMLineDisplayMarqueeType.DISP_MT_DOWN);
		addEEnumLiteral(fsmLineDisplayMarqueeTypeEEnum, FSMLineDisplayMarqueeType.DISP_MT_LEFT);
		addEEnumLiteral(fsmLineDisplayMarqueeTypeEEnum, FSMLineDisplayMarqueeType.DISP_MT_RIGHT);
		addEEnumLiteral(fsmLineDisplayMarqueeTypeEEnum, FSMLineDisplayMarqueeType.DISP_MT_INIT);

		initEEnum(fsmLineDisplayMarqueeFormatEEnum, FSMLineDisplayMarqueeFormat.class, "FSMLineDisplayMarqueeFormat");
		addEEnumLiteral(fsmLineDisplayMarqueeFormatEEnum, FSMLineDisplayMarqueeFormat.DISP_MF_WALK);
		addEEnumLiteral(fsmLineDisplayMarqueeFormatEEnum, FSMLineDisplayMarqueeFormat.DISP_MF_PLACE);

		initEEnum(fsmLineDisplayTextTypeEEnum, FSMLineDisplayTextType.class, "FSMLineDisplayTextType");
		addEEnumLiteral(fsmLineDisplayTextTypeEEnum, FSMLineDisplayTextType.DISP_DT_NORMAL);
		addEEnumLiteral(fsmLineDisplayTextTypeEEnum, FSMLineDisplayTextType.DISP_DT_BLINK);
		addEEnumLiteral(fsmLineDisplayTextTypeEEnum, FSMLineDisplayTextType.DISP_DT_REVERSE);
		addEEnumLiteral(fsmLineDisplayTextTypeEEnum, FSMLineDisplayTextType.DISP_DT_BLINK_REVERSE);

		initEEnum(fsmLineDisplayScrollTextTypeEEnum, FSMLineDisplayScrollTextType.class, "FSMLineDisplayScrollTextType");
		addEEnumLiteral(fsmLineDisplayScrollTextTypeEEnum, FSMLineDisplayScrollTextType.DISP_ST_UP);
		addEEnumLiteral(fsmLineDisplayScrollTextTypeEEnum, FSMLineDisplayScrollTextType.DISP_ST_DOWN);
		addEEnumLiteral(fsmLineDisplayScrollTextTypeEEnum, FSMLineDisplayScrollTextType.DISP_ST_LEFT);
		addEEnumLiteral(fsmLineDisplayScrollTextTypeEEnum, FSMLineDisplayScrollTextType.DISP_ST_RIGHT);

		initEEnum(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.class, "FSMPOSPrinterBarcodeType");
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_UPCA);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_UPCE);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_JAN8);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_EAN8);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_JAN13);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_EAN13);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_TF);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_ITF);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_CODABAR);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_CODE39);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_CODE93);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_CODE128);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_UPCA_S);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_UPCE_S);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_UPCD1);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_UPCD2);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_UPCD3);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_UPCD4);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_UPCD5);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_EAN8_S);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_EAN13_S);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_EAN128);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_OCRA);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_OCRB);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_CODE128_PARSED);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_GS1DATABAR);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_GS1DATABAR_E);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_GS1DATABAR_S);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_GS1DATABAR_ES);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_PDF417);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_MAXICODE);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_DATAMATRIX);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_QRCODE);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_UQRCODE);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_AZTEC);
		addEEnumLiteral(fsmposPrinterBarcodeTypeEEnum, FSMPOSPrinterBarcodeType.PTR_BCS_UPDF417);

		initEEnum(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.class, "FSMFunctionalKeyCodes");
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.BACKSPACE);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.TAB);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.ENTER);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.ESC);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.PGUP);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.PGDOWN);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.END);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.HOME);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.LEFTARROW);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.UPARROW);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.RIGHTARROW);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.DOWNARROW);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.INSERT);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.DELETE);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F1);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F2);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F3);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F4);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F5);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F6);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F7);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F8);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F9);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F10);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F11);
		addEEnumLiteral(fsmFunctionalKeyCodesEEnum, FSMFunctionalKeyCodes.F12);

		// Initialize data types
		initEDataType(internalEObjectEDataType, InternalEObject.class, "InternalEObject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "rootPackage", "statemachinedsl"
		   });
	}

} //StatemachineDSLPackageImpl
