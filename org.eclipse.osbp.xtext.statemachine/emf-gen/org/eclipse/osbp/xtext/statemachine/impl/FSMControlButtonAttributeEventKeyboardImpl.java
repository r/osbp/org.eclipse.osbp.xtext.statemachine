/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Control Button Attribute Event Keyboard</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeEventKeyboardImpl#getKeystroke <em>Keystroke</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMControlButtonAttributeEventKeyboardImpl extends FSMLazyResolverImpl implements FSMControlButtonAttributeEventKeyboard {
	/**
	 * The default value of the '{@link #getKeystroke() <em>Keystroke</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeystroke()
	 * @generated
	 * @ordered
	 */
	protected static final String KEYSTROKE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getKeystroke() <em>Keystroke</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeystroke()
	 * @generated
	 * @ordered
	 */
	protected String keystroke = KEYSTROKE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMControlButtonAttributeEventKeyboardImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getKeystroke() {
		return keystroke;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKeystroke(String newKeystroke) {
		String oldKeystroke = keystroke;
		keystroke = newKeystroke;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD__KEYSTROKE, oldKeystroke, keystroke));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD__KEYSTROKE:
				return getKeystroke();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD__KEYSTROKE:
				setKeystroke((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD__KEYSTROKE:
				setKeystroke(KEYSTROKE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD__KEYSTROKE:
				return KEYSTROKE_EDEFAULT == null ? keystroke != null : !KEYSTROKE_EDEFAULT.equals(keystroke);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (keystroke: ");
		result.append(keystroke);
		result.append(')');
		return result.toString();
	}

} //FSMControlButtonAttributeEventKeyboardImpl
