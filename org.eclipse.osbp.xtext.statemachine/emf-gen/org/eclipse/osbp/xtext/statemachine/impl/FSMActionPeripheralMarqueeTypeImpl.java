/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeType;
import org.eclipse.osbp.xtext.statemachine.FSMLineDisplayMarqueeType;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceLineDisplay;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Action Peripheral Marquee Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeTypeImpl#getDevice <em>Device</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralMarqueeTypeImpl#getMarqueeType <em>Marquee Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMActionPeripheralMarqueeTypeImpl extends FSMLazyResolverImpl implements FSMActionPeripheralMarqueeType {
	/**
	 * The cached value of the '{@link #getDevice() <em>Device</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDevice()
	 * @generated
	 * @ordered
	 */
	protected FSMPeripheralDeviceLineDisplay device;

	/**
	 * The default value of the '{@link #getMarqueeType() <em>Marquee Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMarqueeType()
	 * @generated
	 * @ordered
	 */
	protected static final FSMLineDisplayMarqueeType MARQUEE_TYPE_EDEFAULT = FSMLineDisplayMarqueeType.DISP_MT_NONE;

	/**
	 * The cached value of the '{@link #getMarqueeType() <em>Marquee Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMarqueeType()
	 * @generated
	 * @ordered
	 */
	protected FSMLineDisplayMarqueeType marqueeType = MARQUEE_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMActionPeripheralMarqueeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDeviceLineDisplay getDevice() {
		if (device != null && device.eIsProxy()) {
			InternalEObject oldDevice = (InternalEObject)device;
			device = (FSMPeripheralDeviceLineDisplay)eResolveProxy(oldDevice);
			if (device != oldDevice) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__DEVICE, oldDevice, device));
			}
		}
		return device;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDeviceLineDisplay basicGetDevice() {
		return device;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDevice(FSMPeripheralDeviceLineDisplay newDevice) {
		FSMPeripheralDeviceLineDisplay oldDevice = device;
		device = newDevice;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__DEVICE, oldDevice, device));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMLineDisplayMarqueeType getMarqueeType() {
		return marqueeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMarqueeType(FSMLineDisplayMarqueeType newMarqueeType) {
		FSMLineDisplayMarqueeType oldMarqueeType = marqueeType;
		marqueeType = newMarqueeType == null ? MARQUEE_TYPE_EDEFAULT : newMarqueeType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__MARQUEE_TYPE, oldMarqueeType, marqueeType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__DEVICE:
				if (resolve) return getDevice();
				return basicGetDevice();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__MARQUEE_TYPE:
				return getMarqueeType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__DEVICE:
				setDevice((FSMPeripheralDeviceLineDisplay)newValue);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__MARQUEE_TYPE:
				setMarqueeType((FSMLineDisplayMarqueeType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__DEVICE:
				setDevice((FSMPeripheralDeviceLineDisplay)null);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__MARQUEE_TYPE:
				setMarqueeType(MARQUEE_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__DEVICE:
				return device != null;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE__MARQUEE_TYPE:
				return marqueeType != MARQUEE_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (marqueeType: ");
		result.append(marqueeType);
		result.append(')');
		return result.toString();
	}

} //FSMActionPeripheralMarqueeTypeImpl
