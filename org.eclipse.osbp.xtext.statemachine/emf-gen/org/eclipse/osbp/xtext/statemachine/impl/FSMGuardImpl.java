/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryGuard;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryStatemachineGroup;

import org.eclipse.osbp.xtext.statemachine.FSMGuard;
import org.eclipse.osbp.xtext.statemachine.FSMUserMessageType;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Guard</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMGuardImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMGuardImpl#getGuard <em>Guard</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMGuardImpl#isHasOnFail <em>Has On Fail</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMGuardImpl#getOnFailDescription <em>On Fail Description</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMGuardImpl#getOnFailCaption <em>On Fail Caption</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMGuardImpl#getOnFailType <em>On Fail Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMGuardImpl extends FSMLazyResolverImpl implements FSMGuard {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FunctionLibraryStatemachineGroup group;

	/**
	 * The cached value of the '{@link #getGuard() <em>Guard</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuard()
	 * @generated
	 * @ordered
	 */
	protected FunctionLibraryGuard guard;

	/**
	 * The default value of the '{@link #isHasOnFail() <em>Has On Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasOnFail()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_ON_FAIL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasOnFail() <em>Has On Fail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasOnFail()
	 * @generated
	 * @ordered
	 */
	protected boolean hasOnFail = HAS_ON_FAIL_EDEFAULT;

	/**
	 * The default value of the '{@link #getOnFailDescription() <em>On Fail Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnFailDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String ON_FAIL_DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOnFailDescription() <em>On Fail Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnFailDescription()
	 * @generated
	 * @ordered
	 */
	protected String onFailDescription = ON_FAIL_DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getOnFailCaption() <em>On Fail Caption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnFailCaption()
	 * @generated
	 * @ordered
	 */
	protected static final String ON_FAIL_CAPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOnFailCaption() <em>On Fail Caption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnFailCaption()
	 * @generated
	 * @ordered
	 */
	protected String onFailCaption = ON_FAIL_CAPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getOnFailType() <em>On Fail Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnFailType()
	 * @generated
	 * @ordered
	 */
	protected static final FSMUserMessageType ON_FAIL_TYPE_EDEFAULT = FSMUserMessageType.HUMANIZED_MESSAGE;

	/**
	 * The cached value of the '{@link #getOnFailType() <em>On Fail Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnFailType()
	 * @generated
	 * @ordered
	 */
	protected FSMUserMessageType onFailType = ON_FAIL_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMGuardImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_GUARD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryStatemachineGroup getGroup() {
		if (group != null && group.eIsProxy()) {
			InternalEObject oldGroup = (InternalEObject)group;
			group = (FunctionLibraryStatemachineGroup)eResolveProxy(oldGroup);
			if (group != oldGroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_GUARD__GROUP, oldGroup, group));
			}
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryStatemachineGroup basicGetGroup() {
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroup(FunctionLibraryStatemachineGroup newGroup) {
		FunctionLibraryStatemachineGroup oldGroup = group;
		group = newGroup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_GUARD__GROUP, oldGroup, group));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryGuard getGuard() {
		if (guard != null && guard.eIsProxy()) {
			InternalEObject oldGuard = (InternalEObject)guard;
			guard = (FunctionLibraryGuard)eResolveProxy(oldGuard);
			if (guard != oldGuard) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_GUARD__GUARD, oldGuard, guard));
			}
		}
		return guard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryGuard basicGetGuard() {
		return guard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGuard(FunctionLibraryGuard newGuard) {
		FunctionLibraryGuard oldGuard = guard;
		guard = newGuard;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_GUARD__GUARD, oldGuard, guard));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasOnFail() {
		return hasOnFail;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasOnFail(boolean newHasOnFail) {
		boolean oldHasOnFail = hasOnFail;
		hasOnFail = newHasOnFail;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_GUARD__HAS_ON_FAIL, oldHasOnFail, hasOnFail));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOnFailDescription() {
		return onFailDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnFailDescription(String newOnFailDescription) {
		String oldOnFailDescription = onFailDescription;
		onFailDescription = newOnFailDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_GUARD__ON_FAIL_DESCRIPTION, oldOnFailDescription, onFailDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOnFailCaption() {
		return onFailCaption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnFailCaption(String newOnFailCaption) {
		String oldOnFailCaption = onFailCaption;
		onFailCaption = newOnFailCaption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_GUARD__ON_FAIL_CAPTION, oldOnFailCaption, onFailCaption));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMUserMessageType getOnFailType() {
		return onFailType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnFailType(FSMUserMessageType newOnFailType) {
		FSMUserMessageType oldOnFailType = onFailType;
		onFailType = newOnFailType == null ? ON_FAIL_TYPE_EDEFAULT : newOnFailType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_GUARD__ON_FAIL_TYPE, oldOnFailType, onFailType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_GUARD__GROUP:
				if (resolve) return getGroup();
				return basicGetGroup();
			case StatemachineDSLPackage.FSM_GUARD__GUARD:
				if (resolve) return getGuard();
				return basicGetGuard();
			case StatemachineDSLPackage.FSM_GUARD__HAS_ON_FAIL:
				return isHasOnFail();
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_DESCRIPTION:
				return getOnFailDescription();
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_CAPTION:
				return getOnFailCaption();
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_TYPE:
				return getOnFailType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_GUARD__GROUP:
				setGroup((FunctionLibraryStatemachineGroup)newValue);
				return;
			case StatemachineDSLPackage.FSM_GUARD__GUARD:
				setGuard((FunctionLibraryGuard)newValue);
				return;
			case StatemachineDSLPackage.FSM_GUARD__HAS_ON_FAIL:
				setHasOnFail((Boolean)newValue);
				return;
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_DESCRIPTION:
				setOnFailDescription((String)newValue);
				return;
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_CAPTION:
				setOnFailCaption((String)newValue);
				return;
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_TYPE:
				setOnFailType((FSMUserMessageType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_GUARD__GROUP:
				setGroup((FunctionLibraryStatemachineGroup)null);
				return;
			case StatemachineDSLPackage.FSM_GUARD__GUARD:
				setGuard((FunctionLibraryGuard)null);
				return;
			case StatemachineDSLPackage.FSM_GUARD__HAS_ON_FAIL:
				setHasOnFail(HAS_ON_FAIL_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_DESCRIPTION:
				setOnFailDescription(ON_FAIL_DESCRIPTION_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_CAPTION:
				setOnFailCaption(ON_FAIL_CAPTION_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_TYPE:
				setOnFailType(ON_FAIL_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_GUARD__GROUP:
				return group != null;
			case StatemachineDSLPackage.FSM_GUARD__GUARD:
				return guard != null;
			case StatemachineDSLPackage.FSM_GUARD__HAS_ON_FAIL:
				return hasOnFail != HAS_ON_FAIL_EDEFAULT;
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_DESCRIPTION:
				return ON_FAIL_DESCRIPTION_EDEFAULT == null ? onFailDescription != null : !ON_FAIL_DESCRIPTION_EDEFAULT.equals(onFailDescription);
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_CAPTION:
				return ON_FAIL_CAPTION_EDEFAULT == null ? onFailCaption != null : !ON_FAIL_CAPTION_EDEFAULT.equals(onFailCaption);
			case StatemachineDSLPackage.FSM_GUARD__ON_FAIL_TYPE:
				return onFailType != ON_FAIL_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (hasOnFail: ");
		result.append(hasOnFail);
		result.append(", onFailDescription: ");
		result.append(onFailDescription);
		result.append(", onFailCaption: ");
		result.append(onFailCaption);
		result.append(", onFailType: ");
		result.append(onFailType);
		result.append(')');
		return result.toString();
	}

} //FSMGuardImpl
