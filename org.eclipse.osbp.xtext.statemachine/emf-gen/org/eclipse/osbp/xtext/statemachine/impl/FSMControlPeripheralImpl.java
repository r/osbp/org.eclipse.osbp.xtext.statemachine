/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceCashDrawer;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceLineDisplay;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePOSPrinter;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePT;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceScale;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceSignature;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Control Peripheral</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlPeripheralImpl#getLineDisplays <em>Line Displays</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlPeripheralImpl#getDisplays <em>Displays</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlPeripheralImpl#getPosPrinters <em>Pos Printers</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlPeripheralImpl#getCashDrawers <em>Cash Drawers</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlPeripheralImpl#getPaymentTerminals <em>Payment Terminals</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlPeripheralImpl#getSignaturePads <em>Signature Pads</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlPeripheralImpl#getScales <em>Scales</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMControlPeripheralImpl extends FSMBaseImpl implements FSMControlPeripheral {
	/**
	 * The cached value of the '{@link #getLineDisplays() <em>Line Displays</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineDisplays()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMPeripheralDeviceLineDisplay> lineDisplays;

	/**
	 * The cached value of the '{@link #getDisplays() <em>Displays</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisplays()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMPeripheralDeviceDisplay> displays;

	/**
	 * The cached value of the '{@link #getPosPrinters() <em>Pos Printers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosPrinters()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMPeripheralDevicePOSPrinter> posPrinters;

	/**
	 * The cached value of the '{@link #getCashDrawers() <em>Cash Drawers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCashDrawers()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMPeripheralDeviceCashDrawer> cashDrawers;

	/**
	 * The cached value of the '{@link #getPaymentTerminals() <em>Payment Terminals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPaymentTerminals()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMPeripheralDevicePT> paymentTerminals;

	/**
	 * The cached value of the '{@link #getSignaturePads() <em>Signature Pads</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignaturePads()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMPeripheralDeviceSignature> signaturePads;

	/**
	 * The cached value of the '{@link #getScales() <em>Scales</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScales()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMPeripheralDeviceScale> scales;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMControlPeripheralImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_CONTROL_PERIPHERAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMPeripheralDeviceLineDisplay> getLineDisplays() {
		if (lineDisplays == null) {
			lineDisplays = new EObjectContainmentEList<FSMPeripheralDeviceLineDisplay>(FSMPeripheralDeviceLineDisplay.class, this, StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__LINE_DISPLAYS);
		}
		return lineDisplays;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMPeripheralDeviceDisplay> getDisplays() {
		if (displays == null) {
			displays = new EObjectContainmentEList<FSMPeripheralDeviceDisplay>(FSMPeripheralDeviceDisplay.class, this, StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__DISPLAYS);
		}
		return displays;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMPeripheralDevicePOSPrinter> getPosPrinters() {
		if (posPrinters == null) {
			posPrinters = new EObjectContainmentEList<FSMPeripheralDevicePOSPrinter>(FSMPeripheralDevicePOSPrinter.class, this, StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__POS_PRINTERS);
		}
		return posPrinters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMPeripheralDeviceCashDrawer> getCashDrawers() {
		if (cashDrawers == null) {
			cashDrawers = new EObjectContainmentEList<FSMPeripheralDeviceCashDrawer>(FSMPeripheralDeviceCashDrawer.class, this, StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__CASH_DRAWERS);
		}
		return cashDrawers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMPeripheralDevicePT> getPaymentTerminals() {
		if (paymentTerminals == null) {
			paymentTerminals = new EObjectContainmentEList<FSMPeripheralDevicePT>(FSMPeripheralDevicePT.class, this, StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__PAYMENT_TERMINALS);
		}
		return paymentTerminals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMPeripheralDeviceSignature> getSignaturePads() {
		if (signaturePads == null) {
			signaturePads = new EObjectContainmentEList<FSMPeripheralDeviceSignature>(FSMPeripheralDeviceSignature.class, this, StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SIGNATURE_PADS);
		}
		return signaturePads;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMPeripheralDeviceScale> getScales() {
		if (scales == null) {
			scales = new EObjectContainmentEList<FSMPeripheralDeviceScale>(FSMPeripheralDeviceScale.class, this, StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SCALES);
		}
		return scales;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__LINE_DISPLAYS:
				return ((InternalEList<?>)getLineDisplays()).basicRemove(otherEnd, msgs);
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__DISPLAYS:
				return ((InternalEList<?>)getDisplays()).basicRemove(otherEnd, msgs);
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__POS_PRINTERS:
				return ((InternalEList<?>)getPosPrinters()).basicRemove(otherEnd, msgs);
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__CASH_DRAWERS:
				return ((InternalEList<?>)getCashDrawers()).basicRemove(otherEnd, msgs);
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__PAYMENT_TERMINALS:
				return ((InternalEList<?>)getPaymentTerminals()).basicRemove(otherEnd, msgs);
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SIGNATURE_PADS:
				return ((InternalEList<?>)getSignaturePads()).basicRemove(otherEnd, msgs);
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SCALES:
				return ((InternalEList<?>)getScales()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__LINE_DISPLAYS:
				return getLineDisplays();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__DISPLAYS:
				return getDisplays();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__POS_PRINTERS:
				return getPosPrinters();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__CASH_DRAWERS:
				return getCashDrawers();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__PAYMENT_TERMINALS:
				return getPaymentTerminals();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SIGNATURE_PADS:
				return getSignaturePads();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SCALES:
				return getScales();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__LINE_DISPLAYS:
				getLineDisplays().clear();
				getLineDisplays().addAll((Collection<? extends FSMPeripheralDeviceLineDisplay>)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__DISPLAYS:
				getDisplays().clear();
				getDisplays().addAll((Collection<? extends FSMPeripheralDeviceDisplay>)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__POS_PRINTERS:
				getPosPrinters().clear();
				getPosPrinters().addAll((Collection<? extends FSMPeripheralDevicePOSPrinter>)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__CASH_DRAWERS:
				getCashDrawers().clear();
				getCashDrawers().addAll((Collection<? extends FSMPeripheralDeviceCashDrawer>)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__PAYMENT_TERMINALS:
				getPaymentTerminals().clear();
				getPaymentTerminals().addAll((Collection<? extends FSMPeripheralDevicePT>)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SIGNATURE_PADS:
				getSignaturePads().clear();
				getSignaturePads().addAll((Collection<? extends FSMPeripheralDeviceSignature>)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SCALES:
				getScales().clear();
				getScales().addAll((Collection<? extends FSMPeripheralDeviceScale>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__LINE_DISPLAYS:
				getLineDisplays().clear();
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__DISPLAYS:
				getDisplays().clear();
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__POS_PRINTERS:
				getPosPrinters().clear();
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__CASH_DRAWERS:
				getCashDrawers().clear();
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__PAYMENT_TERMINALS:
				getPaymentTerminals().clear();
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SIGNATURE_PADS:
				getSignaturePads().clear();
				return;
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SCALES:
				getScales().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__LINE_DISPLAYS:
				return lineDisplays != null && !lineDisplays.isEmpty();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__DISPLAYS:
				return displays != null && !displays.isEmpty();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__POS_PRINTERS:
				return posPrinters != null && !posPrinters.isEmpty();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__CASH_DRAWERS:
				return cashDrawers != null && !cashDrawers.isEmpty();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__PAYMENT_TERMINALS:
				return paymentTerminals != null && !paymentTerminals.isEmpty();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SIGNATURE_PADS:
				return signaturePads != null && !signaturePads.isEmpty();
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL__SCALES:
				return scales != null && !scales.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FSMControlPeripheralImpl
