/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEvent;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Control Button Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeImpl#isHasImage <em>Has Image</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeImpl#getImage <em>Image</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMControlButtonAttributeImpl#getEvent <em>Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMControlButtonAttributeImpl extends FSMBaseImpl implements FSMControlButtonAttribute {
	/**
	 * The default value of the '{@link #isHasImage() <em>Has Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasImage()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_IMAGE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasImage() <em>Has Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasImage()
	 * @generated
	 * @ordered
	 */
	protected boolean hasImage = HAS_IMAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getImage() <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImage()
	 * @generated
	 * @ordered
	 */
	protected static final String IMAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImage() <em>Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImage()
	 * @generated
	 * @ordered
	 */
	protected String image = IMAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEvent() <em>Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected FSMControlButtonAttributeEvent event;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMControlButtonAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_CONTROL_BUTTON_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasImage() {
		return hasImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasImage(boolean newHasImage) {
		boolean oldHasImage = hasImage;
		hasImage = newHasImage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__HAS_IMAGE, oldHasImage, hasImage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImage() {
		return image;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImage(String newImage) {
		String oldImage = image;
		image = newImage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__IMAGE, oldImage, image));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlButtonAttributeEvent getEvent() {
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEvent(FSMControlButtonAttributeEvent newEvent, NotificationChain msgs) {
		FSMControlButtonAttributeEvent oldEvent = event;
		event = newEvent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT, oldEvent, newEvent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(FSMControlButtonAttributeEvent newEvent) {
		if (newEvent != event) {
			NotificationChain msgs = null;
			if (event != null)
				msgs = ((InternalEObject)event).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT, null, msgs);
			if (newEvent != null)
				msgs = ((InternalEObject)newEvent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT, null, msgs);
			msgs = basicSetEvent(newEvent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT, newEvent, newEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT:
				return basicSetEvent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__HAS_IMAGE:
				return isHasImage();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__IMAGE:
				return getImage();
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT:
				return getEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__HAS_IMAGE:
				setHasImage((Boolean)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__IMAGE:
				setImage((String)newValue);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT:
				setEvent((FSMControlButtonAttributeEvent)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__HAS_IMAGE:
				setHasImage(HAS_IMAGE_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__IMAGE:
				setImage(IMAGE_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT:
				setEvent((FSMControlButtonAttributeEvent)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__HAS_IMAGE:
				return hasImage != HAS_IMAGE_EDEFAULT;
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__IMAGE:
				return IMAGE_EDEFAULT == null ? image != null : !IMAGE_EDEFAULT.equals(image);
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE__EVENT:
				return event != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (hasImage: ");
		result.append(hasImage);
		result.append(", image: ");
		result.append(image);
		result.append(')');
		return result.toString();
	}

} //FSMControlButtonAttributeImpl
