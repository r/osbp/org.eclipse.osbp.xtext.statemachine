/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.statemachine.FSMFilterProperty;
import org.eclipse.osbp.xtext.statemachine.FSMIsNullFilter;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Is Null Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMIsNullFilterImpl#getPropertyId <em>Property Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMIsNullFilterImpl extends FSMLazyResolverImpl implements FSMIsNullFilter {
	/**
	 * The cached value of the '{@link #getPropertyId() <em>Property Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyId()
	 * @generated
	 * @ordered
	 */
	protected FSMFilterProperty propertyId;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMIsNullFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_IS_NULL_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMFilterProperty getPropertyId() {
		return propertyId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPropertyId(FSMFilterProperty newPropertyId, NotificationChain msgs) {
		FSMFilterProperty oldPropertyId = propertyId;
		propertyId = newPropertyId;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_IS_NULL_FILTER__PROPERTY_ID, oldPropertyId, newPropertyId);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyId(FSMFilterProperty newPropertyId) {
		if (newPropertyId != propertyId) {
			NotificationChain msgs = null;
			if (propertyId != null)
				msgs = ((InternalEObject)propertyId).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_IS_NULL_FILTER__PROPERTY_ID, null, msgs);
			if (newPropertyId != null)
				msgs = ((InternalEObject)newPropertyId).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_IS_NULL_FILTER__PROPERTY_ID, null, msgs);
			msgs = basicSetPropertyId(newPropertyId, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_IS_NULL_FILTER__PROPERTY_ID, newPropertyId, newPropertyId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_IS_NULL_FILTER__PROPERTY_ID:
				return basicSetPropertyId(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_IS_NULL_FILTER__PROPERTY_ID:
				return getPropertyId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_IS_NULL_FILTER__PROPERTY_ID:
				setPropertyId((FSMFilterProperty)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_IS_NULL_FILTER__PROPERTY_ID:
				setPropertyId((FSMFilterProperty)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_IS_NULL_FILTER__PROPERTY_ID:
				return propertyId != null;
		}
		return super.eIsSet(featureID);
	}

} //FSMIsNullFilterImpl
