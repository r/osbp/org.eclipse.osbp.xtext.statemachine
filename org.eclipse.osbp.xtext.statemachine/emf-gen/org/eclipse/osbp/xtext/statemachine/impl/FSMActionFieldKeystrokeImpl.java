/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.statemachine.FSMActionFieldKeystroke;
import org.eclipse.osbp.xtext.statemachine.FSMControlFieldAttribute;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Action Field Keystroke</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldKeystrokeImpl#getAttribute <em>Attribute</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionFieldKeystrokeImpl#getKeystroke <em>Keystroke</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMActionFieldKeystrokeImpl extends FSMLazyResolverImpl implements FSMActionFieldKeystroke {
	/**
	 * The cached value of the '{@link #getAttribute() <em>Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute()
	 * @generated
	 * @ordered
	 */
	protected FSMControlFieldAttribute attribute;

	/**
	 * The default value of the '{@link #getKeystroke() <em>Keystroke</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeystroke()
	 * @generated
	 * @ordered
	 */
	protected static final String KEYSTROKE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getKeystroke() <em>Keystroke</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeystroke()
	 * @generated
	 * @ordered
	 */
	protected String keystroke = KEYSTROKE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMActionFieldKeystrokeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_ACTION_FIELD_KEYSTROKE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlFieldAttribute getAttribute() {
		if (attribute != null && attribute.eIsProxy()) {
			InternalEObject oldAttribute = (InternalEObject)attribute;
			attribute = (FSMControlFieldAttribute)eResolveProxy(oldAttribute);
			if (attribute != oldAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE__ATTRIBUTE, oldAttribute, attribute));
			}
		}
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlFieldAttribute basicGetAttribute() {
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttribute(FSMControlFieldAttribute newAttribute) {
		FSMControlFieldAttribute oldAttribute = attribute;
		attribute = newAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE__ATTRIBUTE, oldAttribute, attribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getKeystroke() {
		return keystroke;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKeystroke(String newKeystroke) {
		String oldKeystroke = keystroke;
		keystroke = newKeystroke;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE__KEYSTROKE, oldKeystroke, keystroke));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE__ATTRIBUTE:
				if (resolve) return getAttribute();
				return basicGetAttribute();
			case StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE__KEYSTROKE:
				return getKeystroke();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE__ATTRIBUTE:
				setAttribute((FSMControlFieldAttribute)newValue);
				return;
			case StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE__KEYSTROKE:
				setKeystroke((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE__ATTRIBUTE:
				setAttribute((FSMControlFieldAttribute)null);
				return;
			case StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE__KEYSTROKE:
				setKeystroke(KEYSTROKE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE__ATTRIBUTE:
				return attribute != null;
			case StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE__KEYSTROKE:
				return KEYSTROKE_EDEFAULT == null ? keystroke != null : !KEYSTROKE_EDEFAULT.equals(keystroke);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (keystroke: ");
		result.append(keystroke);
		result.append(')');
		return result.toString();
	}

} //FSMActionFieldKeystrokeImpl
