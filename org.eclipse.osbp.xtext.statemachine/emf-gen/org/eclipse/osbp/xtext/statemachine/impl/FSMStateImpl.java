/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.statemachine.FSMAction;
import org.eclipse.osbp.xtext.statemachine.FSMControlFieldAttribute;
import org.eclipse.osbp.xtext.statemachine.FSMKeyMapper;
import org.eclipse.osbp.xtext.statemachine.FSMOperation;
import org.eclipse.osbp.xtext.statemachine.FSMState;
import org.eclipse.osbp.xtext.statemachine.FSMTrigger;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStateImpl#getTriggers <em>Triggers</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStateImpl#getConditions <em>Conditions</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStateImpl#getIdentity <em>Identity</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStateImpl#getKeystroke <em>Keystroke</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStateImpl#isHasKeyOperation <em>Has Key Operation</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStateImpl#getKeyOperation <em>Key Operation</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMStateImpl#getKeyMapper <em>Key Mapper</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMStateImpl extends FSMBaseImpl implements FSMState {
	/**
	 * The cached value of the '{@link #getTriggers() <em>Triggers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggers()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMTrigger> triggers;

	/**
	 * The cached value of the '{@link #getConditions() <em>Conditions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMAction> conditions;

	/**
	 * The cached value of the '{@link #getIdentity() <em>Identity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentity()
	 * @generated
	 * @ordered
	 */
	protected FSMOperation identity;

	/**
	 * The cached value of the '{@link #getKeystroke() <em>Keystroke</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeystroke()
	 * @generated
	 * @ordered
	 */
	protected FSMControlFieldAttribute keystroke;

	/**
	 * The default value of the '{@link #isHasKeyOperation() <em>Has Key Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasKeyOperation()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_KEY_OPERATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasKeyOperation() <em>Has Key Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasKeyOperation()
	 * @generated
	 * @ordered
	 */
	protected boolean hasKeyOperation = HAS_KEY_OPERATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getKeyOperation() <em>Key Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyOperation()
	 * @generated
	 * @ordered
	 */
	protected FSMOperation keyOperation;

	/**
	 * The cached value of the '{@link #getKeyMapper() <em>Key Mapper</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyMapper()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMKeyMapper> keyMapper;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMTrigger> getTriggers() {
		if (triggers == null) {
			triggers = new EObjectContainmentEList<FSMTrigger>(FSMTrigger.class, this, StatemachineDSLPackage.FSM_STATE__TRIGGERS);
		}
		return triggers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMAction> getConditions() {
		if (conditions == null) {
			conditions = new EObjectContainmentEList<FSMAction>(FSMAction.class, this, StatemachineDSLPackage.FSM_STATE__CONDITIONS);
		}
		return conditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMOperation getIdentity() {
		return identity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdentity(FSMOperation newIdentity, NotificationChain msgs) {
		FSMOperation oldIdentity = identity;
		identity = newIdentity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_STATE__IDENTITY, oldIdentity, newIdentity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentity(FSMOperation newIdentity) {
		if (newIdentity != identity) {
			NotificationChain msgs = null;
			if (identity != null)
				msgs = ((InternalEObject)identity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_STATE__IDENTITY, null, msgs);
			if (newIdentity != null)
				msgs = ((InternalEObject)newIdentity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_STATE__IDENTITY, null, msgs);
			msgs = basicSetIdentity(newIdentity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_STATE__IDENTITY, newIdentity, newIdentity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlFieldAttribute getKeystroke() {
		if (keystroke != null && keystroke.eIsProxy()) {
			InternalEObject oldKeystroke = (InternalEObject)keystroke;
			keystroke = (FSMControlFieldAttribute)eResolveProxy(oldKeystroke);
			if (keystroke != oldKeystroke) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_STATE__KEYSTROKE, oldKeystroke, keystroke));
			}
		}
		return keystroke;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMControlFieldAttribute basicGetKeystroke() {
		return keystroke;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKeystroke(FSMControlFieldAttribute newKeystroke) {
		FSMControlFieldAttribute oldKeystroke = keystroke;
		keystroke = newKeystroke;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_STATE__KEYSTROKE, oldKeystroke, keystroke));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasKeyOperation() {
		return hasKeyOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasKeyOperation(boolean newHasKeyOperation) {
		boolean oldHasKeyOperation = hasKeyOperation;
		hasKeyOperation = newHasKeyOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_STATE__HAS_KEY_OPERATION, oldHasKeyOperation, hasKeyOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMOperation getKeyOperation() {
		return keyOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetKeyOperation(FSMOperation newKeyOperation, NotificationChain msgs) {
		FSMOperation oldKeyOperation = keyOperation;
		keyOperation = newKeyOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_STATE__KEY_OPERATION, oldKeyOperation, newKeyOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKeyOperation(FSMOperation newKeyOperation) {
		if (newKeyOperation != keyOperation) {
			NotificationChain msgs = null;
			if (keyOperation != null)
				msgs = ((InternalEObject)keyOperation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_STATE__KEY_OPERATION, null, msgs);
			if (newKeyOperation != null)
				msgs = ((InternalEObject)newKeyOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachineDSLPackage.FSM_STATE__KEY_OPERATION, null, msgs);
			msgs = basicSetKeyOperation(newKeyOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_STATE__KEY_OPERATION, newKeyOperation, newKeyOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMKeyMapper> getKeyMapper() {
		if (keyMapper == null) {
			keyMapper = new EObjectContainmentEList<FSMKeyMapper>(FSMKeyMapper.class, this, StatemachineDSLPackage.FSM_STATE__KEY_MAPPER);
		}
		return keyMapper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_STATE__TRIGGERS:
				return ((InternalEList<?>)getTriggers()).basicRemove(otherEnd, msgs);
			case StatemachineDSLPackage.FSM_STATE__CONDITIONS:
				return ((InternalEList<?>)getConditions()).basicRemove(otherEnd, msgs);
			case StatemachineDSLPackage.FSM_STATE__IDENTITY:
				return basicSetIdentity(null, msgs);
			case StatemachineDSLPackage.FSM_STATE__KEY_OPERATION:
				return basicSetKeyOperation(null, msgs);
			case StatemachineDSLPackage.FSM_STATE__KEY_MAPPER:
				return ((InternalEList<?>)getKeyMapper()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_STATE__TRIGGERS:
				return getTriggers();
			case StatemachineDSLPackage.FSM_STATE__CONDITIONS:
				return getConditions();
			case StatemachineDSLPackage.FSM_STATE__IDENTITY:
				return getIdentity();
			case StatemachineDSLPackage.FSM_STATE__KEYSTROKE:
				if (resolve) return getKeystroke();
				return basicGetKeystroke();
			case StatemachineDSLPackage.FSM_STATE__HAS_KEY_OPERATION:
				return isHasKeyOperation();
			case StatemachineDSLPackage.FSM_STATE__KEY_OPERATION:
				return getKeyOperation();
			case StatemachineDSLPackage.FSM_STATE__KEY_MAPPER:
				return getKeyMapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_STATE__TRIGGERS:
				getTriggers().clear();
				getTriggers().addAll((Collection<? extends FSMTrigger>)newValue);
				return;
			case StatemachineDSLPackage.FSM_STATE__CONDITIONS:
				getConditions().clear();
				getConditions().addAll((Collection<? extends FSMAction>)newValue);
				return;
			case StatemachineDSLPackage.FSM_STATE__IDENTITY:
				setIdentity((FSMOperation)newValue);
				return;
			case StatemachineDSLPackage.FSM_STATE__KEYSTROKE:
				setKeystroke((FSMControlFieldAttribute)newValue);
				return;
			case StatemachineDSLPackage.FSM_STATE__HAS_KEY_OPERATION:
				setHasKeyOperation((Boolean)newValue);
				return;
			case StatemachineDSLPackage.FSM_STATE__KEY_OPERATION:
				setKeyOperation((FSMOperation)newValue);
				return;
			case StatemachineDSLPackage.FSM_STATE__KEY_MAPPER:
				getKeyMapper().clear();
				getKeyMapper().addAll((Collection<? extends FSMKeyMapper>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_STATE__TRIGGERS:
				getTriggers().clear();
				return;
			case StatemachineDSLPackage.FSM_STATE__CONDITIONS:
				getConditions().clear();
				return;
			case StatemachineDSLPackage.FSM_STATE__IDENTITY:
				setIdentity((FSMOperation)null);
				return;
			case StatemachineDSLPackage.FSM_STATE__KEYSTROKE:
				setKeystroke((FSMControlFieldAttribute)null);
				return;
			case StatemachineDSLPackage.FSM_STATE__HAS_KEY_OPERATION:
				setHasKeyOperation(HAS_KEY_OPERATION_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_STATE__KEY_OPERATION:
				setKeyOperation((FSMOperation)null);
				return;
			case StatemachineDSLPackage.FSM_STATE__KEY_MAPPER:
				getKeyMapper().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_STATE__TRIGGERS:
				return triggers != null && !triggers.isEmpty();
			case StatemachineDSLPackage.FSM_STATE__CONDITIONS:
				return conditions != null && !conditions.isEmpty();
			case StatemachineDSLPackage.FSM_STATE__IDENTITY:
				return identity != null;
			case StatemachineDSLPackage.FSM_STATE__KEYSTROKE:
				return keystroke != null;
			case StatemachineDSLPackage.FSM_STATE__HAS_KEY_OPERATION:
				return hasKeyOperation != HAS_KEY_OPERATION_EDEFAULT;
			case StatemachineDSLPackage.FSM_STATE__KEY_OPERATION:
				return keyOperation != null;
			case StatemachineDSLPackage.FSM_STATE__KEY_MAPPER:
				return keyMapper != null && !keyMapper.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (hasKeyOperation: ");
		result.append(hasKeyOperation);
		result.append(')');
		return result.toString();
	}

} //FSMStateImpl
