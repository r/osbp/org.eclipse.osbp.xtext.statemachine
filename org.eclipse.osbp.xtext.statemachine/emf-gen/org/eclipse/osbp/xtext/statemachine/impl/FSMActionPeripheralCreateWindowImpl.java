/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow;
import org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceLineDisplay;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM Action Peripheral Create Window</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCreateWindowImpl#getDevice <em>Device</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCreateWindowImpl#getViewportRow <em>Viewport Row</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCreateWindowImpl#getViewportColumn <em>Viewport Column</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCreateWindowImpl#getViewportHeight <em>Viewport Height</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCreateWindowImpl#getViewportWidth <em>Viewport Width</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCreateWindowImpl#getWindowHeight <em>Window Height</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMActionPeripheralCreateWindowImpl#getWindowWidth <em>Window Width</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMActionPeripheralCreateWindowImpl extends FSMLazyResolverImpl implements FSMActionPeripheralCreateWindow {
	/**
	 * The cached value of the '{@link #getDevice() <em>Device</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDevice()
	 * @generated
	 * @ordered
	 */
	protected FSMPeripheralDeviceLineDisplay device;

	/**
	 * The default value of the '{@link #getViewportRow() <em>Viewport Row</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewportRow()
	 * @generated
	 * @ordered
	 */
	protected static final int VIEWPORT_ROW_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getViewportRow() <em>Viewport Row</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewportRow()
	 * @generated
	 * @ordered
	 */
	protected int viewportRow = VIEWPORT_ROW_EDEFAULT;

	/**
	 * The default value of the '{@link #getViewportColumn() <em>Viewport Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewportColumn()
	 * @generated
	 * @ordered
	 */
	protected static final int VIEWPORT_COLUMN_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getViewportColumn() <em>Viewport Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewportColumn()
	 * @generated
	 * @ordered
	 */
	protected int viewportColumn = VIEWPORT_COLUMN_EDEFAULT;

	/**
	 * The default value of the '{@link #getViewportHeight() <em>Viewport Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewportHeight()
	 * @generated
	 * @ordered
	 */
	protected static final int VIEWPORT_HEIGHT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getViewportHeight() <em>Viewport Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewportHeight()
	 * @generated
	 * @ordered
	 */
	protected int viewportHeight = VIEWPORT_HEIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #getViewportWidth() <em>Viewport Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewportWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int VIEWPORT_WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getViewportWidth() <em>Viewport Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewportWidth()
	 * @generated
	 * @ordered
	 */
	protected int viewportWidth = VIEWPORT_WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getWindowHeight() <em>Window Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWindowHeight()
	 * @generated
	 * @ordered
	 */
	protected static final int WINDOW_HEIGHT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getWindowHeight() <em>Window Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWindowHeight()
	 * @generated
	 * @ordered
	 */
	protected int windowHeight = WINDOW_HEIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #getWindowWidth() <em>Window Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWindowWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int WINDOW_WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getWindowWidth() <em>Window Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWindowWidth()
	 * @generated
	 * @ordered
	 */
	protected int windowWidth = WINDOW_WIDTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMActionPeripheralCreateWindowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM_ACTION_PERIPHERAL_CREATE_WINDOW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDeviceLineDisplay getDevice() {
		if (device != null && device.eIsProxy()) {
			InternalEObject oldDevice = (InternalEObject)device;
			device = (FSMPeripheralDeviceLineDisplay)eResolveProxy(oldDevice);
			if (device != oldDevice) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__DEVICE, oldDevice, device));
			}
		}
		return device;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMPeripheralDeviceLineDisplay basicGetDevice() {
		return device;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDevice(FSMPeripheralDeviceLineDisplay newDevice) {
		FSMPeripheralDeviceLineDisplay oldDevice = device;
		device = newDevice;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__DEVICE, oldDevice, device));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getViewportRow() {
		return viewportRow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setViewportRow(int newViewportRow) {
		int oldViewportRow = viewportRow;
		viewportRow = newViewportRow;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_ROW, oldViewportRow, viewportRow));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getViewportColumn() {
		return viewportColumn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setViewportColumn(int newViewportColumn) {
		int oldViewportColumn = viewportColumn;
		viewportColumn = newViewportColumn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_COLUMN, oldViewportColumn, viewportColumn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getViewportHeight() {
		return viewportHeight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setViewportHeight(int newViewportHeight) {
		int oldViewportHeight = viewportHeight;
		viewportHeight = newViewportHeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_HEIGHT, oldViewportHeight, viewportHeight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getViewportWidth() {
		return viewportWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setViewportWidth(int newViewportWidth) {
		int oldViewportWidth = viewportWidth;
		viewportWidth = newViewportWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_WIDTH, oldViewportWidth, viewportWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWindowHeight() {
		return windowHeight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWindowHeight(int newWindowHeight) {
		int oldWindowHeight = windowHeight;
		windowHeight = newWindowHeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_HEIGHT, oldWindowHeight, windowHeight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWindowWidth() {
		return windowWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWindowWidth(int newWindowWidth) {
		int oldWindowWidth = windowWidth;
		windowWidth = newWindowWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_WIDTH, oldWindowWidth, windowWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__DEVICE:
				if (resolve) return getDevice();
				return basicGetDevice();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_ROW:
				return getViewportRow();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_COLUMN:
				return getViewportColumn();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_HEIGHT:
				return getViewportHeight();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_WIDTH:
				return getViewportWidth();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_HEIGHT:
				return getWindowHeight();
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_WIDTH:
				return getWindowWidth();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__DEVICE:
				setDevice((FSMPeripheralDeviceLineDisplay)newValue);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_ROW:
				setViewportRow((Integer)newValue);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_COLUMN:
				setViewportColumn((Integer)newValue);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_HEIGHT:
				setViewportHeight((Integer)newValue);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_WIDTH:
				setViewportWidth((Integer)newValue);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_HEIGHT:
				setWindowHeight((Integer)newValue);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_WIDTH:
				setWindowWidth((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__DEVICE:
				setDevice((FSMPeripheralDeviceLineDisplay)null);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_ROW:
				setViewportRow(VIEWPORT_ROW_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_COLUMN:
				setViewportColumn(VIEWPORT_COLUMN_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_HEIGHT:
				setViewportHeight(VIEWPORT_HEIGHT_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_WIDTH:
				setViewportWidth(VIEWPORT_WIDTH_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_HEIGHT:
				setWindowHeight(WINDOW_HEIGHT_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_WIDTH:
				setWindowWidth(WINDOW_WIDTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__DEVICE:
				return device != null;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_ROW:
				return viewportRow != VIEWPORT_ROW_EDEFAULT;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_COLUMN:
				return viewportColumn != VIEWPORT_COLUMN_EDEFAULT;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_HEIGHT:
				return viewportHeight != VIEWPORT_HEIGHT_EDEFAULT;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__VIEWPORT_WIDTH:
				return viewportWidth != VIEWPORT_WIDTH_EDEFAULT;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_HEIGHT:
				return windowHeight != WINDOW_HEIGHT_EDEFAULT;
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW__WINDOW_WIDTH:
				return windowWidth != WINDOW_WIDTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (viewportRow: ");
		result.append(viewportRow);
		result.append(", viewportColumn: ");
		result.append(viewportColumn);
		result.append(", viewportHeight: ");
		result.append(viewportHeight);
		result.append(", viewportWidth: ");
		result.append(viewportWidth);
		result.append(", windowHeight: ");
		result.append(windowHeight);
		result.append(", windowWidth: ");
		result.append(windowWidth);
		result.append(')');
		return result.toString();
	}

} //FSMActionPeripheralCreateWindowImpl
