/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.statemachine.FSM;
import org.eclipse.osbp.xtext.statemachine.FSMControl;
import org.eclipse.osbp.xtext.statemachine.FSMEvent;
import org.eclipse.osbp.xtext.statemachine.FSMState;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FSM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMImpl#isDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMImpl#getDescriptionValue <em>Description Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMImpl#getInitialEvent <em>Initial Event</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMImpl#getInitialState <em>Initial State</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMImpl#getControls <em>Controls</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.impl.FSMImpl#getStates <em>States</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FSMImpl extends FSMBaseImpl implements FSM {
	/**
	 * The default value of the '{@link #isDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDescription()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DESCRIPTION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDescription()
	 * @generated
	 * @ordered
	 */
	protected boolean description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescriptionValue() <em>Description Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptionValue()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescriptionValue() <em>Description Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptionValue()
	 * @generated
	 * @ordered
	 */
	protected String descriptionValue = DESCRIPTION_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInitialEvent() <em>Initial Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialEvent()
	 * @generated
	 * @ordered
	 */
	protected FSMEvent initialEvent;

	/**
	 * The cached value of the '{@link #getInitialState() <em>Initial State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialState()
	 * @generated
	 * @ordered
	 */
	protected FSMState initialState;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMEvent> events;

	/**
	 * The cached value of the '{@link #getControls() <em>Controls</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControls()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMControl> controls;

	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<FSMState> states;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachineDSLPackage.Literals.FSM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(boolean newDescription) {
		boolean oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescriptionValue() {
		return descriptionValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescriptionValue(String newDescriptionValue) {
		String oldDescriptionValue = descriptionValue;
		descriptionValue = newDescriptionValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM__DESCRIPTION_VALUE, oldDescriptionValue, descriptionValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMEvent getInitialEvent() {
		if (initialEvent != null && initialEvent.eIsProxy()) {
			InternalEObject oldInitialEvent = (InternalEObject)initialEvent;
			initialEvent = (FSMEvent)eResolveProxy(oldInitialEvent);
			if (initialEvent != oldInitialEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM__INITIAL_EVENT, oldInitialEvent, initialEvent));
			}
		}
		return initialEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMEvent basicGetInitialEvent() {
		return initialEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialEvent(FSMEvent newInitialEvent) {
		FSMEvent oldInitialEvent = initialEvent;
		initialEvent = newInitialEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM__INITIAL_EVENT, oldInitialEvent, initialEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMState getInitialState() {
		if (initialState != null && initialState.eIsProxy()) {
			InternalEObject oldInitialState = (InternalEObject)initialState;
			initialState = (FSMState)eResolveProxy(oldInitialState);
			if (initialState != oldInitialState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachineDSLPackage.FSM__INITIAL_STATE, oldInitialState, initialState));
			}
		}
		return initialState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMState basicGetInitialState() {
		return initialState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialState(FSMState newInitialState) {
		FSMState oldInitialState = initialState;
		initialState = newInitialState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachineDSLPackage.FSM__INITIAL_STATE, oldInitialState, initialState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMEvent> getEvents() {
		if (events == null) {
			events = new EObjectContainmentEList<FSMEvent>(FSMEvent.class, this, StatemachineDSLPackage.FSM__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMControl> getControls() {
		if (controls == null) {
			controls = new EObjectContainmentEList<FSMControl>(FSMControl.class, this, StatemachineDSLPackage.FSM__CONTROLS);
		}
		return controls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FSMState> getStates() {
		if (states == null) {
			states = new EObjectContainmentEList<FSMState>(FSMState.class, this, StatemachineDSLPackage.FSM__STATES);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM__EVENTS:
				return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
			case StatemachineDSLPackage.FSM__CONTROLS:
				return ((InternalEList<?>)getControls()).basicRemove(otherEnd, msgs);
			case StatemachineDSLPackage.FSM__STATES:
				return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM__DESCRIPTION:
				return isDescription();
			case StatemachineDSLPackage.FSM__DESCRIPTION_VALUE:
				return getDescriptionValue();
			case StatemachineDSLPackage.FSM__INITIAL_EVENT:
				if (resolve) return getInitialEvent();
				return basicGetInitialEvent();
			case StatemachineDSLPackage.FSM__INITIAL_STATE:
				if (resolve) return getInitialState();
				return basicGetInitialState();
			case StatemachineDSLPackage.FSM__EVENTS:
				return getEvents();
			case StatemachineDSLPackage.FSM__CONTROLS:
				return getControls();
			case StatemachineDSLPackage.FSM__STATES:
				return getStates();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM__DESCRIPTION:
				setDescription((Boolean)newValue);
				return;
			case StatemachineDSLPackage.FSM__DESCRIPTION_VALUE:
				setDescriptionValue((String)newValue);
				return;
			case StatemachineDSLPackage.FSM__INITIAL_EVENT:
				setInitialEvent((FSMEvent)newValue);
				return;
			case StatemachineDSLPackage.FSM__INITIAL_STATE:
				setInitialState((FSMState)newValue);
				return;
			case StatemachineDSLPackage.FSM__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends FSMEvent>)newValue);
				return;
			case StatemachineDSLPackage.FSM__CONTROLS:
				getControls().clear();
				getControls().addAll((Collection<? extends FSMControl>)newValue);
				return;
			case StatemachineDSLPackage.FSM__STATES:
				getStates().clear();
				getStates().addAll((Collection<? extends FSMState>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM__DESCRIPTION_VALUE:
				setDescriptionValue(DESCRIPTION_VALUE_EDEFAULT);
				return;
			case StatemachineDSLPackage.FSM__INITIAL_EVENT:
				setInitialEvent((FSMEvent)null);
				return;
			case StatemachineDSLPackage.FSM__INITIAL_STATE:
				setInitialState((FSMState)null);
				return;
			case StatemachineDSLPackage.FSM__EVENTS:
				getEvents().clear();
				return;
			case StatemachineDSLPackage.FSM__CONTROLS:
				getControls().clear();
				return;
			case StatemachineDSLPackage.FSM__STATES:
				getStates().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachineDSLPackage.FSM__DESCRIPTION:
				return description != DESCRIPTION_EDEFAULT;
			case StatemachineDSLPackage.FSM__DESCRIPTION_VALUE:
				return DESCRIPTION_VALUE_EDEFAULT == null ? descriptionValue != null : !DESCRIPTION_VALUE_EDEFAULT.equals(descriptionValue);
			case StatemachineDSLPackage.FSM__INITIAL_EVENT:
				return initialEvent != null;
			case StatemachineDSLPackage.FSM__INITIAL_STATE:
				return initialState != null;
			case StatemachineDSLPackage.FSM__EVENTS:
				return events != null && !events.isEmpty();
			case StatemachineDSLPackage.FSM__CONTROLS:
				return controls != null && !controls.isEmpty();
			case StatemachineDSLPackage.FSM__STATES:
				return states != null && !states.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", descriptionValue: ");
		result.append(descriptionValue);
		result.append(')');
		return result.toString();
	}

} //FSMImpl
