/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage
 * @generated
 */
public interface StatemachineDSLFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StatemachineDSLFactory eINSTANCE = org.eclipse.osbp.xtext.statemachine.impl.StatemachineDSLFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>FSM Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Model</em>'.
	 * @generated
	 */
	FSMModel createFSMModel();

	/**
	 * Returns a new object of class '<em>FSM Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Lazy Resolver</em>'.
	 * @generated
	 */
	FSMLazyResolver createFSMLazyResolver();

	/**
	 * Returns a new object of class '<em>FSM Base</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Base</em>'.
	 * @generated
	 */
	FSMBase createFSMBase();

	/**
	 * Returns a new object of class '<em>FSM Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Package</em>'.
	 * @generated
	 */
	FSMPackage createFSMPackage();

	/**
	 * Returns a new object of class '<em>FSM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM</em>'.
	 * @generated
	 */
	FSM createFSM();

	/**
	 * Returns a new object of class '<em>FSM Control Button</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Button</em>'.
	 * @generated
	 */
	FSMControlButton createFSMControlButton();

	/**
	 * Returns a new object of class '<em>FSM Control Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Field</em>'.
	 * @generated
	 */
	FSMControlField createFSMControlField();

	/**
	 * Returns a new object of class '<em>FSM Control DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control DTO</em>'.
	 * @generated
	 */
	FSMControlDTO createFSMControlDTO();

	/**
	 * Returns a new object of class '<em>FSM Control Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Scheduler</em>'.
	 * @generated
	 */
	FSMControlScheduler createFSMControlScheduler();

	/**
	 * Returns a new object of class '<em>FSM Control Peripheral</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Peripheral</em>'.
	 * @generated
	 */
	FSMControlPeripheral createFSMControlPeripheral();

	/**
	 * Returns a new object of class '<em>FSM Control Button Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Button Attribute</em>'.
	 * @generated
	 */
	FSMControlButtonAttribute createFSMControlButtonAttribute();

	/**
	 * Returns a new object of class '<em>FSM Control Button Attribute Event Keyboard</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Button Attribute Event Keyboard</em>'.
	 * @generated
	 */
	FSMControlButtonAttributeEventKeyboard createFSMControlButtonAttributeEventKeyboard();

	/**
	 * Returns a new object of class '<em>FSM Control Button Attribute Event Identity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Button Attribute Event Identity</em>'.
	 * @generated
	 */
	FSMControlButtonAttributeEventIdentity createFSMControlButtonAttributeEventIdentity();

	/**
	 * Returns a new object of class '<em>FSM Control Button Attribute Event Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Button Attribute Event Event</em>'.
	 * @generated
	 */
	FSMControlButtonAttributeEventEvent createFSMControlButtonAttributeEventEvent();

	/**
	 * Returns a new object of class '<em>FSM Control Field Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Field Attribute</em>'.
	 * @generated
	 */
	FSMControlFieldAttribute createFSMControlFieldAttribute();

	/**
	 * Returns a new object of class '<em>FSM Control Field Layout</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Field Layout</em>'.
	 * @generated
	 */
	FSMControlFieldLayout createFSMControlFieldLayout();

	/**
	 * Returns a new object of class '<em>FSM Control DTO Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control DTO Attribute</em>'.
	 * @generated
	 */
	FSMControlDTOAttribute createFSMControlDTOAttribute();

	/**
	 * Returns a new object of class '<em>FSM Peripheral Device Display</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Peripheral Device Display</em>'.
	 * @generated
	 */
	FSMPeripheralDeviceDisplay createFSMPeripheralDeviceDisplay();

	/**
	 * Returns a new object of class '<em>FSM Peripheral Device Line Display</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Peripheral Device Line Display</em>'.
	 * @generated
	 */
	FSMPeripheralDeviceLineDisplay createFSMPeripheralDeviceLineDisplay();

	/**
	 * Returns a new object of class '<em>FSM Peripheral Device POS Printer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Peripheral Device POS Printer</em>'.
	 * @generated
	 */
	FSMPeripheralDevicePOSPrinter createFSMPeripheralDevicePOSPrinter();

	/**
	 * Returns a new object of class '<em>FSM Peripheral Device Cash Drawer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Peripheral Device Cash Drawer</em>'.
	 * @generated
	 */
	FSMPeripheralDeviceCashDrawer createFSMPeripheralDeviceCashDrawer();

	/**
	 * Returns a new object of class '<em>FSM Peripheral Device PT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Peripheral Device PT</em>'.
	 * @generated
	 */
	FSMPeripheralDevicePT createFSMPeripheralDevicePT();

	/**
	 * Returns a new object of class '<em>FSM Peripheral Device Signature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Peripheral Device Signature</em>'.
	 * @generated
	 */
	FSMPeripheralDeviceSignature createFSMPeripheralDeviceSignature();

	/**
	 * Returns a new object of class '<em>FSM Peripheral Device Scale</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Peripheral Device Scale</em>'.
	 * @generated
	 */
	FSMPeripheralDeviceScale createFSMPeripheralDeviceScale();

	/**
	 * Returns a new object of class '<em>FSM Control Scheduler Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Scheduler Attribute</em>'.
	 * @generated
	 */
	FSMControlSchedulerAttribute createFSMControlSchedulerAttribute();

	/**
	 * Returns a new object of class '<em>FSM Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Event</em>'.
	 * @generated
	 */
	FSMEvent createFSMEvent();

	/**
	 * Returns a new object of class '<em>FSM State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM State</em>'.
	 * @generated
	 */
	FSMState createFSMState();

	/**
	 * Returns a new object of class '<em>FSM Key Mapper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Key Mapper</em>'.
	 * @generated
	 */
	FSMKeyMapper createFSMKeyMapper();

	/**
	 * Returns a new object of class '<em>FSM Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Trigger</em>'.
	 * @generated
	 */
	FSMTrigger createFSMTrigger();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Blink Rate</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Blink Rate</em>'.
	 * @generated
	 */
	FSMActionPeripheralBlinkRate createFSMActionPeripheralBlinkRate();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Clear</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Clear</em>'.
	 * @generated
	 */
	FSMActionPeripheralClear createFSMActionPeripheralClear();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Create Window</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Create Window</em>'.
	 * @generated
	 */
	FSMActionPeripheralCreateWindow createFSMActionPeripheralCreateWindow();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Cursor Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Cursor Type</em>'.
	 * @generated
	 */
	FSMActionPeripheralCursorType createFSMActionPeripheralCursorType();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Destroy Window</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Destroy Window</em>'.
	 * @generated
	 */
	FSMActionPeripheralDestroyWindow createFSMActionPeripheralDestroyWindow();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Device Brightness</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Device Brightness</em>'.
	 * @generated
	 */
	FSMActionPeripheralDeviceBrightness createFSMActionPeripheralDeviceBrightness();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Display Text</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Display Text</em>'.
	 * @generated
	 */
	FSMActionPeripheralDisplayText createFSMActionPeripheralDisplayText();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Line Display Text</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Line Display Text</em>'.
	 * @generated
	 */
	FSMActionPeripheralLineDisplayText createFSMActionPeripheralLineDisplayText();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Line Display Text At</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Line Display Text At</em>'.
	 * @generated
	 */
	FSMActionPeripheralLineDisplayTextAt createFSMActionPeripheralLineDisplayTextAt();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Inter Character Wait</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Inter Character Wait</em>'.
	 * @generated
	 */
	FSMActionPeripheralInterCharacterWait createFSMActionPeripheralInterCharacterWait();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Marquee Format</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Marquee Format</em>'.
	 * @generated
	 */
	FSMActionPeripheralMarqueeFormat createFSMActionPeripheralMarqueeFormat();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Marquee Repeat Wait</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Marquee Repeat Wait</em>'.
	 * @generated
	 */
	FSMActionPeripheralMarqueeRepeatWait createFSMActionPeripheralMarqueeRepeatWait();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Marquee Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Marquee Type</em>'.
	 * @generated
	 */
	FSMActionPeripheralMarqueeType createFSMActionPeripheralMarqueeType();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Marquee Unit Wait</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Marquee Unit Wait</em>'.
	 * @generated
	 */
	FSMActionPeripheralMarqueeUnitWait createFSMActionPeripheralMarqueeUnitWait();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Scroll</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Scroll</em>'.
	 * @generated
	 */
	FSMActionPeripheralScroll createFSMActionPeripheralScroll();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Open Drawer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Open Drawer</em>'.
	 * @generated
	 */
	FSMActionPeripheralOpenDrawer createFSMActionPeripheralOpenDrawer();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Print Barcode</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Print Barcode</em>'.
	 * @generated
	 */
	FSMActionPeripheralPrintBarcode createFSMActionPeripheralPrintBarcode();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Print Bitmap</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Print Bitmap</em>'.
	 * @generated
	 */
	FSMActionPeripheralPrintBitmap createFSMActionPeripheralPrintBitmap();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Print Cut</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Print Cut</em>'.
	 * @generated
	 */
	FSMActionPeripheralPrintCut createFSMActionPeripheralPrintCut();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Print Register Bitmap</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Print Register Bitmap</em>'.
	 * @generated
	 */
	FSMActionPeripheralPrintRegisterBitmap createFSMActionPeripheralPrintRegisterBitmap();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Print Normal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Print Normal</em>'.
	 * @generated
	 */
	FSMActionPeripheralPrintNormal createFSMActionPeripheralPrintNormal();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral PT Open</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral PT Open</em>'.
	 * @generated
	 */
	FSMActionPeripheralPTOpen createFSMActionPeripheralPTOpen();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral PT Close</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral PT Close</em>'.
	 * @generated
	 */
	FSMActionPeripheralPTClose createFSMActionPeripheralPTClose();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral PT Reversal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral PT Reversal</em>'.
	 * @generated
	 */
	FSMActionPeripheralPTReversal createFSMActionPeripheralPTReversal();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral PT Acknowledge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral PT Acknowledge</em>'.
	 * @generated
	 */
	FSMActionPeripheralPTAcknowledge createFSMActionPeripheralPTAcknowledge();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral PT Registration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral PT Registration</em>'.
	 * @generated
	 */
	FSMActionPeripheralPTRegistration createFSMActionPeripheralPTRegistration();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral PT Authorization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral PT Authorization</em>'.
	 * @generated
	 */
	FSMActionPeripheralPTAuthorization createFSMActionPeripheralPTAuthorization();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Beeper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Beeper</em>'.
	 * @generated
	 */
	FSMActionPeripheralBeeper createFSMActionPeripheralBeeper();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Player</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Player</em>'.
	 * @generated
	 */
	FSMActionPeripheralPlayer createFSMActionPeripheralPlayer();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Sound</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Sound</em>'.
	 * @generated
	 */
	FSMActionPeripheralSound createFSMActionPeripheralSound();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral PT Response</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral PT Response</em>'.
	 * @generated
	 */
	FSMActionPeripheralPTResponse createFSMActionPeripheralPTResponse();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Print Report</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Print Report</em>'.
	 * @generated
	 */
	FSMActionPeripheralPrintReport createFSMActionPeripheralPrintReport();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Signature Open</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Signature Open</em>'.
	 * @generated
	 */
	FSMActionPeripheralSignatureOpen createFSMActionPeripheralSignatureOpen();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Signature Close</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Signature Close</em>'.
	 * @generated
	 */
	FSMActionPeripheralSignatureClose createFSMActionPeripheralSignatureClose();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Signature Clear</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Signature Clear</em>'.
	 * @generated
	 */
	FSMActionPeripheralSignatureClear createFSMActionPeripheralSignatureClear();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Signature Capture</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Signature Capture</em>'.
	 * @generated
	 */
	FSMActionPeripheralSignatureCapture createFSMActionPeripheralSignatureCapture();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Signature Idle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Signature Idle</em>'.
	 * @generated
	 */
	FSMActionPeripheralSignatureIdle createFSMActionPeripheralSignatureIdle();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Signature Label</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Signature Label</em>'.
	 * @generated
	 */
	FSMActionPeripheralSignatureLabel createFSMActionPeripheralSignatureLabel();

	/**
	 * Returns a new object of class '<em>FSM Signature Retrieve</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Signature Retrieve</em>'.
	 * @generated
	 */
	FSMSignatureRetrieve createFSMSignatureRetrieve();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Scale Read Weight</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Scale Read Weight</em>'.
	 * @generated
	 */
	FSMActionPeripheralScaleReadWeight createFSMActionPeripheralScaleReadWeight();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Scale Read Tare Weight</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Scale Read Tare Weight</em>'.
	 * @generated
	 */
	FSMActionPeripheralScaleReadTareWeight createFSMActionPeripheralScaleReadTareWeight();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Scale Tare Weight</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Scale Tare Weight</em>'.
	 * @generated
	 */
	FSMActionPeripheralScaleTareWeight createFSMActionPeripheralScaleTareWeight();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Scale Zero</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Scale Zero</em>'.
	 * @generated
	 */
	FSMActionPeripheralScaleZero createFSMActionPeripheralScaleZero();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Scale Display Text</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Scale Display Text</em>'.
	 * @generated
	 */
	FSMActionPeripheralScaleDisplayText createFSMActionPeripheralScaleDisplayText();

	/**
	 * Returns a new object of class '<em>FSM Action Peripheral Scale Weight Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Peripheral Scale Weight Unit</em>'.
	 * @generated
	 */
	FSMActionPeripheralScaleWeightUnit createFSMActionPeripheralScaleWeightUnit();

	/**
	 * Returns a new object of class '<em>FSM Action Field Source String</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Source String</em>'.
	 * @generated
	 */
	FSMActionFieldSourceString createFSMActionFieldSourceString();

	/**
	 * Returns a new object of class '<em>FSM Action Field Source Number</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Source Number</em>'.
	 * @generated
	 */
	FSMActionFieldSourceNumber createFSMActionFieldSourceNumber();

	/**
	 * Returns a new object of class '<em>FSM Action Field Source Integer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Source Integer</em>'.
	 * @generated
	 */
	FSMActionFieldSourceInteger createFSMActionFieldSourceInteger();

	/**
	 * Returns a new object of class '<em>FSM Action Field Source Boolean</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Source Boolean</em>'.
	 * @generated
	 */
	FSMActionFieldSourceBoolean createFSMActionFieldSourceBoolean();

	/**
	 * Returns a new object of class '<em>FSM Action Field Source Evaluate</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Source Evaluate</em>'.
	 * @generated
	 */
	FSMActionFieldSourceEvaluate createFSMActionFieldSourceEvaluate();

	/**
	 * Returns a new object of class '<em>FSM Action Field Source Translate</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Source Translate</em>'.
	 * @generated
	 */
	FSMActionFieldSourceTranslate createFSMActionFieldSourceTranslate();

	/**
	 * Returns a new object of class '<em>FSM Dot Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Dot Expression</em>'.
	 * @generated
	 */
	FSMDotExpression createFSMDotExpression();

	/**
	 * Returns a new object of class '<em>FSM Dto Ref</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Dto Ref</em>'.
	 * @generated
	 */
	FSMDtoRef createFSMDtoRef();

	/**
	 * Returns a new object of class '<em>FSM Action Field Source Dto Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Source Dto Attribute</em>'.
	 * @generated
	 */
	FSMActionFieldSourceDtoAttribute createFSMActionFieldSourceDtoAttribute();

	/**
	 * Returns a new object of class '<em>FSM Action Field Source Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Source Event</em>'.
	 * @generated
	 */
	FSMActionFieldSourceEvent createFSMActionFieldSourceEvent();

	/**
	 * Returns a new object of class '<em>FSM Action Conditional Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Conditional Transition</em>'.
	 * @generated
	 */
	FSMActionConditionalTransition createFSMActionConditionalTransition();

	/**
	 * Returns a new object of class '<em>FSM Operation Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Operation Parameter</em>'.
	 * @generated
	 */
	FSMOperationParameter createFSMOperationParameter();

	/**
	 * Returns a new object of class '<em>FSM Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Operation</em>'.
	 * @generated
	 */
	FSMOperation createFSMOperation();

	/**
	 * Returns a new object of class '<em>FSM Guard</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Guard</em>'.
	 * @generated
	 */
	FSMGuard createFSMGuard();

	/**
	 * Returns a new object of class '<em>FSM Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Function</em>'.
	 * @generated
	 */
	FSMFunction createFSMFunction();

	/**
	 * Returns a new object of class '<em>FSM Storage Retrieve</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Storage Retrieve</em>'.
	 * @generated
	 */
	FSMStorageRetrieve createFSMStorageRetrieve();

	/**
	 * Returns a new object of class '<em>FSM Storage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Storage</em>'.
	 * @generated
	 */
	FSMStorage createFSMStorage();

	/**
	 * Returns a new object of class '<em>FSM Action Field Concatenation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Concatenation</em>'.
	 * @generated
	 */
	FSMActionFieldConcatenation createFSMActionFieldConcatenation();

	/**
	 * Returns a new object of class '<em>FSM Action Field Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Set</em>'.
	 * @generated
	 */
	FSMActionFieldSet createFSMActionFieldSet();

	/**
	 * Returns a new object of class '<em>FSM Action Field Keystroke</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Keystroke</em>'.
	 * @generated
	 */
	FSMActionFieldKeystroke createFSMActionFieldKeystroke();

	/**
	 * Returns a new object of class '<em>FSM Action Field Clear</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Clear</em>'.
	 * @generated
	 */
	FSMActionFieldClear createFSMActionFieldClear();

	/**
	 * Returns a new object of class '<em>FSM Action Field Get</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Get</em>'.
	 * @generated
	 */
	FSMActionFieldGet createFSMActionFieldGet();

	/**
	 * Returns a new object of class '<em>FSM Action Field Filter Toggle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Filter Toggle</em>'.
	 * @generated
	 */
	FSMActionFieldFilterToggle createFSMActionFieldFilterToggle();

	/**
	 * Returns a new object of class '<em>FSM Action Field Remove</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Field Remove</em>'.
	 * @generated
	 */
	FSMActionFieldRemove createFSMActionFieldRemove();

	/**
	 * Returns a new object of class '<em>FSM Action Item Visible</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Item Visible</em>'.
	 * @generated
	 */
	FSMActionItemVisible createFSMActionItemVisible();

	/**
	 * Returns a new object of class '<em>FSM Action Item Invisible</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Item Invisible</em>'.
	 * @generated
	 */
	FSMActionItemInvisible createFSMActionItemInvisible();

	/**
	 * Returns a new object of class '<em>FSM Action Button Caption</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Button Caption</em>'.
	 * @generated
	 */
	FSMActionButtonCaption createFSMActionButtonCaption();

	/**
	 * Returns a new object of class '<em>FSM Action Button Image</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Button Image</em>'.
	 * @generated
	 */
	FSMActionButtonImage createFSMActionButtonImage();

	/**
	 * Returns a new object of class '<em>FSM Action DTO Find</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action DTO Find</em>'.
	 * @generated
	 */
	FSMActionDTOFind createFSMActionDTOFind();

	/**
	 * Returns a new object of class '<em>FSM Action DTO Clear</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action DTO Clear</em>'.
	 * @generated
	 */
	FSMActionDTOClear createFSMActionDTOClear();

	/**
	 * Returns a new object of class '<em>FSM Action Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Action Scheduler</em>'.
	 * @generated
	 */
	FSMActionScheduler createFSMActionScheduler();

	/**
	 * Returns a new object of class '<em>FSMDTO Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSMDTO Type</em>'.
	 * @generated
	 */
	FSMDTOType createFSMDTOType();

	/**
	 * Returns a new object of class '<em>FSM Field Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Field Type</em>'.
	 * @generated
	 */
	FSMFieldType createFSMFieldType();

	/**
	 * Returns a new object of class '<em>FSM Filter Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Filter Property</em>'.
	 * @generated
	 */
	FSMFilterProperty createFSMFilterProperty();

	/**
	 * Returns a new object of class '<em>FSM Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Filter</em>'.
	 * @generated
	 */
	FSMFilter createFSMFilter();

	/**
	 * Returns a new object of class '<em>FSM Junction Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Junction Filter</em>'.
	 * @generated
	 */
	FSMJunctionFilter createFSMJunctionFilter();

	/**
	 * Returns a new object of class '<em>FSM And Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM And Filter</em>'.
	 * @generated
	 */
	FSMAndFilter createFSMAndFilter();

	/**
	 * Returns a new object of class '<em>FSM Or Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Or Filter</em>'.
	 * @generated
	 */
	FSMOrFilter createFSMOrFilter();

	/**
	 * Returns a new object of class '<em>FSM Between Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Between Filter</em>'.
	 * @generated
	 */
	FSMBetweenFilter createFSMBetweenFilter();

	/**
	 * Returns a new object of class '<em>FSM Compare Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Compare Filter</em>'.
	 * @generated
	 */
	FSMCompareFilter createFSMCompareFilter();

	/**
	 * Returns a new object of class '<em>FSM Is Null Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Is Null Filter</em>'.
	 * @generated
	 */
	FSMIsNullFilter createFSMIsNullFilter();

	/**
	 * Returns a new object of class '<em>FSM Like Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Like Filter</em>'.
	 * @generated
	 */
	FSMLikeFilter createFSMLikeFilter();

	/**
	 * Returns a new object of class '<em>FSM Not Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Not Filter</em>'.
	 * @generated
	 */
	FSMNotFilter createFSMNotFilter();

	/**
	 * Returns a new object of class '<em>FSM String Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM String Filter</em>'.
	 * @generated
	 */
	FSMStringFilter createFSMStringFilter();

	/**
	 * Returns a new object of class '<em>FSM Control Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Control Filter</em>'.
	 * @generated
	 */
	FSMControlFilter createFSMControlFilter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	StatemachineDSLPackage getStatemachineDSLPackage();

} //StatemachineDSLFactory
