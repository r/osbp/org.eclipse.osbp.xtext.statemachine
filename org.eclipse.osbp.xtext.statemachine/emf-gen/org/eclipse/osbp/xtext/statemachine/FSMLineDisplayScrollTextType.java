/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>FSM Line Display Scroll Text Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMLineDisplayScrollTextType()
 * @model
 * @generated
 */
public enum FSMLineDisplayScrollTextType implements Enumerator {
	/**
	 * The '<em><b>DISP ST UP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_ST_UP_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_ST_UP(0, "DISP_ST_UP", "up"),

	/**
	 * The '<em><b>DISP ST DOWN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_ST_DOWN_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_ST_DOWN(0, "DISP_ST_DOWN", "down"),

	/**
	 * The '<em><b>DISP ST LEFT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_ST_LEFT_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_ST_LEFT(0, "DISP_ST_LEFT", "left"),

	/**
	 * The '<em><b>DISP ST RIGHT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_ST_RIGHT_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_ST_RIGHT(0, "DISP_ST_RIGHT", "right");

	/**
	 * The '<em><b>DISP ST UP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP ST UP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_ST_UP
	 * @model literal="up"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_ST_UP_VALUE = 0;

	/**
	 * The '<em><b>DISP ST DOWN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP ST DOWN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_ST_DOWN
	 * @model literal="down"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_ST_DOWN_VALUE = 0;

	/**
	 * The '<em><b>DISP ST LEFT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP ST LEFT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_ST_LEFT
	 * @model literal="left"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_ST_LEFT_VALUE = 0;

	/**
	 * The '<em><b>DISP ST RIGHT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP ST RIGHT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_ST_RIGHT
	 * @model literal="right"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_ST_RIGHT_VALUE = 0;

	/**
	 * An array of all the '<em><b>FSM Line Display Scroll Text Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final FSMLineDisplayScrollTextType[] VALUES_ARRAY =
		new FSMLineDisplayScrollTextType[] {
			DISP_ST_UP,
			DISP_ST_DOWN,
			DISP_ST_LEFT,
			DISP_ST_RIGHT,
		};

	/**
	 * A public read-only list of all the '<em><b>FSM Line Display Scroll Text Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<FSMLineDisplayScrollTextType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>FSM Line Display Scroll Text Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMLineDisplayScrollTextType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMLineDisplayScrollTextType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM Line Display Scroll Text Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMLineDisplayScrollTextType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMLineDisplayScrollTextType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM Line Display Scroll Text Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMLineDisplayScrollTextType get(int value) {
		switch (value) {
			case DISP_ST_UP_VALUE: return DISP_ST_UP;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FSMLineDisplayScrollTextType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //FSMLineDisplayScrollTextType
