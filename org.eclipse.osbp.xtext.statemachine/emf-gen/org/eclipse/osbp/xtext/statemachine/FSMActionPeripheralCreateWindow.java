/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Action Peripheral Create Window</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getDevice <em>Device</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportRow <em>Viewport Row</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportColumn <em>Viewport Column</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportHeight <em>Viewport Height</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportWidth <em>Viewport Width</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getWindowHeight <em>Window Height</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getWindowWidth <em>Window Width</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionPeripheralCreateWindow()
 * @model
 * @generated
 */
public interface FSMActionPeripheralCreateWindow extends FSMAction {
	/**
	 * Returns the value of the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device</em>' reference.
	 * @see #setDevice(FSMPeripheralDeviceLineDisplay)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionPeripheralCreateWindow_Device()
	 * @model
	 * @generated
	 */
	FSMPeripheralDeviceLineDisplay getDevice();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getDevice <em>Device</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Device</em>' reference.
	 * @see #getDevice()
	 * @generated
	 */
	void setDevice(FSMPeripheralDeviceLineDisplay value);

	/**
	 * Returns the value of the '<em><b>Viewport Row</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewport Row</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewport Row</em>' attribute.
	 * @see #setViewportRow(int)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionPeripheralCreateWindow_ViewportRow()
	 * @model unique="false"
	 * @generated
	 */
	int getViewportRow();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportRow <em>Viewport Row</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Viewport Row</em>' attribute.
	 * @see #getViewportRow()
	 * @generated
	 */
	void setViewportRow(int value);

	/**
	 * Returns the value of the '<em><b>Viewport Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewport Column</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewport Column</em>' attribute.
	 * @see #setViewportColumn(int)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionPeripheralCreateWindow_ViewportColumn()
	 * @model unique="false"
	 * @generated
	 */
	int getViewportColumn();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportColumn <em>Viewport Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Viewport Column</em>' attribute.
	 * @see #getViewportColumn()
	 * @generated
	 */
	void setViewportColumn(int value);

	/**
	 * Returns the value of the '<em><b>Viewport Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewport Height</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewport Height</em>' attribute.
	 * @see #setViewportHeight(int)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionPeripheralCreateWindow_ViewportHeight()
	 * @model unique="false"
	 * @generated
	 */
	int getViewportHeight();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportHeight <em>Viewport Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Viewport Height</em>' attribute.
	 * @see #getViewportHeight()
	 * @generated
	 */
	void setViewportHeight(int value);

	/**
	 * Returns the value of the '<em><b>Viewport Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewport Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewport Width</em>' attribute.
	 * @see #setViewportWidth(int)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionPeripheralCreateWindow_ViewportWidth()
	 * @model unique="false"
	 * @generated
	 */
	int getViewportWidth();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getViewportWidth <em>Viewport Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Viewport Width</em>' attribute.
	 * @see #getViewportWidth()
	 * @generated
	 */
	void setViewportWidth(int value);

	/**
	 * Returns the value of the '<em><b>Window Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Window Height</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Window Height</em>' attribute.
	 * @see #setWindowHeight(int)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionPeripheralCreateWindow_WindowHeight()
	 * @model unique="false"
	 * @generated
	 */
	int getWindowHeight();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getWindowHeight <em>Window Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Window Height</em>' attribute.
	 * @see #getWindowHeight()
	 * @generated
	 */
	void setWindowHeight(int value);

	/**
	 * Returns the value of the '<em><b>Window Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Window Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Window Width</em>' attribute.
	 * @see #setWindowWidth(int)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionPeripheralCreateWindow_WindowWidth()
	 * @model unique="false"
	 * @generated
	 */
	int getWindowWidth();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow#getWindowWidth <em>Window Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Window Width</em>' attribute.
	 * @see #getWindowWidth()
	 * @generated
	 */
	void setWindowWidth(int value);

} // FSMActionPeripheralCreateWindow
