/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;

import org.eclipse.osbp.xtext.statemachine.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage
 * @generated
 */
public class StatemachineDSLAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static StatemachineDSLPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachineDSLAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = StatemachineDSLPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StatemachineDSLSwitch<Adapter> modelSwitch =
		new StatemachineDSLSwitch<Adapter>() {
			@Override
			public Adapter caseFSMModel(FSMModel object) {
				return createFSMModelAdapter();
			}
			@Override
			public Adapter caseFSMLazyResolver(FSMLazyResolver object) {
				return createFSMLazyResolverAdapter();
			}
			@Override
			public Adapter caseFSMBase(FSMBase object) {
				return createFSMBaseAdapter();
			}
			@Override
			public Adapter caseFSMPackage(FSMPackage object) {
				return createFSMPackageAdapter();
			}
			@Override
			public Adapter caseFSM(FSM object) {
				return createFSMAdapter();
			}
			@Override
			public Adapter caseFSMControl(FSMControl object) {
				return createFSMControlAdapter();
			}
			@Override
			public Adapter caseFSMControlButton(FSMControlButton object) {
				return createFSMControlButtonAdapter();
			}
			@Override
			public Adapter caseFSMControlField(FSMControlField object) {
				return createFSMControlFieldAdapter();
			}
			@Override
			public Adapter caseFSMControlDTO(FSMControlDTO object) {
				return createFSMControlDTOAdapter();
			}
			@Override
			public Adapter caseFSMControlScheduler(FSMControlScheduler object) {
				return createFSMControlSchedulerAdapter();
			}
			@Override
			public Adapter caseFSMControlPeripheral(FSMControlPeripheral object) {
				return createFSMControlPeripheralAdapter();
			}
			@Override
			public Adapter caseFSMControlButtonAttribute(FSMControlButtonAttribute object) {
				return createFSMControlButtonAttributeAdapter();
			}
			@Override
			public Adapter caseFSMControlButtonAttributeEvent(FSMControlButtonAttributeEvent object) {
				return createFSMControlButtonAttributeEventAdapter();
			}
			@Override
			public Adapter caseFSMControlButtonAttributeEventKeyboard(FSMControlButtonAttributeEventKeyboard object) {
				return createFSMControlButtonAttributeEventKeyboardAdapter();
			}
			@Override
			public Adapter caseFSMControlButtonAttributeEventIdentity(FSMControlButtonAttributeEventIdentity object) {
				return createFSMControlButtonAttributeEventIdentityAdapter();
			}
			@Override
			public Adapter caseFSMControlButtonAttributeEventEvent(FSMControlButtonAttributeEventEvent object) {
				return createFSMControlButtonAttributeEventEventAdapter();
			}
			@Override
			public Adapter caseFSMControlVisibility(FSMControlVisibility object) {
				return createFSMControlVisibilityAdapter();
			}
			@Override
			public Adapter caseFSMControlFieldAttribute(FSMControlFieldAttribute object) {
				return createFSMControlFieldAttributeAdapter();
			}
			@Override
			public Adapter caseFSMControlFieldLayout(FSMControlFieldLayout object) {
				return createFSMControlFieldLayoutAdapter();
			}
			@Override
			public Adapter caseFSMControlDTOAttribute(FSMControlDTOAttribute object) {
				return createFSMControlDTOAttributeAdapter();
			}
			@Override
			public Adapter caseFSMPeripheralDevice(FSMPeripheralDevice object) {
				return createFSMPeripheralDeviceAdapter();
			}
			@Override
			public Adapter caseFSMPeripheralDeviceDisplay(FSMPeripheralDeviceDisplay object) {
				return createFSMPeripheralDeviceDisplayAdapter();
			}
			@Override
			public Adapter caseFSMPeripheralDeviceLineDisplay(FSMPeripheralDeviceLineDisplay object) {
				return createFSMPeripheralDeviceLineDisplayAdapter();
			}
			@Override
			public Adapter caseFSMPeripheralDevicePOSPrinter(FSMPeripheralDevicePOSPrinter object) {
				return createFSMPeripheralDevicePOSPrinterAdapter();
			}
			@Override
			public Adapter caseFSMPeripheralDeviceCashDrawer(FSMPeripheralDeviceCashDrawer object) {
				return createFSMPeripheralDeviceCashDrawerAdapter();
			}
			@Override
			public Adapter caseFSMPeripheralDevicePT(FSMPeripheralDevicePT object) {
				return createFSMPeripheralDevicePTAdapter();
			}
			@Override
			public Adapter caseFSMPeripheralDeviceSignature(FSMPeripheralDeviceSignature object) {
				return createFSMPeripheralDeviceSignatureAdapter();
			}
			@Override
			public Adapter caseFSMPeripheralDeviceScale(FSMPeripheralDeviceScale object) {
				return createFSMPeripheralDeviceScaleAdapter();
			}
			@Override
			public Adapter caseFSMControlSchedulerAttribute(FSMControlSchedulerAttribute object) {
				return createFSMControlSchedulerAttributeAdapter();
			}
			@Override
			public Adapter caseFSMEvent(FSMEvent object) {
				return createFSMEventAdapter();
			}
			@Override
			public Adapter caseFSMState(FSMState object) {
				return createFSMStateAdapter();
			}
			@Override
			public Adapter caseFSMKeyMapper(FSMKeyMapper object) {
				return createFSMKeyMapperAdapter();
			}
			@Override
			public Adapter caseFSMTrigger(FSMTrigger object) {
				return createFSMTriggerAdapter();
			}
			@Override
			public Adapter caseFSMAction(FSMAction object) {
				return createFSMActionAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralBlinkRate(FSMActionPeripheralBlinkRate object) {
				return createFSMActionPeripheralBlinkRateAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralClear(FSMActionPeripheralClear object) {
				return createFSMActionPeripheralClearAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralCreateWindow(FSMActionPeripheralCreateWindow object) {
				return createFSMActionPeripheralCreateWindowAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralCursorType(FSMActionPeripheralCursorType object) {
				return createFSMActionPeripheralCursorTypeAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralDestroyWindow(FSMActionPeripheralDestroyWindow object) {
				return createFSMActionPeripheralDestroyWindowAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralDeviceBrightness(FSMActionPeripheralDeviceBrightness object) {
				return createFSMActionPeripheralDeviceBrightnessAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralDisplayText(FSMActionPeripheralDisplayText object) {
				return createFSMActionPeripheralDisplayTextAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralLineDisplayText(FSMActionPeripheralLineDisplayText object) {
				return createFSMActionPeripheralLineDisplayTextAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralLineDisplayTextAt(FSMActionPeripheralLineDisplayTextAt object) {
				return createFSMActionPeripheralLineDisplayTextAtAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralInterCharacterWait(FSMActionPeripheralInterCharacterWait object) {
				return createFSMActionPeripheralInterCharacterWaitAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralMarqueeFormat(FSMActionPeripheralMarqueeFormat object) {
				return createFSMActionPeripheralMarqueeFormatAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralMarqueeRepeatWait(FSMActionPeripheralMarqueeRepeatWait object) {
				return createFSMActionPeripheralMarqueeRepeatWaitAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralMarqueeType(FSMActionPeripheralMarqueeType object) {
				return createFSMActionPeripheralMarqueeTypeAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralMarqueeUnitWait(FSMActionPeripheralMarqueeUnitWait object) {
				return createFSMActionPeripheralMarqueeUnitWaitAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralScroll(FSMActionPeripheralScroll object) {
				return createFSMActionPeripheralScrollAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralOpenDrawer(FSMActionPeripheralOpenDrawer object) {
				return createFSMActionPeripheralOpenDrawerAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPrintBarcode(FSMActionPeripheralPrintBarcode object) {
				return createFSMActionPeripheralPrintBarcodeAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPrintBitmap(FSMActionPeripheralPrintBitmap object) {
				return createFSMActionPeripheralPrintBitmapAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPrintCut(FSMActionPeripheralPrintCut object) {
				return createFSMActionPeripheralPrintCutAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPrintRegisterBitmap(FSMActionPeripheralPrintRegisterBitmap object) {
				return createFSMActionPeripheralPrintRegisterBitmapAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPrintNormal(FSMActionPeripheralPrintNormal object) {
				return createFSMActionPeripheralPrintNormalAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPTOpen(FSMActionPeripheralPTOpen object) {
				return createFSMActionPeripheralPTOpenAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPTClose(FSMActionPeripheralPTClose object) {
				return createFSMActionPeripheralPTCloseAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPTReversal(FSMActionPeripheralPTReversal object) {
				return createFSMActionPeripheralPTReversalAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPTAcknowledge(FSMActionPeripheralPTAcknowledge object) {
				return createFSMActionPeripheralPTAcknowledgeAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPTRegistration(FSMActionPeripheralPTRegistration object) {
				return createFSMActionPeripheralPTRegistrationAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPTAuthorization(FSMActionPeripheralPTAuthorization object) {
				return createFSMActionPeripheralPTAuthorizationAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralBeeper(FSMActionPeripheralBeeper object) {
				return createFSMActionPeripheralBeeperAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPlayer(FSMActionPeripheralPlayer object) {
				return createFSMActionPeripheralPlayerAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralSound(FSMActionPeripheralSound object) {
				return createFSMActionPeripheralSoundAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPTResponse(FSMActionPeripheralPTResponse object) {
				return createFSMActionPeripheralPTResponseAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralPrintReport(FSMActionPeripheralPrintReport object) {
				return createFSMActionPeripheralPrintReportAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralSignatureOpen(FSMActionPeripheralSignatureOpen object) {
				return createFSMActionPeripheralSignatureOpenAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralSignatureClose(FSMActionPeripheralSignatureClose object) {
				return createFSMActionPeripheralSignatureCloseAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralSignatureClear(FSMActionPeripheralSignatureClear object) {
				return createFSMActionPeripheralSignatureClearAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralSignatureCapture(FSMActionPeripheralSignatureCapture object) {
				return createFSMActionPeripheralSignatureCaptureAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralSignatureIdle(FSMActionPeripheralSignatureIdle object) {
				return createFSMActionPeripheralSignatureIdleAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralSignatureLabel(FSMActionPeripheralSignatureLabel object) {
				return createFSMActionPeripheralSignatureLabelAdapter();
			}
			@Override
			public Adapter caseFSMSignatureRetrieve(FSMSignatureRetrieve object) {
				return createFSMSignatureRetrieveAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralScaleReadWeight(FSMActionPeripheralScaleReadWeight object) {
				return createFSMActionPeripheralScaleReadWeightAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralScaleReadTareWeight(FSMActionPeripheralScaleReadTareWeight object) {
				return createFSMActionPeripheralScaleReadTareWeightAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralScaleTareWeight(FSMActionPeripheralScaleTareWeight object) {
				return createFSMActionPeripheralScaleTareWeightAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralScaleZero(FSMActionPeripheralScaleZero object) {
				return createFSMActionPeripheralScaleZeroAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralScaleDisplayText(FSMActionPeripheralScaleDisplayText object) {
				return createFSMActionPeripheralScaleDisplayTextAdapter();
			}
			@Override
			public Adapter caseFSMActionPeripheralScaleWeightUnit(FSMActionPeripheralScaleWeightUnit object) {
				return createFSMActionPeripheralScaleWeightUnitAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldSource(FSMActionFieldSource object) {
				return createFSMActionFieldSourceAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldSourceString(FSMActionFieldSourceString object) {
				return createFSMActionFieldSourceStringAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldSourceNumber(FSMActionFieldSourceNumber object) {
				return createFSMActionFieldSourceNumberAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldSourceInteger(FSMActionFieldSourceInteger object) {
				return createFSMActionFieldSourceIntegerAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldSourceBoolean(FSMActionFieldSourceBoolean object) {
				return createFSMActionFieldSourceBooleanAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldSourceEvaluate(FSMActionFieldSourceEvaluate object) {
				return createFSMActionFieldSourceEvaluateAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldSourceTranslate(FSMActionFieldSourceTranslate object) {
				return createFSMActionFieldSourceTranslateAdapter();
			}
			@Override
			public Adapter caseFSMRef(FSMRef object) {
				return createFSMRefAdapter();
			}
			@Override
			public Adapter caseFSMDotExpression(FSMDotExpression object) {
				return createFSMDotExpressionAdapter();
			}
			@Override
			public Adapter caseFSMDtoRef(FSMDtoRef object) {
				return createFSMDtoRefAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldSourceDtoAttribute(FSMActionFieldSourceDtoAttribute object) {
				return createFSMActionFieldSourceDtoAttributeAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldSourceEvent(FSMActionFieldSourceEvent object) {
				return createFSMActionFieldSourceEventAdapter();
			}
			@Override
			public Adapter caseFSMActionConditionalTransition(FSMActionConditionalTransition object) {
				return createFSMActionConditionalTransitionAdapter();
			}
			@Override
			public Adapter caseFSMOperationParameter(FSMOperationParameter object) {
				return createFSMOperationParameterAdapter();
			}
			@Override
			public Adapter caseFSMOperation(FSMOperation object) {
				return createFSMOperationAdapter();
			}
			@Override
			public Adapter caseFSMGuard(FSMGuard object) {
				return createFSMGuardAdapter();
			}
			@Override
			public Adapter caseFSMFunction(FSMFunction object) {
				return createFSMFunctionAdapter();
			}
			@Override
			public Adapter caseFSMStorageRetrieve(FSMStorageRetrieve object) {
				return createFSMStorageRetrieveAdapter();
			}
			@Override
			public Adapter caseFSMStorage(FSMStorage object) {
				return createFSMStorageAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldConcatenation(FSMActionFieldConcatenation object) {
				return createFSMActionFieldConcatenationAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldSet(FSMActionFieldSet object) {
				return createFSMActionFieldSetAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldKeystroke(FSMActionFieldKeystroke object) {
				return createFSMActionFieldKeystrokeAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldClear(FSMActionFieldClear object) {
				return createFSMActionFieldClearAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldGet(FSMActionFieldGet object) {
				return createFSMActionFieldGetAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldFilterToggle(FSMActionFieldFilterToggle object) {
				return createFSMActionFieldFilterToggleAdapter();
			}
			@Override
			public Adapter caseFSMActionFieldRemove(FSMActionFieldRemove object) {
				return createFSMActionFieldRemoveAdapter();
			}
			@Override
			public Adapter caseFSMActionItemVisible(FSMActionItemVisible object) {
				return createFSMActionItemVisibleAdapter();
			}
			@Override
			public Adapter caseFSMActionItemInvisible(FSMActionItemInvisible object) {
				return createFSMActionItemInvisibleAdapter();
			}
			@Override
			public Adapter caseFSMActionButtonCaption(FSMActionButtonCaption object) {
				return createFSMActionButtonCaptionAdapter();
			}
			@Override
			public Adapter caseFSMActionButtonImage(FSMActionButtonImage object) {
				return createFSMActionButtonImageAdapter();
			}
			@Override
			public Adapter caseFSMActionDTOFind(FSMActionDTOFind object) {
				return createFSMActionDTOFindAdapter();
			}
			@Override
			public Adapter caseFSMActionDTOClear(FSMActionDTOClear object) {
				return createFSMActionDTOClearAdapter();
			}
			@Override
			public Adapter caseFSMActionScheduler(FSMActionScheduler object) {
				return createFSMActionSchedulerAdapter();
			}
			@Override
			public Adapter caseFSMDTOType(FSMDTOType object) {
				return createFSMDTOTypeAdapter();
			}
			@Override
			public Adapter caseFSMFieldType(FSMFieldType object) {
				return createFSMFieldTypeAdapter();
			}
			@Override
			public Adapter caseFSMAbstractFilter(FSMAbstractFilter object) {
				return createFSMAbstractFilterAdapter();
			}
			@Override
			public Adapter caseFSMFilterProperty(FSMFilterProperty object) {
				return createFSMFilterPropertyAdapter();
			}
			@Override
			public Adapter caseFSMFilter(FSMFilter object) {
				return createFSMFilterAdapter();
			}
			@Override
			public Adapter caseFSMJunctionFilter(FSMJunctionFilter object) {
				return createFSMJunctionFilterAdapter();
			}
			@Override
			public Adapter caseFSMAndFilter(FSMAndFilter object) {
				return createFSMAndFilterAdapter();
			}
			@Override
			public Adapter caseFSMOrFilter(FSMOrFilter object) {
				return createFSMOrFilterAdapter();
			}
			@Override
			public Adapter caseFSMBetweenFilter(FSMBetweenFilter object) {
				return createFSMBetweenFilterAdapter();
			}
			@Override
			public Adapter caseFSMCompareFilter(FSMCompareFilter object) {
				return createFSMCompareFilterAdapter();
			}
			@Override
			public Adapter caseFSMIsNullFilter(FSMIsNullFilter object) {
				return createFSMIsNullFilterAdapter();
			}
			@Override
			public Adapter caseFSMLikeFilter(FSMLikeFilter object) {
				return createFSMLikeFilterAdapter();
			}
			@Override
			public Adapter caseFSMNotFilter(FSMNotFilter object) {
				return createFSMNotFilterAdapter();
			}
			@Override
			public Adapter caseFSMStringFilter(FSMStringFilter object) {
				return createFSMStringFilterAdapter();
			}
			@Override
			public Adapter caseFSMControlFilter(FSMControlFilter object) {
				return createFSMControlFilterAdapter();
			}
			@Override
			public Adapter caseLLazyResolver(LLazyResolver object) {
				return createLLazyResolverAdapter();
			}
			@Override
			public Adapter caseLPackage(LPackage object) {
				return createLPackageAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMModel <em>FSM Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMModel
	 * @generated
	 */
	public Adapter createFSMModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMLazyResolver <em>FSM Lazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLazyResolver
	 * @generated
	 */
	public Adapter createFSMLazyResolverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMBase <em>FSM Base</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMBase
	 * @generated
	 */
	public Adapter createFSMBaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMPackage <em>FSM Package</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPackage
	 * @generated
	 */
	public Adapter createFSMPackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSM <em>FSM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSM
	 * @generated
	 */
	public Adapter createFSMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControl <em>FSM Control</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControl
	 * @generated
	 */
	public Adapter createFSMControlAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton <em>FSM Control Button</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButton
	 * @generated
	 */
	public Adapter createFSMControlButtonAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlField <em>FSM Control Field</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlField
	 * @generated
	 */
	public Adapter createFSMControlFieldAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlDTO <em>FSM Control DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlDTO
	 * @generated
	 */
	public Adapter createFSMControlDTOAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlScheduler <em>FSM Control Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlScheduler
	 * @generated
	 */
	public Adapter createFSMControlSchedulerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral <em>FSM Control Peripheral</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlPeripheral
	 * @generated
	 */
	public Adapter createFSMControlPeripheralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute <em>FSM Control Button Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute
	 * @generated
	 */
	public Adapter createFSMControlButtonAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEvent <em>FSM Control Button Attribute Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEvent
	 * @generated
	 */
	public Adapter createFSMControlButtonAttributeEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard <em>FSM Control Button Attribute Event Keyboard</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard
	 * @generated
	 */
	public Adapter createFSMControlButtonAttributeEventKeyboardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventIdentity <em>FSM Control Button Attribute Event Identity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventIdentity
	 * @generated
	 */
	public Adapter createFSMControlButtonAttributeEventIdentityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventEvent <em>FSM Control Button Attribute Event Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventEvent
	 * @generated
	 */
	public Adapter createFSMControlButtonAttributeEventEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlVisibility <em>FSM Control Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlVisibility
	 * @generated
	 */
	public Adapter createFSMControlVisibilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlFieldAttribute <em>FSM Control Field Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlFieldAttribute
	 * @generated
	 */
	public Adapter createFSMControlFieldAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlFieldLayout <em>FSM Control Field Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlFieldLayout
	 * @generated
	 */
	public Adapter createFSMControlFieldLayoutAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute <em>FSM Control DTO Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlDTOAttribute
	 * @generated
	 */
	public Adapter createFSMControlDTOAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevice <em>FSM Peripheral Device</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevice
	 * @generated
	 */
	public Adapter createFSMPeripheralDeviceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay <em>FSM Peripheral Device Display</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceDisplay
	 * @generated
	 */
	public Adapter createFSMPeripheralDeviceDisplayAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceLineDisplay <em>FSM Peripheral Device Line Display</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceLineDisplay
	 * @generated
	 */
	public Adapter createFSMPeripheralDeviceLineDisplayAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePOSPrinter <em>FSM Peripheral Device POS Printer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePOSPrinter
	 * @generated
	 */
	public Adapter createFSMPeripheralDevicePOSPrinterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceCashDrawer <em>FSM Peripheral Device Cash Drawer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceCashDrawer
	 * @generated
	 */
	public Adapter createFSMPeripheralDeviceCashDrawerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePT <em>FSM Peripheral Device PT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDevicePT
	 * @generated
	 */
	public Adapter createFSMPeripheralDevicePTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceSignature <em>FSM Peripheral Device Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceSignature
	 * @generated
	 */
	public Adapter createFSMPeripheralDeviceSignatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceScale <em>FSM Peripheral Device Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMPeripheralDeviceScale
	 * @generated
	 */
	public Adapter createFSMPeripheralDeviceScaleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute <em>FSM Control Scheduler Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlSchedulerAttribute
	 * @generated
	 */
	public Adapter createFSMControlSchedulerAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMEvent <em>FSM Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMEvent
	 * @generated
	 */
	public Adapter createFSMEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMState <em>FSM State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMState
	 * @generated
	 */
	public Adapter createFSMStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMKeyMapper <em>FSM Key Mapper</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMKeyMapper
	 * @generated
	 */
	public Adapter createFSMKeyMapperAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger <em>FSM Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMTrigger
	 * @generated
	 */
	public Adapter createFSMTriggerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMAction <em>FSM Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMAction
	 * @generated
	 */
	public Adapter createFSMActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBlinkRate <em>FSM Action Peripheral Blink Rate</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBlinkRate
	 * @generated
	 */
	public Adapter createFSMActionPeripheralBlinkRateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralClear <em>FSM Action Peripheral Clear</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralClear
	 * @generated
	 */
	public Adapter createFSMActionPeripheralClearAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow <em>FSM Action Peripheral Create Window</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCreateWindow
	 * @generated
	 */
	public Adapter createFSMActionPeripheralCreateWindowAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCursorType <em>FSM Action Peripheral Cursor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralCursorType
	 * @generated
	 */
	public Adapter createFSMActionPeripheralCursorTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDestroyWindow <em>FSM Action Peripheral Destroy Window</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDestroyWindow
	 * @generated
	 */
	public Adapter createFSMActionPeripheralDestroyWindowAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDeviceBrightness <em>FSM Action Peripheral Device Brightness</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDeviceBrightness
	 * @generated
	 */
	public Adapter createFSMActionPeripheralDeviceBrightnessAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText <em>FSM Action Peripheral Display Text</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralDisplayText
	 * @generated
	 */
	public Adapter createFSMActionPeripheralDisplayTextAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText <em>FSM Action Peripheral Line Display Text</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayText
	 * @generated
	 */
	public Adapter createFSMActionPeripheralLineDisplayTextAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt <em>FSM Action Peripheral Line Display Text At</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralLineDisplayTextAt
	 * @generated
	 */
	public Adapter createFSMActionPeripheralLineDisplayTextAtAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralInterCharacterWait <em>FSM Action Peripheral Inter Character Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralInterCharacterWait
	 * @generated
	 */
	public Adapter createFSMActionPeripheralInterCharacterWaitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeFormat <em>FSM Action Peripheral Marquee Format</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeFormat
	 * @generated
	 */
	public Adapter createFSMActionPeripheralMarqueeFormatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeRepeatWait <em>FSM Action Peripheral Marquee Repeat Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeRepeatWait
	 * @generated
	 */
	public Adapter createFSMActionPeripheralMarqueeRepeatWaitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeType <em>FSM Action Peripheral Marquee Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeType
	 * @generated
	 */
	public Adapter createFSMActionPeripheralMarqueeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeUnitWait <em>FSM Action Peripheral Marquee Unit Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralMarqueeUnitWait
	 * @generated
	 */
	public Adapter createFSMActionPeripheralMarqueeUnitWaitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll <em>FSM Action Peripheral Scroll</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScroll
	 * @generated
	 */
	public Adapter createFSMActionPeripheralScrollAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralOpenDrawer <em>FSM Action Peripheral Open Drawer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralOpenDrawer
	 * @generated
	 */
	public Adapter createFSMActionPeripheralOpenDrawerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode <em>FSM Action Peripheral Print Barcode</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBarcode
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPrintBarcodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBitmap <em>FSM Action Peripheral Print Bitmap</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintBitmap
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPrintBitmapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintCut <em>FSM Action Peripheral Print Cut</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintCut
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPrintCutAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintRegisterBitmap <em>FSM Action Peripheral Print Register Bitmap</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintRegisterBitmap
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPrintRegisterBitmapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal <em>FSM Action Peripheral Print Normal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintNormal
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPrintNormalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen <em>FSM Action Peripheral PT Open</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTOpen
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPTOpenAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTClose <em>FSM Action Peripheral PT Close</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTClose
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPTCloseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal <em>FSM Action Peripheral PT Reversal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTReversal
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPTReversalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAcknowledge <em>FSM Action Peripheral PT Acknowledge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAcknowledge
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPTAcknowledgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration <em>FSM Action Peripheral PT Registration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTRegistration
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPTRegistrationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization <em>FSM Action Peripheral PT Authorization</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPTAuthorizationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBeeper <em>FSM Action Peripheral Beeper</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralBeeper
	 * @generated
	 */
	public Adapter createFSMActionPeripheralBeeperAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPlayer <em>FSM Action Peripheral Player</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPlayer
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPlayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSound <em>FSM Action Peripheral Sound</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSound
	 * @generated
	 */
	public Adapter createFSMActionPeripheralSoundAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTResponse <em>FSM Action Peripheral PT Response</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTResponse
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPTResponseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport <em>FSM Action Peripheral Print Report</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPrintReport
	 * @generated
	 */
	public Adapter createFSMActionPeripheralPrintReportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureOpen <em>FSM Action Peripheral Signature Open</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureOpen
	 * @generated
	 */
	public Adapter createFSMActionPeripheralSignatureOpenAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClose <em>FSM Action Peripheral Signature Close</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClose
	 * @generated
	 */
	public Adapter createFSMActionPeripheralSignatureCloseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClear <em>FSM Action Peripheral Signature Clear</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureClear
	 * @generated
	 */
	public Adapter createFSMActionPeripheralSignatureClearAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureCapture <em>FSM Action Peripheral Signature Capture</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureCapture
	 * @generated
	 */
	public Adapter createFSMActionPeripheralSignatureCaptureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureIdle <em>FSM Action Peripheral Signature Idle</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureIdle
	 * @generated
	 */
	public Adapter createFSMActionPeripheralSignatureIdleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel <em>FSM Action Peripheral Signature Label</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralSignatureLabel
	 * @generated
	 */
	public Adapter createFSMActionPeripheralSignatureLabelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMSignatureRetrieve <em>FSM Signature Retrieve</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMSignatureRetrieve
	 * @generated
	 */
	public Adapter createFSMSignatureRetrieveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadWeight <em>FSM Action Peripheral Scale Read Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadWeight
	 * @generated
	 */
	public Adapter createFSMActionPeripheralScaleReadWeightAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadTareWeight <em>FSM Action Peripheral Scale Read Tare Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleReadTareWeight
	 * @generated
	 */
	public Adapter createFSMActionPeripheralScaleReadTareWeightAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleTareWeight <em>FSM Action Peripheral Scale Tare Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleTareWeight
	 * @generated
	 */
	public Adapter createFSMActionPeripheralScaleTareWeightAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleZero <em>FSM Action Peripheral Scale Zero</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleZero
	 * @generated
	 */
	public Adapter createFSMActionPeripheralScaleZeroAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleDisplayText <em>FSM Action Peripheral Scale Display Text</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleDisplayText
	 * @generated
	 */
	public Adapter createFSMActionPeripheralScaleDisplayTextAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleWeightUnit <em>FSM Action Peripheral Scale Weight Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralScaleWeightUnit
	 * @generated
	 */
	public Adapter createFSMActionPeripheralScaleWeightUnitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSource <em>FSM Action Field Source</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSource
	 * @generated
	 */
	public Adapter createFSMActionFieldSourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceString <em>FSM Action Field Source String</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceString
	 * @generated
	 */
	public Adapter createFSMActionFieldSourceStringAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceNumber <em>FSM Action Field Source Number</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceNumber
	 * @generated
	 */
	public Adapter createFSMActionFieldSourceNumberAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceInteger <em>FSM Action Field Source Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceInteger
	 * @generated
	 */
	public Adapter createFSMActionFieldSourceIntegerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceBoolean <em>FSM Action Field Source Boolean</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceBoolean
	 * @generated
	 */
	public Adapter createFSMActionFieldSourceBooleanAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvaluate <em>FSM Action Field Source Evaluate</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvaluate
	 * @generated
	 */
	public Adapter createFSMActionFieldSourceEvaluateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceTranslate <em>FSM Action Field Source Translate</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceTranslate
	 * @generated
	 */
	public Adapter createFSMActionFieldSourceTranslateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMRef <em>FSM Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMRef
	 * @generated
	 */
	public Adapter createFSMRefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMDotExpression <em>FSM Dot Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMDotExpression
	 * @generated
	 */
	public Adapter createFSMDotExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMDtoRef <em>FSM Dto Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMDtoRef
	 * @generated
	 */
	public Adapter createFSMDtoRefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute <em>FSM Action Field Source Dto Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceDtoAttribute
	 * @generated
	 */
	public Adapter createFSMActionFieldSourceDtoAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvent <em>FSM Action Field Source Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvent
	 * @generated
	 */
	public Adapter createFSMActionFieldSourceEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition <em>FSM Action Conditional Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionConditionalTransition
	 * @generated
	 */
	public Adapter createFSMActionConditionalTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMOperationParameter <em>FSM Operation Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMOperationParameter
	 * @generated
	 */
	public Adapter createFSMOperationParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMOperation <em>FSM Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMOperation
	 * @generated
	 */
	public Adapter createFSMOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMGuard <em>FSM Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMGuard
	 * @generated
	 */
	public Adapter createFSMGuardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMFunction <em>FSM Function</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFunction
	 * @generated
	 */
	public Adapter createFSMFunctionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMStorageRetrieve <em>FSM Storage Retrieve</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStorageRetrieve
	 * @generated
	 */
	public Adapter createFSMStorageRetrieveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMStorage <em>FSM Storage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStorage
	 * @generated
	 */
	public Adapter createFSMStorageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldConcatenation <em>FSM Action Field Concatenation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldConcatenation
	 * @generated
	 */
	public Adapter createFSMActionFieldConcatenationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSet <em>FSM Action Field Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldSet
	 * @generated
	 */
	public Adapter createFSMActionFieldSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldKeystroke <em>FSM Action Field Keystroke</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldKeystroke
	 * @generated
	 */
	public Adapter createFSMActionFieldKeystrokeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldClear <em>FSM Action Field Clear</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldClear
	 * @generated
	 */
	public Adapter createFSMActionFieldClearAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldGet <em>FSM Action Field Get</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldGet
	 * @generated
	 */
	public Adapter createFSMActionFieldGetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldFilterToggle <em>FSM Action Field Filter Toggle</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldFilterToggle
	 * @generated
	 */
	public Adapter createFSMActionFieldFilterToggleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldRemove <em>FSM Action Field Remove</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionFieldRemove
	 * @generated
	 */
	public Adapter createFSMActionFieldRemoveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionItemVisible <em>FSM Action Item Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionItemVisible
	 * @generated
	 */
	public Adapter createFSMActionItemVisibleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionItemInvisible <em>FSM Action Item Invisible</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionItemInvisible
	 * @generated
	 */
	public Adapter createFSMActionItemInvisibleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionButtonCaption <em>FSM Action Button Caption</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionButtonCaption
	 * @generated
	 */
	public Adapter createFSMActionButtonCaptionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionButtonImage <em>FSM Action Button Image</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionButtonImage
	 * @generated
	 */
	public Adapter createFSMActionButtonImageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind <em>FSM Action DTO Find</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionDTOFind
	 * @generated
	 */
	public Adapter createFSMActionDTOFindAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionDTOClear <em>FSM Action DTO Clear</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionDTOClear
	 * @generated
	 */
	public Adapter createFSMActionDTOClearAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMActionScheduler <em>FSM Action Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMActionScheduler
	 * @generated
	 */
	public Adapter createFSMActionSchedulerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMDTOType <em>FSMDTO Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMDTOType
	 * @generated
	 */
	public Adapter createFSMDTOTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMFieldType <em>FSM Field Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFieldType
	 * @generated
	 */
	public Adapter createFSMFieldTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMAbstractFilter <em>FSM Abstract Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMAbstractFilter
	 * @generated
	 */
	public Adapter createFSMAbstractFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMFilterProperty <em>FSM Filter Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFilterProperty
	 * @generated
	 */
	public Adapter createFSMFilterPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMFilter <em>FSM Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFilter
	 * @generated
	 */
	public Adapter createFSMFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMJunctionFilter <em>FSM Junction Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMJunctionFilter
	 * @generated
	 */
	public Adapter createFSMJunctionFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMAndFilter <em>FSM And Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMAndFilter
	 * @generated
	 */
	public Adapter createFSMAndFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMOrFilter <em>FSM Or Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMOrFilter
	 * @generated
	 */
	public Adapter createFSMOrFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter <em>FSM Between Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMBetweenFilter
	 * @generated
	 */
	public Adapter createFSMBetweenFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMCompareFilter <em>FSM Compare Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMCompareFilter
	 * @generated
	 */
	public Adapter createFSMCompareFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMIsNullFilter <em>FSM Is Null Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMIsNullFilter
	 * @generated
	 */
	public Adapter createFSMIsNullFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMLikeFilter <em>FSM Like Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMLikeFilter
	 * @generated
	 */
	public Adapter createFSMLikeFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMNotFilter <em>FSM Not Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMNotFilter
	 * @generated
	 */
	public Adapter createFSMNotFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter <em>FSM String Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMStringFilter
	 * @generated
	 */
	public Adapter createFSMStringFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.statemachine.FSMControlFilter <em>FSM Control Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlFilter
	 * @generated
	 */
	public Adapter createFSMControlFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver <em>LLazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver
	 * @generated
	 */
	public Adapter createLLazyResolverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.dsl.semantic.common.types.LPackage <em>LPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LPackage
	 * @generated
	 */
	public Adapter createLPackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //StatemachineDSLAdapterFactory
