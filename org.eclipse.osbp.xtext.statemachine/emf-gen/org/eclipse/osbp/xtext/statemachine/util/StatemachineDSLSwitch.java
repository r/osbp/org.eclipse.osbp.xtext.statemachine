/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;

import org.eclipse.osbp.xtext.statemachine.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage
 * @generated
 */
public class StatemachineDSLSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static StatemachineDSLPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachineDSLSwitch() {
		if (modelPackage == null) {
			modelPackage = StatemachineDSLPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case StatemachineDSLPackage.FSM_MODEL: {
				FSMModel fsmModel = (FSMModel)theEObject;
				T result = caseFSMModel(fsmModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_LAZY_RESOLVER: {
				FSMLazyResolver fsmLazyResolver = (FSMLazyResolver)theEObject;
				T result = caseFSMLazyResolver(fsmLazyResolver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_BASE: {
				FSMBase fsmBase = (FSMBase)theEObject;
				T result = caseFSMBase(fsmBase);
				if (result == null) result = caseFSMLazyResolver(fsmBase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_PACKAGE: {
				FSMPackage fsmPackage = (FSMPackage)theEObject;
				T result = caseFSMPackage(fsmPackage);
				if (result == null) result = caseLPackage(fsmPackage);
				if (result == null) result = caseLLazyResolver(fsmPackage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM: {
				FSM fsm = (FSM)theEObject;
				T result = caseFSM(fsm);
				if (result == null) result = caseFSMBase(fsm);
				if (result == null) result = caseFSMLazyResolver(fsm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL: {
				FSMControl fsmControl = (FSMControl)theEObject;
				T result = caseFSMControl(fsmControl);
				if (result == null) result = caseFSMBase(fsmControl);
				if (result == null) result = caseFSMLazyResolver(fsmControl);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON: {
				FSMControlButton fsmControlButton = (FSMControlButton)theEObject;
				T result = caseFSMControlButton(fsmControlButton);
				if (result == null) result = caseFSMControl(fsmControlButton);
				if (result == null) result = caseFSMControlVisibility(fsmControlButton);
				if (result == null) result = caseFSMBase(fsmControlButton);
				if (result == null) result = caseFSMLazyResolver(fsmControlButton);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_FIELD: {
				FSMControlField fsmControlField = (FSMControlField)theEObject;
				T result = caseFSMControlField(fsmControlField);
				if (result == null) result = caseFSMControl(fsmControlField);
				if (result == null) result = caseFSMBase(fsmControlField);
				if (result == null) result = caseFSMLazyResolver(fsmControlField);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_DTO: {
				FSMControlDTO fsmControlDTO = (FSMControlDTO)theEObject;
				T result = caseFSMControlDTO(fsmControlDTO);
				if (result == null) result = caseFSMControl(fsmControlDTO);
				if (result == null) result = caseFSMBase(fsmControlDTO);
				if (result == null) result = caseFSMLazyResolver(fsmControlDTO);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_SCHEDULER: {
				FSMControlScheduler fsmControlScheduler = (FSMControlScheduler)theEObject;
				T result = caseFSMControlScheduler(fsmControlScheduler);
				if (result == null) result = caseFSMControl(fsmControlScheduler);
				if (result == null) result = caseFSMBase(fsmControlScheduler);
				if (result == null) result = caseFSMLazyResolver(fsmControlScheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_PERIPHERAL: {
				FSMControlPeripheral fsmControlPeripheral = (FSMControlPeripheral)theEObject;
				T result = caseFSMControlPeripheral(fsmControlPeripheral);
				if (result == null) result = caseFSMControl(fsmControlPeripheral);
				if (result == null) result = caseFSMBase(fsmControlPeripheral);
				if (result == null) result = caseFSMLazyResolver(fsmControlPeripheral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE: {
				FSMControlButtonAttribute fsmControlButtonAttribute = (FSMControlButtonAttribute)theEObject;
				T result = caseFSMControlButtonAttribute(fsmControlButtonAttribute);
				if (result == null) result = caseFSMControlVisibility(fsmControlButtonAttribute);
				if (result == null) result = caseFSMBase(fsmControlButtonAttribute);
				if (result == null) result = caseFSMLazyResolver(fsmControlButtonAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT: {
				FSMControlButtonAttributeEvent fsmControlButtonAttributeEvent = (FSMControlButtonAttributeEvent)theEObject;
				T result = caseFSMControlButtonAttributeEvent(fsmControlButtonAttributeEvent);
				if (result == null) result = caseFSMLazyResolver(fsmControlButtonAttributeEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_KEYBOARD: {
				FSMControlButtonAttributeEventKeyboard fsmControlButtonAttributeEventKeyboard = (FSMControlButtonAttributeEventKeyboard)theEObject;
				T result = caseFSMControlButtonAttributeEventKeyboard(fsmControlButtonAttributeEventKeyboard);
				if (result == null) result = caseFSMControlButtonAttributeEvent(fsmControlButtonAttributeEventKeyboard);
				if (result == null) result = caseFSMLazyResolver(fsmControlButtonAttributeEventKeyboard);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_IDENTITY: {
				FSMControlButtonAttributeEventIdentity fsmControlButtonAttributeEventIdentity = (FSMControlButtonAttributeEventIdentity)theEObject;
				T result = caseFSMControlButtonAttributeEventIdentity(fsmControlButtonAttributeEventIdentity);
				if (result == null) result = caseFSMControlButtonAttributeEvent(fsmControlButtonAttributeEventIdentity);
				if (result == null) result = caseFSMLazyResolver(fsmControlButtonAttributeEventIdentity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_BUTTON_ATTRIBUTE_EVENT_EVENT: {
				FSMControlButtonAttributeEventEvent fsmControlButtonAttributeEventEvent = (FSMControlButtonAttributeEventEvent)theEObject;
				T result = caseFSMControlButtonAttributeEventEvent(fsmControlButtonAttributeEventEvent);
				if (result == null) result = caseFSMControlButtonAttributeEvent(fsmControlButtonAttributeEventEvent);
				if (result == null) result = caseFSMLazyResolver(fsmControlButtonAttributeEventEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_VISIBILITY: {
				FSMControlVisibility fsmControlVisibility = (FSMControlVisibility)theEObject;
				T result = caseFSMControlVisibility(fsmControlVisibility);
				if (result == null) result = caseFSMBase(fsmControlVisibility);
				if (result == null) result = caseFSMLazyResolver(fsmControlVisibility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_FIELD_ATTRIBUTE: {
				FSMControlFieldAttribute fsmControlFieldAttribute = (FSMControlFieldAttribute)theEObject;
				T result = caseFSMControlFieldAttribute(fsmControlFieldAttribute);
				if (result == null) result = caseFSMControlVisibility(fsmControlFieldAttribute);
				if (result == null) result = caseFSMBase(fsmControlFieldAttribute);
				if (result == null) result = caseFSMLazyResolver(fsmControlFieldAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_FIELD_LAYOUT: {
				FSMControlFieldLayout fsmControlFieldLayout = (FSMControlFieldLayout)theEObject;
				T result = caseFSMControlFieldLayout(fsmControlFieldLayout);
				if (result == null) result = caseFSMControlVisibility(fsmControlFieldLayout);
				if (result == null) result = caseFSMBase(fsmControlFieldLayout);
				if (result == null) result = caseFSMLazyResolver(fsmControlFieldLayout);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_DTO_ATTRIBUTE: {
				FSMControlDTOAttribute fsmControlDTOAttribute = (FSMControlDTOAttribute)theEObject;
				T result = caseFSMControlDTOAttribute(fsmControlDTOAttribute);
				if (result == null) result = caseFSMBase(fsmControlDTOAttribute);
				if (result == null) result = caseFSMLazyResolver(fsmControlDTOAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE: {
				FSMPeripheralDevice fsmPeripheralDevice = (FSMPeripheralDevice)theEObject;
				T result = caseFSMPeripheralDevice(fsmPeripheralDevice);
				if (result == null) result = caseFSMBase(fsmPeripheralDevice);
				if (result == null) result = caseFSMLazyResolver(fsmPeripheralDevice);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_DISPLAY: {
				FSMPeripheralDeviceDisplay fsmPeripheralDeviceDisplay = (FSMPeripheralDeviceDisplay)theEObject;
				T result = caseFSMPeripheralDeviceDisplay(fsmPeripheralDeviceDisplay);
				if (result == null) result = caseFSMPeripheralDevice(fsmPeripheralDeviceDisplay);
				if (result == null) result = caseFSMBase(fsmPeripheralDeviceDisplay);
				if (result == null) result = caseFSMLazyResolver(fsmPeripheralDeviceDisplay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_LINE_DISPLAY: {
				FSMPeripheralDeviceLineDisplay fsmPeripheralDeviceLineDisplay = (FSMPeripheralDeviceLineDisplay)theEObject;
				T result = caseFSMPeripheralDeviceLineDisplay(fsmPeripheralDeviceLineDisplay);
				if (result == null) result = caseFSMPeripheralDevice(fsmPeripheralDeviceLineDisplay);
				if (result == null) result = caseFSMBase(fsmPeripheralDeviceLineDisplay);
				if (result == null) result = caseFSMLazyResolver(fsmPeripheralDeviceLineDisplay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_POS_PRINTER: {
				FSMPeripheralDevicePOSPrinter fsmPeripheralDevicePOSPrinter = (FSMPeripheralDevicePOSPrinter)theEObject;
				T result = caseFSMPeripheralDevicePOSPrinter(fsmPeripheralDevicePOSPrinter);
				if (result == null) result = caseFSMPeripheralDevice(fsmPeripheralDevicePOSPrinter);
				if (result == null) result = caseFSMBase(fsmPeripheralDevicePOSPrinter);
				if (result == null) result = caseFSMLazyResolver(fsmPeripheralDevicePOSPrinter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_CASH_DRAWER: {
				FSMPeripheralDeviceCashDrawer fsmPeripheralDeviceCashDrawer = (FSMPeripheralDeviceCashDrawer)theEObject;
				T result = caseFSMPeripheralDeviceCashDrawer(fsmPeripheralDeviceCashDrawer);
				if (result == null) result = caseFSMPeripheralDevice(fsmPeripheralDeviceCashDrawer);
				if (result == null) result = caseFSMBase(fsmPeripheralDeviceCashDrawer);
				if (result == null) result = caseFSMLazyResolver(fsmPeripheralDeviceCashDrawer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_PT: {
				FSMPeripheralDevicePT fsmPeripheralDevicePT = (FSMPeripheralDevicePT)theEObject;
				T result = caseFSMPeripheralDevicePT(fsmPeripheralDevicePT);
				if (result == null) result = caseFSMPeripheralDevice(fsmPeripheralDevicePT);
				if (result == null) result = caseFSMBase(fsmPeripheralDevicePT);
				if (result == null) result = caseFSMLazyResolver(fsmPeripheralDevicePT);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_SIGNATURE: {
				FSMPeripheralDeviceSignature fsmPeripheralDeviceSignature = (FSMPeripheralDeviceSignature)theEObject;
				T result = caseFSMPeripheralDeviceSignature(fsmPeripheralDeviceSignature);
				if (result == null) result = caseFSMPeripheralDevice(fsmPeripheralDeviceSignature);
				if (result == null) result = caseFSMBase(fsmPeripheralDeviceSignature);
				if (result == null) result = caseFSMLazyResolver(fsmPeripheralDeviceSignature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_PERIPHERAL_DEVICE_SCALE: {
				FSMPeripheralDeviceScale fsmPeripheralDeviceScale = (FSMPeripheralDeviceScale)theEObject;
				T result = caseFSMPeripheralDeviceScale(fsmPeripheralDeviceScale);
				if (result == null) result = caseFSMPeripheralDevice(fsmPeripheralDeviceScale);
				if (result == null) result = caseFSMBase(fsmPeripheralDeviceScale);
				if (result == null) result = caseFSMLazyResolver(fsmPeripheralDeviceScale);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_SCHEDULER_ATTRIBUTE: {
				FSMControlSchedulerAttribute fsmControlSchedulerAttribute = (FSMControlSchedulerAttribute)theEObject;
				T result = caseFSMControlSchedulerAttribute(fsmControlSchedulerAttribute);
				if (result == null) result = caseFSMBase(fsmControlSchedulerAttribute);
				if (result == null) result = caseFSMLazyResolver(fsmControlSchedulerAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_EVENT: {
				FSMEvent fsmEvent = (FSMEvent)theEObject;
				T result = caseFSMEvent(fsmEvent);
				if (result == null) result = caseFSMBase(fsmEvent);
				if (result == null) result = caseFSMLazyResolver(fsmEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_STATE: {
				FSMState fsmState = (FSMState)theEObject;
				T result = caseFSMState(fsmState);
				if (result == null) result = caseFSMBase(fsmState);
				if (result == null) result = caseFSMLazyResolver(fsmState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_KEY_MAPPER: {
				FSMKeyMapper fsmKeyMapper = (FSMKeyMapper)theEObject;
				T result = caseFSMKeyMapper(fsmKeyMapper);
				if (result == null) result = caseFSMLazyResolver(fsmKeyMapper);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_TRIGGER: {
				FSMTrigger fsmTrigger = (FSMTrigger)theEObject;
				T result = caseFSMTrigger(fsmTrigger);
				if (result == null) result = caseFSMLazyResolver(fsmTrigger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION: {
				FSMAction fsmAction = (FSMAction)theEObject;
				T result = caseFSMAction(fsmAction);
				if (result == null) result = caseFSMLazyResolver(fsmAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_BLINK_RATE: {
				FSMActionPeripheralBlinkRate fsmActionPeripheralBlinkRate = (FSMActionPeripheralBlinkRate)theEObject;
				T result = caseFSMActionPeripheralBlinkRate(fsmActionPeripheralBlinkRate);
				if (result == null) result = caseFSMAction(fsmActionPeripheralBlinkRate);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralBlinkRate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CLEAR: {
				FSMActionPeripheralClear fsmActionPeripheralClear = (FSMActionPeripheralClear)theEObject;
				T result = caseFSMActionPeripheralClear(fsmActionPeripheralClear);
				if (result == null) result = caseFSMAction(fsmActionPeripheralClear);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralClear);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CREATE_WINDOW: {
				FSMActionPeripheralCreateWindow fsmActionPeripheralCreateWindow = (FSMActionPeripheralCreateWindow)theEObject;
				T result = caseFSMActionPeripheralCreateWindow(fsmActionPeripheralCreateWindow);
				if (result == null) result = caseFSMAction(fsmActionPeripheralCreateWindow);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralCreateWindow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_CURSOR_TYPE: {
				FSMActionPeripheralCursorType fsmActionPeripheralCursorType = (FSMActionPeripheralCursorType)theEObject;
				T result = caseFSMActionPeripheralCursorType(fsmActionPeripheralCursorType);
				if (result == null) result = caseFSMAction(fsmActionPeripheralCursorType);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralCursorType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_DESTROY_WINDOW: {
				FSMActionPeripheralDestroyWindow fsmActionPeripheralDestroyWindow = (FSMActionPeripheralDestroyWindow)theEObject;
				T result = caseFSMActionPeripheralDestroyWindow(fsmActionPeripheralDestroyWindow);
				if (result == null) result = caseFSMAction(fsmActionPeripheralDestroyWindow);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralDestroyWindow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_DEVICE_BRIGHTNESS: {
				FSMActionPeripheralDeviceBrightness fsmActionPeripheralDeviceBrightness = (FSMActionPeripheralDeviceBrightness)theEObject;
				T result = caseFSMActionPeripheralDeviceBrightness(fsmActionPeripheralDeviceBrightness);
				if (result == null) result = caseFSMAction(fsmActionPeripheralDeviceBrightness);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralDeviceBrightness);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_DISPLAY_TEXT: {
				FSMActionPeripheralDisplayText fsmActionPeripheralDisplayText = (FSMActionPeripheralDisplayText)theEObject;
				T result = caseFSMActionPeripheralDisplayText(fsmActionPeripheralDisplayText);
				if (result == null) result = caseFSMAction(fsmActionPeripheralDisplayText);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralDisplayText);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT: {
				FSMActionPeripheralLineDisplayText fsmActionPeripheralLineDisplayText = (FSMActionPeripheralLineDisplayText)theEObject;
				T result = caseFSMActionPeripheralLineDisplayText(fsmActionPeripheralLineDisplayText);
				if (result == null) result = caseFSMAction(fsmActionPeripheralLineDisplayText);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralLineDisplayText);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_LINE_DISPLAY_TEXT_AT: {
				FSMActionPeripheralLineDisplayTextAt fsmActionPeripheralLineDisplayTextAt = (FSMActionPeripheralLineDisplayTextAt)theEObject;
				T result = caseFSMActionPeripheralLineDisplayTextAt(fsmActionPeripheralLineDisplayTextAt);
				if (result == null) result = caseFSMAction(fsmActionPeripheralLineDisplayTextAt);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralLineDisplayTextAt);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_INTER_CHARACTER_WAIT: {
				FSMActionPeripheralInterCharacterWait fsmActionPeripheralInterCharacterWait = (FSMActionPeripheralInterCharacterWait)theEObject;
				T result = caseFSMActionPeripheralInterCharacterWait(fsmActionPeripheralInterCharacterWait);
				if (result == null) result = caseFSMAction(fsmActionPeripheralInterCharacterWait);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralInterCharacterWait);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_FORMAT: {
				FSMActionPeripheralMarqueeFormat fsmActionPeripheralMarqueeFormat = (FSMActionPeripheralMarqueeFormat)theEObject;
				T result = caseFSMActionPeripheralMarqueeFormat(fsmActionPeripheralMarqueeFormat);
				if (result == null) result = caseFSMAction(fsmActionPeripheralMarqueeFormat);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralMarqueeFormat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_REPEAT_WAIT: {
				FSMActionPeripheralMarqueeRepeatWait fsmActionPeripheralMarqueeRepeatWait = (FSMActionPeripheralMarqueeRepeatWait)theEObject;
				T result = caseFSMActionPeripheralMarqueeRepeatWait(fsmActionPeripheralMarqueeRepeatWait);
				if (result == null) result = caseFSMAction(fsmActionPeripheralMarqueeRepeatWait);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralMarqueeRepeatWait);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_TYPE: {
				FSMActionPeripheralMarqueeType fsmActionPeripheralMarqueeType = (FSMActionPeripheralMarqueeType)theEObject;
				T result = caseFSMActionPeripheralMarqueeType(fsmActionPeripheralMarqueeType);
				if (result == null) result = caseFSMAction(fsmActionPeripheralMarqueeType);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralMarqueeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_MARQUEE_UNIT_WAIT: {
				FSMActionPeripheralMarqueeUnitWait fsmActionPeripheralMarqueeUnitWait = (FSMActionPeripheralMarqueeUnitWait)theEObject;
				T result = caseFSMActionPeripheralMarqueeUnitWait(fsmActionPeripheralMarqueeUnitWait);
				if (result == null) result = caseFSMAction(fsmActionPeripheralMarqueeUnitWait);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralMarqueeUnitWait);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCROLL: {
				FSMActionPeripheralScroll fsmActionPeripheralScroll = (FSMActionPeripheralScroll)theEObject;
				T result = caseFSMActionPeripheralScroll(fsmActionPeripheralScroll);
				if (result == null) result = caseFSMAction(fsmActionPeripheralScroll);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralScroll);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_OPEN_DRAWER: {
				FSMActionPeripheralOpenDrawer fsmActionPeripheralOpenDrawer = (FSMActionPeripheralOpenDrawer)theEObject;
				T result = caseFSMActionPeripheralOpenDrawer(fsmActionPeripheralOpenDrawer);
				if (result == null) result = caseFSMAction(fsmActionPeripheralOpenDrawer);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralOpenDrawer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_BARCODE: {
				FSMActionPeripheralPrintBarcode fsmActionPeripheralPrintBarcode = (FSMActionPeripheralPrintBarcode)theEObject;
				T result = caseFSMActionPeripheralPrintBarcode(fsmActionPeripheralPrintBarcode);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPrintBarcode);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPrintBarcode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_BITMAP: {
				FSMActionPeripheralPrintBitmap fsmActionPeripheralPrintBitmap = (FSMActionPeripheralPrintBitmap)theEObject;
				T result = caseFSMActionPeripheralPrintBitmap(fsmActionPeripheralPrintBitmap);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPrintBitmap);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPrintBitmap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_CUT: {
				FSMActionPeripheralPrintCut fsmActionPeripheralPrintCut = (FSMActionPeripheralPrintCut)theEObject;
				T result = caseFSMActionPeripheralPrintCut(fsmActionPeripheralPrintCut);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPrintCut);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPrintCut);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_REGISTER_BITMAP: {
				FSMActionPeripheralPrintRegisterBitmap fsmActionPeripheralPrintRegisterBitmap = (FSMActionPeripheralPrintRegisterBitmap)theEObject;
				T result = caseFSMActionPeripheralPrintRegisterBitmap(fsmActionPeripheralPrintRegisterBitmap);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPrintRegisterBitmap);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPrintRegisterBitmap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_NORMAL: {
				FSMActionPeripheralPrintNormal fsmActionPeripheralPrintNormal = (FSMActionPeripheralPrintNormal)theEObject;
				T result = caseFSMActionPeripheralPrintNormal(fsmActionPeripheralPrintNormal);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPrintNormal);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPrintNormal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_OPEN: {
				FSMActionPeripheralPTOpen fsmActionPeripheralPTOpen = (FSMActionPeripheralPTOpen)theEObject;
				T result = caseFSMActionPeripheralPTOpen(fsmActionPeripheralPTOpen);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPTOpen);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPTOpen);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_CLOSE: {
				FSMActionPeripheralPTClose fsmActionPeripheralPTClose = (FSMActionPeripheralPTClose)theEObject;
				T result = caseFSMActionPeripheralPTClose(fsmActionPeripheralPTClose);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPTClose);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPTClose);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REVERSAL: {
				FSMActionPeripheralPTReversal fsmActionPeripheralPTReversal = (FSMActionPeripheralPTReversal)theEObject;
				T result = caseFSMActionPeripheralPTReversal(fsmActionPeripheralPTReversal);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPTReversal);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPTReversal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_ACKNOWLEDGE: {
				FSMActionPeripheralPTAcknowledge fsmActionPeripheralPTAcknowledge = (FSMActionPeripheralPTAcknowledge)theEObject;
				T result = caseFSMActionPeripheralPTAcknowledge(fsmActionPeripheralPTAcknowledge);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPTAcknowledge);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPTAcknowledge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_REGISTRATION: {
				FSMActionPeripheralPTRegistration fsmActionPeripheralPTRegistration = (FSMActionPeripheralPTRegistration)theEObject;
				T result = caseFSMActionPeripheralPTRegistration(fsmActionPeripheralPTRegistration);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPTRegistration);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPTRegistration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_AUTHORIZATION: {
				FSMActionPeripheralPTAuthorization fsmActionPeripheralPTAuthorization = (FSMActionPeripheralPTAuthorization)theEObject;
				T result = caseFSMActionPeripheralPTAuthorization(fsmActionPeripheralPTAuthorization);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPTAuthorization);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPTAuthorization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_BEEPER: {
				FSMActionPeripheralBeeper fsmActionPeripheralBeeper = (FSMActionPeripheralBeeper)theEObject;
				T result = caseFSMActionPeripheralBeeper(fsmActionPeripheralBeeper);
				if (result == null) result = caseFSMAction(fsmActionPeripheralBeeper);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralBeeper);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PLAYER: {
				FSMActionPeripheralPlayer fsmActionPeripheralPlayer = (FSMActionPeripheralPlayer)theEObject;
				T result = caseFSMActionPeripheralPlayer(fsmActionPeripheralPlayer);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPlayer);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPlayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SOUND: {
				FSMActionPeripheralSound fsmActionPeripheralSound = (FSMActionPeripheralSound)theEObject;
				T result = caseFSMActionPeripheralSound(fsmActionPeripheralSound);
				if (result == null) result = caseFSMAction(fsmActionPeripheralSound);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralSound);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PT_RESPONSE: {
				FSMActionPeripheralPTResponse fsmActionPeripheralPTResponse = (FSMActionPeripheralPTResponse)theEObject;
				T result = caseFSMActionPeripheralPTResponse(fsmActionPeripheralPTResponse);
				if (result == null) result = caseFSMActionFieldSource(fsmActionPeripheralPTResponse);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPTResponse);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPTResponse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_PRINT_REPORT: {
				FSMActionPeripheralPrintReport fsmActionPeripheralPrintReport = (FSMActionPeripheralPrintReport)theEObject;
				T result = caseFSMActionPeripheralPrintReport(fsmActionPeripheralPrintReport);
				if (result == null) result = caseFSMAction(fsmActionPeripheralPrintReport);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralPrintReport);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_OPEN: {
				FSMActionPeripheralSignatureOpen fsmActionPeripheralSignatureOpen = (FSMActionPeripheralSignatureOpen)theEObject;
				T result = caseFSMActionPeripheralSignatureOpen(fsmActionPeripheralSignatureOpen);
				if (result == null) result = caseFSMAction(fsmActionPeripheralSignatureOpen);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralSignatureOpen);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_CLOSE: {
				FSMActionPeripheralSignatureClose fsmActionPeripheralSignatureClose = (FSMActionPeripheralSignatureClose)theEObject;
				T result = caseFSMActionPeripheralSignatureClose(fsmActionPeripheralSignatureClose);
				if (result == null) result = caseFSMAction(fsmActionPeripheralSignatureClose);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralSignatureClose);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_CLEAR: {
				FSMActionPeripheralSignatureClear fsmActionPeripheralSignatureClear = (FSMActionPeripheralSignatureClear)theEObject;
				T result = caseFSMActionPeripheralSignatureClear(fsmActionPeripheralSignatureClear);
				if (result == null) result = caseFSMAction(fsmActionPeripheralSignatureClear);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralSignatureClear);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_CAPTURE: {
				FSMActionPeripheralSignatureCapture fsmActionPeripheralSignatureCapture = (FSMActionPeripheralSignatureCapture)theEObject;
				T result = caseFSMActionPeripheralSignatureCapture(fsmActionPeripheralSignatureCapture);
				if (result == null) result = caseFSMAction(fsmActionPeripheralSignatureCapture);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralSignatureCapture);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_IDLE: {
				FSMActionPeripheralSignatureIdle fsmActionPeripheralSignatureIdle = (FSMActionPeripheralSignatureIdle)theEObject;
				T result = caseFSMActionPeripheralSignatureIdle(fsmActionPeripheralSignatureIdle);
				if (result == null) result = caseFSMAction(fsmActionPeripheralSignatureIdle);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralSignatureIdle);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SIGNATURE_LABEL: {
				FSMActionPeripheralSignatureLabel fsmActionPeripheralSignatureLabel = (FSMActionPeripheralSignatureLabel)theEObject;
				T result = caseFSMActionPeripheralSignatureLabel(fsmActionPeripheralSignatureLabel);
				if (result == null) result = caseFSMAction(fsmActionPeripheralSignatureLabel);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralSignatureLabel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_SIGNATURE_RETRIEVE: {
				FSMSignatureRetrieve fsmSignatureRetrieve = (FSMSignatureRetrieve)theEObject;
				T result = caseFSMSignatureRetrieve(fsmSignatureRetrieve);
				if (result == null) result = caseFSMActionFieldSource(fsmSignatureRetrieve);
				if (result == null) result = caseFSMAction(fsmSignatureRetrieve);
				if (result == null) result = caseFSMLazyResolver(fsmSignatureRetrieve);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_READ_WEIGHT: {
				FSMActionPeripheralScaleReadWeight fsmActionPeripheralScaleReadWeight = (FSMActionPeripheralScaleReadWeight)theEObject;
				T result = caseFSMActionPeripheralScaleReadWeight(fsmActionPeripheralScaleReadWeight);
				if (result == null) result = caseFSMActionFieldSource(fsmActionPeripheralScaleReadWeight);
				if (result == null) result = caseFSMAction(fsmActionPeripheralScaleReadWeight);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralScaleReadWeight);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_READ_TARE_WEIGHT: {
				FSMActionPeripheralScaleReadTareWeight fsmActionPeripheralScaleReadTareWeight = (FSMActionPeripheralScaleReadTareWeight)theEObject;
				T result = caseFSMActionPeripheralScaleReadTareWeight(fsmActionPeripheralScaleReadTareWeight);
				if (result == null) result = caseFSMActionFieldSource(fsmActionPeripheralScaleReadTareWeight);
				if (result == null) result = caseFSMAction(fsmActionPeripheralScaleReadTareWeight);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralScaleReadTareWeight);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_TARE_WEIGHT: {
				FSMActionPeripheralScaleTareWeight fsmActionPeripheralScaleTareWeight = (FSMActionPeripheralScaleTareWeight)theEObject;
				T result = caseFSMActionPeripheralScaleTareWeight(fsmActionPeripheralScaleTareWeight);
				if (result == null) result = caseFSMActionFieldSource(fsmActionPeripheralScaleTareWeight);
				if (result == null) result = caseFSMAction(fsmActionPeripheralScaleTareWeight);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralScaleTareWeight);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_ZERO: {
				FSMActionPeripheralScaleZero fsmActionPeripheralScaleZero = (FSMActionPeripheralScaleZero)theEObject;
				T result = caseFSMActionPeripheralScaleZero(fsmActionPeripheralScaleZero);
				if (result == null) result = caseFSMActionFieldSource(fsmActionPeripheralScaleZero);
				if (result == null) result = caseFSMAction(fsmActionPeripheralScaleZero);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralScaleZero);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_DISPLAY_TEXT: {
				FSMActionPeripheralScaleDisplayText fsmActionPeripheralScaleDisplayText = (FSMActionPeripheralScaleDisplayText)theEObject;
				T result = caseFSMActionPeripheralScaleDisplayText(fsmActionPeripheralScaleDisplayText);
				if (result == null) result = caseFSMActionFieldSource(fsmActionPeripheralScaleDisplayText);
				if (result == null) result = caseFSMAction(fsmActionPeripheralScaleDisplayText);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralScaleDisplayText);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_PERIPHERAL_SCALE_WEIGHT_UNIT: {
				FSMActionPeripheralScaleWeightUnit fsmActionPeripheralScaleWeightUnit = (FSMActionPeripheralScaleWeightUnit)theEObject;
				T result = caseFSMActionPeripheralScaleWeightUnit(fsmActionPeripheralScaleWeightUnit);
				if (result == null) result = caseFSMActionFieldSource(fsmActionPeripheralScaleWeightUnit);
				if (result == null) result = caseFSMAction(fsmActionPeripheralScaleWeightUnit);
				if (result == null) result = caseFSMLazyResolver(fsmActionPeripheralScaleWeightUnit);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE: {
				FSMActionFieldSource fsmActionFieldSource = (FSMActionFieldSource)theEObject;
				T result = caseFSMActionFieldSource(fsmActionFieldSource);
				if (result == null) result = caseFSMAction(fsmActionFieldSource);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_STRING: {
				FSMActionFieldSourceString fsmActionFieldSourceString = (FSMActionFieldSourceString)theEObject;
				T result = caseFSMActionFieldSourceString(fsmActionFieldSourceString);
				if (result == null) result = caseFSMActionFieldSource(fsmActionFieldSourceString);
				if (result == null) result = caseFSMAction(fsmActionFieldSourceString);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldSourceString);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_NUMBER: {
				FSMActionFieldSourceNumber fsmActionFieldSourceNumber = (FSMActionFieldSourceNumber)theEObject;
				T result = caseFSMActionFieldSourceNumber(fsmActionFieldSourceNumber);
				if (result == null) result = caseFSMActionFieldSource(fsmActionFieldSourceNumber);
				if (result == null) result = caseFSMAction(fsmActionFieldSourceNumber);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldSourceNumber);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_INTEGER: {
				FSMActionFieldSourceInteger fsmActionFieldSourceInteger = (FSMActionFieldSourceInteger)theEObject;
				T result = caseFSMActionFieldSourceInteger(fsmActionFieldSourceInteger);
				if (result == null) result = caseFSMActionFieldSource(fsmActionFieldSourceInteger);
				if (result == null) result = caseFSMAction(fsmActionFieldSourceInteger);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldSourceInteger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_BOOLEAN: {
				FSMActionFieldSourceBoolean fsmActionFieldSourceBoolean = (FSMActionFieldSourceBoolean)theEObject;
				T result = caseFSMActionFieldSourceBoolean(fsmActionFieldSourceBoolean);
				if (result == null) result = caseFSMActionFieldSource(fsmActionFieldSourceBoolean);
				if (result == null) result = caseFSMAction(fsmActionFieldSourceBoolean);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldSourceBoolean);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_EVALUATE: {
				FSMActionFieldSourceEvaluate fsmActionFieldSourceEvaluate = (FSMActionFieldSourceEvaluate)theEObject;
				T result = caseFSMActionFieldSourceEvaluate(fsmActionFieldSourceEvaluate);
				if (result == null) result = caseFSMActionFieldSource(fsmActionFieldSourceEvaluate);
				if (result == null) result = caseFSMAction(fsmActionFieldSourceEvaluate);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldSourceEvaluate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_TRANSLATE: {
				FSMActionFieldSourceTranslate fsmActionFieldSourceTranslate = (FSMActionFieldSourceTranslate)theEObject;
				T result = caseFSMActionFieldSourceTranslate(fsmActionFieldSourceTranslate);
				if (result == null) result = caseFSMActionFieldSource(fsmActionFieldSourceTranslate);
				if (result == null) result = caseFSMAction(fsmActionFieldSourceTranslate);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldSourceTranslate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_REF: {
				FSMRef fsmRef = (FSMRef)theEObject;
				T result = caseFSMRef(fsmRef);
				if (result == null) result = caseFSMBase(fsmRef);
				if (result == null) result = caseFSMLazyResolver(fsmRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_DOT_EXPRESSION: {
				FSMDotExpression fsmDotExpression = (FSMDotExpression)theEObject;
				T result = caseFSMDotExpression(fsmDotExpression);
				if (result == null) result = caseFSMRef(fsmDotExpression);
				if (result == null) result = caseFSMBase(fsmDotExpression);
				if (result == null) result = caseFSMLazyResolver(fsmDotExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_DTO_REF: {
				FSMDtoRef fsmDtoRef = (FSMDtoRef)theEObject;
				T result = caseFSMDtoRef(fsmDtoRef);
				if (result == null) result = caseFSMRef(fsmDtoRef);
				if (result == null) result = caseFSMBase(fsmDtoRef);
				if (result == null) result = caseFSMLazyResolver(fsmDtoRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_DTO_ATTRIBUTE: {
				FSMActionFieldSourceDtoAttribute fsmActionFieldSourceDtoAttribute = (FSMActionFieldSourceDtoAttribute)theEObject;
				T result = caseFSMActionFieldSourceDtoAttribute(fsmActionFieldSourceDtoAttribute);
				if (result == null) result = caseFSMActionFieldSource(fsmActionFieldSourceDtoAttribute);
				if (result == null) result = caseFSMAction(fsmActionFieldSourceDtoAttribute);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldSourceDtoAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SOURCE_EVENT: {
				FSMActionFieldSourceEvent fsmActionFieldSourceEvent = (FSMActionFieldSourceEvent)theEObject;
				T result = caseFSMActionFieldSourceEvent(fsmActionFieldSourceEvent);
				if (result == null) result = caseFSMActionFieldSource(fsmActionFieldSourceEvent);
				if (result == null) result = caseFSMAction(fsmActionFieldSourceEvent);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldSourceEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_CONDITIONAL_TRANSITION: {
				FSMActionConditionalTransition fsmActionConditionalTransition = (FSMActionConditionalTransition)theEObject;
				T result = caseFSMActionConditionalTransition(fsmActionConditionalTransition);
				if (result == null) result = caseFSMAction(fsmActionConditionalTransition);
				if (result == null) result = caseFSMLazyResolver(fsmActionConditionalTransition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_OPERATION_PARAMETER: {
				FSMOperationParameter fsmOperationParameter = (FSMOperationParameter)theEObject;
				T result = caseFSMOperationParameter(fsmOperationParameter);
				if (result == null) result = caseFSMAction(fsmOperationParameter);
				if (result == null) result = caseFSMLazyResolver(fsmOperationParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_OPERATION: {
				FSMOperation fsmOperation = (FSMOperation)theEObject;
				T result = caseFSMOperation(fsmOperation);
				if (result == null) result = caseFSMActionFieldSource(fsmOperation);
				if (result == null) result = caseFSMAction(fsmOperation);
				if (result == null) result = caseFSMLazyResolver(fsmOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_GUARD: {
				FSMGuard fsmGuard = (FSMGuard)theEObject;
				T result = caseFSMGuard(fsmGuard);
				if (result == null) result = caseFSMLazyResolver(fsmGuard);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_FUNCTION: {
				FSMFunction fsmFunction = (FSMFunction)theEObject;
				T result = caseFSMFunction(fsmFunction);
				if (result == null) result = caseFSMActionFieldSource(fsmFunction);
				if (result == null) result = caseFSMAction(fsmFunction);
				if (result == null) result = caseFSMLazyResolver(fsmFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_STORAGE_RETRIEVE: {
				FSMStorageRetrieve fsmStorageRetrieve = (FSMStorageRetrieve)theEObject;
				T result = caseFSMStorageRetrieve(fsmStorageRetrieve);
				if (result == null) result = caseFSMActionFieldSource(fsmStorageRetrieve);
				if (result == null) result = caseFSMAction(fsmStorageRetrieve);
				if (result == null) result = caseFSMLazyResolver(fsmStorageRetrieve);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_STORAGE: {
				FSMStorage fsmStorage = (FSMStorage)theEObject;
				T result = caseFSMStorage(fsmStorage);
				if (result == null) result = caseFSMAction(fsmStorage);
				if (result == null) result = caseFSMLazyResolver(fsmStorage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_CONCATENATION: {
				FSMActionFieldConcatenation fsmActionFieldConcatenation = (FSMActionFieldConcatenation)theEObject;
				T result = caseFSMActionFieldConcatenation(fsmActionFieldConcatenation);
				if (result == null) result = caseFSMAction(fsmActionFieldConcatenation);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldConcatenation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_SET: {
				FSMActionFieldSet fsmActionFieldSet = (FSMActionFieldSet)theEObject;
				T result = caseFSMActionFieldSet(fsmActionFieldSet);
				if (result == null) result = caseFSMAction(fsmActionFieldSet);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_KEYSTROKE: {
				FSMActionFieldKeystroke fsmActionFieldKeystroke = (FSMActionFieldKeystroke)theEObject;
				T result = caseFSMActionFieldKeystroke(fsmActionFieldKeystroke);
				if (result == null) result = caseFSMAction(fsmActionFieldKeystroke);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldKeystroke);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_CLEAR: {
				FSMActionFieldClear fsmActionFieldClear = (FSMActionFieldClear)theEObject;
				T result = caseFSMActionFieldClear(fsmActionFieldClear);
				if (result == null) result = caseFSMAction(fsmActionFieldClear);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldClear);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_GET: {
				FSMActionFieldGet fsmActionFieldGet = (FSMActionFieldGet)theEObject;
				T result = caseFSMActionFieldGet(fsmActionFieldGet);
				if (result == null) result = caseFSMActionFieldSource(fsmActionFieldGet);
				if (result == null) result = caseFSMAction(fsmActionFieldGet);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldGet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_FILTER_TOGGLE: {
				FSMActionFieldFilterToggle fsmActionFieldFilterToggle = (FSMActionFieldFilterToggle)theEObject;
				T result = caseFSMActionFieldFilterToggle(fsmActionFieldFilterToggle);
				if (result == null) result = caseFSMActionFieldSource(fsmActionFieldFilterToggle);
				if (result == null) result = caseFSMAction(fsmActionFieldFilterToggle);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldFilterToggle);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_FIELD_REMOVE: {
				FSMActionFieldRemove fsmActionFieldRemove = (FSMActionFieldRemove)theEObject;
				T result = caseFSMActionFieldRemove(fsmActionFieldRemove);
				if (result == null) result = caseFSMActionFieldSource(fsmActionFieldRemove);
				if (result == null) result = caseFSMAction(fsmActionFieldRemove);
				if (result == null) result = caseFSMLazyResolver(fsmActionFieldRemove);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_ITEM_VISIBLE: {
				FSMActionItemVisible fsmActionItemVisible = (FSMActionItemVisible)theEObject;
				T result = caseFSMActionItemVisible(fsmActionItemVisible);
				if (result == null) result = caseFSMAction(fsmActionItemVisible);
				if (result == null) result = caseFSMLazyResolver(fsmActionItemVisible);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_ITEM_INVISIBLE: {
				FSMActionItemInvisible fsmActionItemInvisible = (FSMActionItemInvisible)theEObject;
				T result = caseFSMActionItemInvisible(fsmActionItemInvisible);
				if (result == null) result = caseFSMAction(fsmActionItemInvisible);
				if (result == null) result = caseFSMLazyResolver(fsmActionItemInvisible);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_BUTTON_CAPTION: {
				FSMActionButtonCaption fsmActionButtonCaption = (FSMActionButtonCaption)theEObject;
				T result = caseFSMActionButtonCaption(fsmActionButtonCaption);
				if (result == null) result = caseFSMAction(fsmActionButtonCaption);
				if (result == null) result = caseFSMLazyResolver(fsmActionButtonCaption);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_BUTTON_IMAGE: {
				FSMActionButtonImage fsmActionButtonImage = (FSMActionButtonImage)theEObject;
				T result = caseFSMActionButtonImage(fsmActionButtonImage);
				if (result == null) result = caseFSMAction(fsmActionButtonImage);
				if (result == null) result = caseFSMLazyResolver(fsmActionButtonImage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_DTO_FIND: {
				FSMActionDTOFind fsmActionDTOFind = (FSMActionDTOFind)theEObject;
				T result = caseFSMActionDTOFind(fsmActionDTOFind);
				if (result == null) result = caseFSMAction(fsmActionDTOFind);
				if (result == null) result = caseFSMLazyResolver(fsmActionDTOFind);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_DTO_CLEAR: {
				FSMActionDTOClear fsmActionDTOClear = (FSMActionDTOClear)theEObject;
				T result = caseFSMActionDTOClear(fsmActionDTOClear);
				if (result == null) result = caseFSMAction(fsmActionDTOClear);
				if (result == null) result = caseFSMLazyResolver(fsmActionDTOClear);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ACTION_SCHEDULER: {
				FSMActionScheduler fsmActionScheduler = (FSMActionScheduler)theEObject;
				T result = caseFSMActionScheduler(fsmActionScheduler);
				if (result == null) result = caseFSMAction(fsmActionScheduler);
				if (result == null) result = caseFSMLazyResolver(fsmActionScheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSMDTO_TYPE: {
				FSMDTOType fsmdtoType = (FSMDTOType)theEObject;
				T result = caseFSMDTOType(fsmdtoType);
				if (result == null) result = caseFSMLazyResolver(fsmdtoType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_FIELD_TYPE: {
				FSMFieldType fsmFieldType = (FSMFieldType)theEObject;
				T result = caseFSMFieldType(fsmFieldType);
				if (result == null) result = caseFSMLazyResolver(fsmFieldType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_ABSTRACT_FILTER: {
				FSMAbstractFilter fsmAbstractFilter = (FSMAbstractFilter)theEObject;
				T result = caseFSMAbstractFilter(fsmAbstractFilter);
				if (result == null) result = caseFSMLazyResolver(fsmAbstractFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_FILTER_PROPERTY: {
				FSMFilterProperty fsmFilterProperty = (FSMFilterProperty)theEObject;
				T result = caseFSMFilterProperty(fsmFilterProperty);
				if (result == null) result = caseFSMLazyResolver(fsmFilterProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_FILTER: {
				FSMFilter fsmFilter = (FSMFilter)theEObject;
				T result = caseFSMFilter(fsmFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_JUNCTION_FILTER: {
				FSMJunctionFilter fsmJunctionFilter = (FSMJunctionFilter)theEObject;
				T result = caseFSMJunctionFilter(fsmJunctionFilter);
				if (result == null) result = caseFSMAbstractFilter(fsmJunctionFilter);
				if (result == null) result = caseFSMLazyResolver(fsmJunctionFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_AND_FILTER: {
				FSMAndFilter fsmAndFilter = (FSMAndFilter)theEObject;
				T result = caseFSMAndFilter(fsmAndFilter);
				if (result == null) result = caseFSMJunctionFilter(fsmAndFilter);
				if (result == null) result = caseFSMAbstractFilter(fsmAndFilter);
				if (result == null) result = caseFSMLazyResolver(fsmAndFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_OR_FILTER: {
				FSMOrFilter fsmOrFilter = (FSMOrFilter)theEObject;
				T result = caseFSMOrFilter(fsmOrFilter);
				if (result == null) result = caseFSMJunctionFilter(fsmOrFilter);
				if (result == null) result = caseFSMAbstractFilter(fsmOrFilter);
				if (result == null) result = caseFSMLazyResolver(fsmOrFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_BETWEEN_FILTER: {
				FSMBetweenFilter fsmBetweenFilter = (FSMBetweenFilter)theEObject;
				T result = caseFSMBetweenFilter(fsmBetweenFilter);
				if (result == null) result = caseFSMAbstractFilter(fsmBetweenFilter);
				if (result == null) result = caseFSMLazyResolver(fsmBetweenFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_COMPARE_FILTER: {
				FSMCompareFilter fsmCompareFilter = (FSMCompareFilter)theEObject;
				T result = caseFSMCompareFilter(fsmCompareFilter);
				if (result == null) result = caseFSMAbstractFilter(fsmCompareFilter);
				if (result == null) result = caseFSMLazyResolver(fsmCompareFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_IS_NULL_FILTER: {
				FSMIsNullFilter fsmIsNullFilter = (FSMIsNullFilter)theEObject;
				T result = caseFSMIsNullFilter(fsmIsNullFilter);
				if (result == null) result = caseFSMAbstractFilter(fsmIsNullFilter);
				if (result == null) result = caseFSMLazyResolver(fsmIsNullFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_LIKE_FILTER: {
				FSMLikeFilter fsmLikeFilter = (FSMLikeFilter)theEObject;
				T result = caseFSMLikeFilter(fsmLikeFilter);
				if (result == null) result = caseFSMAbstractFilter(fsmLikeFilter);
				if (result == null) result = caseFSMLazyResolver(fsmLikeFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_NOT_FILTER: {
				FSMNotFilter fsmNotFilter = (FSMNotFilter)theEObject;
				T result = caseFSMNotFilter(fsmNotFilter);
				if (result == null) result = caseFSMAbstractFilter(fsmNotFilter);
				if (result == null) result = caseFSMLazyResolver(fsmNotFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_STRING_FILTER: {
				FSMStringFilter fsmStringFilter = (FSMStringFilter)theEObject;
				T result = caseFSMStringFilter(fsmStringFilter);
				if (result == null) result = caseFSMAbstractFilter(fsmStringFilter);
				if (result == null) result = caseFSMLazyResolver(fsmStringFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StatemachineDSLPackage.FSM_CONTROL_FILTER: {
				FSMControlFilter fsmControlFilter = (FSMControlFilter)theEObject;
				T result = caseFSMControlFilter(fsmControlFilter);
				if (result == null) result = caseFSMBase(fsmControlFilter);
				if (result == null) result = caseFSMLazyResolver(fsmControlFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMModel(FSMModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Lazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMLazyResolver(FSMLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Base</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Base</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMBase(FSMBase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Package</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Package</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMPackage(FSMPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSM(FSM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControl(FSMControl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Button</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Button</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlButton(FSMControlButton object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlField(FSMControlField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control DTO</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control DTO</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlDTO(FSMControlDTO object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlScheduler(FSMControlScheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Peripheral</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Peripheral</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlPeripheral(FSMControlPeripheral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Button Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Button Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlButtonAttribute(FSMControlButtonAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Button Attribute Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Button Attribute Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlButtonAttributeEvent(FSMControlButtonAttributeEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Button Attribute Event Keyboard</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Button Attribute Event Keyboard</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlButtonAttributeEventKeyboard(FSMControlButtonAttributeEventKeyboard object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Button Attribute Event Identity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Button Attribute Event Identity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlButtonAttributeEventIdentity(FSMControlButtonAttributeEventIdentity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Button Attribute Event Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Button Attribute Event Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlButtonAttributeEventEvent(FSMControlButtonAttributeEventEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Visibility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Visibility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlVisibility(FSMControlVisibility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Field Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Field Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlFieldAttribute(FSMControlFieldAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Field Layout</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Field Layout</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlFieldLayout(FSMControlFieldLayout object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control DTO Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control DTO Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlDTOAttribute(FSMControlDTOAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Peripheral Device</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Peripheral Device</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMPeripheralDevice(FSMPeripheralDevice object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Peripheral Device Display</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Peripheral Device Display</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMPeripheralDeviceDisplay(FSMPeripheralDeviceDisplay object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Peripheral Device Line Display</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Peripheral Device Line Display</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMPeripheralDeviceLineDisplay(FSMPeripheralDeviceLineDisplay object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Peripheral Device POS Printer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Peripheral Device POS Printer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMPeripheralDevicePOSPrinter(FSMPeripheralDevicePOSPrinter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Peripheral Device Cash Drawer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Peripheral Device Cash Drawer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMPeripheralDeviceCashDrawer(FSMPeripheralDeviceCashDrawer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Peripheral Device PT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Peripheral Device PT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMPeripheralDevicePT(FSMPeripheralDevicePT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Peripheral Device Signature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Peripheral Device Signature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMPeripheralDeviceSignature(FSMPeripheralDeviceSignature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Peripheral Device Scale</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Peripheral Device Scale</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMPeripheralDeviceScale(FSMPeripheralDeviceScale object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Scheduler Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Scheduler Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlSchedulerAttribute(FSMControlSchedulerAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMEvent(FSMEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMState(FSMState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Key Mapper</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Key Mapper</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMKeyMapper(FSMKeyMapper object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Trigger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMTrigger(FSMTrigger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMAction(FSMAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Blink Rate</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Blink Rate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralBlinkRate(FSMActionPeripheralBlinkRate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Clear</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Clear</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralClear(FSMActionPeripheralClear object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Create Window</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Create Window</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralCreateWindow(FSMActionPeripheralCreateWindow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Cursor Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Cursor Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralCursorType(FSMActionPeripheralCursorType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Destroy Window</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Destroy Window</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralDestroyWindow(FSMActionPeripheralDestroyWindow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Device Brightness</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Device Brightness</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralDeviceBrightness(FSMActionPeripheralDeviceBrightness object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Display Text</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Display Text</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralDisplayText(FSMActionPeripheralDisplayText object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Line Display Text</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Line Display Text</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralLineDisplayText(FSMActionPeripheralLineDisplayText object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Line Display Text At</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Line Display Text At</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralLineDisplayTextAt(FSMActionPeripheralLineDisplayTextAt object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Inter Character Wait</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Inter Character Wait</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralInterCharacterWait(FSMActionPeripheralInterCharacterWait object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Marquee Format</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Marquee Format</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralMarqueeFormat(FSMActionPeripheralMarqueeFormat object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Marquee Repeat Wait</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Marquee Repeat Wait</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralMarqueeRepeatWait(FSMActionPeripheralMarqueeRepeatWait object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Marquee Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Marquee Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralMarqueeType(FSMActionPeripheralMarqueeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Marquee Unit Wait</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Marquee Unit Wait</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralMarqueeUnitWait(FSMActionPeripheralMarqueeUnitWait object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scroll</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scroll</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralScroll(FSMActionPeripheralScroll object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Open Drawer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Open Drawer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralOpenDrawer(FSMActionPeripheralOpenDrawer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Barcode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Barcode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPrintBarcode(FSMActionPeripheralPrintBarcode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Bitmap</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Bitmap</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPrintBitmap(FSMActionPeripheralPrintBitmap object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Cut</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Cut</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPrintCut(FSMActionPeripheralPrintCut object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Register Bitmap</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Register Bitmap</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPrintRegisterBitmap(FSMActionPeripheralPrintRegisterBitmap object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Normal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Normal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPrintNormal(FSMActionPeripheralPrintNormal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Open</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Open</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPTOpen(FSMActionPeripheralPTOpen object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Close</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Close</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPTClose(FSMActionPeripheralPTClose object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Reversal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Reversal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPTReversal(FSMActionPeripheralPTReversal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Acknowledge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Acknowledge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPTAcknowledge(FSMActionPeripheralPTAcknowledge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Registration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Registration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPTRegistration(FSMActionPeripheralPTRegistration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Authorization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Authorization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPTAuthorization(FSMActionPeripheralPTAuthorization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Beeper</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Beeper</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralBeeper(FSMActionPeripheralBeeper object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Player</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Player</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPlayer(FSMActionPeripheralPlayer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Sound</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Sound</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralSound(FSMActionPeripheralSound object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Response</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral PT Response</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPTResponse(FSMActionPeripheralPTResponse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Report</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Print Report</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralPrintReport(FSMActionPeripheralPrintReport object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Open</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Open</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralSignatureOpen(FSMActionPeripheralSignatureOpen object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Close</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Close</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralSignatureClose(FSMActionPeripheralSignatureClose object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Clear</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Clear</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralSignatureClear(FSMActionPeripheralSignatureClear object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Capture</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Capture</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralSignatureCapture(FSMActionPeripheralSignatureCapture object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Idle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Idle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralSignatureIdle(FSMActionPeripheralSignatureIdle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Label</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Signature Label</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralSignatureLabel(FSMActionPeripheralSignatureLabel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Signature Retrieve</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Signature Retrieve</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMSignatureRetrieve(FSMSignatureRetrieve object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Read Weight</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Read Weight</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralScaleReadWeight(FSMActionPeripheralScaleReadWeight object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Read Tare Weight</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Read Tare Weight</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralScaleReadTareWeight(FSMActionPeripheralScaleReadTareWeight object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Tare Weight</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Tare Weight</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralScaleTareWeight(FSMActionPeripheralScaleTareWeight object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Zero</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Zero</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralScaleZero(FSMActionPeripheralScaleZero object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Display Text</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Display Text</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralScaleDisplayText(FSMActionPeripheralScaleDisplayText object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Weight Unit</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Peripheral Scale Weight Unit</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionPeripheralScaleWeightUnit(FSMActionPeripheralScaleWeightUnit object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldSource(FSMActionFieldSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Source String</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Source String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldSourceString(FSMActionFieldSourceString object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Source Number</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Source Number</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldSourceNumber(FSMActionFieldSourceNumber object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Source Integer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Source Integer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldSourceInteger(FSMActionFieldSourceInteger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Source Boolean</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Source Boolean</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldSourceBoolean(FSMActionFieldSourceBoolean object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Source Evaluate</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Source Evaluate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldSourceEvaluate(FSMActionFieldSourceEvaluate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Source Translate</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Source Translate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldSourceTranslate(FSMActionFieldSourceTranslate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMRef(FSMRef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Dot Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Dot Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMDotExpression(FSMDotExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Dto Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Dto Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMDtoRef(FSMDtoRef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Source Dto Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Source Dto Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldSourceDtoAttribute(FSMActionFieldSourceDtoAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Source Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Source Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldSourceEvent(FSMActionFieldSourceEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Conditional Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Conditional Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionConditionalTransition(FSMActionConditionalTransition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Operation Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Operation Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMOperationParameter(FSMOperationParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMOperation(FSMOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Guard</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Guard</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMGuard(FSMGuard object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMFunction(FSMFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Storage Retrieve</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Storage Retrieve</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMStorageRetrieve(FSMStorageRetrieve object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Storage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Storage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMStorage(FSMStorage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Concatenation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Concatenation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldConcatenation(FSMActionFieldConcatenation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldSet(FSMActionFieldSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Keystroke</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Keystroke</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldKeystroke(FSMActionFieldKeystroke object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Clear</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Clear</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldClear(FSMActionFieldClear object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Get</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Get</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldGet(FSMActionFieldGet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Filter Toggle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Filter Toggle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldFilterToggle(FSMActionFieldFilterToggle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Field Remove</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Field Remove</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionFieldRemove(FSMActionFieldRemove object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Item Visible</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Item Visible</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionItemVisible(FSMActionItemVisible object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Item Invisible</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Item Invisible</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionItemInvisible(FSMActionItemInvisible object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Button Caption</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Button Caption</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionButtonCaption(FSMActionButtonCaption object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Button Image</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Button Image</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionButtonImage(FSMActionButtonImage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action DTO Find</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action DTO Find</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionDTOFind(FSMActionDTOFind object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action DTO Clear</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action DTO Clear</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionDTOClear(FSMActionDTOClear object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Action Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Action Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMActionScheduler(FSMActionScheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSMDTO Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSMDTO Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMDTOType(FSMDTOType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Field Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Field Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMFieldType(FSMFieldType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Abstract Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Abstract Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMAbstractFilter(FSMAbstractFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Filter Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Filter Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMFilterProperty(FSMFilterProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMFilter(FSMFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Junction Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Junction Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMJunctionFilter(FSMJunctionFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM And Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM And Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMAndFilter(FSMAndFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Or Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Or Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMOrFilter(FSMOrFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Between Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Between Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMBetweenFilter(FSMBetweenFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Compare Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Compare Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMCompareFilter(FSMCompareFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Is Null Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Is Null Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMIsNullFilter(FSMIsNullFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Like Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Like Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMLikeFilter(FSMLikeFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Not Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Not Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMNotFilter(FSMNotFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM String Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM String Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMStringFilter(FSMStringFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Control Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Control Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMControlFilter(FSMControlFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLLazyResolver(LLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLPackage(LPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //StatemachineDSLSwitch
