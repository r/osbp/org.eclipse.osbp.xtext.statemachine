/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.osbp.dsl.semantic.dto.LDto;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSMDTO Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMDTOType#getAttributeType <em>Attribute Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMDTOType()
 * @model
 * @generated
 */
public interface FSMDTOType extends FSMLazyResolver {
	/**
	 * Returns the value of the '<em><b>Attribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Type</em>' reference.
	 * @see #setAttributeType(LDto)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMDTOType_AttributeType()
	 * @model
	 * @generated
	 */
	LDto getAttributeType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMDTOType#getAttributeType <em>Attribute Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Type</em>' reference.
	 * @see #getAttributeType()
	 * @generated
	 */
	void setAttributeType(LDto value);

} // FSMDTOType
