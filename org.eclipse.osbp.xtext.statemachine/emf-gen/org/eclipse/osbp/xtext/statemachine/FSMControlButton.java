/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Control Button</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getEventType <em>Event Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getButtons <em>Buttons</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#isHasRange <em>Has Range</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getStart <em>Start</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getEnd <em>End</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getRangedName <em>Ranged Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButton()
 * @model
 * @generated
 */
public interface FSMControlButton extends FSMControl, FSMControlVisibility {
	/**
	 * Returns the value of the '<em><b>Event Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Type</em>' attribute.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType
	 * @see #setEventType(FSMControlButtonEventType)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButton_EventType()
	 * @model unique="false"
	 * @generated
	 */
	FSMControlButtonEventType getEventType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getEventType <em>Event Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Type</em>' attribute.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMControlButtonEventType
	 * @see #getEventType()
	 * @generated
	 */
	void setEventType(FSMControlButtonEventType value);

	/**
	 * Returns the value of the '<em><b>Buttons</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Buttons</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Buttons</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButton_Buttons()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMControlButtonAttribute> getButtons();

	/**
	 * Returns the value of the '<em><b>Has Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Range</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Range</em>' attribute.
	 * @see #setHasRange(boolean)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButton_HasRange()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasRange();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#isHasRange <em>Has Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Range</em>' attribute.
	 * @see #isHasRange()
	 * @generated
	 */
	void setHasRange(boolean value);

	/**
	 * Returns the value of the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' attribute.
	 * @see #setStart(int)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButton_Start()
	 * @model unique="false"
	 * @generated
	 */
	int getStart();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getStart <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' attribute.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(int value);

	/**
	 * Returns the value of the '<em><b>End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End</em>' attribute.
	 * @see #setEnd(int)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButton_End()
	 * @model unique="false"
	 * @generated
	 */
	int getEnd();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getEnd <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End</em>' attribute.
	 * @see #getEnd()
	 * @generated
	 */
	void setEnd(int value);

	/**
	 * Returns the value of the '<em><b>Ranged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ranged Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ranged Name</em>' attribute.
	 * @see #setRangedName(String)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButton_RangedName()
	 * @model unique="false"
	 * @generated
	 */
	String getRangedName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButton#getRangedName <em>Ranged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ranged Name</em>' attribute.
	 * @see #getRangedName()
	 * @generated
	 */
	void setRangedName(String value);

} // FSMControlButton
