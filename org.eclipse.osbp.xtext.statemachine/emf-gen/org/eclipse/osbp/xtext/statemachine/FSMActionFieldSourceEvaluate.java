/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Action Field Source Evaluate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvaluate#getEvaluationtype <em>Evaluationtype</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionFieldSourceEvaluate()
 * @model
 * @generated
 */
public interface FSMActionFieldSourceEvaluate extends FSMActionFieldSource {
	/**
	 * Returns the value of the '<em><b>Evaluationtype</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.statemachine.FSMEvaluationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluationtype</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluationtype</em>' attribute.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMEvaluationType
	 * @see #setEvaluationtype(FSMEvaluationType)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionFieldSourceEvaluate_Evaluationtype()
	 * @model unique="false"
	 * @generated
	 */
	FSMEvaluationType getEvaluationtype();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionFieldSourceEvaluate#getEvaluationtype <em>Evaluationtype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluationtype</em>' attribute.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMEvaluationType
	 * @see #getEvaluationtype()
	 * @generated
	 */
	void setEvaluationtype(FSMEvaluationType value);

} // FSMActionFieldSourceEvaluate
