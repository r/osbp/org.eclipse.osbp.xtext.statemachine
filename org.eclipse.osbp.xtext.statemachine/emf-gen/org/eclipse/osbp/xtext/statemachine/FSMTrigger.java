/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Trigger</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#isHasTransition <em>Has Transition</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#getTransition <em>Transition</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#getTriggers <em>Triggers</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#getGuards <em>Guards</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMTrigger()
 * @model
 * @generated
 */
public interface FSMTrigger extends FSMLazyResolver {
	/**
	 * Returns the value of the '<em><b>Has Transition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Transition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Transition</em>' attribute.
	 * @see #setHasTransition(boolean)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMTrigger_HasTransition()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasTransition();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#isHasTransition <em>Has Transition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Transition</em>' attribute.
	 * @see #isHasTransition()
	 * @generated
	 */
	void setHasTransition(boolean value);

	/**
	 * Returns the value of the '<em><b>Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' reference.
	 * @see #setTransition(FSMState)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMTrigger_Transition()
	 * @model
	 * @generated
	 */
	FSMState getTransition();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMTrigger#getTransition <em>Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transition</em>' reference.
	 * @see #getTransition()
	 * @generated
	 */
	void setTransition(FSMState value);

	/**
	 * Returns the value of the '<em><b>Triggers</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Triggers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Triggers</em>' reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMTrigger_Triggers()
	 * @model
	 * @generated
	 */
	EList<FSMEvent> getTriggers();

	/**
	 * Returns the value of the '<em><b>Guards</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMGuard}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Guards</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guards</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMTrigger_Guards()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMGuard> getGuards();

	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMTrigger_Actions()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMAction> getActions();

} // FSMTrigger
