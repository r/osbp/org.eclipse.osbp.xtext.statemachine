/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>FSM User Message Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMUserMessageType()
 * @model
 * @generated
 */
public enum FSMUserMessageType implements Enumerator {
	/**
	 * The '<em><b>HUMANIZED MESSAGE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HUMANIZED_MESSAGE_VALUE
	 * @generated
	 * @ordered
	 */
	HUMANIZED_MESSAGE(0, "HUMANIZED_MESSAGE", "humanized"),

	/**
	 * The '<em><b>WARNING MESSAGE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WARNING_MESSAGE_VALUE
	 * @generated
	 * @ordered
	 */
	WARNING_MESSAGE(0, "WARNING_MESSAGE", "warning"),

	/**
	 * The '<em><b>ERROR MESSAGE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ERROR_MESSAGE_VALUE
	 * @generated
	 * @ordered
	 */
	ERROR_MESSAGE(0, "ERROR_MESSAGE", "error"),

	/**
	 * The '<em><b>TRAY NOTIFICATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TRAY_NOTIFICATION_VALUE
	 * @generated
	 * @ordered
	 */
	TRAY_NOTIFICATION(0, "TRAY_NOTIFICATION", "tray"),

	/**
	 * The '<em><b>ASSISTIVE NOTIFICATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASSISTIVE_NOTIFICATION_VALUE
	 * @generated
	 * @ordered
	 */
	ASSISTIVE_NOTIFICATION(0, "ASSISTIVE_NOTIFICATION", "assistive");

	/**
	 * The '<em><b>HUMANIZED MESSAGE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>HUMANIZED MESSAGE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HUMANIZED_MESSAGE
	 * @model literal="humanized"
	 * @generated
	 * @ordered
	 */
	public static final int HUMANIZED_MESSAGE_VALUE = 0;

	/**
	 * The '<em><b>WARNING MESSAGE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>WARNING MESSAGE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WARNING_MESSAGE
	 * @model literal="warning"
	 * @generated
	 * @ordered
	 */
	public static final int WARNING_MESSAGE_VALUE = 0;

	/**
	 * The '<em><b>ERROR MESSAGE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ERROR MESSAGE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ERROR_MESSAGE
	 * @model literal="error"
	 * @generated
	 * @ordered
	 */
	public static final int ERROR_MESSAGE_VALUE = 0;

	/**
	 * The '<em><b>TRAY NOTIFICATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TRAY NOTIFICATION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRAY_NOTIFICATION
	 * @model literal="tray"
	 * @generated
	 * @ordered
	 */
	public static final int TRAY_NOTIFICATION_VALUE = 0;

	/**
	 * The '<em><b>ASSISTIVE NOTIFICATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ASSISTIVE NOTIFICATION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSISTIVE_NOTIFICATION
	 * @model literal="assistive"
	 * @generated
	 * @ordered
	 */
	public static final int ASSISTIVE_NOTIFICATION_VALUE = 0;

	/**
	 * An array of all the '<em><b>FSM User Message Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final FSMUserMessageType[] VALUES_ARRAY =
		new FSMUserMessageType[] {
			HUMANIZED_MESSAGE,
			WARNING_MESSAGE,
			ERROR_MESSAGE,
			TRAY_NOTIFICATION,
			ASSISTIVE_NOTIFICATION,
		};

	/**
	 * A public read-only list of all the '<em><b>FSM User Message Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<FSMUserMessageType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>FSM User Message Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMUserMessageType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMUserMessageType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM User Message Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMUserMessageType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMUserMessageType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM User Message Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMUserMessageType get(int value) {
		switch (value) {
			case HUMANIZED_MESSAGE_VALUE: return HUMANIZED_MESSAGE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FSMUserMessageType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //FSMUserMessageType
