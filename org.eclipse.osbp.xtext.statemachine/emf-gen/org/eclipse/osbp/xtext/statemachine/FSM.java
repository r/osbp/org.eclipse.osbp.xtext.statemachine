/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSM#isDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSM#getDescriptionValue <em>Description Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSM#getInitialEvent <em>Initial Event</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSM#getInitialState <em>Initial State</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSM#getEvents <em>Events</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSM#getControls <em>Controls</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSM#getStates <em>States</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSM()
 * @model
 * @generated
 */
public interface FSM extends FSMBase {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(boolean)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSM_Description()
	 * @model unique="false"
	 * @generated
	 */
	boolean isDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSM#isDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #isDescription()
	 * @generated
	 */
	void setDescription(boolean value);

	/**
	 * Returns the value of the '<em><b>Description Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description Value</em>' attribute.
	 * @see #setDescriptionValue(String)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSM_DescriptionValue()
	 * @model unique="false"
	 * @generated
	 */
	String getDescriptionValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSM#getDescriptionValue <em>Description Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description Value</em>' attribute.
	 * @see #getDescriptionValue()
	 * @generated
	 */
	void setDescriptionValue(String value);

	/**
	 * Returns the value of the '<em><b>Initial Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Event</em>' reference.
	 * @see #setInitialEvent(FSMEvent)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSM_InitialEvent()
	 * @model
	 * @generated
	 */
	FSMEvent getInitialEvent();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSM#getInitialEvent <em>Initial Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Event</em>' reference.
	 * @see #getInitialEvent()
	 * @generated
	 */
	void setInitialEvent(FSMEvent value);

	/**
	 * Returns the value of the '<em><b>Initial State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial State</em>' reference.
	 * @see #setInitialState(FSMState)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSM_InitialState()
	 * @model
	 * @generated
	 */
	FSMState getInitialState();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSM#getInitialState <em>Initial State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial State</em>' reference.
	 * @see #getInitialState()
	 * @generated
	 */
	void setInitialState(FSMState value);

	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSM_Events()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMEvent> getEvents();

	/**
	 * Returns the value of the '<em><b>Controls</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMControl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controls</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controls</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSM_Controls()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMControl> getControls();

	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSM_States()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMState> getStates();

} // FSM
