/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Action Peripheral PT Authorization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization#getDevice <em>Device</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization#getAmount <em>Amount</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionPeripheralPTAuthorization()
 * @model
 * @generated
 */
public interface FSMActionPeripheralPTAuthorization extends FSMAction {
	/**
	 * Returns the value of the '<em><b>Device</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device</em>' reference.
	 * @see #setDevice(FSMPeripheralDevicePT)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionPeripheralPTAuthorization_Device()
	 * @model
	 * @generated
	 */
	FSMPeripheralDevicePT getDevice();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization#getDevice <em>Device</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Device</em>' reference.
	 * @see #getDevice()
	 * @generated
	 */
	void setDevice(FSMPeripheralDevicePT value);

	/**
	 * Returns the value of the '<em><b>Amount</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Amount</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Amount</em>' containment reference.
	 * @see #setAmount(FSMActionFieldConcatenation)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMActionPeripheralPTAuthorization_Amount()
	 * @model containment="true"
	 * @generated
	 */
	FSMActionFieldConcatenation getAmount();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMActionPeripheralPTAuthorization#getAmount <em>Amount</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Amount</em>' containment reference.
	 * @see #getAmount()
	 * @generated
	 */
	void setAmount(FSMActionFieldConcatenation value);

} // FSMActionPeripheralPTAuthorization
