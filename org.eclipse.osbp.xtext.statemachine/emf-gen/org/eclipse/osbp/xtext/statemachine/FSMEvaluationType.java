/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>FSM Evaluation Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMEvaluationType()
 * @model
 * @generated
 */
public enum FSMEvaluationType implements Enumerator {
	/**
	 * The '<em><b>IP ADDRESS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IP_ADDRESS_VALUE
	 * @generated
	 * @ordered
	 */
	IP_ADDRESS(0, "IP_ADDRESS", "getIPAddress"),

	/**
	 * The '<em><b>HOSTNAME</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HOSTNAME_VALUE
	 * @generated
	 * @ordered
	 */
	HOSTNAME(0, "HOSTNAME", "getHostName"),

	/**
	 * The '<em><b>NOW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOW_VALUE
	 * @generated
	 * @ordered
	 */
	NOW(0, "NOW", "getNow"),

	/**
	 * The '<em><b>USER AGENT INFO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USER_AGENT_INFO_VALUE
	 * @generated
	 * @ordered
	 */
	USER_AGENT_INFO(0, "USER_AGENT_INFO", "getUserAgentInfo"),

	/**
	 * The '<em><b>IS TOUCH DEVICE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_TOUCH_DEVICE_VALUE
	 * @generated
	 * @ordered
	 */
	IS_TOUCH_DEVICE(0, "IS_TOUCH_DEVICE", "isTouchDevice"),

	/**
	 * The '<em><b>IS HTTPS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IS_HTTPS_VALUE
	 * @generated
	 * @ordered
	 */
	IS_HTTPS(0, "IS_HTTPS", "isHttps"),

	/**
	 * The '<em><b>BROWSER LOCALE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BROWSER_LOCALE_VALUE
	 * @generated
	 * @ordered
	 */
	BROWSER_LOCALE(0, "BROWSER_LOCALE", "getBrowserLocale"),

	/**
	 * The '<em><b>USER NAME</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USER_NAME_VALUE
	 * @generated
	 * @ordered
	 */
	USER_NAME(0, "USER_NAME", "getUserName"),

	/**
	 * The '<em><b>USER PASSWORD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USER_PASSWORD_VALUE
	 * @generated
	 * @ordered
	 */
	USER_PASSWORD(0, "USER_PASSWORD", "getUserPassword"),

	/**
	 * The '<em><b>USER EMAIL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USER_EMAIL_VALUE
	 * @generated
	 * @ordered
	 */
	USER_EMAIL(0, "USER_EMAIL", "getUserEmail"),

	/**
	 * The '<em><b>USER POSITION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USER_POSITION_VALUE
	 * @generated
	 * @ordered
	 */
	USER_POSITION(0, "USER_POSITION", "getUserPosition"),

	/**
	 * The '<em><b>USER PRTSERVICE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USER_PRTSERVICE_VALUE
	 * @generated
	 * @ordered
	 */
	USER_PRTSERVICE(0, "USER_PRTSERVICE", "getUserPrintService"),

	/**
	 * The '<em><b>SCREEN WIDTH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SCREEN_WIDTH_VALUE
	 * @generated
	 * @ordered
	 */
	SCREEN_WIDTH(0, "SCREEN_WIDTH", "getSceenWidth"),

	/**
	 * The '<em><b>SCREEN HEIGHT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SCREEN_HEIGHT_VALUE
	 * @generated
	 * @ordered
	 */
	SCREEN_HEIGHT(0, "SCREEN_HEIGHT", "getScreenHeight"),

	/**
	 * The '<em><b>TRIGGER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TRIGGER_VALUE
	 * @generated
	 * @ordered
	 */
	TRIGGER(0, "TRIGGER", "getTrigger");

	/**
	 * The '<em><b>IP ADDRESS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>IP ADDRESS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IP_ADDRESS
	 * @model literal="getIPAddress"
	 * @generated
	 * @ordered
	 */
	public static final int IP_ADDRESS_VALUE = 0;

	/**
	 * The '<em><b>HOSTNAME</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>HOSTNAME</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HOSTNAME
	 * @model literal="getHostName"
	 * @generated
	 * @ordered
	 */
	public static final int HOSTNAME_VALUE = 0;

	/**
	 * The '<em><b>NOW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NOW</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOW
	 * @model literal="getNow"
	 * @generated
	 * @ordered
	 */
	public static final int NOW_VALUE = 0;

	/**
	 * The '<em><b>USER AGENT INFO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>USER AGENT INFO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #USER_AGENT_INFO
	 * @model literal="getUserAgentInfo"
	 * @generated
	 * @ordered
	 */
	public static final int USER_AGENT_INFO_VALUE = 0;

	/**
	 * The '<em><b>IS TOUCH DEVICE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>IS TOUCH DEVICE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_TOUCH_DEVICE
	 * @model literal="isTouchDevice"
	 * @generated
	 * @ordered
	 */
	public static final int IS_TOUCH_DEVICE_VALUE = 0;

	/**
	 * The '<em><b>IS HTTPS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>IS HTTPS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IS_HTTPS
	 * @model literal="isHttps"
	 * @generated
	 * @ordered
	 */
	public static final int IS_HTTPS_VALUE = 0;

	/**
	 * The '<em><b>BROWSER LOCALE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BROWSER LOCALE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BROWSER_LOCALE
	 * @model literal="getBrowserLocale"
	 * @generated
	 * @ordered
	 */
	public static final int BROWSER_LOCALE_VALUE = 0;

	/**
	 * The '<em><b>USER NAME</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>USER NAME</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #USER_NAME
	 * @model literal="getUserName"
	 * @generated
	 * @ordered
	 */
	public static final int USER_NAME_VALUE = 0;

	/**
	 * The '<em><b>USER PASSWORD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>USER PASSWORD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #USER_PASSWORD
	 * @model literal="getUserPassword"
	 * @generated
	 * @ordered
	 */
	public static final int USER_PASSWORD_VALUE = 0;

	/**
	 * The '<em><b>USER EMAIL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>USER EMAIL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #USER_EMAIL
	 * @model literal="getUserEmail"
	 * @generated
	 * @ordered
	 */
	public static final int USER_EMAIL_VALUE = 0;

	/**
	 * The '<em><b>USER POSITION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>USER POSITION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #USER_POSITION
	 * @model literal="getUserPosition"
	 * @generated
	 * @ordered
	 */
	public static final int USER_POSITION_VALUE = 0;

	/**
	 * The '<em><b>USER PRTSERVICE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>USER PRTSERVICE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #USER_PRTSERVICE
	 * @model literal="getUserPrintService"
	 * @generated
	 * @ordered
	 */
	public static final int USER_PRTSERVICE_VALUE = 0;

	/**
	 * The '<em><b>SCREEN WIDTH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SCREEN WIDTH</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SCREEN_WIDTH
	 * @model literal="getSceenWidth"
	 * @generated
	 * @ordered
	 */
	public static final int SCREEN_WIDTH_VALUE = 0;

	/**
	 * The '<em><b>SCREEN HEIGHT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SCREEN HEIGHT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SCREEN_HEIGHT
	 * @model literal="getScreenHeight"
	 * @generated
	 * @ordered
	 */
	public static final int SCREEN_HEIGHT_VALUE = 0;

	/**
	 * The '<em><b>TRIGGER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TRIGGER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRIGGER
	 * @model literal="getTrigger"
	 * @generated
	 * @ordered
	 */
	public static final int TRIGGER_VALUE = 0;

	/**
	 * An array of all the '<em><b>FSM Evaluation Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final FSMEvaluationType[] VALUES_ARRAY =
		new FSMEvaluationType[] {
			IP_ADDRESS,
			HOSTNAME,
			NOW,
			USER_AGENT_INFO,
			IS_TOUCH_DEVICE,
			IS_HTTPS,
			BROWSER_LOCALE,
			USER_NAME,
			USER_PASSWORD,
			USER_EMAIL,
			USER_POSITION,
			USER_PRTSERVICE,
			SCREEN_WIDTH,
			SCREEN_HEIGHT,
			TRIGGER,
		};

	/**
	 * A public read-only list of all the '<em><b>FSM Evaluation Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<FSMEvaluationType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>FSM Evaluation Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMEvaluationType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMEvaluationType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM Evaluation Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMEvaluationType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMEvaluationType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM Evaluation Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMEvaluationType get(int value) {
		switch (value) {
			case IP_ADDRESS_VALUE: return IP_ADDRESS;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FSMEvaluationType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //FSMEvaluationType
