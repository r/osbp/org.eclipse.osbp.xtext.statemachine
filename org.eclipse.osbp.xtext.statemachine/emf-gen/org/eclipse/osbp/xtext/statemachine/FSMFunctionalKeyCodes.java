/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>FSM Functional Key Codes</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMFunctionalKeyCodes()
 * @model
 * @generated
 */
public enum FSMFunctionalKeyCodes implements Enumerator {
	/**
	 * The '<em><b>BACKSPACE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BACKSPACE_VALUE
	 * @generated
	 * @ordered
	 */
	BACKSPACE(0, "BACKSPACE", "backspaceKey"),

	/**
	 * The '<em><b>TAB</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TAB_VALUE
	 * @generated
	 * @ordered
	 */
	TAB(0, "TAB", "tabKey"),

	/**
	 * The '<em><b>ENTER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENTER_VALUE
	 * @generated
	 * @ordered
	 */
	ENTER(0, "ENTER", "enterKey"),

	/**
	 * The '<em><b>ESC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ESC_VALUE
	 * @generated
	 * @ordered
	 */
	ESC(0, "ESC", "escKey"),

	/**
	 * The '<em><b>PGUP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PGUP_VALUE
	 * @generated
	 * @ordered
	 */
	PGUP(0, "PGUP", "pgupKey"),

	/**
	 * The '<em><b>PGDOWN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PGDOWN_VALUE
	 * @generated
	 * @ordered
	 */
	PGDOWN(0, "PGDOWN", "pgdownKey"),

	/**
	 * The '<em><b>END</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #END_VALUE
	 * @generated
	 * @ordered
	 */
	END(0, "END", "endKey"),

	/**
	 * The '<em><b>HOME</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HOME_VALUE
	 * @generated
	 * @ordered
	 */
	HOME(0, "HOME", "homeKey"),

	/**
	 * The '<em><b>LEFTARROW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LEFTARROW_VALUE
	 * @generated
	 * @ordered
	 */
	LEFTARROW(0, "LEFTARROW", "leftarrowKey"),

	/**
	 * The '<em><b>UPARROW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UPARROW_VALUE
	 * @generated
	 * @ordered
	 */
	UPARROW(0, "UPARROW", "uparrowKey"),

	/**
	 * The '<em><b>RIGHTARROW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RIGHTARROW_VALUE
	 * @generated
	 * @ordered
	 */
	RIGHTARROW(0, "RIGHTARROW", "rightarrowKey"),

	/**
	 * The '<em><b>DOWNARROW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOWNARROW_VALUE
	 * @generated
	 * @ordered
	 */
	DOWNARROW(0, "DOWNARROW", "downarrowKey"),

	/**
	 * The '<em><b>INSERT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INSERT_VALUE
	 * @generated
	 * @ordered
	 */
	INSERT(0, "INSERT", "insertKey"),

	/**
	 * The '<em><b>DELETE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DELETE_VALUE
	 * @generated
	 * @ordered
	 */
	DELETE(0, "DELETE", "deleteKey"),

	/**
	 * The '<em><b>F1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F1_VALUE
	 * @generated
	 * @ordered
	 */
	F1(0, "F1", "f1"),

	/**
	 * The '<em><b>F2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F2_VALUE
	 * @generated
	 * @ordered
	 */
	F2(0, "F2", "f2"),

	/**
	 * The '<em><b>F3</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F3_VALUE
	 * @generated
	 * @ordered
	 */
	F3(0, "F3", "f3"),

	/**
	 * The '<em><b>F4</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F4_VALUE
	 * @generated
	 * @ordered
	 */
	F4(0, "F4", "f4"),

	/**
	 * The '<em><b>F5</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F5_VALUE
	 * @generated
	 * @ordered
	 */
	F5(0, "F5", "f5"),

	/**
	 * The '<em><b>F6</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F6_VALUE
	 * @generated
	 * @ordered
	 */
	F6(0, "F6", "f6"),

	/**
	 * The '<em><b>F7</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F7_VALUE
	 * @generated
	 * @ordered
	 */
	F7(0, "F7", "f7"),

	/**
	 * The '<em><b>F8</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F8_VALUE
	 * @generated
	 * @ordered
	 */
	F8(0, "F8", "f8"),

	/**
	 * The '<em><b>F9</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F9_VALUE
	 * @generated
	 * @ordered
	 */
	F9(0, "F9", "f9"),

	/**
	 * The '<em><b>F10</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F10_VALUE
	 * @generated
	 * @ordered
	 */
	F10(0, "F10", "f10"),

	/**
	 * The '<em><b>F11</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F11_VALUE
	 * @generated
	 * @ordered
	 */
	F11(0, "F11", "f11"),

	/**
	 * The '<em><b>F12</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #F12_VALUE
	 * @generated
	 * @ordered
	 */
	F12(0, "F12", "f12");

	/**
	 * The '<em><b>BACKSPACE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BACKSPACE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BACKSPACE
	 * @model literal="backspaceKey"
	 * @generated
	 * @ordered
	 */
	public static final int BACKSPACE_VALUE = 0;

	/**
	 * The '<em><b>TAB</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TAB</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TAB
	 * @model literal="tabKey"
	 * @generated
	 * @ordered
	 */
	public static final int TAB_VALUE = 0;

	/**
	 * The '<em><b>ENTER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ENTER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ENTER
	 * @model literal="enterKey"
	 * @generated
	 * @ordered
	 */
	public static final int ENTER_VALUE = 0;

	/**
	 * The '<em><b>ESC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ESC</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ESC
	 * @model literal="escKey"
	 * @generated
	 * @ordered
	 */
	public static final int ESC_VALUE = 0;

	/**
	 * The '<em><b>PGUP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PGUP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PGUP
	 * @model literal="pgupKey"
	 * @generated
	 * @ordered
	 */
	public static final int PGUP_VALUE = 0;

	/**
	 * The '<em><b>PGDOWN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PGDOWN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PGDOWN
	 * @model literal="pgdownKey"
	 * @generated
	 * @ordered
	 */
	public static final int PGDOWN_VALUE = 0;

	/**
	 * The '<em><b>END</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>END</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #END
	 * @model literal="endKey"
	 * @generated
	 * @ordered
	 */
	public static final int END_VALUE = 0;

	/**
	 * The '<em><b>HOME</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>HOME</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HOME
	 * @model literal="homeKey"
	 * @generated
	 * @ordered
	 */
	public static final int HOME_VALUE = 0;

	/**
	 * The '<em><b>LEFTARROW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LEFTARROW</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LEFTARROW
	 * @model literal="leftarrowKey"
	 * @generated
	 * @ordered
	 */
	public static final int LEFTARROW_VALUE = 0;

	/**
	 * The '<em><b>UPARROW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UPARROW</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UPARROW
	 * @model literal="uparrowKey"
	 * @generated
	 * @ordered
	 */
	public static final int UPARROW_VALUE = 0;

	/**
	 * The '<em><b>RIGHTARROW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RIGHTARROW</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RIGHTARROW
	 * @model literal="rightarrowKey"
	 * @generated
	 * @ordered
	 */
	public static final int RIGHTARROW_VALUE = 0;

	/**
	 * The '<em><b>DOWNARROW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DOWNARROW</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOWNARROW
	 * @model literal="downarrowKey"
	 * @generated
	 * @ordered
	 */
	public static final int DOWNARROW_VALUE = 0;

	/**
	 * The '<em><b>INSERT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INSERT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INSERT
	 * @model literal="insertKey"
	 * @generated
	 * @ordered
	 */
	public static final int INSERT_VALUE = 0;

	/**
	 * The '<em><b>DELETE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DELETE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DELETE
	 * @model literal="deleteKey"
	 * @generated
	 * @ordered
	 */
	public static final int DELETE_VALUE = 0;

	/**
	 * The '<em><b>F1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F1</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F1
	 * @model literal="f1"
	 * @generated
	 * @ordered
	 */
	public static final int F1_VALUE = 0;

	/**
	 * The '<em><b>F2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F2</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F2
	 * @model literal="f2"
	 * @generated
	 * @ordered
	 */
	public static final int F2_VALUE = 0;

	/**
	 * The '<em><b>F3</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F3</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F3
	 * @model literal="f3"
	 * @generated
	 * @ordered
	 */
	public static final int F3_VALUE = 0;

	/**
	 * The '<em><b>F4</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F4</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F4
	 * @model literal="f4"
	 * @generated
	 * @ordered
	 */
	public static final int F4_VALUE = 0;

	/**
	 * The '<em><b>F5</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F5</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F5
	 * @model literal="f5"
	 * @generated
	 * @ordered
	 */
	public static final int F5_VALUE = 0;

	/**
	 * The '<em><b>F6</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F6</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F6
	 * @model literal="f6"
	 * @generated
	 * @ordered
	 */
	public static final int F6_VALUE = 0;

	/**
	 * The '<em><b>F7</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F7</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F7
	 * @model literal="f7"
	 * @generated
	 * @ordered
	 */
	public static final int F7_VALUE = 0;

	/**
	 * The '<em><b>F8</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F8</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F8
	 * @model literal="f8"
	 * @generated
	 * @ordered
	 */
	public static final int F8_VALUE = 0;

	/**
	 * The '<em><b>F9</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F9</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F9
	 * @model literal="f9"
	 * @generated
	 * @ordered
	 */
	public static final int F9_VALUE = 0;

	/**
	 * The '<em><b>F10</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F10</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F10
	 * @model literal="f10"
	 * @generated
	 * @ordered
	 */
	public static final int F10_VALUE = 0;

	/**
	 * The '<em><b>F11</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F11</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F11
	 * @model literal="f11"
	 * @generated
	 * @ordered
	 */
	public static final int F11_VALUE = 0;

	/**
	 * The '<em><b>F12</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>F12</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #F12
	 * @model literal="f12"
	 * @generated
	 * @ordered
	 */
	public static final int F12_VALUE = 0;

	/**
	 * An array of all the '<em><b>FSM Functional Key Codes</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final FSMFunctionalKeyCodes[] VALUES_ARRAY =
		new FSMFunctionalKeyCodes[] {
			BACKSPACE,
			TAB,
			ENTER,
			ESC,
			PGUP,
			PGDOWN,
			END,
			HOME,
			LEFTARROW,
			UPARROW,
			RIGHTARROW,
			DOWNARROW,
			INSERT,
			DELETE,
			F1,
			F2,
			F3,
			F4,
			F5,
			F6,
			F7,
			F8,
			F9,
			F10,
			F11,
			F12,
		};

	/**
	 * A public read-only list of all the '<em><b>FSM Functional Key Codes</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<FSMFunctionalKeyCodes> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>FSM Functional Key Codes</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMFunctionalKeyCodes get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMFunctionalKeyCodes result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM Functional Key Codes</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMFunctionalKeyCodes getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMFunctionalKeyCodes result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM Functional Key Codes</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMFunctionalKeyCodes get(int value) {
		switch (value) {
			case BACKSPACE_VALUE: return BACKSPACE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FSMFunctionalKeyCodes(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //FSMFunctionalKeyCodes
