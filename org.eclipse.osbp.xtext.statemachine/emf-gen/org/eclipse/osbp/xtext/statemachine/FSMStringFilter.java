/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM String Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#getPropertyId <em>Property Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#getFilterString <em>Filter String</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#isOnlyMatchPrefix <em>Only Match Prefix</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#isIgnoreCase <em>Ignore Case</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMStringFilter()
 * @model
 * @generated
 */
public interface FSMStringFilter extends FSMAbstractFilter {
	/**
	 * Returns the value of the '<em><b>Property Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Id</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Id</em>' containment reference.
	 * @see #setPropertyId(FSMFilterProperty)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMStringFilter_PropertyId()
	 * @model containment="true"
	 * @generated
	 */
	FSMFilterProperty getPropertyId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#getPropertyId <em>Property Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Id</em>' containment reference.
	 * @see #getPropertyId()
	 * @generated
	 */
	void setPropertyId(FSMFilterProperty value);

	/**
	 * Returns the value of the '<em><b>Filter String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter String</em>' attribute.
	 * @see #setFilterString(String)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMStringFilter_FilterString()
	 * @model unique="false"
	 * @generated
	 */
	String getFilterString();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#getFilterString <em>Filter String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter String</em>' attribute.
	 * @see #getFilterString()
	 * @generated
	 */
	void setFilterString(String value);

	/**
	 * Returns the value of the '<em><b>Only Match Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Only Match Prefix</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Only Match Prefix</em>' attribute.
	 * @see #setOnlyMatchPrefix(boolean)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMStringFilter_OnlyMatchPrefix()
	 * @model unique="false"
	 * @generated
	 */
	boolean isOnlyMatchPrefix();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#isOnlyMatchPrefix <em>Only Match Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Only Match Prefix</em>' attribute.
	 * @see #isOnlyMatchPrefix()
	 * @generated
	 */
	void setOnlyMatchPrefix(boolean value);

	/**
	 * Returns the value of the '<em><b>Ignore Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ignore Case</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ignore Case</em>' attribute.
	 * @see #setIgnoreCase(boolean)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMStringFilter_IgnoreCase()
	 * @model unique="false"
	 * @generated
	 */
	boolean isIgnoreCase();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMStringFilter#isIgnoreCase <em>Ignore Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ignore Case</em>' attribute.
	 * @see #isIgnoreCase()
	 * @generated
	 */
	void setIgnoreCase(boolean value);

} // FSMStringFilter
