/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Key Mapper</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMKeyMapper#getKeyCode <em>Key Code</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMKeyMapper#getKeyEvent <em>Key Event</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMKeyMapper()
 * @model
 * @generated
 */
public interface FSMKeyMapper extends FSMLazyResolver {
	/**
	 * Returns the value of the '<em><b>Key Code</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.statemachine.FSMFunctionalKeyCodes}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key Code</em>' attribute.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFunctionalKeyCodes
	 * @see #setKeyCode(FSMFunctionalKeyCodes)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMKeyMapper_KeyCode()
	 * @model unique="false"
	 * @generated
	 */
	FSMFunctionalKeyCodes getKeyCode();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMKeyMapper#getKeyCode <em>Key Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key Code</em>' attribute.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMFunctionalKeyCodes
	 * @see #getKeyCode()
	 * @generated
	 */
	void setKeyCode(FSMFunctionalKeyCodes value);

	/**
	 * Returns the value of the '<em><b>Key Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key Event</em>' reference.
	 * @see #setKeyEvent(FSMEvent)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMKeyMapper_KeyEvent()
	 * @model
	 * @generated
	 */
	FSMEvent getKeyEvent();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMKeyMapper#getKeyEvent <em>Key Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key Event</em>' reference.
	 * @see #getKeyEvent()
	 * @generated
	 */
	void setKeyEvent(FSMEvent value);

} // FSMKeyMapper
