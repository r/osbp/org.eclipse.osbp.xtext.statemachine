/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMState#getTriggers <em>Triggers</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMState#getConditions <em>Conditions</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMState#getIdentity <em>Identity</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMState#getKeystroke <em>Keystroke</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMState#isHasKeyOperation <em>Has Key Operation</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMState#getKeyOperation <em>Key Operation</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMState#getKeyMapper <em>Key Mapper</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMState()
 * @model
 * @generated
 */
public interface FSMState extends FSMBase {
	/**
	 * Returns the value of the '<em><b>Triggers</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMTrigger}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Triggers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Triggers</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMState_Triggers()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMTrigger> getTriggers();

	/**
	 * Returns the value of the '<em><b>Conditions</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditions</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMState_Conditions()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMAction> getConditions();

	/**
	 * Returns the value of the '<em><b>Identity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identity</em>' containment reference.
	 * @see #setIdentity(FSMOperation)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMState_Identity()
	 * @model containment="true"
	 * @generated
	 */
	FSMOperation getIdentity();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMState#getIdentity <em>Identity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identity</em>' containment reference.
	 * @see #getIdentity()
	 * @generated
	 */
	void setIdentity(FSMOperation value);

	/**
	 * Returns the value of the '<em><b>Keystroke</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Keystroke</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Keystroke</em>' reference.
	 * @see #setKeystroke(FSMControlFieldAttribute)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMState_Keystroke()
	 * @model
	 * @generated
	 */
	FSMControlFieldAttribute getKeystroke();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMState#getKeystroke <em>Keystroke</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Keystroke</em>' reference.
	 * @see #getKeystroke()
	 * @generated
	 */
	void setKeystroke(FSMControlFieldAttribute value);

	/**
	 * Returns the value of the '<em><b>Has Key Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Key Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Key Operation</em>' attribute.
	 * @see #setHasKeyOperation(boolean)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMState_HasKeyOperation()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasKeyOperation();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMState#isHasKeyOperation <em>Has Key Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Key Operation</em>' attribute.
	 * @see #isHasKeyOperation()
	 * @generated
	 */
	void setHasKeyOperation(boolean value);

	/**
	 * Returns the value of the '<em><b>Key Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key Operation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key Operation</em>' containment reference.
	 * @see #setKeyOperation(FSMOperation)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMState_KeyOperation()
	 * @model containment="true"
	 * @generated
	 */
	FSMOperation getKeyOperation();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMState#getKeyOperation <em>Key Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key Operation</em>' containment reference.
	 * @see #getKeyOperation()
	 * @generated
	 */
	void setKeyOperation(FSMOperation value);

	/**
	 * Returns the value of the '<em><b>Key Mapper</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMKeyMapper}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key Mapper</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key Mapper</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMState_KeyMapper()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMKeyMapper> getKeyMapper();

} // FSMState
