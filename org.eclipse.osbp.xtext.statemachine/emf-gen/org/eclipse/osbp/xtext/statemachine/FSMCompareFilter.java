/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Compare Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getPropertyId <em>Property Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getOperand <em>Operand</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMCompareFilter()
 * @model
 * @generated
 */
public interface FSMCompareFilter extends FSMAbstractFilter {
	/**
	 * Returns the value of the '<em><b>Property Id</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Id</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Id</em>' containment reference.
	 * @see #setPropertyId(FSMFilterProperty)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMCompareFilter_PropertyId()
	 * @model containment="true"
	 * @generated
	 */
	FSMFilterProperty getPropertyId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getPropertyId <em>Property Id</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Id</em>' containment reference.
	 * @see #getPropertyId()
	 * @generated
	 */
	void setPropertyId(FSMFilterProperty value);

	/**
	 * Returns the value of the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operand</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operand</em>' containment reference.
	 * @see #setOperand(FSMActionFieldSource)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMCompareFilter_Operand()
	 * @model containment="true"
	 * @generated
	 */
	FSMActionFieldSource getOperand();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getOperand <em>Operand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operand</em>' containment reference.
	 * @see #getOperand()
	 * @generated
	 */
	void setOperand(FSMActionFieldSource value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.statemachine.FSMCompareOperationEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' attribute.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMCompareOperationEnum
	 * @see #setOperation(FSMCompareOperationEnum)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMCompareFilter_Operation()
	 * @model unique="false"
	 * @generated
	 */
	FSMCompareOperationEnum getOperation();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMCompareFilter#getOperation <em>Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' attribute.
	 * @see org.eclipse.osbp.xtext.statemachine.FSMCompareOperationEnum
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(FSMCompareOperationEnum value);

} // FSMCompareFilter
