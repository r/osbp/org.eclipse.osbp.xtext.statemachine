/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Lazy Resolver</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMLazyResolver()
 * @model
 * @generated
 */
public interface FSMLazyResolver extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" proxyDataType="org.eclipse.osbp.xtext.statemachine.InternalEObject" proxyUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return <%org.eclipse.osbp.xtext.oxtype.resource.EcoreUtil3%>.resolve(proxy, this.eResource().getResourceSet());'"
	 * @generated
	 */
	EObject eResolveProxy(InternalEObject proxy);

} // FSMLazyResolver
