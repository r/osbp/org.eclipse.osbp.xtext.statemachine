/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryFunction;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryStatemachineGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMFunction#getGroup <em>Group</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMFunction#getFunction <em>Function</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMFunction#getFirst <em>First</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMFunction#getMore <em>More</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMFunction()
 * @model
 * @generated
 */
public interface FSMFunction extends FSMActionFieldSource {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' reference.
	 * @see #setGroup(FunctionLibraryStatemachineGroup)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMFunction_Group()
	 * @model
	 * @generated
	 */
	FunctionLibraryStatemachineGroup getGroup();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMFunction#getGroup <em>Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group</em>' reference.
	 * @see #getGroup()
	 * @generated
	 */
	void setGroup(FunctionLibraryStatemachineGroup value);

	/**
	 * Returns the value of the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function</em>' reference.
	 * @see #setFunction(FunctionLibraryFunction)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMFunction_Function()
	 * @model
	 * @generated
	 */
	FunctionLibraryFunction getFunction();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMFunction#getFunction <em>Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function</em>' reference.
	 * @see #getFunction()
	 * @generated
	 */
	void setFunction(FunctionLibraryFunction value);

	/**
	 * Returns the value of the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First</em>' containment reference.
	 * @see #setFirst(FSMOperationParameter)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMFunction_First()
	 * @model containment="true"
	 * @generated
	 */
	FSMOperationParameter getFirst();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMFunction#getFirst <em>First</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First</em>' containment reference.
	 * @see #getFirst()
	 * @generated
	 */
	void setFirst(FSMOperationParameter value);

	/**
	 * Returns the value of the '<em><b>More</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.statemachine.FSMOperationParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>More</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>More</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMFunction_More()
	 * @model containment="true"
	 * @generated
	 */
	EList<FSMOperationParameter> getMore();

} // FSMFunction
