/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>FSMPOS Printer Barcode Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMPOSPrinterBarcodeType()
 * @model
 * @generated
 */
public enum FSMPOSPrinterBarcodeType implements Enumerator {
	/**
	 * The '<em><b>PTR BCS UPCA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCA_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_UPCA(0, "PTR_BCS_UPCA", "upca"),

	/**
	 * The '<em><b>PTR BCS UPCE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCE_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_UPCE(0, "PTR_BCS_UPCE", "upcb"),

	/**
	 * The '<em><b>PTR BCS JAN8</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_JAN8_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_JAN8(0, "PTR_BCS_JAN8", "jan8"),

	/**
	 * The '<em><b>PTR BCS EAN8</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_EAN8_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_EAN8(0, "PTR_BCS_EAN8", "ean8"),

	/**
	 * The '<em><b>PTR BCS JAN13</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_JAN13_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_JAN13(0, "PTR_BCS_JAN13", "jan13"),

	/**
	 * The '<em><b>PTR BCS EAN13</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_EAN13_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_EAN13(0, "PTR_BCS_EAN13", "ean13"),

	/**
	 * The '<em><b>PTR BCS TF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_TF_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_TF(0, "PTR_BCS_TF", "tf"),

	/**
	 * The '<em><b>PTR BCS ITF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_ITF_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_ITF(0, "PTR_BCS_ITF", "itf"),

	/**
	 * The '<em><b>PTR BCS Codabar</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_CODABAR_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_CODABAR(0, "PTR_BCS_Codabar", "codeabar"),

	/**
	 * The '<em><b>PTR BCS Code39</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_CODE39_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_CODE39(0, "PTR_BCS_Code39", "code39"),

	/**
	 * The '<em><b>PTR BCS Code93</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_CODE93_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_CODE93(0, "PTR_BCS_Code93", "code93"),

	/**
	 * The '<em><b>PTR BCS Code128</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_CODE128_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_CODE128(0, "PTR_BCS_Code128", "code128"),

	/**
	 * The '<em><b>PTR BCS UPCA S</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCA_S_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_UPCA_S(0, "PTR_BCS_UPCA_S", "upca_s"),

	/**
	 * The '<em><b>PTR BCS UPCE S</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCE_S_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_UPCE_S(0, "PTR_BCS_UPCE_S", "upce_s"),

	/**
	 * The '<em><b>PTR BCS UPCD1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCD1_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_UPCD1(0, "PTR_BCS_UPCD1", "upcd1"),

	/**
	 * The '<em><b>PTR BCS UPCD2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCD2_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_UPCD2(0, "PTR_BCS_UPCD2", "upcd2"),

	/**
	 * The '<em><b>PTR BCS UPCD3</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCD3_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_UPCD3(0, "PTR_BCS_UPCD3", "upcd3"),

	/**
	 * The '<em><b>PTR BCS UPCD4</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCD4_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_UPCD4(0, "PTR_BCS_UPCD4", "upcd4"),

	/**
	 * The '<em><b>PTR BCS UPCD5</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCD5_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_UPCD5(0, "PTR_BCS_UPCD5", "upcd5"),

	/**
	 * The '<em><b>PTR BCS EAN8 S</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_EAN8_S_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_EAN8_S(0, "PTR_BCS_EAN8_S", "ean8_s"),

	/**
	 * The '<em><b>PTR BCS EAN13 S</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_EAN13_S_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_EAN13_S(0, "PTR_BCS_EAN13_S", "ean13_s"),

	/**
	 * The '<em><b>PTR BCS EAN128</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_EAN128_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_EAN128(0, "PTR_BCS_EAN128", "ean128"),

	/**
	 * The '<em><b>PTR BCS OCRA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_OCRA_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_OCRA(0, "PTR_BCS_OCRA", "orca"),

	/**
	 * The '<em><b>PTR BCS OCRB</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_OCRB_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_OCRB(0, "PTR_BCS_OCRB", "ocrb"),

	/**
	 * The '<em><b>PTR BCS Code128 Parsed</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_CODE128_PARSED_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_CODE128_PARSED(0, "PTR_BCS_Code128_Parsed", "code128_parsed"),

	/**
	 * The '<em><b>PTR BCS GS1DATABAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_GS1DATABAR_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_GS1DATABAR(0, "PTR_BCS_GS1DATABAR", "gs1databar"),

	/**
	 * The '<em><b>PTR BCS GS1DATABAR E</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_GS1DATABAR_E_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_GS1DATABAR_E(0, "PTR_BCS_GS1DATABAR_E", "gs1databar_e"),

	/**
	 * The '<em><b>PTR BCS GS1DATABAR S</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_GS1DATABAR_S_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_GS1DATABAR_S(0, "PTR_BCS_GS1DATABAR_S", "gs1databar_s"),

	/**
	 * The '<em><b>PTR BCS GS1DATABAR ES</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_GS1DATABAR_ES_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_GS1DATABAR_ES(0, "PTR_BCS_GS1DATABAR_E_S", "gs1databar_e_s"),

	/**
	 * The '<em><b>PTR BCS PDF417</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_PDF417_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_PDF417(0, "PTR_BCS_PDF417", "pdf417"),

	/**
	 * The '<em><b>PTR BCS MAXICODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_MAXICODE_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_MAXICODE(0, "PTR_BCS_MAXICODE", "maxicode"),

	/**
	 * The '<em><b>PTR BCS DATAMATRIX</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_DATAMATRIX_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_DATAMATRIX(0, "PTR_BCS_DATAMATRIX", "datamatrix"),

	/**
	 * The '<em><b>PTR BCS QRCODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_QRCODE_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_QRCODE(0, "PTR_BCS_QRCODE", "qrcode"),

	/**
	 * The '<em><b>PTR BCS UQRCODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UQRCODE_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_UQRCODE(0, "PTR_BCS_UQRCODE", "uqrcode"),

	/**
	 * The '<em><b>PTR BCS AZTEC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_AZTEC_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_AZTEC(0, "PTR_BCS_AZTEC", "aztec"),

	/**
	 * The '<em><b>PTR BCS UPDF417</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPDF417_VALUE
	 * @generated
	 * @ordered
	 */
	PTR_BCS_UPDF417(0, "PTR_BCS_UPDF417", "updf417");

	/**
	 * The '<em><b>PTR BCS UPCA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS UPCA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCA
	 * @model literal="upca"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_UPCA_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS UPCE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS UPCE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCE
	 * @model literal="upcb"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_UPCE_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS JAN8</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS JAN8</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_JAN8
	 * @model literal="jan8"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_JAN8_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS EAN8</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS EAN8</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_EAN8
	 * @model literal="ean8"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_EAN8_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS JAN13</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS JAN13</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_JAN13
	 * @model literal="jan13"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_JAN13_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS EAN13</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS EAN13</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_EAN13
	 * @model literal="ean13"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_EAN13_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS TF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS TF</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_TF
	 * @model literal="tf"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_TF_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS ITF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS ITF</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_ITF
	 * @model literal="itf"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_ITF_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS Codabar</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS Codabar</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_CODABAR
	 * @model name="PTR_BCS_Codabar" literal="codeabar"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_CODABAR_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS Code39</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS Code39</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_CODE39
	 * @model name="PTR_BCS_Code39" literal="code39"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_CODE39_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS Code93</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS Code93</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_CODE93
	 * @model name="PTR_BCS_Code93" literal="code93"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_CODE93_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS Code128</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS Code128</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_CODE128
	 * @model name="PTR_BCS_Code128" literal="code128"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_CODE128_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS UPCA S</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS UPCA S</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCA_S
	 * @model literal="upca_s"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_UPCA_S_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS UPCE S</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS UPCE S</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCE_S
	 * @model literal="upce_s"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_UPCE_S_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS UPCD1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS UPCD1</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCD1
	 * @model literal="upcd1"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_UPCD1_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS UPCD2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS UPCD2</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCD2
	 * @model literal="upcd2"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_UPCD2_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS UPCD3</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS UPCD3</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCD3
	 * @model literal="upcd3"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_UPCD3_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS UPCD4</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS UPCD4</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCD4
	 * @model literal="upcd4"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_UPCD4_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS UPCD5</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS UPCD5</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPCD5
	 * @model literal="upcd5"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_UPCD5_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS EAN8 S</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS EAN8 S</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_EAN8_S
	 * @model literal="ean8_s"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_EAN8_S_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS EAN13 S</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS EAN13 S</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_EAN13_S
	 * @model literal="ean13_s"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_EAN13_S_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS EAN128</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS EAN128</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_EAN128
	 * @model literal="ean128"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_EAN128_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS OCRA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS OCRA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_OCRA
	 * @model literal="orca"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_OCRA_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS OCRB</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS OCRB</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_OCRB
	 * @model literal="ocrb"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_OCRB_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS Code128 Parsed</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS Code128 Parsed</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_CODE128_PARSED
	 * @model name="PTR_BCS_Code128_Parsed" literal="code128_parsed"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_CODE128_PARSED_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS GS1DATABAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS GS1DATABAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_GS1DATABAR
	 * @model literal="gs1databar"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_GS1DATABAR_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS GS1DATABAR E</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS GS1DATABAR E</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_GS1DATABAR_E
	 * @model literal="gs1databar_e"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_GS1DATABAR_E_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS GS1DATABAR S</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS GS1DATABAR S</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_GS1DATABAR_S
	 * @model literal="gs1databar_s"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_GS1DATABAR_S_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS GS1DATABAR ES</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS GS1DATABAR ES</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_GS1DATABAR_ES
	 * @model name="PTR_BCS_GS1DATABAR_E_S" literal="gs1databar_e_s"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_GS1DATABAR_ES_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS PDF417</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS PDF417</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_PDF417
	 * @model literal="pdf417"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_PDF417_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS MAXICODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS MAXICODE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_MAXICODE
	 * @model literal="maxicode"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_MAXICODE_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS DATAMATRIX</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS DATAMATRIX</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_DATAMATRIX
	 * @model literal="datamatrix"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_DATAMATRIX_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS QRCODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS QRCODE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_QRCODE
	 * @model literal="qrcode"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_QRCODE_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS UQRCODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS UQRCODE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UQRCODE
	 * @model literal="uqrcode"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_UQRCODE_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS AZTEC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS AZTEC</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_AZTEC
	 * @model literal="aztec"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_AZTEC_VALUE = 0;

	/**
	 * The '<em><b>PTR BCS UPDF417</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PTR BCS UPDF417</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PTR_BCS_UPDF417
	 * @model literal="updf417"
	 * @generated
	 * @ordered
	 */
	public static final int PTR_BCS_UPDF417_VALUE = 0;

	/**
	 * An array of all the '<em><b>FSMPOS Printer Barcode Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final FSMPOSPrinterBarcodeType[] VALUES_ARRAY =
		new FSMPOSPrinterBarcodeType[] {
			PTR_BCS_UPCA,
			PTR_BCS_UPCE,
			PTR_BCS_JAN8,
			PTR_BCS_EAN8,
			PTR_BCS_JAN13,
			PTR_BCS_EAN13,
			PTR_BCS_TF,
			PTR_BCS_ITF,
			PTR_BCS_CODABAR,
			PTR_BCS_CODE39,
			PTR_BCS_CODE93,
			PTR_BCS_CODE128,
			PTR_BCS_UPCA_S,
			PTR_BCS_UPCE_S,
			PTR_BCS_UPCD1,
			PTR_BCS_UPCD2,
			PTR_BCS_UPCD3,
			PTR_BCS_UPCD4,
			PTR_BCS_UPCD5,
			PTR_BCS_EAN8_S,
			PTR_BCS_EAN13_S,
			PTR_BCS_EAN128,
			PTR_BCS_OCRA,
			PTR_BCS_OCRB,
			PTR_BCS_CODE128_PARSED,
			PTR_BCS_GS1DATABAR,
			PTR_BCS_GS1DATABAR_E,
			PTR_BCS_GS1DATABAR_S,
			PTR_BCS_GS1DATABAR_ES,
			PTR_BCS_PDF417,
			PTR_BCS_MAXICODE,
			PTR_BCS_DATAMATRIX,
			PTR_BCS_QRCODE,
			PTR_BCS_UQRCODE,
			PTR_BCS_AZTEC,
			PTR_BCS_UPDF417,
		};

	/**
	 * A public read-only list of all the '<em><b>FSMPOS Printer Barcode Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<FSMPOSPrinterBarcodeType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>FSMPOS Printer Barcode Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMPOSPrinterBarcodeType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMPOSPrinterBarcodeType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSMPOS Printer Barcode Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMPOSPrinterBarcodeType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMPOSPrinterBarcodeType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSMPOS Printer Barcode Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMPOSPrinterBarcodeType get(int value) {
		switch (value) {
			case PTR_BCS_UPCA_VALUE: return PTR_BCS_UPCA;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FSMPOSPrinterBarcodeType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //FSMPOSPrinterBarcodeType
