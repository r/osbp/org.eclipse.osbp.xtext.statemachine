/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM Control Button Attribute Event Keyboard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard#getKeystroke <em>Keystroke</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButtonAttributeEventKeyboard()
 * @model
 * @generated
 */
public interface FSMControlButtonAttributeEventKeyboard extends FSMControlButtonAttributeEvent {
	/**
	 * Returns the value of the '<em><b>Keystroke</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Keystroke</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Keystroke</em>' attribute.
	 * @see #setKeystroke(String)
	 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMControlButtonAttributeEventKeyboard_Keystroke()
	 * @model unique="false"
	 * @generated
	 */
	String getKeystroke();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.statemachine.FSMControlButtonAttributeEventKeyboard#getKeystroke <em>Keystroke</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Keystroke</em>' attribute.
	 * @see #getKeystroke()
	 * @generated
	 */
	void setKeystroke(String value);

} // FSMControlButtonAttributeEventKeyboard
