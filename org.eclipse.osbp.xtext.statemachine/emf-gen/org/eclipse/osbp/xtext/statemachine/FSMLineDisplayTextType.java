/**
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.statemachine;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>FSM Line Display Text Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.statemachine.StatemachineDSLPackage#getFSMLineDisplayTextType()
 * @model
 * @generated
 */
public enum FSMLineDisplayTextType implements Enumerator {
	/**
	 * The '<em><b>DISP DT NORMAL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_DT_NORMAL_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_DT_NORMAL(0, "DISP_DT_NORMAL", "normal"),

	/**
	 * The '<em><b>DISP DT BLINK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_DT_BLINK_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_DT_BLINK(0, "DISP_DT_BLINK", "blink"),

	/**
	 * The '<em><b>DISP DT REVERSE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_DT_REVERSE_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_DT_REVERSE(0, "DISP_DT_REVERSE", "reverse"),

	/**
	 * The '<em><b>DISP DT BLINK REVERSE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DISP_DT_BLINK_REVERSE_VALUE
	 * @generated
	 * @ordered
	 */
	DISP_DT_BLINK_REVERSE(0, "DISP_DT_BLINK_REVERSE", "blinkreverse");

	/**
	 * The '<em><b>DISP DT NORMAL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP DT NORMAL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_DT_NORMAL
	 * @model literal="normal"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_DT_NORMAL_VALUE = 0;

	/**
	 * The '<em><b>DISP DT BLINK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP DT BLINK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_DT_BLINK
	 * @model literal="blink"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_DT_BLINK_VALUE = 0;

	/**
	 * The '<em><b>DISP DT REVERSE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP DT REVERSE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_DT_REVERSE
	 * @model literal="reverse"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_DT_REVERSE_VALUE = 0;

	/**
	 * The '<em><b>DISP DT BLINK REVERSE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DISP DT BLINK REVERSE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DISP_DT_BLINK_REVERSE
	 * @model literal="blinkreverse"
	 * @generated
	 * @ordered
	 */
	public static final int DISP_DT_BLINK_REVERSE_VALUE = 0;

	/**
	 * An array of all the '<em><b>FSM Line Display Text Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final FSMLineDisplayTextType[] VALUES_ARRAY =
		new FSMLineDisplayTextType[] {
			DISP_DT_NORMAL,
			DISP_DT_BLINK,
			DISP_DT_REVERSE,
			DISP_DT_BLINK_REVERSE,
		};

	/**
	 * A public read-only list of all the '<em><b>FSM Line Display Text Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<FSMLineDisplayTextType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>FSM Line Display Text Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMLineDisplayTextType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMLineDisplayTextType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM Line Display Text Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMLineDisplayTextType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FSMLineDisplayTextType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>FSM Line Display Text Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FSMLineDisplayTextType get(int value) {
		switch (value) {
			case DISP_DT_NORMAL_VALUE: return DISP_DT_NORMAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FSMLineDisplayTextType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //FSMLineDisplayTextType
