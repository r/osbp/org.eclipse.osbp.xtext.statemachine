/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 */
package org.eclipse.osbp.xtext.statemachine.ui.contentassist;

import com.google.common.base.Objects;
import javax.inject.Inject;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.osbp.xtext.basic.ui.contentassist.BasicDSLProposalProviderHelper;
import org.eclipse.osbp.xtext.statemachine.ui.contentassist.AbstractStatemachineDSLProposalProvider;
import org.eclipse.osbp.xtext.statemachine.ui.contentassist.ImageFileNameTextApplier;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.common.ui.contentassist.TerminalsProposalProvider;
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
@SuppressWarnings("all")
public class StatemachineDSLProposalProvider extends AbstractStatemachineDSLProposalProvider {
  @Inject
  private TerminalsProposalProvider provider;
  
  @Inject
  private BasicDSLProposalProviderHelper providerHelper;
  
  /**
   * This override will enable 1 length non letter characters as keyword.
   */
  @Override
  protected boolean isKeywordWorthyToPropose(final Keyword keyword) {
    return true;
  }
  
  @Override
  protected boolean isValidProposal(final String proposal, final String prefix, final ContentAssistContext context) {
    boolean result = super.isValidProposal(proposal, prefix, context);
    return result;
  }
  
  public boolean isActionValidProposal(final EObject obj, final String proposal, final boolean result) {
    return true;
  }
  
  public void imageFilePickerProposal(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor, final String fileExtensions) {
    ICompletionProposal _createCompletionProposal = this.createCompletionProposal("Select image file...", context);
    ConfigurableCompletionProposal fileName = ((ConfigurableCompletionProposal) _createCompletionProposal);
    boolean _notEquals = (!Objects.equal(fileName, null));
    if (_notEquals) {
      ImageFileNameTextApplier applier = new ImageFileNameTextApplier();
      applier.setExtensions(fileExtensions.split(","));
      applier.setContext(context);
      fileName.setTextApplier(applier);
    }
    acceptor.accept(fileName);
  }
  
  @Override
  public void complete_QualifiedName(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this);
  }
  
  @Override
  public void complete_QualifiedNameWithWildcard(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this);
  }
  
  @Override
  public void complete_TRANSLATABLESTRING(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.provider.complete_STRING(model, ruleCall, context, acceptor);
  }
}
