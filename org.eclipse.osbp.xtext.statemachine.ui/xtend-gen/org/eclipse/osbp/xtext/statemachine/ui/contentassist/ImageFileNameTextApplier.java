/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 */
package org.eclipse.osbp.xtext.statemachine.ui.contentassist;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ReplacementTextApplier;
import org.mihalis.opal.imageSelector.ImageSelectorDialog;

@SuppressWarnings("all")
public class ImageFileNameTextApplier extends ReplacementTextApplier {
  private ContentAssistContext context;
  
  private String[] extensions;
  
  public ContentAssistContext setContext(final ContentAssistContext context) {
    return this.context = context;
  }
  
  public String[] setExtensions(final String[] fileExtensions) {
    return this.extensions = fileExtensions;
  }
  
  @Override
  public String getActualReplacementString(final ConfigurableCompletionProposal proposal) {
    Display display = this.context.getViewer().getTextWidget().getDisplay();
    Shell shell = new Shell(display);
    Point _point = new Point((display.getActiveShell().getLocation().x + 30), (display.getActiveShell().getLocation().y + 30));
    shell.setLocation(_point);
    FillLayout _fillLayout = new FillLayout();
    shell.setLayout(_fillLayout);
    ImageSelectorDialog imageSelectorDialog = new ImageSelectorDialog(shell, 16);
    imageSelectorDialog.setFilterExtensions(this.extensions);
    String imageFileName = imageSelectorDialog.open(true);
    return "\"".concat(imageFileName).concat("\"");
  }
}
