/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.statemachine.ui.contentassist

import javax.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.osbp.xtext.basic.ui.contentassist.BasicDSLProposalProviderHelper
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout.FillLayout
import org.eclipse.swt.widgets.Shell
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.common.ui.contentassist.TerminalsProposalProvider
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.eclipse.xtext.ui.editor.contentassist.ReplacementTextApplier
import org.mihalis.opal.imageSelector.ImageSelectorDialog

class ImageFileNameTextApplier extends ReplacementTextApplier {
	var ContentAssistContext context
	var String[] extensions

	def setContext(ContentAssistContext context) {
		this.context = context
	}

	def setExtensions(String[] fileExtensions) {
		extensions = fileExtensions
	}

	// this will inject a file dialog when selecting the file picker proposal 
	override getActualReplacementString(ConfigurableCompletionProposal proposal) {
		var display = context.getViewer().getTextWidget().getDisplay();
		var shell = new Shell(display);
		shell.setLocation(new Point(display.activeShell.location.x + 30, display.activeShell.location.y + 30))
		shell.setLayout(new FillLayout());
		var imageSelectorDialog = new ImageSelectorDialog(shell, 16);
		imageSelectorDialog.setFilterExtensions(extensions)
		var imageFileName = imageSelectorDialog.open(true);
		return "\"".concat(imageFileName).concat("\"");
	}
}

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
class StatemachineDSLProposalProvider extends AbstractStatemachineDSLProposalProvider {
	@Inject TerminalsProposalProvider provider
	@Inject BasicDSLProposalProviderHelper providerHelper

	/**
	 * This override will enable 1 length non letter characters as keyword.
	 */
	override protected boolean isKeywordWorthyToPropose(Keyword keyword) {
		return true;
	}

	override protected boolean isValidProposal(String proposal, String prefix, ContentAssistContext context) {
		var result = super.isValidProposal(proposal, prefix, context);
		return result;
	}

	def isActionValidProposal(EObject obj, String proposal, boolean result) {
		return true
	}

		
//	override completeStatemachineActionButton_Image(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
//		imageFilePickerProposal(model, assignment, context, acceptor, ".jpg,.jpeg,.png")
//	}

	def imageFilePickerProposal(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor, String fileExtensions) {
		var fileName = createCompletionProposal("Select image file...", context) as ConfigurableCompletionProposal
		if (fileName != null) {
			var applier = new ImageFileNameTextApplier()
			applier.setExtensions(fileExtensions.split(","))
			applier.setContext(context)
			fileName.setTextApplier = applier
		}
		acceptor.accept(fileName)
	}

	override public void complete_QualifiedName(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this)
	}

	override public void complete_QualifiedNameWithWildcard(EObject model, RuleCall ruleCall,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this)
	}

	// ------------------------ delegates to TerminalsProposalProvider -----------------
	override public void complete_TRANSLATABLESTRING(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, ruleCall, context, acceptor)
	}
}
