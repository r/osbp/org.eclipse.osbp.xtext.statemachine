/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.statemachine.ui.quickfix

import org.eclipse.osbp.xtext.oxtype.ui.quickfix.OXtypeQuickfixProvider

//import org.eclipse.xtext.ui.editor.quickfix.Fix
//import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
//import org.eclipse.xtext.validation.Issue
/**
 * Custom quickfixes.
 *
 * see http://www.eclipse.org/Xtext/documentation.html#quickfixes
 */
class StatemachineDSLQuickfixProvider extends OXtypeQuickfixProvider {
	//	@Fix(MyDslValidator::INVALID_NAME)
	//	def capitalizeName(Issue issue, IssueResolutionAcceptor acceptor) {
	//		acceptor.accept(issue, 'Capitalize name', 'Capitalize the name.', 'upcase.png') [
	//			context |
	//			val xtextDocument = context.xtextDocument
	//			val firstLetter = xtextDocument.get(issue.offset, 1)
	//			xtextDocument.replace(issue.offset, 1, firstLetter.toUpperCase)
	//		]
	//	}
}
