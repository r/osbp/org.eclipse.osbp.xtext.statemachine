/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.statemachine.ui;

import org.eclipse.xtext.ui.editor.hover.IEObjectHoverProvider;

import org.eclipse.osbp.xtext.basic.ui.BasicDSLEObjectHover;

public class StatemachineDSLEObjectHover extends BasicDSLEObjectHover {
	
	@Override
	public IEObjectHoverProvider getHoverProvider() {
		return StatemachineDSLEObjectHoverProvider.instance();
	}
}
