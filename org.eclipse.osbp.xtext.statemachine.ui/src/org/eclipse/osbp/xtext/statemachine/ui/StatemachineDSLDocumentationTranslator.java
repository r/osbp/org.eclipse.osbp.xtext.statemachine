/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.statemachine.ui;

import java.util.ResourceBundle;

import org.eclipse.osbp.xtext.basic.ui.BasicDSLDocumentationTranslator;



/**
 * Documentation Translator for ReportDSL
 */
public class StatemachineDSLDocumentationTranslator extends BasicDSLDocumentationTranslator {
	
	private static StatemachineDSLDocumentationTranslator INSTANCE = new StatemachineDSLDocumentationTranslator();

	/**
	 * @return the one and only translator instance
	 */
	public static BasicDSLDocumentationTranslator instance() {
		return INSTANCE;
	}
	
	@Override
	protected ResourceBundle getResourceBundle() {
        return java.util.ResourceBundle.getBundle("i18n.I18N", getLocale(), getClass().getClassLoader());
	}
}
